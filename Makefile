SHORT_TITLE = WENP
CHAPTERS := \
	  00-Vorwort.md\
	  DieRolleDerMinze.pa\
	  01-Studiengestruepp.md\
	  02-Astgedanken.md\
	  03-Plansamen.md\
	  04-Sitznesseln.md\
	  DieAnzahlDerSchritte.pa\
	  05-Einladen.md\
	  06-Paartanz.md\
	  07-Triangulierung.md\
	  08-Revier.md\
	  DieNakelDesSchabers.pa\
	  09-Fanfahren.md\
	  10-Funkenfrequenz.md\
	  11-Fordergrund.md\
	  12-FuturII.md\
	  EntwicklungDanksagungEastereggs.md
INPUTS := $(CHAPTERS:%=chapters/%) metadata.yaml
OUTPUTS := WennEsNichtPassiert.pdf WennEsNichtPassiert.epub WennEsNichtPassiert.tex WennEsNichtPassiert.html ContentNotes.tex $(SHORT_TITLE)/ContentNotes.md $(SHORT_TITLE)
SPBUCHSATZV := SPBuchsatz_Projekte_1_4_9
#SPBUCHSATZV := SPBuchsatz_Projekte_1_4__Pre_Beta_11
#SPBUCHSATZV := SPBuchsatz_Projekte_1_3

all: $(OUTPUTS)

WennEsNichtPassiert.epub: $(INPUTS) ContentNotes_epub.md
	pandoc \
		-t epub2\
		--css epub.css\
		-L SPepub.lua\
		--epub-cover-image=img/WennEsNichtPassiert1000x.png\
		$(INPUTS) \
		ContentNotes_epub.md\
		-o $@

WennEsNichtPassiert.html: $(INPUTS)
	pandoc \
		$(INPUTS) \
		-o $@


WennEsNichtPassiert.tex: $(INPUTS)
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$(INPUTS) \
		-o $@

ContentNotes.tex: chapters/ContentNotes.md
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$^ \
		-o $@

ContentNotes_epub.md: chapters/ContentNotes.md
	@echo >$@ 'Content Notes'
	@echo >>$@ '============='
	@echo >>$@ ''
	@cat $< >>$@

# changed Ifstr to ifstr in SPBuchsatz_System/Helferlein/Hilfsfunktionen.tex 
WennEsNichtPassiert.pdf: WennEsNichtPassiert.tex ContentNotes.tex
	cp WennEsNichtPassiert.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/Haupttext.tex
	cp ContentNotes.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/ContentNotes.tex
	cp TitleAuthor.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/TitleAuthor.tex
	cd deps/$(SPBUCHSATZV)/Buch/build/ ; \
		lualatex ../tex/Buch.tex
	cp deps/$(SPBUCHSATZV)/Buch/build/Buch.pdf WennEsNichtPassiert.pdf

$(SHORT_TITLE)/titlelist.txt: Makefile
	mkdir -p $(shell dirname $@)
	@echo '$(filter-out %.pa, $(CHAPTERS))' | xargs -n 1 | cat -n >$@

get_chapter_number = $(shell \
	grep -E '^[[:digit:][:space:]]+$(1:chapters/%=%)$$' $(SHORT_TITLE)/titlelist.txt | \
		awk '{ print $$1 - 1 }' \
)

$(SHORT_TITLE)/%: chapters/% $(SHORT_TITLE)/titlelist.txt
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "$(call get_chapter_number,$<) - $(shell \
			head -n 1 '$<' \
		)"'
	@echo >>$@ 'number: $(call get_chapter_number,$<)'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	pandoc \
		-L SPmarkdown.lua\
		$< \
		-o tmp-md-to-md.md
	@cat tmp-md-to-md.md >>$@
	rm tmp-md-to-md.md

$(SHORT_TITLE)/ContentNotes.md: chapters/ContentNotes.md $(SHORT_TITLE)/titlelist.txt
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "Content-Notes"'
	@echo >>$@ 'number: 0'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	@echo >>$@ ''
	@cat $< >>$@

$(SHORT_TITLE): $(filter-out %.pa, $(CHAPTERS:%=$(SHORT_TITLE)/%)) ;

clean:
	rm -rf $(OUTPUTS)

.PHONY: clean $(SHORT_TITLE)

