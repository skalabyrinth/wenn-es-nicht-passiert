Paartanz
========

\Beitext{Nurek}

Nurek kniete in ihrem Bett, das direkt in die Fensternische
gebaut war, und blickte die Schneelandschaft an. Es war
so schön! Und Marim täte es wahrscheinlich in den Augen
weh. Er hatte oft Schwierigkeiten mit zu weißen Flächen, aber
ignorierte es, solange es ging.

Die Neujahrsfeier lag hinter ihnen, aber Marim war einfach
geblieben. Er fragte ungefähr jeden Tag, ob er doch zu viel
würde, er besser abreisen sollte und jedes Mal versicherten
sie ihm, dass er bleiben möge, solange er wollte. Nurek
erwischte sich bei dem Gedanken, dass sie schön fände, zöge er
einfach ganz ein. Er war nicht aufdringlich, verbrachte viel
Zeit mit sich selbst, half mit, wenn Gemeinschaftsarbeit anfiel und
wenn er Gesellschaft mochte, stand seine Tür offen oder
er hielt sich im Gemeinschaftsraum auf, sodass sie ihn
aufsuchen konnte. Manchmal saß sie auch schon da, wenn er
kam, zum Beispiel sich mit Ivaness unterhaltend. Dann
legte er seinen Kopf in ihren Schoß und genoss, wie
sie durch diese wundervollen, feinen Haare fuhr, während
sie sich weiter unterhielt. Sein Kuschelkrokodil lag dabei
oft in seinen Armen.

Ihr Zimmer hatte er bislang noch nicht betreten, noch nicht
einmal in der Tür gestanden, wenn diese offen
stand. Sie fragte sich, warum. Und
dann fragte sie ihn einfach:

*Nurek: Scheust du dich davor, mein Zimmer zu betreten?*

Marim antwortete häufig rasch, es sei denn, er war in einem
Versuch. Aber das kündigte er normalerweise vorher an. Dieses
Mal dauerte es eine Weile. Nurek versuchte, darein nicht
zu interpretieren, dass etwas nicht stimmte. Er konnte
auch einfach etwas essen oder so.

*Marim: Ja.*

*Marim: Wenn du möchtest, dass ich reinkomme, komme ich. Aber
ohne direkte Einladung würde ich das nicht tun.*

Nurek fing eine Nachricht an zu tippen, ob sie sich ihm
aufgedrängt hätte, als sie in sein Zimmer gegangen war. Aber
sie löschte sie wieder, als sie sich daran erinnerte, dass
sie das gefragt hatte, bevor sie es getan hatte. Nur eben
von sich aus.

*Nurek: Komm rüber, wenn du magst.*

Kurz darauf hörte sie seine leisen Schritte auf dem Flur. Er
schlurfte nie. Dann stand er in der Tür. Dieser kleine Elb mit
den geringelten Strümpfen und glitzernden Strumpfbändern, heute
wieder in einer kurzen, grauen Hose und einer ärmellosen, weichen Bluse
in schlichtem rot. Er lächelte nur ein wenig, als er durch ihr
Zimmer blickte. Über die Bilder an den Wänden, nur flüchtig
über den chaotischen Schreibtisch, dann zu ihr aufs Bett, wo
sie im Schneidersitz saß und wartete, bis er mit dem
Verarbeiten von Eindrücken halbwegs fertig war.

Dann klopfte sie auf das Bett neben sich. Er grinste und folgte
der Einladung. Sein Blick fiel wie automatisch aus dem Fenster. Er
beschwerte sich nicht über das Weiß. "Wow.", murmelte er. "Eine
Fensterbettnische ist einfach das Optimum eines Schlafplatzes."

Sie nickte. Irgendetwas schnürte ihr die Luft ab. Vielleicht
der Gedanke, die weiche Bluse anzufassen und ihn vorsichtig
zu streicheln. Vielleicht die Verliebtheit. Ob die wohl irgendwann
nachließ?

"Magst du mich?", fragte sie. Sie realisierte erst hinterher, dass
sie die Frage gestellt hatte. Es war eine Stim-Frage, aber davon
kannte er doch viele noch nicht.

Er blickte sie überrascht an. Dann nickte er energisch. "Hast
du irgendwelche Zweifel?", fragte er. Er wirkte auf einmal
selbst verzweifelt.

Nurek schüttelte kurz den Kopf, ohne ihn aus den Augen zu
lassen. "Manchmal höre ich es gern, wenn Leute es sagen.", erklärte
sie. "Und ich bin dann so dreist, nachzufragen."

Marim streckte die Hand aus und strich ihr kurz über die
Wange. "Ich mag dich. Sehr.", sagte er sanft. "Du darfst
das immer fragen. Dann sage ich es dir."

Die Stelle, wo er sie berührt hatte, fühlte den Reiz noch
intensiver nach, als sie die Berührung selbst gefühlt hatte. Nurek
lächelte und ließ sich im Schneidersitz nach hinten umfallen. Leider
hatte sie die Strecke bis zur Wand falsch eingeschätzt und stieß
sich den Kopf, aber da sie gewohnt war, Strecken falsch
einzuschätzen, hatte sie sich vorsichtig fallen lassen und es
tat nicht schlimm weh. Die Dehnung in den Beinen tat für ein
paar Momente gut, dann streckte sie sie an der Fensterseite
hinter Marim entlang aus und verschob ungeschickt den ganzen
Körper ein wenig, bis ihr Kopf nicht mehr bei jeder Bewegung mit der
Wand kollidieren würde.

Marim folgte ihren Bewegungen aufmerksam mit dem Blick, machte
ihr ein wenig Platz, und als sie ruhig lag, legte er sich mit
einer halben Armlänge Abstand neben sie. Das war dicht. Das war romantisch
dicht. Es war diese Distanz, die eine Spannung aufbaute, die sie gerade
mit niemandem sonst teilen mochte. Nurek atmete etwas langsamer
und angespannter. Viele hätten schneller geatmet, aber bei Nurek
waren solche Reaktionen wie Puls und Atmung genau umgekehrt. Bei
Aufregung hatte sie einen niedrigen Puls und atmete weniger.

Sie spürte Marims Atem im Gesicht, nur ein wenig, nur gerade so, dass
es nicht störte. Außerdem liebte sie, wie der Atem roch. Und
bei ihm merkte sie, dass er etwas schneller atmete.

"Wie fühlt es sich für dich an?", fragte er.

Nurek schluckte. "Aufregend.", sagte sie. "Romantisch. Ich
spüre das Verliebtsein stärker. Ich mag die Spannung. Die, dass
wir uns noch nicht berühren, aber irgendwann vielleicht schon. Die
Nähe, die ich so nur mit dir mag. Und auf diese Art vielleicht intim."

Marim lächelte, als sie wieder auf in sein Gesicht blickte. "Dann
warte ich vielleicht noch, bis ich dich anfasse."

Nurek fühlte, wie sich alles in ihr zusammenzog. Sie wollte
die Gegenfrage stellen, aber brauchte eine Weile, bis sie
Sprache wieder ansteuern konnte. Marim wartete freundlicherweise, bis
sie fragte: "Und für dich?"

"Ähnlich.", leitete er ein. "Ich fühle dabei noch, dass ich in
deinem Zimmer bin. Das fühlt sich nach Vertrauen an, dabei weiß
ich gar nicht, wieviel es ist. Aber eben, als dürfte ich in deinem
Leben sein."

Nurek nickte hastig. "Auf jeden Fall!", sagte sie. "Also, Zimmer
ist schon persönlich, das auch. Aber da gibt es Persönlicheres, was
ich schon mit dir teile, glaube ich." Auch, wenn ihr gerade nichts
einfiel. "Ich möchte dich in meinem Leben haben."

"Es fühlt sich intim an, und romantisch. Aber ich bin vielleicht
etwas ungeduldig.", sagte er. "Ich würde dich am liebsten streicheln
oder in den Arm nehmen, wenn du das magst."

Nurek atmete bewusst tief ein und aus, weil sie inzwischen so wenig geatmet
hatte, dass ihr etwas schwummrig geworden war. Dann grinste sie
kurz über sich selbst und nickte. Aber Marim rührte sich nicht.

Er hielt sich dann wohl zurück. Sie wusste nicht, wie mühsam. Sie
wollte ihn anfassen, damit er wusste, dass es okay wäre, aber sie
brauchte noch eine ganze Weile, bis ihr Körper ihr gehorchte. Als
er soweit war, streckte sie ihre Hand vorsichtig aus und berührte
den dicken, weichen Stoff der Bluse an seiner Hüfte, ohne ihn auf
seinen Körper zu drücken. Er zuckte. Es kitzelte wohl. Aber er
wehrte sich nicht. Nurek bemühte sich trotzdem, ihn so anzufassen, dass
es weniger kitzelte. Er hielt sich noch eine ganze Weile zurück, aber
als sie vorsichtig anfing, sein Haar zu streicheln, waren sie auf
einmal ein Körperknäuel. Seine Brille landete irgendwo auf
ihrem Nachttisch. Er hielt sie fest, streichelte sie erst
sanft, dann kräftig, und dann wieder sanft, meist über den Rücken, aber
auch über ihre Arme. Und ihre Beine verknoteten sich kurz ineinander.

---

Später lagen sie sich entspannt in den Armen und bewegten sich
nicht mehr. Dieses Mal lag ihr Kopf unter seinem Kinn und seine
Arme überall über ihren Rücken verteilt. Er lag mehr auf dem
Rücken, sie mehr auf der Seite. So konnte das bleiben.

Nurek realisierte, dass sie gerade planlos ohne Zeitdruck in den Tag lebte, und
das war auch irgendwie sehr schön. Ihr eines nacktes Bein lag zwischen
seinen bestrumpften Füßen. Bei der Gelegenheit fiel ihr wieder einmal
auf, dass sie tatsächlich ähnlich groß waren. Das war ein beinahe
ungewohntes Gefühl.

"Ich habe eine Frage an dich, die mir aber ein bisschen unsensibel
vorkommt.", leitete Nurek ein.

"Frag, und ich sage dir direkt dazu, ob sie es ist.", erwiderte Marim.

"Okay.", sagte Nurek. Und trotzdem brauchte sie ein bisschen, bis
diese eigentlich sehr simple, schon fertig formulierte Frage wirklich
ausgesprochen werden mochte. "Bist du Waldelb?"

"Ja.", sagte Marim. "Solange du nicht irgendwie anfängst zu fragen, ob
ich denn was mit Wäldern zu tun habe, ein besonderes Gespür dafür
hätte oder einen Drang, in einem zu leben, finde ich die Frage in
diesem Moment nicht unsensibel.", erklärte er. "Es ist allerdings
kompliziert. Ich werde das wegen meiner Größe oft gefragt. Aber
Größenvariationen gibt es unter allen Elben. Manche Waldelben sind
groß, manche andere Elben klein. Es nervt auf Dauer, dass
Leute fragen, wirkt
meist oberflächlich oder als wollten sie mich damit in eine
Kategorie einordnen, die ich nicht einmal verstehe. Bei dir
habe ich den Eindruck nicht. Aber ich kann nicht beantworten,
warum nicht, oder ob das Sinn ergibt."

Nurek grinste. "Puh, Glück gehabt, würde ich sagen."

"Vielleicht kennst du solche Fragen auch in Hinblick auf das
Glasauge?", fragte Marim.

"Mehr wegen der Augenhöhle, aber ich treffe nicht so oft
völlig neue Leute.", erwiderte sie. "Oh, das könnte auch
noch interessant auf einem Live-Konzert werden: Wie reagiere
ich darauf, dass Leute deswegen auf mich irgendwie reagieren?"

"Ich habe tatsächlich eine persönliche Glasaugenfrage, bei
der ich meinerseits nicht so sicher bin, wie sensibel
sie ist.", sagte Marim.

"Gleiche Bedingungen umgekehrt.", beschloss Nurek.

"Wäre es mit heutiger Medizin und Technik
möglich, dass du ein Auge bekämst, dass an dein Gehirn
passende Seh-Signale sendet?", fragte er. "Ist das in irgendeiner
Form sinnvoll ausgedrückt?"

"Ich verstehe, was du meinst.", versicherte Nurek. Sie
spürte, wie innerlich in ihr eine Mauer hochfuhr, wie immer, wenn
dieses Thema angesprochen wurde. "Es gibt mehr oder weniger erfolgreiche
Eingriffe, ja. Es ist dabei nicht ganz klar, ob ich nach so
einem Eingriff auch 3D-sehen lernen könnte. Gehirne sind verschieden
in der Lage, so etwas nachzulernen. Es gibt inzwischen auch
unterstützende Stimulationsverfahren mit verhältnismäßig
hohen Erfolgsquoten. Ist deine nächste Frage, ob ich das möchte?"

Marim nickte. "Wenn sie okay ist. Du wirkst gerade nicht
so."

Wow, war er sensibel. Nicht einmal ihr Geschwister merkte immer, wenn
ihre Mauern hochfuhren. "Ich reiße es an und setze Grenzen.", sagte
sie. "Ich möchte das nicht, zumindest erstmal. Aktuell ist meine
Angst, dass sich durch den Eingriff gleich viele Dinge verbessern wie
verschlechtern. Es ist ein neuer Reiz der dazukommt, an den ich mich
gewöhnen müsste, und ich weiß, wie schwer mir so eine Veränderung
fallen kann. Gleichzeitig kann etwas dazukommen, was ich gut finde, sodass
ich dann da sitze und die Wahl habe: Richtig schlimm dauerreizüberflutet
zu sein, aber in 3D, oder zurück aber mit dem Wissen wie 3D sein
könnte, wenn ich mich nur dran gewöhnte. Und so wichtig, dieses Risiko
einzugehen, kommt mir 3D dann doch nicht vor."

Marim nickte. "Das klingt irgendwie sehr nachvollziehbar und
verständlich. Selbst, wenn ich vor so einer Entscheidung nie stand."

"Es nervt eben eher, dass ich dadurch einen gewissen Ausschluss erfahre, aber
für die meisten Dinge gibt es automatische Konvertierung
in 2D, und mein persönliches Umfeld macht das sogar oft mit, wenn
ich dabei bin, siehe Film.", fuhr sie fort. "Bei diesem wäre es
ja möglich gewesen, dass ihr 3D guckt, während ich 2D sehe, aber
du hast es trotzdem nicht getan."

"Ich bin mir nicht einmal sicher, ob Anuka das gemacht hat.", murmelte
Marim. "Ich fand es, wie gesagt, mal wieder sehr spannend, einen Film
in 2D zu sehen."

"Linoschka macht da auch solidarisch mit.", sagte Nurek. "Sie meint, wir
reden sonst manchmal anders darüber, hinterher. Was ich mir gerade
bei dem surrealen Film gut vorstellen kann."

Marim nickte. "Ich mir auch ein wenig. Ich bin unsicher."

"Zurück zum Punkt.", sagte Nurek. "Diese Umstellung hätte mir im
Kindesalter vielleicht nicht so viel ausgemacht. Aber ich bin in
einem toxischen Umfeld groß geworden, in dem ich nicht die Möglichkeit
habe wahrnehmen können. Wenn diese Frage gestellt wird, die du
da gestellt hast, bilden sich bei mir Mauern, um nicht über diese
Zeit zu reden. Das mache ich nur mit psychologischen KIs, und habe einmal
mit einer Outernet-Therapieperson gesprochen. Ich weiß, wie ich mir selber
helfen kann, und ich möchte darüber mit dir nicht reden."

"Das ist in Ordnung.", versicherte Marim. "Ich werde niemals
nachfragen. Möchtest du, dass wir das Thema wechseln?"

"Ja bitte.", bat Nurek. Sie merkte, wie sie anfing, leicht zu
zittern. Nur ein wenig. Vielleicht musste sie an das Thema
noch einmal mit einer psychologischen Hilfestellung herangehen. Aber
gerade wollte sie nicht.

"Als nächster Punkt auf der Liste steht eine größere Party mit
nicht vertrauten Leuten.", sagte Marim. "Das ist ein nervlich auch
anspannendes Thema, aber ein anderes, oder? Passt das als
Thema?"

Nurek nickte. Sie versuchte, sich daran zu erinnern, welcher Punkt das auf
der Liste war, die sie abarbeiten wollten, um für das
Live-Konzert mit Schabernakel zu proben. Oder eben, um auszuprobieren,
ob es überhaupt Sinn ergab, zusammen dort hinzugehen. Aber während sie
sonst damit gut umgehen konnte, wenn etwas scheitern könnte, war ihr die
Anspannung gerade zu viel. Sie schüttelte doch den Kopf. "Kannst du mich
fest in den Arm nehmen und mir etwas vorsingen oder mir von deiner
Studie erzählen?"

Marim folgte der Aufforderung und erzählte ihr
von einer recht frischen Virtualität, die
sich eine Person gewünscht hatte, die damit einverstanden war, dass
sie in die Galerie gestellt würde. Es handelte sich um eine
Bibliothek mit Büchern, die quasselten, die sich gegenseitig
darüber austauschten, welche Charaktere in ihnen so vorkamen
und jene verglichen. Oder sie waren neidisch, weil andere Bücher
ein Happy End hatten und sie nicht, oder umgekehrt. Bücher, die von den Besuchenden
regelmäßig zu Ruhe aufgefordert wurden, aber das
Gequassel nicht lassen konnten. Es war eine witzige Virtualität, bei
der sie sich viel über den Sinn austauschen konnten: Warum
gingen Personen überhaupt in eine Bibliothek mit quasselnden
Büchern, wenn sie das Gequassel störte?

---

Als sie aufwachte, war es späte Nacht. Ihr war gar nicht
bewusst gewesen, dass sie eingeschlafen war. Marim schlief
leise neben ihr und sah dabei friedlich aus. Er schnarchte
nicht. Sie hatten noch nie beieinander geschlafen, fiel
ihr dabei auf. Ihr Blick fiel auf den Himmel, der einen
blassen Grünstich hatte. Nordlicht, aber kein so sehr
beeindruckendes. Aber Nordlicht! Marim hatte noch keines
gesehen. Sie blickte ihn eine Weile an und überwand sich dann, ihn
zu wecken. Er wachte so langsam auf, als wäre er wirklich
tiefenentspannt gewesen, und wirkte auch im verschlafenen
Wachzustand noch sehr entspannt.

"Nordlicht.", flüsterte Nurek.

Marims Blick wanderte aus dem Fenster. Eine ganze Weile sagte
er nichts. Dann: "Gehen wir raus?"

Nurek nickte. "Die andere Seite des Himmels ist
wahrscheinlich rötlich.", sagte sie. "Es ist eher von der
langweiligeren Sorte, aber es ist trotzdem Nordlicht!"

Sie behielt recht. Als sie zitternd draußen im Schnee standen, weil
ihre Körper wegen der Müdigkeit nicht stark genug gegenan
heizten, wölbte sich über sie ein Himmel, der auf der einen Seite
grünlich und auf der anderen Seite rötlich war. Es wurde vielleicht
langsam stärker und dann wieder schwächer, aber so langsam, dass
es kaum mit bloßem Auge auszumachen war. Marim nahm Nurek gegen
die Kälte von hinten in den Arm. Auch wenn sie das Phänomen schon
einige Male gesehen hatte, war es immer wieder wunderschön und
beeindruckte sie, egal, was sie Marim vorher geschrieben hatte.

"Hättest du eigentlich eine Idee für eine größere Party mit
überwiegend fremden Leuten?", fragte Nurek.

"Meine Tante hat mich zu einem Ball eingeladen.", sagte Marim. "Sie
kennt sehr viele Leute und schmeißt sehr große Partys. Sie
sind meist Internet-Outernet-Hybriden. Es gibt Live-Musik, und
einen Tanzraum nur für Outernet-Teilhabe, aber die Musik wird auch
in Virtualitäten übertragen und dort wird auch getanzt. Ich war
bisher zwei Mal auf so einem Ball."

"Hast du vor, da zu tanzen? Hast du es gelernt?", fragte Nurek.

"Ich habe es so ein bisschen gelernt, nicht viel. Mit meiner
Tante habe ich allerdings immer gern getanzt. Sie führt aber
auch einfach sehr gut.", antwortete er. "Kannst du tanzen?"

"Schon ja.", sagte Nurek zurückhaltend. "Es ist zehn Jahre her. Ich
habe mit meinem Geschwisterherz Paartanz gelernt."

"Paartanz ist das wesentliche Programm.", sagte Marim. "Aber es
gibt auch immer wieder Reihentänze dazwischen. Die sehen sehr schick
aus, weil alle das Gleiche machen. Es gibt einfache und
schwierige und Übungen, sodass möglichst alle, die möchten, bei ein paar mitmachen
können, aber es werden auch richtig fortgeschrittene Tänze vorgeführt werden."

Nurek nickte. Es klang interessant, und enorm stressig. Also genau
der Stresstest, den sie eigentlich wollte. Sie bemühte sich, den
inneren Drang, einfach abzulehnen, niederzuringen.

"Kannst du denn führen?", fragte Marim.

"Ich habe auch eher folgen gelernt.", sagte Nurek. Sie spürte dabei
kurz das Gefühl, dass etwas zwischen ihnen nicht perfekt zusammenpasste, was
irgendwie ungewohnt und dadurch befremdlich war. Aber auch vorhersehbar, dass
so etwas passierte, und eigentlich echt unwesentlich. Ein
albernes Gefühl.

"Ich würde vermuten, wir schaffen es trotzdem irgendwie, etwas zu
tanzen, wenn du willst.", sagte er. "Es gibt ja auch die koränischen
Tänze unter den Paartänzen, wo Führ- und Folgefiguren sich kaum
unterscheiden."

"Das stimmt.", sagte Nurek. "Bei denen habe ich auch am ehesten mal
ausprobiert zu führen."

"Oder wir müssen Ivaness mitnehmen.", schlug Marim vor. "Oder durchkreuzt
das den Plan von fast niemanden kennen?"

Nurek nickte sachte. "So gern ich as dabei habe, ich glaube, wenn
es mir nicht gut ginge, wäre as dann die Person, die sich kümmern
würde, und nicht du. Das wäre kein hilfreicher Stresstest." Nach
kurzem Zögern fügte sie hinzu: "Dein Vorschlag ist wirklich ein
krasser Stresstest."

"Zu krass?", fragte er.

"Gib mir noch einen Tag, mich da reinzufühlen.", bat Nurek.

---

Paartanz. Sie hatten, kurz nachdem sie die Lerngruppe mit den
anderen verlassen hatte, gemeinsam Ferien in Geesthaven verbracht, wo
über den Sommer über draußen viele Tanzkurse angeboten wurden. Und
ganz viele andere Dinge. Dort hatte sie auch Grundlegendes zu
Tauchen und Schnorcheln gelernt. Aber einige von ihnen hatten auch
einen Tanzkurs mitgemacht. Weitergetanzt -- dann in Online-Kursen --
hatten nur zwei Paare, Mø und Julipp, mit dem sie eine Fernbeziehung
führte, und eben Ivaness und sie. Irgendwann waren andere Hobbys
mehr in den Vordergrund gerückt und sie hatten damit aufgehört.

Nurek stand nun halb unschlüssig im Gemeinschaftsraum. Sie
war vor Marim aufgewacht, der sich nicht einmal gerührt
hatte, als sie über ihn drübergestiegen war. Die Terrassentür
stand offen und kalter Schneewind ohne Schnee wehte herein. Eben
Wind, der nach Schnee roch, aber es schneite gerade nicht.
Linoschka war draußen mit ihrer Morgenroutine beschäftigt -- nachdem
sie Schnee geschippt hatte.

Eigentlich wollte Nurek mit Ivaness testweise tanzen, wie es sich nun
anfühlte, aber as schlief noch. Sie hatten eingerichtet, dass
sie sich gegenseitig einen Weckton schicken könnten, wenn es
emotional gerade sehr schlimm war. Aber so schlimm war es dann
vielleicht doch nicht. Dachte Nurek, als sie merkte, dass
sie schon wieder leise Pflanzennamen vor sich hinmurmelte und
die Haptik der Worte im Mund auskostete. Das konnte auch ein
Zeichen sein, dass sie sich sehr wohl und zu Hause fühlte, aber
gerade war es Anspannung.

Sie beschloss, dass sie Linoschka nicht im Gemeinschaftsraum
stehend zusehen wollte, was dieser den Eindruck vermitteln
könnte, es wäre akut ein
Problem zu lösen, und machte es sich stattdessen gemütlich: Sie
druckte sich in der Küche einen Monua -- ein herbsüßes
Genussgetränk --, lieh sich Ivaness' warmen Mantel von der
Garderobe und setzte sich draußen zu Linoschka in den
Schaukelstuhl. Linoschka war bei den Liegestützen
angekommen. Mit fingerlosen Handschuhen auf den gefegten
Steinen. Die Finger waren rot, aber Linoschka hatte, außer
genau währenddessen und kurz danach, nie kalte Hände. Sie
bekam in regelmäßigen Abständen zu hören 'Ich könnte
das ja nicht!' und reagierte regelmäßig mit der Antwort, dass
Körper und Bedürfnisse eben verschieden wären. Niemand
müsste, und sie kostete es keine Überwindung, weil es eher
ihr Körper war, der die Bewegung und Kälte einforderte.

Nurek fragte sich oft, ob es eine erwartete oder angemessenere
Reaktion auf so einen Kommentar gab. Es kam ihr vor wie Floskelsalat, auf
den eigentlich eine entsprechende Floskelsalatsoße
gehörte, aber Linoschka erklärte eher, dass der Salat zu
einfach gedacht war. Für Nurek erschlossen sich die
Bedürfnisse der Kommentierenden nicht. Und für Linoschka
wahrscheinlich auch nicht.

Linoschka schloss das Training mit einer Dehnungs- und Entspannungsübung
ab. Die Endhaltung war ein Knien auf dem Boden. Ihre Oberschenkel wirkten
dabei breiter und vielleicht muskulöser als sonst, und Nurek hatte
das aus ihr unerfindlichen Gründen schon immer gefallen.

"Willst du draußen bleiben, oder setzen wir uns rein?", fragte
Linoschka.

Nurek dachte länger über die Frage nach, bis sie zum Ergebnis kam, dass
sie keine Antwort hatte. "Heute ist einer dieser Tage, an dem Entscheidungen
nicht gehen.", sagte sie.

"Dann machen wir die Sache dazwischen.", sagte Linoschka. "Wir setzen
uns rein, aber lassen die Tür noch offen."

Nurek nickte, und verfrachtete den Schaukelstuhl, den sie gerade erst
rausgestellt hatte, wieder hinein. Sie bekam es kaum auf die Reihe. Der
Prozess beinhaltete, dass sie die Tasse Monua zuvor auf den Tisch drinnen
stellen musste und vermeiden musste, sie direkt nach dem Abstellen
umzuschmeißen, weil sie mal wieder nah dran war, zu vergessen, dass zum Abschluss
des Abstellens auch das Loslassen gehörte. Als sie im Schaukelstuhl
saß, reichte Linoschka ihr die Tasse, weil Nurek sie schon wieder
vergessen hatte. "Das ist ja schlimm heute mit mir.", murmelte
Nurek.

"Das kriegen wir hin. Ich bin da.", beruhigte Linoschka sie.

Das war eine der schönsten Antworten, fand Nurek, die auf so
eine Bemerkung gegeben werden konnte. Es wurde ihr nicht abgesprochen, dass
es schlimm war. Sie wurde einfach vollständig ernst genommen.

Linoschka organisierte sich ein kühles, flüssigeres Getränk und
setzte sich Nurek gegenüber in einen Schneidersitz. Sie wirkte, als
habe sie sehr viel Zeit.

Nurek erzählte ihr holprig, was gerade anstand, und es kam ihr
lächerlich vor, dass es sie so stresste.

"Verstehe ich das richtig, es geht um die Entscheidung, ob du
auf einen Outernet-Ball gehst?", fragte Linoschka.

Nurek nickte.

"Und du fragst dich das an einem Tag, an dem dir Entscheidungen
generell schwer fallen?", fuhr sie fort.

"Oh.", sagte Nurek. "Daher kommt der unerwartete Stressanteil."

Linoschka grinste kurz. "Ich verstehe aber auch, dass du die
Entscheidung gern gefällt hättest."

Nurek nickte. "Die Offenheit der Entscheidung stresst sehr. Ich
würde gern mit meinem Geschwisterherz tanzen, um mich in die
Entscheidung reinzufühlen."

"Weck Ivaness.", riet Linoschka.

"Meinst du wirklich, dass das dringend genug ist?", fragte Nurek.

"Das war der Versuch, dir die Entscheidung abzunehmen.", erklärte
Linoschka. "Ob du dir das gefallen lässt, entscheide ich nicht für
dich."

Nurek entschied, sich das gefallen zu lassen. Es dauerte trotzdem
noch eine ganze Weile, bis sie sich überwand, das Signal an Ivaness
auch tatsächlich zu senden.

---

Als Marim endlich zu ihnen in den Gemeinschaftsraum stieß, hatten sie
die Möbel umgestellt und die WG-Musikanlage wieder in Betrieb
genommen. Über diese konnte in jedem Raum die gleiche Musik gehört
werden. Sie hatten sie lange nicht benutzt, weil sie sich eher
an Musikübertragung über Hinterohrhörer -- diese arbeiteten
mit etwas ähnlichem wie Knochenschall -- oder Hörimplantate gewöhnt
hatten. Ivaness hatte aber gemeint, dass Musik über Boxen
für das Reinfühlen in eine Ballsituation mit Live-Musik besser
wäre, weil der Schall mit dieser eben doch nicht überall gleichverteilt
war. Ivaness versuchte Nurek rudimentär beizubringen, zu führen. Linoschka
sah an sich zu, war aber auch überwiegend in irgendein Projekt an ihrem
Faltrechner vertieft. Marim setzte sich dazu. Im Gegensatz zu
Linoschka war er voll dabei. Und das machte Nurek ziemlich
nervös. Als sie merkte, dass die Anspannung zu groß wurde, pausierte
sie. Sie fragte sich, wie das denn werden sollte, wenn es ihr
auf dem Ball auch so ginge. Ob Marim irgendwelche Erwartungen an
sie hätte, dass sie viel tanzen würde.

Aber als Ivaness nun Marim aufforderte, änderte sich ihr
Gefühl überraschend. Ivaness tat es irgendwie provokant. As
fragte es nicht sachlich, sondern winkte ihn mit einem Finger
zu sich. Marim stand nervös auf und folgte der Aufforderung. Ivaness
hatte eine liebevoll sadistische Art zu führen. As war
herausfordernd, immer etwas mehr Figuren führend, als Marim
kannte, und er stolperte oft, aber es lag Albernheit und
Provokation in der Stimmung, die das ganze entspannte. Vielleicht
hätte es nicht jede Person so mit sich machen lassen, aber
Marim zeigte damit keine Probleme. Das brachte
Nurek dazu, auch mit Marim tanzen zu wollen, auch wenn sie
wohl Ivaness Führungsstil nicht nachahmen können würde.

Sie probierten es. Es war tatsächlich alles sehr unbeholfen.
Nurek hatte überhaupt kein Training, zu führen. Sie tauschten
auch mal Rollen, aber Marim ging es nicht anders.

"Erwartest du mehr Können für den Ball?", fragte Nurek.

"Im Gegenteil.", sagte Marim. "Ich wäre mit dir da auch
hingegangen, ohne ein einziges Mal mit dir zu tanzen, wenn
das für dich nichts gewesen wäre. Es ist eine lockere
Veranstaltung. Da tanzen viele sehr holprig und probieren
sich eben aus."

Nurek atmete erleichtert aus. Dann gleich noch einmal, weil
es so gut getan hatte.

Sie setzten sich zusammen und Ivaness, Linoschka und Nurek
löcherten Marim über jedes Detail, wie so
ein Ball ablaufen könnte. Es wäre nicht sein erster Ball dieser
Art.

"Oh, vielleicht ein wesentliches Detail:", fiel ihm plötzlich
ein. "Ich trage zu so einem Anlass gern Mode des Adels von vor
so 400 Jahren aus, so im Dreh, weil ich sie sehr schön finde."

"Das ist wirklich wesentlich.", meinte Nurek. "Dann muss ich
mich ja fragen: Will ich das auch?"

"Macht ihr dann Partnerlook? Oder Kleidung, die so zusammenpasst, dass
ihr als Paar erkennbar seid?", fragte Ivaness.

"Tragen viele Leute dort so etwas?", fragte Nurek.

Marim ignorierte die erste Frage und beantwortete erst einmal
nur Nureks: "Ja. Nicht die Mehrheit. Es gibt eher so eine
Gruppe von 20 bis 30 Leuten, die aus diesem immer noch recht
groß gefassten Moderepertiore dieser Jahrhunderte aus verschiedenen
Regionen Ardas, wo es ansatzweise vergleichbare Monarchien
gab, Kleidung wählt. Es gibt auch traditionell ein paar
Tänze, bei denen genau die Personen aufgefordert werden, die
Kleidung auszutragen. Aber es gibt auch ganz andere alte
Moderepertoires die vorgeführt werden und eigenen Raum
bekommen."

"Ich geb mir das.", entschied Nurek. "Ich weiß nicht, ob
es meins ist, aber es gibt inzwischen so viel daran zum
Ausprobieren und ich liebe Ausprobieren. Ich komme mit!"

---

Der Ball fand in Rumpf statt. Das war verglichen mit
anderen typischen Orten, die sich für Veranstaltungen
dieser Größe eigneten, gar nicht so weit weg. Vielleicht etwas
mehr als eine Stunde Fahrt. Marim und Nurek einigten sich darauf, mit
dem Zug hinzufahren, und auf dem Rückweg, je nach Energie, dann
die Fähre zu nehmen oder wieder ohne Umstieg den Zug.

Die Kleidung hatten sie in einer Virtualität ausprobiert, und
sie würde dann in der Herberge bereitliegen. Die Kleider
hatten den gleichen Schnitt, aber Teile der Farben waren
jeweils vertauscht, sodass es nicht Partnerlook war, aber
nah dran.

Sie kamen im Entendorf unter. Es wohnten inzwischen sogar viele
Enten hier, niemand wusste so recht, wer dafür verantwortlich
war, dabei hatte es ursprünglich gar nichts mit Enten zu tun
gehabt. Es hatte mal Studentendorf geheißen, als die ehemalige
Universität Rumpfs noch hauptsächlich physisch besucht worden
war und nicht virtuell und dezentral. Einige der
Labore waren noch in Betrieb, aber große
Teile der Uni waren zu Veranstaltungsräumen geworden, weil
sie schon damals mit ausgezeichnetem Parkett ausgelegt worden
waren. Das war im Rahmen der Deloquenz-Bewegung geschehen, wie
Marim sie scherzhaft nannte, die gegen das Gatekeeping vorgegangen
war, was denn eine Wissenschaft sein dürfte und was nicht. Es
war gerade im Gespräch gewesen, ob das Studentendorf in Studierendendorf
umbenannt werden sollte, aber eine Mehrheit hatte sich
zum Spaß für die Umbenennung in Entendorf entschieden, zumal
die Wohnungen nun überwiegend von temporären Besuchenden
genutzt wurden und gar nicht mehr von Studierenden. So, wie
Marim und Nurek das nun taten.

Aus entsprechend naheliegenden Gründen hatten Nurek und Marim
auf dem Weg dahin angefangen, sich in Quak-Lauten zu
unterhalten. Ohne viel Inhalt, eher um die jeweils eigenen
Stimmen zu hören und miteiander zu interagieren. Vielleicht
war es eine sehr neuroatypische Interpretation von Smalltalk.

Sie ruhten sich einen Moment aus, aßen etwas und zogen sich
dann bereits um. Das hatten sie sich so überlegt, um
auf diesem Ball ein vergleichbares Stresslevel zu dem
eines Live-Konzerts zu erreichen. Obwohl dafür eigentlich zu viele Komponenten
unbekannt waren, um zu entscheiden, welche Planung am
nächsten daran käme.

Nurek hatte sich inzwischen die Liste noch einmal angesehen. Es stand
als nächstes noch Zelten an, und dann bereits der letzte Punkt,
Absprachen mit Notfall-Ansprechpersonen. Eigentlich war die
Liste nicht mehr sehr lang, aber weil die Punkte so groß
waren, fühlte es sich für Nurek nicht so an, als hätten sie schon
über die Hälfte abgearbeitet.

Der Weg zum Veranstaltungsgebäude führte über eine lange Brücke über
einen tiefer gelegenen Fahrradweg durchs Grüne. Nurek mochte und genoss
den Weg sofort.

Sie hätte sich nicht gewundert, wenn Marim High Heels getragen
hätte, aber seine Schritte waren sehr leise. Sie versuchte sein
Schuhwerk zu erhaschen, aber der Rock war zu lang.

"Darf ich dir unter den Rock gucken?", fragte sie und fühlte
sich dabei albern und dreist.

"Öh, warum?", fragte Marim.

"Muss auch nicht, ich wollte nicht drängen.", ruderte Nurek
rasch zurück. "Ich war neugierig auf dein Schuhwerk. Das hatte
ich nicht mitbekommen."

Marim blieb stehen und machte eine sehr elegante Bewegung, bei
der er ein Bein ausstreckte und den Rock selbst etwas lüftete. Er
war so gut in solchen Bewegungen, fand Nurek. Er trug sehr
flache, weiche, dünne Schuhe. Welche, die auch zum Tanzen geeignet
sein könnten, aber dazu konnten sie sich am Veranstaltungsort
auch Schuhe leihen.

"Die Ringelstrümpfe!", jubelte Nurek. "Du hast immer noch
Ringelstrümpfe an."

Marim nickte gewichtig, zog den Rock nah am Bein noch weiter
hoch, um ihr auch kurz das Strumpfband zu zeigen. Dann ließ
er ihn sinken und strich sich das offene Haar zurück. "Ich
mag Ringelstrümpfe und Strumpfbänder einfach sehr."

"Ich glaube, ohne dich hätte ich dazu keine Meinung, aber ich
mag, dass sie Teil von dir sind und als solchen freue ich
mich einfach jedes Mal.", sagte Nurek. Aber dann überlegte
sie noch einmal gründlicher. "Doch, Ringel an Füßen und
Unterbeinen mochte ich auch schon, bevor ich dich kannte."

Marim lächelte verlegen den Boden an.

---

Von außen wirkte das Gebäude unscheinbar. Vielleicht, wie
eine alte Kantine, ursprünglich einmal weiß verkleidet, aber das
Material war angelaufen und hatte nun einen strähnigen, beige-cremigen
Farbton. Innen hätte es vielleicht auch unscheinbar ausgesehen, aber
zu diesem Zeitpunkt waren die Wände und Säulen im Eingangsbereich
in dunkle, schalldämpfende Tücher verpackt, auf die zurückhaltend
leuchtende LEDs angebracht waren. Die meisten waren weiß, aber
manche auch gelblich, rötlich oder bläulich, sodass es
den Eindruck eines Nachthimmels erweckte.

Der Tanzschuhverleih war Nurek zu voll. Marim organisierte, dass
sie sich mit ein paar Schuhen zum Testen in einen Raum
zurückziehen konnten.

"Das hätte mit Vortesten in einer Virtualität besser
organisiert werden können.", merkte Nurek an.

"Das stimmt. Ich gebe das an meine Tante weiter.", antwortete
Marim.

"Wie heißt die Tante eigentlich?" Nurek sollte sich vielleicht
wundern, nachdem sie ein paar Monate sehr oft über diesen
Ball gesprochen hatten, dass sie das noch nie gefragt hatte.

"Mallil."

---

Marim zeigte ihr das Gebäude. Es gab viele Treppen und Aufzüge,
ebenerdig einen riesigen Saal, im ersten Obergeschoss mehrere
kleinere, die aber auch noch groß genug waren, um gemütlich zu
tanzen, und im Kellergewölbe ein großes Becken. Hier hatte
das ausgehängte Tuch eine andere Konsistenz und leuchtete
selbst, aber nicht überall, sondern eher in Schlieren. Sie
waren sehr früh da, deshalb war es noch relativ leer. Nur
zwei Nixen, die im flachen Wasser am Rand des Beckens lagen, waren in
ein leises Gespräch vertieft. Sie trugen zur Deckenverkleidung
passende Kleider und die implantierten Schuppen leuchteten
ebenfalls.

Nixen waren den Grenlanndwalen ähnlich, teils von ihnen
geboren, teils von anderen Nixen, und hatten für gewöhnlich
keine Schuppen -- außer vielleicht die Schuppen, die manche
Personen, ob Nixe oder nicht, eben auf der Kopfhaut
produzierten. Aber viele Nixen verzierten ihren Körper im
Laufe ihres Lebens mit Schuppen, die sie durch
eine alte Implantierungstechnik halb in den Körper
einbrachten, so ähnlich, wie manche Personen Ohrringe oder
Piercings trugen. Für die Implantierungstechnik gab es
einen eigenen Namen, den Nurek vergessen hatte, aber Nixen
übersetzten es meist mit Implantieren ins Kadulan. Nurek
fragte sich, ob die Schuppen schon beim Implantieren
Leuchtschuppen gewesen waren, oder ob für heute etwas
auf sie aufgetragen worden war, wie vielleicht Nagellack. Bei
dem Gedanken warf sie einen Blick auf Marims halb abgeblätterten
Nagellack. Den hatte sie auch immer gemocht.

Die beiden Personen hatten ihr Gespräch unterbrochen und
blickten zu Marim und Nurek auf. "Wollt ihr euch dazusetzen?", fragte
die eine. "Wobei, die Kleider sehen nicht so wasserfest aus. Aber
wir können auch näher ans Ufer."

Marim blickte Nurek fragend an. Nurek war unentschieden, aber nickte
schließlich. Aus seiner Körpersprache wagte sie zu schließen, dass
er vielleicht wollte. Und sie hatte eben auch nichts dagegen.

Es machte Nurek Spaß, ihre komplizierten Röcke zu sortieren, als
sie sich auf ein Kissen nahe des Wassers setzte. Auch die Kissen
hatten diese Konsistenz, die die Deckenbehängung hatte. "Was ist
das für ein Material?", traute sie sich zu fragen.

Die beiden Fremden waren nähergerückt und hatten sich wieder
niedergelassen. "Gummerlatech heißt die Pflanze, glaube ich.", sagte
die eine Nixe und sah die andere an. "Oder?"

"Ja.", bestätigte diese. "Gemeinhin und unspezifischer auch
bekannt als Alge. Aber das Leuchten kommt nicht von der Pflanze, sondern
von Mikroorganismen, die das beim Verdauen tun."

"Essen sie den Gummerlatech auf?", fragte Marim grinsend.

"*Das* Gummerlatech.", korrigierte die zweite Nixe. "Nein. Gummerlatech
in verarbeiteter Form ist gefühlt unzerstörbar. Daher
ist es ja auch so ein beliebtes Material für Stoffe. Also, für
Leute und Orte, die permanent mit Wasser in Berührung
kommen. Wir haben es heute mit leckeren Sporen besprüht.
Ungefährlichen. Und dann die Gespensterkrabsen dran gelassen. Das sind
die, die so leuchten."

"Es ist wunderschön!", sagte Nurek.

"Es ist definitiv sehr beliebt.", sagte die Person. "Auch der
Spritztanz mit Lichtshow heute im Programm, falls ihr Lust
habt."

Nurek hatte überlegt, ob eine Vorstellungsrunde gut
wäre, aber sie würde sich Namen von heute ohnehin nicht
merken können. Vielleicht dachten die anderen genau so.

Marim holte seinen Taschenrechner hervor. "Meine Tante
hat gerade ein paar Momente Ruhe und Zeit, ist in einem
ruhigen Raum und fragt mich, ob ich sie sehen und ihr
mein Herzwesen vorstellen möchte. Magst du?"

Nurek nickte.

---

Es brauchte bis nach der Vorstellung und ihrem gemeinsamen
Erkunden, wie Nurek sich mit Trinken versorgen würde, bis
Nurek ihr Planungsproblem verstand. Sie wollte zum Spritztanz
mit Lichtshow gehen, das interessierte sie sehr. Aber das
Problem war, dass sie vorher wissen musste, wie nass sie dabei
werden würde, und was sie täte, wenn sie hinterher nasse
Kleidung auf der Haut hätte, die störte. Sie hätte sich
wahrscheinlich darüber informieren können, wie es
ablaufen würde und was sie dahingehend zu erwarten hätte. Die
Feier war mit einer dafür zuständigen KI versorgt, die
über jedes Detail so Bescheid wusste, wie das eben im Rahmen
von etwas Unvorhersehbarkeit möglich war, und alle Fragen
beantwortete. Sie war auch dafür zuständig, mögliche
Probleme zu klären oder Kompromisse zu arrangieren. Irgendwann
waren zum Beispiel vorübergehend die Symbole, die die Toiletten
auswiesen, größer und leuchtender geworden. Nurek waren
sie nur durch die Veränderung aufgefallen.

Aber sie war nicht in der Lage, mit der KI zu
kommunizieren, weder schriftlich noch mündlich, weil
Marim bei ihr war und ihr Kopf meinte, es wäre ein
unerträgliches Gefühl, nicht instant gesprächsbereit
für ihn zu sein. Also versuchte sie, ihn anzusprechen, und
als Reden nicht funktionieren wollte, nahm sie sich eines
der dafür gedachten Schreibpads von der Wand, um ein
handschriftliches Gespräch mit ihm zu führen. Ihre Schrift
war wie eh und je sehr krakelig, aber das Pad glättete sie
automatisch in etwas Lesbares.

Marim las die Fragen, nickte vor sich hin und erklärte
schließlich: "Ich war da schon ein paarmal. Es wird niemand
nassgespritzt. Eigentlich ist vorgesehen, dass nicht einmal
ein Tropfen außerhalb des Beckens landet, aber manchmal sind
unerfahrenere Tanzende dabei und dann gibt es vielleicht
mal den ein oder anderen Tropfen, aber mehr nicht. Im
Wesentlichen geht es darum, dass aus Wasserfronten mit
entsprechender Beleuchtung bewegte Kunst gezeigt wird."

Nurek wollte sagen, dass sie noch nie live bei so etwas
dabei gewesen wäre und sich freute, aber ihr kam kein Ton
aus dem Mund.

Marim schlug vor, ein wenig rauszugehen und zu entspannen
und dann vielleicht zu tanzen. Nurek nickte bloß. Allerdings
fragte sie sich auch, ob sie bei einem Live-Konzert von
Schabernakel auch irgendwann rausgehen müsste, aber es dann
keine Möglichkeit gäbe.

---

Sie hatten sich vielleicht eine halbe Stunde ausgeruht und
angefangen, sich wieder zu unterhalten -- die Fähigkeit,
sprechen zu können, war so uneingeschränkt wieder da, dass
Nurek sich kaum mehr an das Gefühl erinnern konnte, es
nicht zu können --, als
eine Person in einem weichen Schlafanzug vor Marim
trat, die ihn kannte. Nurek hatte einige Personen in gemütlicher
Nacht- oder Wohlfühlbekleidung gesehen. Es sollte
auch einen improvisierten Kuscheltanz geben, an dem
einige von ihnen teilnehmen würden.

"Hi!", grüßte die Person Marim und fragte mit einer Geste
nach einer Umarmung, die Marim erwiederte. Dabei drückte
sich sein Rock von der Person weg. Sie war etwas größer
als er. Sie wandte sich Nurek zu und stellte sich vor: "Ich
bin Vronika. Sie, ihr, ihr, sie, oder auch they, their, them, them, das
ist mir gleich."

"Nurek, sie, ihr, ihr, sie.", stellte Nurek sich vor.

"Seid ihr zusammen hier?", fragte Vronika, aber schien sich dann
kurz über sich selbst zu ärgern und fügte hinzu: "Vielleicht sollte
mir das klar sein. Ihr habt zusammengehörende Kleidung."

Marim nickte. "Wir sind wohl auch sowas wie ein Paar. Ich
bin nicht sicher, ist das ein Begriff, der für dich gut
klingt, Nurek?"

Nurek nickte zögernd. Er fühlte sich technisch passend an, aber
sie mochte ihn irgendwie nicht. "Ich muss vielleicht darüber
nachdenken."

"Musst du nicht.", widersprach Marim. "Wir können auch einfach keinen
Namen dafür haben."

"Schön miteinander seid ihr jedenfalls.", sagte Vronika. "Wie
ist das, Marim: Sind Fragen, die den meisten viel zu direkt und
aufdringlich vorkommen, immer noch okay für dich?"

Marim nickte.

"Ich weiß nicht, ob ihr die ganze Zeit beieinander sein wollt. Aber
falls nicht, hättest du Lust auf etwas One-Night-Stand-Artiges?", fragte
Vronika.

Wow, dachte Nurek. Das war direkt zum Punkt kommend. Aber irgendwie
war es ihr auch sympathisch. Vronika stand fast zwei Meter von Marim
entfernt und nichts an ihrem Auftreten wirkte, als könnte they eine
Grenze nicht absolut respektieren.

"Wir sind heute die ganze Zeit beieinander.", sagte Marim. "Du
darfst wieder fragen, aber heute möchte ich nicht."

"Du musst nicht auf mich Rücksicht nehmen.", machte Nurek
klar. Es war ihr ein Bedürfnis. Sie wollte Marim wirklich nicht
einschränken. Er erzählte ihr fast nichts von seinem Sexualleben, aber
sie wusste schon, dass er gelegentlich mit eher oberflächlichen
Bekanntschaften sexuelle Dinge ausprobiert hatte und er
grundsätzlich auch ein Bedürfnis nach diesem Ausprobieren
hatte.

Marim sah sie sanft lächelnd an. Er sagte nicht gleich etwas.
Vielleicht musste er dafür länger nachdenken. "Ich möchte
unabhängig von irgendwelchen Absprachen gern den
ganzen Tag und die ganze Nacht mit dir verbringen. Solange
du möchtest zumindest.", sagte er schließlich. "Aber ich
bin zugleich auch hier, weil wir eine Absprache haben. Und
mein Eindruck ist gerade, dass du siehst, hier könnte irgendwas
sein, was mir besser gefallen könnte, als mich an diese Absprache
zu halten, und du deshalb gewillt bist, weniger Rücksicht auf
dich selbst zu nehmen. Kann das sein?" Nach kurzem Zögern
fügte er hinzu: "Ich glaube, das war nicht perfekt ausgedrückt.
Weißt du trotzdem ungefähr, was ich meine?"

Nurek nickte. Etwas schnürte ihr die Kehle zu. Sie
spürte, dass ihre Augen feucht werden wollten, aber konnte
nicht genau erfühlen, ob sie es auch taten. Sie presste
ihren Kopf gegen Marims Schulter und er nahm sie in den
Arm.

"Aw!", sagte Vronika. "Sollte ich vielleicht nicht sagen, weil
es so verniedlichend wirkt. Ich meine einfach: Es ist so
schön zu beobachten, wie ihr für einander gut seid! Ich
verziehe mich mal. Vielleicht sehen wir uns später noch, ja?"

Marim bestätigte und kurz darauf waren Marim und Nurek mit
sich und ihren Gefühlen wieder allein.

---

Sie tanzten nur für etwa eine halbe Stunde, dann war bei Nurek die
Energie raus. Sie zitterte und es ging eigentlich nichts mehr. Trotzdem
wollte sie noch ein bisschen beim Vortanzen verschiedener Gruppen
zuschauen. Der Spritztanz mit Lichtshow fand gegen Mitternacht
statt. Etwa ein Zehntel der Personen, die mitspritzten, waren
keine Nixen, sondern gehörten zum Fußvolk, wie Nixen Orks, Elben,
Lobbuds, Menschen, Zwerge und andere Völker oft zusammenfassten. Sie
trugen Flossen für beide Füße zusammen, oder sogar Fischschwänze
zum Überziehen und machten mit. Nurek bekam Lust, so etwas auch
lernen zu wollen. Vielleicht irgendwann, sollte sie
zwischen all ihren anderen halb angefangenen Projekten mal
eine Lücke und einen Anlass finden.

Die Show war sehr beeindruckend. Die Tanzenden formten
Wassermassen zu Formen, die auf Nurek beinahe physikalisch
unmöglich wirkten -- aber das machte auch der Lichteffekt --, und
die dieses Gefühl von Sanftheit und Glattheit mitbrachten, das
sie hatte, wenn sie ihren Finger in die Wasserfläche steckte, die
dabei entstand, wenn ein Wasserstrahl in einen Löffel oder eine
kleine Schale traf.

Sie war froh, hinterher nicht alleine den Weg nach Hause finden
zu müssen. Marim hätte vielleicht länger ausgehalten, aber er
sagte nichts dazu, begleitete sie einfach. Und sie ließ sich
fallen. Sie war auch froh, direkt danach nicht allein zu sein. Das
hinterließ oft bei ihr ein Loch, eine plötzliche Einsamkeit. Sie
schliefen nebeneinander, ohne sich anzufassen. Und sie wachte
von frisch gedrucktem Frühstück wieder auf, das Marim ihr und
sich neben das Bett brachte.
