Triangulierung
==============

\Beitext{Marim}

Nurek lag mit geschlossenen Augen auf dem Rücken. Marim
strich sehr sanft mit dem Zeigefinger ihre Augenbrauen
nach, Muster auf ihre Stirn und Schläfen und über die
weichen Lobbudohren. Nurek öffnete dabei die Lippen
einen Spaltbreit und entspannte, hielt ganz still. Marim
erfüllte dabei ein unbeschreibliches Glücksgefühl. Und
dann schaute kurz der ungebetene Gedanke vorbei, womit
er dieses Glück verdient hätte, dieser Person so
nahe sein zu dürfen. Bevor er dem Gedanken jeglichen
Sinn absprach und ihn damit auflöste, fragte er sich, ob
er noch Resultat dieser toxischen Leistungsgesellschaft
war, die sich parallel zu Kommunismus-Versuchen
unter den meisten Völkern Maerdhas entwickelt hatte. Er
korrigierte sich: Es waren nicht nur Kommunismus-Versuche
gewesen. Den ehemaligen Kommunistischen Arbeitsstaat der Zwerge als
kommunistisch zu bezeichnen, hatte durchaus eine Berechtigung.

Er schweifte ab. Seine Finger hatten die Berührungen unbeobachtet von
alleine fortgeführt. Dabei war es viel schöner, ihnen
dabei zuzusehen. Es spielte jedenfalls keine Rolle, ob
er dieses Glück verdient hätte oder nicht. Er hatte es, und
das war absolut in Ordnung, nichts sprach dagegen. Der
Hauptgrund seines Zweifels war wohl, dass es sich im Rahmen
seiner bisherigen Lebenserfahrung
so unrealistisch anfühlte.

Das heutige politische System konnte
noch so perfektioniert auf die Bedürfnisse möglichst
aller eingehen, es würde immer Personen geben, die ihren
Platz und ihre Nischen erst finden mussten. Eine Erleichterung
stellte dar, dass niemand gezwungen war, in einem
Umfeld zu bleiben, das nicht gut tat. Aber das war eben noch
einmal etwas anderes, als eines zu finden, dass gut tat.

Nun wohnte er hier. Schon vor einer ganzen Weile hatte die WG sich
mit ihm zusammengesetzt und ihn gefragt, ob er eigentlich
Lust hätte, ganz einzuziehen. Und vor etwa einem Monat
hatte er den Mut gefasst, die kleine Wohnung in
Minzter freizugeben. Er hatte nicht einmal zurückkehren müssen. Er
hatte sie sauber hinterlassen und reiste, wenn länger als einen
Tag, ohnehin immer mit all seinen Sachen. Er hatte nicht allzu
viel, es passte in einen großen Wanderrucksack und eine
kleine Reisetasche. Zumindest im Winter, wenn seine Multi-Funktions-Jacke
vollständig zusammengezippt an seinem Körper war.

Aktuell konnte er sich nicht vorstellen, je
wieder wegzuziehen. Trotz seiner gar nicht mal so alten Gedanken, dass
er oft einen Wechsel brauchte. Vielleicht war
sein Ortswechseldrang vor allem dem geschuldet, dass
er eben keinen örtlichen Halt gefunden hatte, aber nun
schon. Eigentlich keinen örtlichen, sondern einen zu den
Personen, die hier lebten. Wahrscheinlich würde er irgendwann
wegmüssen, aber vielleicht wäre dies ein guter Ort, um
wieder zurückzukehren. Manchmal änderten sich Dinge.

Linoschka kam in den Gemeinschaftsraum und ließ sich erschöpft
auf einen Sessel plumpsen, auf dem sie sich verknotete. Sie
sah aus, als hätte sie die Nacht über nicht geschlafen. Jetzt
erst erinnerte sich Marim daran, dass sie die Morgenroutine hatte
ausfallen lassen. "Kann ich dich irgendwomit verwöhnen?", fragte
er.

"Du bist gerade besetzt.", hielt Linoschka fest. "Nurek
sieht so gemütlich aus. Das beruhigt."

"Willst du erzählen, was los ist?", fragte Nurek. Sie
öffnete nicht einmal die Augen dafür.

"Ja.", antwortete Linoschka, stand aber dann doch noch
einmal auf, als ihr Magen knurrte. Sie kam wieder
mit einer Schale Reisbrei und einer Birne. Sie
sprach auch nicht während sie langsam aß. Sie sah
hin und wieder genau hin, wie Marims Finger Nureks
Konturen nachfuhren und ein Lächeln bewegte sich
um ihren Mund. Zwischen Brei und Birne strich
sie sich selbst mit den Fingern einmal sanft durchs
Gesicht.

"Ich würde dich auch streicheln, wenn du das wollen
würdest.", bot Marim an.

Linoschka schüttelte bloß den Kopf. "Leute, die sich
selber sachte streicheln, sollten ein gewohnterer Anblick
werden.", murmelte sie.

Nureks Kopf nickte, und das war die erste Bewegung abgesehen
von den Lippenbewegungen seit vielleicht einer halben Stunde, die
dieser Körper tat. Auch Marim stimmte zu.

Linoschka lächelte ihre Birne an, während sie sie aß, samt
Strunk aber ohne Stiel. Anschließend brachte sie den Stiel
in den Kompost, wusch sich die Hände und verschränkte sich
wieder auf dem Sessel. Sie trug immer noch bloß ein Nachthemd
mit Gespenstern darauf. Sie strich sich, weiterhin in sich
gekehrt lächelnd, über Kopf, Arme und Beine und blickte
schließlich auf. "Bjork trainiert mich diese Woche nicht
in Orkando, weil er am Spiel teilnimmt und gerade die zweite
Phase läuft."

"Ich dachte, er wäre rausgeflogen, hattest du gesagt.", erinnerte sich
Nurek.

"Eine der Personen, mit der er zuletzt in einem Team gewesen
ist, -- *Spielgruppen* heißen die Teams im Spielkontext meistens --,
hat eine neue Teilnahmeurkunde gefunden, eine Gruppe gegründet und ihn wieder
gefragt.", berichtete Linoschka. "Alles ein bisschen kompliziert
mit den Regeln. Jedenfalls ist Bjork wieder dabei. Um Mitternacht begann
die erste Woche der Phase. Die Spielgruppen fetzen sich gegenseitig
auf mehr oder weniger unfreundliche Art in Virtualitäten. Ich
habe die halbe Nacht Übertragungen davon gesehen und" Linoschka hörte mitten im
Satz auf und führte ihn erst ein paar Momente später zu Ende, als
wäre nichts gewesen. "ich habe sehr gemischte Gefühle."

"Interessierst du dich so, weil dir Bjork ans Herz gewachsen
ist?", fragte Marim.

Linoschka hob die Brauen und schüttelte langsam und unsicher
den Kopf. "Also, ja, ich mag Bjork. Ich kann mir gut vorstellen, mit
ihm irgendwann mehr zu haben als nur ein Trainingsverhältnis. Und
ich glaube, er mag sich auch mehr mit mir anfreunden.", sagte Linoschka. "Aber
mein Interesse an Freundschaft bewirkt nicht, dass ich
Spiel-Übertragung gegen Schlaf und Morgenroutine eintauschen
würde." Sie seufzte oder gähnte, oder irgendwas dazwischen. "Bjork
meinte, ich solle mir im Laufe der Woche Gedanken machen, ob
ich vielleicht für die nächste Phase Teil der Spielgruppe werden
möchte. Die nächste Phase ist zwar noch viel länger hin, aber
er meint, es ist sinnvoll, wenn ich es in einer Woche
weiß.", erklärte sie. "Sie hätten
die vergangenen Male immer Leute für die dritte Phase aufgenommen, die
eine gute Kondition haben, Orkando einigermaßen können
und noch ein paar Kriterien. Bjork meinte, ich würde ins Team
passen, soweit er das beurteilen könnte, und er würde mich
vorschlagen, wenn ich wollte."

"Hui.", sagte Nurek. Es klang einigermaßen sachlich, weil sie
dabei weiterhin den Körper einschließlich dem Mund so wenig
wie möglich bewegte. "Ich habe eine nervige Zwischenfrage, die
nichts mit dem Thema zu tun hat, aber die mein Gehirn
dominiert, bis sie ausgesprochen ist."

"Frag!", forderte Linoschka auf. "Ich möchte zwar dringend darüber
reden, aber es drängt nur inhaltlich, nicht zeitlich."

"Marim?", fragte Nurek.

Marim musste fast ein bisschen kichern und seine Finger
zitterten kurz auf ihren Wangenknochen. "Ja?"

"Würdest du mir die Haare färben?", fragte Nurek.

"Ja, klar!", antwortete Marim. "Richtig was Dauerhaftes? Oder
eher die Farbe, die mit Creme wieder entfernbar wäre? Welche
Farbe?"

"Ich möchte gern was Dauerhaftes, weil ich jede Nuance
der Veränderung der Haarstruktur merke, und die gibt es bei
den anderen Methoden immer mindestens leicht.", sagte
Nurek. "Ich möchte nicht blau, zumindest nicht nur. Ich
dachte an lila mit ein paar blauen oder roten Strähnen."

"Dann lese ich mir nachher durch, wie das geht.", versprach
Marim.

"Das war's.", sagte Nurek. "Zurück zum Spiel. Hatte ich das
richtig verstanden, dass dieses Virtualitätengedöns, was im
Spiel jeweils die zweite Phase bildet, insgesamt zwei
Wochen geht, aber unterbrochen von mindestens einem Monat dazwischen?"

"Ja.", antwortete Linoschka. "Beziehungsweise, jein. Es gibt
ein Spielziel. Es wird gespielt, bis entweder die Hälfte der
Gruppen das Spielziel erreicht hat und somit in die dritte Phase
kommt, oder bis die zwei Wochen mit dem Monat Unterbrechung zwischen
diesen um ist. Dann wird geschaut, wer am weitesten ist."

"Also möchte Bjork von dir nach einer Woche eine Antwort, während
die Phase aber wahrscheinlich noch im vollen Gange ist?", fragte
Nurek.

"Wieder jein.", sagte Linoschka. "Bjork meinte, beim letzten Mal
waren sie in der zweiten Phase innerhalb der ersten Woche schon
durch, und sie wären arrogant genug, zu glauben, dass sie in
dieser Runde wieder nur eine Woche bis zum Erreichen des Spielziels
bräuchten."

"Also wirst du für eine extrem gute Spielgruppe gefragt?", fragte
Nurek. Ihr Tonfall klang skeptisch, fand Marim.

"Ja.", antwortete Linoschka. Sie klang auch nicht glücklich, glaubte
Marim herauszuhören.

"Verstehe.", murmelte Nurek. "Also, glaube ich. Ich bin nicht sicher."

"Ich nicht.", sagte Marim.

"Schwer zu erklären. So in kurzgefasst.", sagte Linoschka. "Das
Spiel ist alt. Sehr kreativ, sollte an sich auch lustig sein oder
gemeinsames Schaffen und Entwickeln fördern, aber
einige spielen ziemlich verbissen. Oder ungerecht. Teils korrupt, oder
wie es genannt wird, wenn eine Spielgruppe mit einer anderen
Tipps austauscht, am besten noch um dritte Gruppen platt
zu machen. Ausspionieren und so gehört auch dazu."

"Also respektloses bis hin zu gewaltvollem Sozialverhalten.", fasste
Marim zusammen.

"Und wenn eine Spielgruppe besonders gut ist, liegt nahe, dass
sie zu der respektloseren, gewaltvolleren Sorte gehört, und
nicht einfach so sehr gut ist.", fügte Nurek hinzu.

"Das ist zumindest meine Sorge, und das ist auch Æreniks Ruf. Ærenik
leitet die Gruppe.", sagte Linoscka. "Ich werde mir beim Zuschauen
mit mir selber noch nicht einig, wie ich das werten soll. Das war
wohl der Grund, warum ich mich so ausgiebig
mit der Spielphase beschäftigt habe. Ein
Ruf kommt ja manchmal auch, weil Leute ein Verhalten nicht
verstehen, oder weil allgemein alles kritisiert wird -- was gut
ist, vor allem, wenn es um Abwertung oder Schutz von Leuten
geht, aber es schwer macht, sich zu überlegen, ob ich es noch
vertretbar finde, mit so einer Person zusammenzuarbeiten. Das
Problem beim Beurteilen sind vor allem die anderen Spielgruppen. Ich
brauche Tee."

Linoschka stand wieder auf, gähnte ausgiebig und verließ den
Raum. Als sie wiederkam, öffnete sie die Terrassentür, die vorher
nur auf Kipp gestanden hatte. Eine angenehme Sommermorgenbrise
wehte hinein. Marim genoss, dass es hier in Röversjard nicht
so warm war.

"Da hätten wir eine Spielgruppe mit einem Altersschnitt von
so 12 oder 13. Ich frage mich manchmal, wenn so junge Kinder
oder Jugendliche an Urkunden für die Teilnahme kommen können, ob
es nicht sinnvoll wäre, Jugendrunden und Erwachsenenrunden zu
trennen. Oder Regeln zum Schutz von Jugendlichen aufzustellen.", sagte
Linoschka. "Ich verstehe ja den Reiz, keine Vorgaben zu machen. Aber
was für beschissene Mittel angewandt werden, um diese Jugendlichen
möglichst zu verunsichern oder über ihre Grenzen zu treiben, finde
ich nicht okay. Das kann ganz schön langwierige psychisch-gesundheitliche
Schäden nach sich ziehen."

Nurek nickte wieder. Marim spürte den Kopf dadurch intensiver
in seinem Schoß. Er hatte überraschende Kanten, fand er
immer wieder.

Marim hatte gar nicht bemerkt, wann er dazu
übergegangen war, sachte mit den Fingern durch ihre Haare zu
streichen statt über das Gesicht. Den Anblick dieser
genießenden Person hätte er weiterhin freiwillig gegen nicht viel
eingetauscht.

"Beim Zuschauen habe ich ein wenig das Gefühl, dass Bjorks
Spielgruppe nicht so richtig weiß, wie sie mit den anderen
Gruppen umgehen soll, die viel unerfahrener sind. Aber
gleichzeitig auch überraschend gut.", fuhr Linoschka fort. "Und
ich glaube, auch wenn ich insgesamt weniger fies bin, wüsste
ich das auch nicht. Angenommen, diese Kinder-Spielgruppe kommt
in die nächste Runde. Was mache ich, wenn ich Nea begegne?
Nea ist ein Lobbud, wahrscheinlich irgendwas zwischen 11 und
14, und leitet eine Gruppe, macht sie auch echt gut, aber
körperlich könnte sie mir nichts entgegen setzen. In der
dritten Phase ist aber zum Beispiel Körperkampf wesentlicher
Bestandteil. Was mache ich, wenn ich dieser kleinen, wenn
ich nicht alles falsch einschätze, verhältnismäßig wehrlosen
Person, in die Arme laufe, und eigentlich kämpfen
dran wäre."

"Hm.", machte Marim. "Wäre dann eine Möglichkeit, zu
sagen: Hey, es ist eigentlich jetzt kämpfen dran. Wollen
wir ausdiskutieren, wie die Situation enden würde, und
am für dich realistisch best möglichen Ausgang friedlich
weitermachen?"

"Hm.", machte Linoschka und dachte eine Weile nach, bevor
sie fortfuhr: "Sie könnte weglaufen."

"Ich mag ja an sich Leute weglaufen zu lassen.", sagte
Marim. "Aber könntest du Festhalten, ohne dass es
weh tut oder so etwas?" Ihm kam sein eigener Vorschlag
nicht so gut vor.

Linoschka kam er anscheinend auch nicht so gut vor. "Ich
finde diese Körperunterschied-Sache so, so ungerecht!", fuhr
sie auf. Für ihre Verhältnisse. Sie war nie besonders
laut. Wenn Marim sie nicht inzwischen ein bisschen gekannt
hätte, hätte er es sachlich empfunden.

"Das verstehe ich.", sagte er. "Meine Antwort fühlte sich
schon beim Aussprechen falsch an."

"Hast du Gründe, warum du teilnehmen möchten würdest?", fragte
Nurek.

Linoschka grinste und Marim kicherte wegen des doppelten
Konjunktivs.

"Ja.", sagte Linoschka. "Schon."

"Hm.", machte Nurek. "Du könntest mit uns Campen und
mich zur Übung angreifen. Ich bin völlig hilflos."

"Das fühlt sich allein im Kopf schon furchtbar an.", sagte
Linoschka.

"Genau.", bestätigte Nurek. "Ich habe mehr Stresstest dadurch, dass
du Dinge probierst. Taugt das für dich als Gelegenheit, Wege
für dich auszutesten?"

Linoschka nickte ein bisschen, dafür aber lang und runzelte
die Stirn. Und vielleicht weil Nurek das alles nicht sah, weil die
Lider geschlossen waren, ergänzte Linoschka die Gestik
durch ein halb zustimmendes Summen. "Ihr wollt dabei
nicht unter euch sein?", wandte sie sich, mit Gestik
andeutend, auch deutlich an Marim.

"Das muss Nurek entscheiden. Das ist ihre Übung. Ich mache
alles mit.", sagte Marim.

"Ich glaube, mehr Stress macht den Stresstest besser.", sagte
sie.

"Warum möchtest du teilnehmen?", fragte Marim.

Linoschka pustete vorsichtig über den Tee. Der Wasserdampf
bewegte sich dabei wunderschön. Dann trank sie einen kleinen Schluck. "Ich
mag Survival-Gedöns.", sagte sie. "Ich weiß nicht genau, warum. Ich
mag solche Challenges wie, ohne Internet auskommen. Und
dann kommen meist individuelle Herausforderungen hinzu. Ich
mag ungefähr alles daran außer den Wettkampf-Teil. Aber
nur für mich alleine würde ich sowas eben trotzdem nie
tun. Ich brauche sozusagen einen Anlass und Leute, die
ich im Vorfeld ein bisschen kenne."

"Sollen wir unseren Campingausflug dann auch ohne
Internet machen?", fragte Nurek.

"Notfallinternet sollten wir haben.", sagte Marim.

"Notfallinternet gibt es auch beim Spiel.", bestätigte
Linoschka. "Nur, damit ich nichts durcheinander bringe: Euer
Camp-Vorhaben ist als Vorbereitung für das Live-Konzert, richtig?"

Nurek nickte wieder und Marim bestätigte. Außerdem stand noch
auf der Liste, dass sie sich mit Notfall-Ansprechpersonen
absprechen wollten. Zu fast allen Live-Veranstaltungen gab
es ein Team von Leuten, die sich um Personen kümmerten, die
in Schwierigkeiten gerieten, oder auch von vornherein welche
mitbrachten. Aber bevor sie diesen Punkt angehen könnten,
müssten sie sich noch entscheiden, zu
welchem Schabernakel-Konzert sie gehen würden. Es sollte am
besten ein kleines Festival sein, damit sie danach nicht
direkt zu irgendeinem Schlafplatz reisen müssten, sondern
ihn in einem Zelt vor Ort hätten. Deshalb das Probe-Campen.

---

"Das Funkenfest.", sagte Anuka.

"Viel zu groß.", protestierte Marim. "Geht es eine Nummer kleiner, als
eines der größten Musikfestivals Maerdhas überhaupt zu wählen?"

"Ich habe mir eben das Konzertprogramm angeschaut.", sagte Anuka. "Schabernakel
hat zwei Konzerte von nur je einer halben Stunde und ohne Festival dran und
ein Dreihalbstündiges auf dem Funkenfest. Die zwei anderen finden in
Flangochlen, einer Kleinstadt weit im Süden, und Nyanberg, auch
sehr im Süden, statt."

"Nyanberg ist auch einfach keine so schöne Stadt.", murmelte Marim.

"Findest du?", fragte Anuka.

Sie hatten sich zu einem Kurzfilmvormittag getroffen. Das taten sie
ungefähr einmal in der Woche zu zweit in einer Virtualität. Manchmal
auch zu einem längeren Film, und manchmal auch zu mehrt. Heute trafen
sie sich nur zu zweit, weil sie sich einen queeren Erotikfilm
ausgesucht hatten, an dem Nurek vorhersehbarer
Weise kein Interesse gehabt hatte. Und vorher redeten sie immer
ein wenig.

"Das Funkenfest ist aber vielleicht trotzdem nicht die schlechteste
Wahl.", sagte Anuka. "Ja, es ist riesig, aber anders als andere
Festivals haben die Veranstaltenden sehr viel Übung bezüglich
Planung und Vorbereitung. Es platzt üblicherweise nicht aus
allen Nähten. Sie nutzen Schalltechnik, die ermöglicht, dass es
auf dem Gelände erträglich leise ist, während die Konzerte laufen. Die
Bühnen und Vorbühnenplätze sind riesig. Und das Team, das sich um
verschiedene persönliche Bedürfnisse oder Probleme kümmert, steht
zu großen Teilen jetzt schon, ist gut organisiert und groß. Unperfekt
ist vor allem noch Barrierearmut für Personen, die ein EM-Feld brauchen, aber
auch weit weg vom Kern wohnen müssen, wegen Lärm oder Reizüberflutung. Sie
können technisch bisher nur nahe der Bühnen ein brauchbares EM-Feld
aufrechterhalten. Aber das Problem betrifft euch ja nicht, oder?"

Marim schüttelte den Kopf und dachte nach. Die Vorstellung, dass
eines der größten Festivals überhaupt weniger überfordernd sein
sollte als ein kleines, stieß in seinem Denken zunächst
auf Widerstand. "Das Team steht schon?", versicherte er sich.

"Es heißt Securiteam und es steht nicht ganz. Es wächst meistens
spontan nochmal auf die doppelte Größe an mit Nachwuchs oder
mit Leuten, die sich erst melden, wenn sie ihren Konzertplan
kurz vor knapp erstellt haben.", erläuterte Anuka. Er
war schon zwei Mal dort gewesen, fiel Marim ein. "Aber es
steht schon ein Teil des Teams fest. Personen, die sich auskennen, die
jedes Mal dabei sind, die ausbilden. Und sie bieten im Vorfeld
an, Termine mit ihnen zum Kennenlernen auszumachen."

"Ich werde Nurek fragen, was sie dazu meint.", beschloss
Marim. Er hatte es ohnehin nicht alleine zu entscheiden.

Anuka lehnte sich an ihn und Marim legte ihm einen Arm um
die Schulter. "Ist das okay?", fragte Anuka.

Marim nickte.

"Läuft deine Studie langsamer als sonst?", fragte Anuka.

Marim schnaubte und gluckste ein bisschen. "Durchaus."

"Zu verliebt?", fragte Anuka mit einem breiten Grinsen
in der Stimme.

"Vielleicht auch.", sagte Marim. Das Gefühl, das durch seinen
Körper strömte, war nicht so stark, wie am Anfang, aber
vielleicht mochte er es deshalb sogar lieber, weil er es
vollständig mit seinen Sinnen erfassen konnte. "Ich glaube
aber, ich werde nach dem Funkenfest mit der Studie
wieder schneller vorankommen. Gerade habe
ich so eine Art Hyperfokus auf das Projekt Schabernakel-Konzert."

"Du hast Funkenfest gesagt." Anuka grinste sehr breit und
blickte Marim mit lebendigen Augen an, die fast unter den
hochgezogenen Wangen verschwanden.

"Oh.", machte Marim sachlich. Scheinbar fand ein eher
unterbewusster Teil von ihm den Vorschlag gut.

---

Nach dem Film, der nur eine Drittelstunde gedauert
hatte, duschte er, aber nur kurz, weil Tjaren für die
ganze Bagage -- wie Tjaren sich oft ausdrückte -- Essen
gedruckt hatte. Es standen eine Vielzahl Karbonschüsseln
auf dem Tisch. Schüsseln, die nicht so sehr klapperten, die
eine Abperlstruktur hatten und leicht gesäubert werden
konnten, und dies auch noch mit EM-Feld von alleine taten, weil
in ihnen ein entsprechendes feines Drahtgeflecht verbaut
war. Sie rieben sich selbst gegen dafür gedachte Tücher. Das
hatte sich als umweltschonender herausgestellt, als
irgendwelche Kästen, bei denen ausschließlich
oder vorwiegend mit Sprühtechnik gearbeitet wurde. Anstrengend
wäre für Marim gewesen, dass die WG für das Geschirrspülen ständig
Routinen änderte, weil Nurek all die neue Technik
ausprobierte. Gerade vor einer Woche war eine neue
Tuchanlage geliefert worden. Aber er hatte es anfangs
einmal vorsichtig geäußert und sie waren zu so
einer Art viel zu gutem Kompromiss gekommen: Nurek
kümmerte sich um sein Geschirr. Auf der anderen Seite, auch
wenn es ihm dauernd so vorkam, dass dem nicht so wäre, kümmerte
er sich durchaus um eine Menge. Marim verwöhnte gern. Das
lief für ihn irgendwie so nebenbei, dass er es manchmal
gar nicht mitbekam, und vor allem selten abspeicherte.

Jetzt jedenfalls setzte er sich in einem frischen
Sommerkleid und geringelten Netzstrumpfhosen -- weil
es für Strümpfe definitiv zu warm war -- mit an den
Tisch. Linoschka fehlte. Auf Rufen kam mehrfach ein
"gleich" zurück und schließlich ein "Fangt gefälligst
ohne mich an.". Es war selten, dass sie aufeinander
warteten, aber heute hätte es sich andernfalls so
ergeben. Schließlich kam Linoschka und wirkte noch
unausgeschlafener und erschöpfter als vorhin. Sie
aß auch nicht gleich. Sie trug immer noch nur das
Nachthemd. Sie atmete erst ein paar Mal tief durch, bevor
sie überhaupt zu realisieren schien, dass Essen auf dem
Tisch stand.

"Was zockst du?", fragte Ivaness.

"Gar nichts.", sagte Linoschka. "Ich schaue mir
Spiel-Doku und andere Übertragungen davon an. Uffz, ist
das spannend."

"Macht es dir was aus, dass ich das Spiel eine
furchtbare Sache finde?", fragte Ivaness.

"Wenn du damit leben kannst, dass ich vielleicht
teilnehme?", fragte Linoschka zurück.

Ivaness reagierte nicht.

"Ist das ein vielleicht, das einfach nur die Möglichkeit
offen hält, wie vorhin, oder neigst du langsam dazu?", fragte
Nurek.

"Ich merke schon, dass ich gern den Rückhalt von euch
hätte.", murmelte Linoschka und blickte auf ihren immer noch
leeren Teller. "Wenn ihr meint, dass das ethisch nicht vertretbar
ist, dann sollte ich das vielleicht lassen."

"Unfug.", meinte Ivaness. "Das Spiel selbst basiert auf einer guten
Idee, die einfach noch nie gut umgesetzt worden ist. Es gibt
immer Mal wieder die ein oder andere Spielgruppe, die hinter der
Idee steht. Gemeinsam etwas Neues erschaffen, Subkulturen
verbinden, Technik einem großen Publikum nahezubringen. Da
stehst *du* eben voll hinter." Ivaness' Stimme wurde sanfter, sehr
konträr zu dem, was as nun sagte: "Die meisten Teilnehmenden verhalten
sich nur, als wäre das Spiel eine Welt, in der sie endlich mal
sämtliche Moral- und Ethikvorstellungen respektvollen Miteinanders
in Abfälligkeit und Niedermachen eintauschen könnten, ohne
dafür kritisiert zu werden, weil sich doch alle im Spielkontext
freiwillig darauf einließen."

"Ja, sowas sagte auch ein Artikel von Rosa Pride-Away.", murmelte
Linoschka.

"Oh, sie hat einen Artikel zum Spiel geschrieben?", fragte Ivaness. "Den
muss ich lesen!"

"Einen an sich ziemlich differenzierten, aber -- wie für sie üblich -- auch
sehr derb formulierten.", bestätigte Linoschka. "Jedenfalls
meint sie ungefähr das, was du gerade gesagt hast, und dass, wenn
Leute sich gern unbedingt in irgendeinem Kontext so benehmen wollten, dass
es vielleicht sinnvoll wäre, wenn sie sich stattdessen in einem
Rollenspiel zusammentun und sich gegenseitig konsensuell
kaputt machen würden, aber ein Kulturevent sollte eben auch offen sein
für Kultur."

"Ich lese das nachher.", versprach Ivaness. "Jedenfalls, wenn du daran
teilnehmen möchtest, bin ich mir bei dir sicher, dass der Wettkampf
von dir profitiert und dass du deine Ethik- und Moralvorstellungen nicht
fallen lässt. Das wird kompliziert in der Interaktion, aber du hast
mich hinter dir und meine volle Unterstützung."

"Heute hat meine Orkando-Trainerperson gegen ein Kind gekämpft.", sagte
Linoschka. "Und verloren. Und zwar nicht, weil er sich zurückgehalten
hätte. Höchstens am Anfang noch ein bisschen, aber er hat schnell
gemerkt, dass dieses Kind einen sehr guten und fairen Kampfstil
hat. Ich habe mir das angesehen. Es ist so beeindruckend. Erst ist
diese Kinderspielgruppe -- das klingt missverständlich -- auf einen
sehr einfachen alten Trick reingefallen, als wären sie sehr unbedarft, und
dann haben sie sich richtig spannend mit Bjorks Spielgruppe bekämpft, dass
es sehr ausgeglichen wirkte. Es verwirrt mich so."

"Vielleicht solltest du erstmal schlafen.", riet Ivaness.

"Bjork ist nun eine Runde schlafen gegangen. Aber kurz davor hat
er noch meine Nachrichten gelesen, dass ich vorhabe, mit Herzwesen einen
Campingausflug zum Testen zu machen, um mich ins Draußenschlafen
reinzufühlen, und darein, kleinere Leute anzugreifen. Er
hat gefragt, ob ich das in die Woche nach der Phase verschieben
könnte und würde, sodass er dabei sein könnte, um mich besser kennen
zu lernen. Vorausgesetzt, sie schaffen das Spielziel
tatsächlich in der ersten Woche.", fasste Linoschka
zusammen. "Das wollte ich eigentlich
erzählen." Sie blickte Nurek und Marim an. "Bevor ich einen
Nachessensschlaf mache."

"Ich hoffe, du hast dir keine automatische Benachrichtigung
eingerichtet, die dich weckt, sobald Bjork wieder spielt.", murmelte
Ivaness.

Linoschka blickte as länger an, holte dann ihren Taschenrechner
hervor und bediente ihn kurz. "Nun nicht mehr."

"Bjork klingt nach einer netten Person. Außerdem, wie jede
neue Person, wie ein Stressfaktor beim Camping-Test.", hielt
Nurek fest. "Ich fände das in Ordnung."

"So kitschig das klingt, ich mache alles mit, was du
möchtest.", sagte Marim. Und dann verzog er doch das
Gesicht und kratzte sich am Kopf. "Das ist viel zu
unpräzise. Oder auch nicht. Wenn du möchtest, dass ich
was für mich hart Ungesundes tue, dann mache ich das
nicht, aber sowas würdest du auch nicht möchten."

"Nun ja, was wir planen, ist schon recht ungesund.", kommentierte
Nurek. Sie machte eine bewusste Essenspause und verknotete
die Beine auf dem Stuhl neu. "Traust du uns das Funkenfest
zu?"

"Darüber wollte ich mit dir auch reden.", murmelte Marim. "Anuka
meinte, wenn wir dieses Jahr wollen, haben wir nicht viel
Auswahl."

"Korrekt.", sagte Nurek. "Und ich würde gern dieses Jahr. Ich
habe überlegt, noch ein Jahr zu warten. Aber dann bin ich
die ganze Zeit untergründig aufgeregt und hibbelig und
habe am Ende weniger Energie. Es kommt dann vermutlich
aufs Gleiche raus, ob ich ein kleines Festival mit wenig
Energie versuche, oder eines der größten mit etwas mehr."

Marim nickte. "Anuka hatte noch ein paar Argumente. Es
soll sehr gut organisiert sein."

"Das Funkenfest? Auf jeden Fall.", sagte Mø. "Auch sehr
transparent. Da wären wir wieder bei Rosa Pride-Away. Sie
ist schwerbehindert, war mehrfach dort und hat darüber
mehrere Artikel geschrieben. Es läuft nicht immer alles
perfekt, aber sie legen alles offen, beschreiben die
Möglichkeiten im Vorfeld detailliert und unterstützen überdurchschnittlich
gut. Ihr Herzwesen ist vor einer Weile dem Securiteam beigetreten, dem
Team, das sich generell um Schwierigkeiten und Accessibility kümmert."

"Ob es möglich ist, einen recht genauen Plan des
Platzes zu bekommen, um Akustik und Geräuschkulisse
zu simulieren?", fragte Nurek.

"Bestimmt.", antwortete Mø.

---

Zwei Tage später hatte Linoschka Ringe unter den Augen und
Ivaness motivierte sie dazu, gemeinsam mit Nurek und Marim baden zu gehen. Das
Wetter war warm. Marim merkte den Unterschied zu Minzter
allerdings deutlich. Sie waren viel weiter im
Norden und auch noch am raueren Meer, wo der Wind mehr wehte. Tjaren
und Mø blieben daheim und genossen, das Haus für sich
zu haben, aber Marim und Nurek kamen mit.

Ivaness und Nurek verschwanden zügig im Wasser, während
Marim noch einen Moment auf seinem frisch ausgebreiteten
Handtuch auf dem Felsen saß und das Gestein bewusst
unter den Fingern spürte. Linoschka nahm die Welt um
sich wohl kaum auf. Sie kniete sich einfach neben ihn
auf den Fels und las auf ihrem Taschenrechner.

"Deine Augen tränen.", teilte Marim ihr mit.

Linoschka nickte abwesend.

Marim seufzte. "Soll ich dir etwas vorlesen?" Er fand
die Idee nur mäßig sinnvoll. Eigentlich gab er Ivaness
recht, dass Linoschka wirkte, als könnte sie mal eine
Pause vertragen. Mehr Schlaf, vielleicht sogar mehr
Bewegung für den gewohnten Energiedurchsatz, den dieser
Körper sonst so hatte. Aber er wollte nicht über sie
bestimmen. Und wenn er in dem Zuge wenigstens dazu
beitragen könnte, dass ihre Augen mal ausruhen könnten, dann
war das vielleicht besser als nichts.

Endlich blickte Linoschka auf. Zu seiner Überraschung reichte
sie ihm den Taschenrechner, nachdem sie mit einer klassischen
Geste für so etwas alle anderen Anwendungen für ihn
sperrte, den Bildschirm daran hinderte, auszugehen, und die
Gestik für diese Anwendung auf Standardeinstellungen
einstellte. Er nahm den Taschenrechner, ohne genau
hinzusehen. Stattdessen blickte er in ihre Augen und
lauschte auf den Atem durch die zugeschwollenen
Atemwege. Die Tränen kamen nicht nur von Überlastung
der Augen, schloss er. Linoschka weinte ein wenig.

Der Mensch legte sich einfach nach hinten auf den Fels
ab. Marim legte vorsichtig den Taschenrechner beiseite, in
der Hoffnung, dass er vorsichtig genug war, dass er nicht gleich durch ein
Ungeschick im Wasser landen würde -- aber im Zweifel
hatte Nurek Schnorchelausrüstung dabei --, breitete die
weiche Decke aus und lud Linoschka ein, darauf
umzuziehen. Linoschka stand nicht auf dazu, sondern rollte
sich einmal um die Hochachse und blieb auf der Decke
erschöpft und mit geschlossenen Augen liegen.

Marim sammelte den Taschenrechner wieder ein und blickte
auf den Text vor sich. "*Hat Raffinesse Schönheit, wenn
sie weh tut?*", las Marim die Überschrift vor.

"Lies nicht laut.", bat Linoschka. "Ich würde auch eher
*traumatisiert* als *weh tut* schreiben, und die Frage
klar mit *nein* beantworten. Der Artikel ist okay-ish
geschrieben, teils bisschen eklig, schon, finde ich. Aber
es ist der, der am besten auf die eigentlichen Fakten
runterbrennt, ohne zu 50% aus Moralanalysen zu bestehen."

Marim nickte und las den Artikel still für sich. Es
war einer naiven teilnehmenden Person eine Falle
gestellt worden, aus der sie sich hatte über einen
dramatisch langen Zeitraum hinweg nicht
befreien können. Die Vorgehensweise und Vorbereitung der
Falle war kompliziert gewesen -- raffiniert, wie der
Artikel betonte. Die Person war danach erst einmal
nicht mehr spielfähig gewesen, also durch dieses
widerwärtige Verhalten aus dem Verkehr gezogen
worden. Marim wurde physisch beim Lesen leicht flau
im Bauch, als er weiterlas, wie strategisch die
Spielgruppe, die die Falle gestellt hatte, außerdem abwertende
Sprüche gegenüber marginalisierten Personen fallen ließ, um
diese wütend und mürbe zu machen, um deren Spielfähigkeit
negativ zu beeinflussen. Die
Person, die den Artikel verfasst hatte, analysierte das Verhalten relativ
sachlich. "Die Spielgruppe, um die es hier geht, ist aber nicht die mit
Bjork?", fragte Marim.

Linoschka schüttelte den Kopf. "Bjork schreibt mir
manchmal über diese schreckliche Spielgruppe. Sie hatte
im Vorfeld Kontakt mit seiner Gruppe aufgenommen. *Gothilla*
ist die Spielgruppe, in der Bjork ist."

"Konnte Gothilla gegen die Falle, um die der Artikel geht, irgendetwas
ausrichten?", fragte Marim.

"Hätten sie vielleicht gekonnt, aber sie haben sich gerade
hart mit besagter Spielgruppe gebattlet, als es passiert ist, und haben es zu spät
mitbekommen.", antwortete Linoschka. "Bjork macht sich deswegen
Vorwürfe. Auch weil die Vereinbarung im Vorfeld am Ende mit zur
Falle beigetragen hat, aber das wussten sie nicht. Sie dachten, es
ginge nur um einen guten Ruf, der ohnehin im Spiel bei
korrupten Spielgruppen nie alt wird."

"Weinst du wegen der Falle?", fragte Marim.

"Ist dir beim Artikel nicht anders geworden?", fragte Linoschka.

"Doch.", sagte Marim. "Flau. Ziemlich. Die Gruppe ist widerlich."

Linoschka nickte. "Hab diese Emotion nach ungefähr drei Tagen
mit zu wenig Schlaf und zu viel Informationsinput. Und
Überlastung und so."

"Ich verstehe. Es braucht aktuell weniger, um dich über den
Punkt von wirklich zu viel zu bringen.", fasste Marim
zusammen.

Linoschka nickte wieder. "Es sieht nach derzeitigen Hochrechnungen
so aus, als käme diese widerliche Spielgruppe in die nächste Phase. Kaum
verwunderlich, sie sind sehr gut.", berichtete Linoschka. "Gothilla
fragt sich, welche Fallen sie stellen können, um sie
aufzuhalten. Keine so fiesen wie im Artikel natürlich."

"Ich bin mir nicht einmal sicher, ob ich noch im
Stande wäre, diese Rücksicht zu nehmen.", überlegte Marim.

"Stopp, Marim.", sagte Linoschka. "Du bist nicht der Typ dafür,
dir zu überlegen: Hey, dieser Person sage ich im Moment, kurz bevor
sie eine wichtige Aufgabe erledigen möchte, dass sie keinen
Rückhalt in der eigenen Spielgruppe hätte, und irgendwelche
beleidigenden Abwertungen, die bewusst und präzise ins
Selbstwertgefühl einschlagen. In keiner Situation. Punkt."

Marim nickte. Und atmete tief durch. "Es tut mir leid, du
hast recht."

"Was tut dir denn leid?", fragte Linoschka.

"Dass ich mich so ausgedrückt habe, dass ich die Situation
besser verstanden hätte als du oder die Spielgruppe, in die
du vielleicht eintreten möchtest.", konkretisierte er.

Linoschka seufzte. Vielleicht klang es eine Spur genervt. Das
war ein krasses Zeichen, dass ihr alles zu viel war. Sie
klang eigentlich nie genervt. "Ich bin nicht in der Verfassung
zu analysieren, ob du dich gerade eine Spur zurecht kritisierst
oder einfach kompletten Unsinn redest.", sagte sie.

Marim grinste und kicherte ein bisschen.

Linoschka steckte es immerhin an. "Jedenfalls spielen
sie möglichst effizient und sind natürlich auch mit
eigenem Vorankommen schon ausgelastet. Fallenstellen
ist dann nur am Rande eine Option. Bjork hat
so nebenbei gemeint, wenn mir irgendwas einfiele, solle
ich Bescheid geben. Und nun grübele ich nach. Mir fällt
es schwer, damit aufzuhören."

Marim nickte. "Aber Schlaf ist wichtig.", sagte er. "Gothilla
ist bestimmt nicht morgen schon durch. Ruh dich bis dahin
aus. Ich helfe dir."

"Du hilfst mir?", fragte Linoschka skeptisch.

"Ich lasse Programme über die Spielaufnahmen laufen und analysiere,
was vielleicht für Möglichkeiten bestehen.", versprach
er. "Es ist lange her, aber zu meiner Schulzeit habe
ich mich mal intensiv mit dem Spiel und den Regeln auseinandergesetzt."

---

"Triangulierung!", rief Nurek, als sie triefend nass wieder bei
Marim und Linoschka ankam. Sie setzte sich auf den Fels, um
dort einen großen Wasserfleck zu hinterlassen, der langsam
in der Sonne trocknen würde.

Marim war sich nicht sicher, ob es ein Stimm-Wort war, das
Nurek einfach durch den Kopf geschossen war, oder ob
es einen Zusammenhang zu irgendetwas hatte. Beides passierte
oft: Wörter, die sie einfach mochte, oder die sich ihr
aufdrängten, die sie dann rief, manchmal auch mehrfach, und
Worte, die durch Assoziationsketten passierten, die die
meisten Personen im Umfeld nicht so rasch nachvollziehen
konnten. Marim fragte erst einmal nicht nach. "Ruhig, Linoschka
schläft.", murmelte er.

"Nope.", sagte Linoschka. Aber immerhin wirkte sie entspannt.

Ivaness, das auch gerade ankam, nahm hinter Linoschkas Handtuch
Platz, ebenfalls ohne sich abzutrocknen, und massierte ihr
den Nacken. Linoschka gab ein kurzes, wohliges Geräusch
von sich, das, soweit Marim beurteilen konnte, lediglich
dazu da war, Konsens auszudrücken.

"Wir haben gespritzt und Wellen gemacht.", sagte Nurek. Sie
versuchte, ruhig zu sein, aber irgendetwas begeisterte
sie. "Ich dachte, wir könnten uns Daten über das Gelände
bei Fork geben lassen, wo das Festival stattfindet, und
ein möglichst ähnliches hier finden. Und dann die
Beschallung nachsimulieren."

Es war also die Assoziationskette. "Du bist mir noch ein paar
Schritte zu schnell. Wo kommt die Triangulierung ins Spiel?", fragte
Marim.

"Beim Abgleich der Gegenden.", sagte Nurek. "Und bei der
Schallsimulation."

"Oh, und viel wichtiger, was ist Triangulierung noch gleich?", fragte
Marim.

"Triangulierung gehört zu einer Approximationsmethode.", fing
Nurek an, aber unterbrach sich direkt selbst: "Oh,
ich glaube, ich habe dir mal gesagt, weniger
Fachwörter wären besser. Und sollte mich mal selber daran
halten." Sie dachte einen Moment nach. "Approximation
heißt Annäherung. Beim Funkenfest wird es Schall von Konzerten
geben und Schall von Leuten, die dort campen oder anders
hausen und die dabei auch noch selber Musik oder Party
machen. Diese Daten werden anonymisiert jedes Jahr aufgenommen, sodass
von jenen abgeleitet hochgerechnet werden kann, wie sie dieses Mal
aussehen werden. Darin steckt bereits eine Unsicherheit, aber
dagegen können wir nichts tun."

"Hat das schon mit Triangulierung zu tun?", fragte Marim.

"Ich bin nicht sicher, was für Methoden sie anwenden, um die
Geräusche lokal möglichst gut zu simulieren, aber darum
geht es mir auch nicht.", antwortete Nurek. "Triangulierung
kann aber bei solch einer Art von Simulation helfen. Als
Beispiel könnte der unebene Platz trianguliert werden. Das
heißt, wir nähern diesen Platz mit seinen Hügeln und
Tälern mit Dreiecken an. Sehr vereinfacht wäre ein Hügel
zum Beispiel eine Pyramide."

"*Sehr* vereinfacht!", kommentierte Ivaness.

"Wahrscheinlicher ist, dass wir eher sowas in der Größenordnung
von mindestens sieben Dreiecken nehmen, um einen Hügel
nachzuahmen.", bestätigte Nurek. "Es gibt verschiedene
Methoden, um eine passende Sammlung von Dreiecken zu finden.
Meistens wird mit einem groben Gitter angefangen, das womöglich
nur aus ein bis zwei Dreiecken besteht, und dann werden sie
weiter unterteilt. An Orten, an denen viele kleine Unebenheiten
sind, vielleicht sogar feiner als an anderen."

"Okay, ich habe ein Bild einer Triangulierung.", sagte
Marim und lächelte. Es machte ihm Spaß, zuzuhören. Er
merkte erst ein paar Momente später, dass es nicht nur
daran lag, dass er das Thema interessant fand, sondern auch
und vor allem, weil Nurek da mit so viel Elan drinsteckte. Es
war wunderschön.

"Jedenfalls war der Gedankengang so:", leitete Nurek neu
ein. "Wir wollen campen, und dabei vorübergehend die
Geräuschkulisse des Funkenfests simulieren. Ivaness
und mir war schnell klar, dass wir nicht Boxen im
Nirgendwo ohne Zugang zu Strom aufstellen werden. Wir
werden die Geräusche abhängig vom Ort, wo wir jeweils
stehen, an die Hörimplantate oder auf die jeweiligen
Ohrhörer ausgeben. Wenn wir aber genau die Geräuschkulisse
übernehmen, die uns die Funkenfest-Orga fürs Funkenfest
zuschickt, dann wird sich das nicht stimmig anfühlen, weil
sich Akustik ja auch vor allem dadurch ändert, wie
die Landschaft aussieht. Wenn du hinter einen Baum gehst, und
es wird lauter, wird sich das nicht richtig anfühlen." Nurek
pausierte noch einen Moment, aber an ihrer nachdenklich
gerunzelten Stirn glaubte Marim zu erkennen, dass sie
nur auf der Suche nach dem Faden war, und noch nicht
fertig. Er behielt recht. "Jedenfalls dachte ich, wir könnten
für den Campingausflug die Gegend hier mit ein paar kleinen
Drohnen abscannen, ob ein Ort möglichst ähnliche Geographie
hat wie der Ort, wo das Funkenfest stattfindet, und
die Geräuschkulisse vom Funkenfest dann daran anpassen. Das
müsste der beste Kompromiss sein."

Marim nickte. "Hast du auch überlegt, dass du dir dann
mit aufgesetzter VR-Brille auch einen Overlay für die
Vision bauen könntest, sodass du auch andere Personen
um dich herum campen siehst? So als Geräuschquelle für
die Akustik, meine ich."

"Machst du dich lustig?", fragte Nurek. "Du willst darauf
hinaus, dass ich auch gleich in einer Virtualität campen
kann, richtig?"

"Uffz.", machte Marim. "Ich hatte viele Gedanken, und ja, das
war einer davon.", gab er zu. "Eigentlich war der Hauptgedanke,
dass, wenn ein Baum irritiert, der dem Schall im Weg stehen sollte, aber
diesen Einfluss nicht hat, dass vielleicht das
Fehlen von Geräuschquellen auch irritiert."

"Das kommt drauf an, ob ich gerade durch ein simuliertes
Camp gehe oder versuche, mich bei einem Spaziergang etwas
außerhalb des Camps zu entspannen.", überlegte Nurek. "Ich
verstehe dein Problem, aber ich würde es auch trotzdem gern
probieren."

"Vor allem, weil es Spaß macht, vermute ich?", fragte Marim.

Nurek dachte kurz irritiert nach. "Ähm, ja.", gab sie zu. "Aber
noch etwas: Eine Virtualität kann nie so gut simulieren, wie
das ist, wenn die Toilette nicht durch Verlassen der Virtualität
und dann in derselben Wohnung erreicht wird, sondern
ich tatsächlich durch eine Geräuschkulisse auf eine mir
nicht vertraute gehen muss."

Marim nickte. Darüber hatten sie sich auch noch keine Gedanken
gemacht, wie sie das beim Campen mit der Nähe einer Toilette
handhaben wollten.

---

Der Rest der Woche wurde sehr anstrengend. Am Folgeabend trafen
sich Marim und Nurek mit einer Person mit Namen Gabriane in
einer Virtualität, die dieses Jahr das Securiteam mit anleitete. Sie
war schon lange dabei, war gelassen und ruhig, konnte alle
Fragen beantworten, ihnen erklären, welche Daten zur Verfügung
standen und wie sie damit umgehen könnten. Das hätte auch eine
KI machen können, aber Nurek hätte dann nicht genügend konkrete
Fragen zum Funkenfest gehabt, um dabei mit einer Person aus
dem Securiteam ausreichend vertraut zu werden. Also sprachen
sie über interessante, gegebenenfalls nerdige Themen, bis
Nurek soweit war, dass sie sich trauen würde, im Falle, das
ein Problem vorläge, Gabriane tatsächlich zu kontaktieren.

Marim fand das vom Securiteam tatsächlich sehr weit durchdacht. Er
kannte diese kurzen Miminalvorstellungen, 'Hier, deine Ansprechperson,
viel Spaß, sie wiederzuerkennen', aus der Lerngruppenzeit. Es
war nicht schlimm gewesen, -- er hatte sich in virtuellen Lerngruppen
eben tatsächlich mit seiner Lern-KI immer helfen können --, aber
für ein Outernet-Event war eine Kennenlernrunde mit Ansprechpersonen
im Vorfeld sinnvoll, die über ein kurzes 'Hallo' hinausging.

"Wie managt ihr solche Gespräche zeitlich?", fragte Marim.

"Wir sind viele. Und nicht alle brauchen so etwas.", antwortete
Gabriane. "Naja, und zugegeben: Ich bin seit drei Monaten mit nichts
anderem beschäftigt. Danach werde ich vermutlich einen Monat nur
ausruhen. So ein Festival ist eine Lebensaufgabe, aber eine, die
Spaß macht."

Marim konnte es ein wenig nachvollziehen. Es fühlte sich auch für ihn
so an, als hätte er im vergangenen halben Jahr sein Leben ziemlich
darauf fokussiert. Aber immerhin nicht nur. Und er kümmerte sich bei
der Vorbereitung nur um Nurek und sich selbst. Er spürte starke
Bewunderung für Gabrianes Arbeit.

Diese Woche allerdings überlastete er sich. Denn wenn er nicht
gerade mit Nurek begeistert über Triangulierung sprach, saß er mit
Linoschka zusammen vor alter und frischer Spiel-Doku und anderem
Material wie Blogartikeln, Analysen, Analysetools zum Selberanwenden, um
möglichst gute Strategien für Gothilla zu entwickeln, der widerlichen
Spielgruppe faire Steine in den Weg zu legen.
