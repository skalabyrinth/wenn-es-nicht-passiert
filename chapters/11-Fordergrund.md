Fordergrund
===========

\Beitext{Marim}

Nurek erzählte ihm oft, dass er immer gelassen wirkte. Er war
sicher auch oft gelassen. Aber nun war er es nicht. Er fragte sich, ob
sie es mitbekam, oder was sie überhaupt mitbekam. Vielleicht
war seine Außenwirkung auch immer noch gelassen. Aber er
hatte Angst. Sie sprach überwiegend nicht, außer
vorhin, als sie ein paar Passierende
angeschrien hatte, mit Worten, die keinen Sinn ergeben hatten. Sie
zitterte und heulte. Letzteres immer noch, dabei waren sie
längst im Zelt. Marim fragte sich, ob er mehr tun konnte oder
sollte, ob er das Securiteam informieren sollte. Aber wenn dies
ein Overload oder Meltdown war, dann waren mehr Leute, die
sich kümmerten, oder ein Umzug kontraproduktiv. Solange
es kein Umzug durch ein Portal nach Röversjard war. Er brauchte
gefühlt viel zu lange, um auf die Idee zu kommen, Linoschka
anzurufen. Linoschka war nicht erreichbar, also probierte er
es bei Ivaness.

"Shit.", war saine Reaktion als er berichtet hatte. "Das
ist heftig."

"Weißt du, was ich tun sollte?", fragte Marim. Nun merkte er
es selbst: Er klang gelassen und ruhig, aber er fühlte
sich, als hätte er es heulend geschrien.

"Nurek hatte so eine Reaktion schon einmal, als ich noch jugendlich
war, sie vielleicht auch gerade so noch.", erinnerte sich
Ivaness. "Das war kein wholesomes Umfeld. Im Wesentlichen wurde
ihr gesagt, sie solle ins Bett und sich wieder einkriegen. Ich
bin dann mit, weil sie das nicht alleine hingekriegt hat. Klingt
ein bisschen, wie bei dir." Ivaness seufzte nachdenklich. "Nun
bist du natürlich da und könntest dich wirklich kümmern, was damals
niemand gemacht hat. Aber ich glaube, mehr als Abwarten kannst
du auch nicht. Das ließ damals einfach irgendwann nach und Schlaf
half. Aber erwarte nicht, dass es ihr morgen wieder munter geht."

"Auf keinen Fall.", sagte Marim.

Auch, wenn Ivaness keinen besseren Plan hatte, tat es gut, mit
Nureks Reaktion auf das Geschehen, und auch mit dem nicht
reagieren auf jegliche Fragen, nicht allein zu sein. 

"Hattest du probiert, Linoschka anzurufen?", fragte Ivaness.

Marim bestätigte und berichtete, dass sie nicht erreichbar war.

"Ja, ich hatte sie auch nicht erreicht.", sagte Ivaness.

Irgendwann, als sie weiter nichts zu sagen hatten, verabschiedete
sich Ivaness zum Schlafen, aber bat ihn, iem eine Nachricht
zu schreiben, sobald es Nurek etwas besser ginge.

Als das Gespräch
beendet war, fühlte Marim sich überraschend allein. Er legte
sich auf die Seite und beobachtete Nurek beim Wimmern. Es
kam in Schüben. Manchmal lag sie auch eine Weile einfach ruhig
da. Irgendwann -- vielleicht dauerte alles überhaupt nicht lang und
kam ihm nur lang vor -- gewöhnte er sich ein bisschen an die
Situation. Und dann strömte die Erkenntnis in sein Bewusstsein, dass
Linoschka vor allem heute sicher versucht hätte, erreichbar
zu sein, und Ivaness bestimmt auch gefragt hatte, weil as
sich sorgte. Er verdrängte die Sorge. Von hier konnte er nichts
tun.

"Es war schön.", murmelte Nurek mit kaum vorhandener
Stimme, bevor sie wieder wimmerte.

"Hast du Schmerzen?", fragte Marim. Er hatte das vorhin
schon einmal gefragt.

Nurek hatte eine Hand gegen die Stirn gepresst. Das hatte
sie die ganze Zeit mal mehr und mal weniger getan und nun
verstärkte sie den Druck. "Ja."

Marim holte Medizin und die Wasserflasche hervor und
wollte sie zur Einnahme präparieren, aber er ließ sich
vom lauter werdenden Wimmern unterbrechen.

"Wasser ist eklig.", sagte Nurek.

"Weißt du, was ginge?", fragte Marim.

"Kalter Gewürzaufguss ohne Kümmel.", sagte sie. Es war ein
Gemisch aus Wimmern und Kichern, aber das Wimmern gewann.

Sie musste sich die ganze Zeit Gedanken gemacht haben, was
gerade ginge, und hatte deshalb zuvor die Frage nicht beantworten
können. Und nun amüsierte sie in diesem Zusammenbruch, dass
sie oddly specific war. "Das sollte doch machbar sein.", sagte
Marim. "Wir leben schließlich nicht ohne Grund in einer Zeit
mit Lebensmitteldruckern."

"Nicht weggehen.", erschwerte Nurek die Challenge. Und
nur Momente, nachdem Marim seinen Taschenrechner gezückt
hatte, noch ein wenig mehr: "Nicht telefonieren."

Marim lächelte sie an. Bis jetzt blieben Möglichkeiten übrig. Das
reichte doch. Er tippte lautlos eine Nachricht ans Securiteam:

*Marim Präsenz: Wir bräuchten zwecks Medikamenteneinnahme kalten
Gewürzaufguss ohne Kümmel, aus einem Karbon- oder Metallgefäß mit
Deckel. Wir können uns darum beide gerade nicht kümmern. Wir
sind in unserem Zelt.*

Marim fühlte sich trotzdem etwas seltsam dabei, die Nachricht zu
verschicken. Nicht, weil er es nicht absolut richtig und valid
fand, das zu tun, sondern einfach, weil er so etwas noch nie
gemacht hatte. Es folgte eine rasche Antwort:

*Anonym41: ETA in 0.07h. Bereite auf Eintreffen von Fremdperson
vor, wenn geht. Soll ich möglichst schnell wieder weg sein?*

Das war schnell. Unter einer Zehntel Stunde, damit hätte Marim
nicht unbedingt gerechnet. Er tat, wie ihm geheißen und berichtete
Nurek davon, gab die Frage an sie weiter, aber sie konnte sie
ebenso wenig beantworten. Schließlich schloss Marim, dass
die Personen des Securiteams so schnell da sein konnten, dass
es nichts ausmachte, mit 'ja' zu reagieren, zumal, wenn Nurek
ihm Telefonieren verboten hatte, dann vielleicht auch
reden generell schlecht war.

Reden blieb allerdings nicht ganz aus, als das Getränk gebracht
wurde. Statt ein akustisches Zeichen zu geben, schrieb die Person
per Nachricht, dass sie vorm Zelt stünde. Marim öffnete es und
Nurek fing wieder leise zu wimmern an. Vor dem Zelt hockte die
Urukene, die ihnen anfangs das Zelt gezeigt hatte. "Meistens
sagen kranke Personen kalt, aber meinen, nicht heiß oder warm. Für
den Fall habe ich den zweiten Becher mitgebracht.", sagte
der Ork leise und drückte Marim zwei Becher in die Hände, beide
aus kühlem Metall mit Deckel, dazu Trinkhalme aus einem dünnen
karbonartigen Material, die nicht heiß wurden und weich waren.

Sie brauchten sie nicht. Nurek mochte das Gefühl von Schlucken durch
Trinkhalme nicht. Aber Marim schaltete nicht sofort, sie gleich wieder
mit abzugeben. "Wie heißt du eigentlich?", fragte Marim.

"Auf diesem Festival Anonym41, Pronomen sie.", sagte Anonym41.

Marim vermutete, dass vor allem die Zahl am Ende wechselte, wenn
sie auf anderen Veranstaltungen unterwegs war, und
dass das Namensschema verbreitet war. Aber es reichte. Er wollte
einfach wissen, wie er genau sie wieder erreichen könnte, wäre
dies nötig.

Sie schwiegen einen Moment, und als Marim beschloss, dass nun verabschieden
sinnvoll wäre, aber völlig vergessen hatte, wie das ging, sah
Anonym41 ihm das vielleicht an. Sie machte eine Geste zwischen
Abschied und Danke-sagen, stand elegant und vor allem Gelassenheit
ausstrahlend auf und ging. Zügig, aber nicht hetzend, wohl einfach
so rasch, wie das mit der Körpergröße und Kraft natürlich war.

Nurek richtete sich auf und verzog dabei so heftig das Gesicht, hielt
die Luft an, dass Marim die Schmerzen fast mitfühlte. Sie
hatte allerdings mitgehört. Sie nahm das Gefäß mit der abgekühlten aber
nicht kalten Flüssigkeit und schluckte die Medizin, die Marim ihr
reichte. Dann zwang sie sich, den ganzen Becher in kleinen
Schlückchen leer zu trinken. Das erschöpfte sie völlig. Sie
legte sich wieder hin und den Becher gegen ihre Stirn. Allerdings
nur für ein paar Momente. Vielleicht war ihr das Material unangenehm.

Wieder fühlte es sich lange an, bis sie wieder sprach. Aber dann
sprach sie immerhin nicht mehr wimmerich, als hätte sich etwas
gelöst. "Anonym41 versteht was von Gewürzaufgüssen.", murmelte
sie.

"Du meinst, das war nicht der Standard, den ein Drucker druckt, wenn
so ein Auftrag gegeben wird?", fragte Marim.

"Ich kann mir sogar vorstellen, dass der nicht einmal gedruckt
war.", sagte Nurek. Sie drückte nun wieder ihre Finger in
die Stirn. "Kannst du mich vorsichtig mit kalten Fingern
im Nacken massieren?"

Marim registrierte erst jetzt, dass er den anderen Becher mit den
Fingern umklammert hatte, um für so einen eventuellen Zweck selbige
abzukühlen. Er lächelte. "Gern.", sagte er. Und das tat
er dann auch, bis sie eingeschlafen war.

---

Er schlief unruhig ein. Sein Kopf war zu überfordert mit den ganzen
neuen Erlebnissen, um sinnvoll rasch runterfahren zu können. Stattdessen
hörte er die Stimmen und Musik, die er im Laufe des Tages gehört
hatte, zu schnell und übereinandergelagert im Kopf, die Kopfschmerzen, die
er vorhin verdrängt hatte, wurden schlimmer, und ihm fiel erst
nach einer gefühlten Stunde ein, dass er sehr wohl auch Medikation
einnehmen konnte, auch wenn Nurek das bereits getan hatte, dass sich
das nicht gegenseitig ausschloss. Aber er brauchte noch einmal
einige lange Momente, bis er es tatsächlich schaffte, sich dazu auch
zu bewegen.

---

Er wachte viel später auf als sonst und trotzdem noch vor
Nurek. Er fühlte sich erschöpft und matt. Er lauschte auf Nureks
ruhigen Atem, der sehr leise war. Als es ihn ausreichend beruhigt
hatte, setzte er seine Brille auf und entrollte den Minifaltrechner,
den sie mitgenommen hatten, um das Internet zu lesen.

"Wie geht es dir?", fragte Nurek verschlafen.

Marim erschreckte sich und warf dabei fast den frisch entfalteten
Rechner durchs Zelt. "Ich wollte dich nicht wecken."

"Hast du nicht.", sagte Nurek. "Ich habe nur halb geschlafen und
mich einfach gar nicht gerührt." Das tat sie auch jetzt noch
fast nicht. "Du hast unruhig geschlafen und im Schlaf geredet. Wie
geht es dir?"

Marim legte Rechner und Brille zur Seite und sich wieder hin. Was
für eine Frage. Manchmal war sie ganz schön schwierig. "So
viel auf einmal.", sagte er. "Besorgt, erschöpft, verliebt.", zählte
er auf, dachte nach und fügte hinzu: "Angespannt. Ich fühle
Schamgefühl oder sowas. Erschöpft hatte ich schon, oder?"

"Warum Schamgefühl?", fragte Nurek.

"Unser Gespräch über, nun ja, Abhängigkeit.", sagte Marim. "Es
fällt mir schwer, damit umzugehen. Willst du, dass ich darüber
jetzt rede?"

"Ja." Nurek hatte ruhig gesprochen, aber es klang trotzdem unverkennbar
überzeugt.

"Da ist einmal das unlösbare Problem, dass weder du noch ich es
mögen. Aber wir wollten auf dieses Konzert gehen, und für dich
war das nur möglich, wenn wir das Abhängigkeitsverhältnis so
aufbauen.", erklärte Marim. "Es fühlt sich schon furchtbar an, es
so auszusprechen irgendwie."

"Das verstehe ich.", sagte Nurek. "Ich freue mich ein bisschen darauf,
wieder zu Hause zu sein. Aber eigentlich mehr wegen der Dauerbelastung, die
ich hier habe, durch Geräusch- und Bewegungskulisse, Termine, Orte
wechseln, lauter Routinenbrüche, und so weiter. Diese Abhängigkeitssache
ist ein Stressfaktor, aber nur ein kleiner so im Vergleich. Ich möchte
das eigentlich auch wieder tun."

"Du möchtest auf noch ein Festival oder Konzert mit mir gehen?", fragte
Marim.

"Ja. Ich glaub' schon.", sagte Nurek leise. Sie hatte nicht einmal
die Augen geöffnet, lag immer noch still da. "Was noch? Du hast mit
'Da ist einmal' eingeleitet."

"Das Schamgefühl kommt wahrscheinlich daher, wie andere Leute uns
sehen. Oder sehen könnten.", sagte Marim. "Ich erinnere mich
daran, dass ich mit Herzwesen viel über Beziehungen gesprochen
habe, wie solche idealerweise aussehen sollten. Oder woran ich
erkennen kann, dass sie toxisch sein könnten. Es gibt ja solche, wo
Personen aneinanderkleben und die eine über die andere quasi bestimmt.
Ich glaube, das ist bei uns anders. Es geht ja nie darum, dir einen
Willen aufzuzwingen. Aber ich habe irgendwie Angst, dass es so
rüberkommt." 

"Fällt es dir schwer, zuzugeben, dass dir nicht egal ist, was die
Leute denken?", fragte Nurek.

"Ja, weil ich das Gefühl habe, das sollte weit entfernt von meiner
größten Sorge sein.", gab Marim zu.

"Es ist weit entfernt von deiner größten Sorge.", sagte Nurek. "Aus
dem Grund bearbeitest du alle anderen Sorgen permanent und diese
erst jetzt, wo du auf alle anderen zwar nicht unbedingt befriedigende, aber
durchaus Antworten hast."

"Und nun hilfst du mir, mit einem Problem zurechtzukommen, das
eigentlich du als deine Sorge angesprochen hast.", murmelte Marim, halb
belustigt und halb frustriert. Aber er fügte ein sehr ernst
gemeintes "Danke!" hinzu.

"Wir stecken da zusammen drin.", erinnerte Nurek. "Warum habt ihr
über möglicherweise toxische Beziehungen geredet? Möchtest du darüber
reden, oder macht das schlimme Fässer auf?"

Marim lachte leise. "Beides. Vielleicht. Aber eigentlich sind die
bei mir daueroffen. Ich war mal in einer
toxischen Beziehung, in der unter anderem über mich bestimmt
wurde. Ich bin dafür eigentlich ein bisschen anfällig sozusagen."

"Das kann ich mir vorstellen. Das passt zu dir.", sagte Nurek. Sie
öffnete endlich die Augen und wirkte erschreckt. "Oh je, war das
mies von mir?"

"Nein, du hast recht.", sagte Marim. "Ich glaube nur, dass ich auch
aus dem Grund hier so empfindlich reagiere. Weil mein soziales
Umfeld mir damals eben Tipps gegeben hat, wie ich erkenne, ob eine
Beziehung toxisch ist, und es mir in dieser Situation schwer
fällt zu differenzieren."

"Und weil du große Angst hast, toxisch zu sein.", fügte Nurek hinzu.

"Ja.", gab Marim zu.

"Und wenn ich sage, dass dies die untoxischste
Beziehung ever ist, bringt es gar nichts, weil Personen in toxischen Beziehungen oft
nicht merken, wenn sie toxisch wäre.", überlegte Nurek.

Marim grinste. "Das wäre eine Angst. Aber ich vertraue deinen
Fähigkeiten, sowas zu erkennen, mehr als meinen."

"Und wenn ich dir sagte, dass es unwahrscheinlich ist, dass dies
eine toxische Beziehung ist, weil du Angst hast, dass
es eine sein könnte, *und* gleichzeitig sehr kritikfähig bist?", fragte Nurek.

Marim ließ sich die Logik dieser Frage eine Weile durch den Kopf
gehen, bis er erfasste, dass da wirklich was dran war. "Hui.", sagte
er. "Das Argument ist gut."

Nurek schlang einen Arm um ihn und er ließ zu, dass sie seinen Kopf
sachte auf ihre Brust zog. Er hatte das gern. Er fühlte sich sehr
gemocht. Und allmählich entspannte sich das Schamgefühl und
verzog sich erstmal.

---

Es war Linoschkas Anruf, der ihn wieder aus dem Schlaf riss. Ihm war
nicht einmal klar gewesen, dass er wieder eingeschlafen war. Sein
Hinterkopf traf beim Hochschnellen an Nureks Kinn, zum Glück nicht
besonders doll. Er hatte in ihrem Schoß geschlafen, während sie dieses
Mal halb in Kissen aufgerichtet gesessen und gelesen hatte.

"Nehmen wir das Gespräch wieder gemeinsam an?", fragte sie und
rieb sich dabei das Kinn.

Marim nickte. Er fragte sich, inwiefern Nurek mitbekommen hatte, dass
Linoschka nicht erreichbar gewesen war.

"Es tut mir so leid!", rief Linoschka, als sie die Verbindung
angenommen hatten.

"Braucht es nicht. Uns geht es inzwischen wieder ganz gut.", beruhigte
Marim. "Was war?"

"Mir geht es auch gut.", sagte Linoschka, statt zu antworten. "Es
ist nichts Schlimmes passiert. Ich war sehr abgelenkt. Ich möchte
davon eigentlich lieber erzählen, wenn wir zusammen in der Hack-Kommune
sind. Aber ich verstehe, wenn das stresst und würde in dem Fall
auch jetzt schon kurz was erzählen."

"Wann sehen wir uns dort denn wieder?", fragte Nurek.

"Wenn ihr nach Hause kommt.", sagte Linoschka. "Ich brauchte
Pause. Ich habe mich für eine Woche abgemeldet und bin nach
Hause gefahren."

"Das klingt irgendwie schon, als wäre was Schlimmes passiert.", sagte
Nurek nachdenklich. "Aber wenn du sagst, da war nichts, vertraue
ich darauf und ich persönlich kann das gut bis dahin aushalten."

"Es ist wirklich nichts Schlimmes passiert.", wiederholte
Linoschka. "Es sind einfach viele neue Eindrücke und ich vermisse
euch."

---

Das Gespräch dauerte nicht lange. Als Marim auch bestätigt hatte, dass
für ihn warten in Ordnung ging, vertagten sie es. Nurek beschloss, dass
vom Konzert zu erzählen dann auch entspannter wäre, wenn sie es erst
daheim täten.

"Haben wir eigentlich *Die Fenster* verpasst?", fragte sie.

"Nein, noch nicht. Traust du dir das zu?", fragte Marim. "Uns?"

"Ich glaube schon. Wollen wir einfach mal aufstehen und essen
gehen?"

Aber maßlos erschöpft wirkte sie trotzdem. 

Sie gingen erneut zu einem der künstlichen Feuer. Ihr Stammfeuer sozusagen
war um diese Uhrzeit sogar sehr wenig besucht. Sie druckten sich
etwas zu essen und setzten sich gemeinsam auf eine der Bänke, die um das
Feuer herumstanden. Sie aßen schweigend, und als sie gerade fertig
waren, tauchte Gabriane auf und setzte sich dazu, nachdem sie mit
Gesten erfragt hatte, ob das in Ordnung war.

"Wie war es?", fragte sie.

"Sehr gut!", sagte Nurek. "Fand ich zumindest." Sie wandte
sich an Marim. "Du hast noch gar nichts dazu gesagt."

"Ich fand es auch sehr schön!", sagte er. Es wurde ihm erst
jetzt ein bisschen bewusst. Er hatte es währenddessen sehr genossen, aber
danach keinen Kopf mehr dafür gehabt, sich in die frische Erinnerung
hineinzufühlen.

"Das freut mich so sehr!", sagte Gabriane. "So so sehr! Wollt ihr noch
mehr Konzerte anhören?"

"*Die Fenster*.", sagte Nurek. "Und vielleicht das Funkenkonzert."

Das war ein Überraschungskonzert, bei dem eine der hier aufgetreten
haben werdenden^[Anmerkung des Schreibfischs: Dieses Futur II Partizip
oder was das ist, ist hier einfach zu präzise, um es nicht als
Forshadowing aufs nächste Kapitel einzusetzen.] Bands noch einmal auftrat, die das Funkenfest
abschloss. Während des Konzerts wurden Lichter angezündet, die über
das Gelände davonschwebten. Marim blickte sie an, vielleicht leicht
überrascht.

"Es sei denn, das ist dir dann zu viel.", sagte Nurek.

"Das kann ich dir noch nicht sagen.", sagte Marim. "Lust hätte
ich schon. Aber ich bin auch sehr erschöpft."

"Das Funkenkonzert ist bekanntlich ein Überraschungskonzert.", wiederholte
Gabriane, woran Marim sich gerade erinnert hatte. "Das Securiteam weiß
allerdings Bescheid, welche Band spielt und ungefähr welche Lautstärke
und Instrumente für das Programm erwartet werden. Helfen euch genauere
Informationen? Die ihr dann tunlichst für euch behaltet, um keiner
Person die Überraschung zu verderben, die die Überraschung gern hätte?"

Nurek warf einen weiteren Blick auf Marim und nickte vorsichtig.

"Ich brauche keine Überraschung. Das kann helfen.", fasste Marim ihre
Geste und seine Gedanken zusammen.

"Es spielen *Die Träume der Dunkelheit*.", verriet Gabriane. "Die
Band hat lauteres und leiseres Repertoire. Sie hat uns gesagt, sie
würde auf dem Funkenkonzert nur letzteres spielen. Aber ich weiß, dass
die Band ein etwas experimentelles Selbstverständnis von 'leise' hat, und
würde ein gewisses Risiko vermuten, dass drei bis vier Stücke nicht
unwahrscheinlich in die Kategorie *Runaway Metal* fallen."

"Vielen Dank.", sagte Nurek. "Ich kenne die Band und bin nicht
gerade Fan. Einige Stücke mag ich schon. Aber vielleicht ist es
trotzdem schön, das Funkenfest mit dem Funkenkonzert abzuschließen. Wir
überlegen uns das zusammen, denke ich?"

Marim lächelte und nickte. Er hatte einen ähnlichen Eindruck von
den *Träumen der Dunkelheit* wie Nurek, das hatten sie schon
einmal herausgefunden.

---

Aber es kam zu gar nichts mehr von alledem. Auf dem Weg
zurück zu ihrem Zelt rempelte irgendeine Person Nurek ausversehen
an. Sie fing sich eigentlich relativ schnell wieder, aber zwei
Kreuzungen später nervte sie ein Stein in ihrem Schuh. Sie zog ihn
aus und versuchte ihn, auf einem Bein stehend, herauszuschütteln.
Verschiedene Techniken versagten und schließlich schleuderte sie
den Schuh quer über den Weg. Es war ein relativ leerer Weg --
sie traf niemanden. Sie blickte dem Schuh nach und weinte.

"Magst du hier in der Wiese am Wegrand sitzen und ich hole ihn
wieder?", fragte Marim.

Nurek nickte. Die Reaktion kam verzögert. Aber dann setzte sie
sich in die Wiese. Marim ging erst, als sie bereits saß. Mit dem
Schuh setzte er sich neben sie und durchsuchte ihn nach dem
Stein.

"Ich muss nach Hause.", sagte Nurek leise. "Ich hatte irgendwie
gehofft, dass es jetzt besser ist als früher. Aber wenn ich einmal
so richtig energieleer war, dann kommt das in so einer Gegend nicht
zurück. Dann bin ich permament in dem Modus, dass mir sowas
passiert. Ich hätte ab jetzt die ganze Zeit Angst davor. Ich
muss nach Hause."

Marim nickte nur. "Dann fahren wir nach Hause."

"Packst du dieses Mal?", fragte Nurek.

Auch dies bestätigte Marim. Er verstand warum. Alles, was nicht
ganz wie geplant lief, wäre im Moment für Nurek ein Risikofaktor, der
bei ihr ein Fass zum Überlaufen bringen könnte. Auf
dem Weg zum Zelt versuchte er bereits Verbindungen
rauszusuchen. Es gab keine zeitnahe, einigermaßen durchgehende. Die
angenehmste wäre wahrscheinlich eine mit Umstieg in Geesthaven in
die Fähre. Er fragte Nurek, ob das okay wäre. Sie bestätigte, aber
wirkte nicht begeistert. "Lieber am Abend reisen? Dann ginge es
durchgängig.", fragte Marim.

"Entscheide du.", sagte Nurek.

Marim überlegte kurz, und entschied. "Wir steigen um. Und ich
frage Linoschka, ob sie uns für das Gepäck entgegen kommt. Dann
wird der Umstieg entspannter."

Nurek nickte. "Ich traue mir kaum zu, mein Gepäck zu tragen.", gab
sie zu.

"Ich frage im Securiteam nach, ob uns jemand damit hilft.", schlug
Marim vor, fast mehr, wie ein Beschluss. Falls es sie entspannte, es
als einen aufzufassen.

"Neue Personen wären schlecht. Wenn Anonym41 oder Gabriane Zeit
haben, wäre das okay.", sagte sie. "Ich hasse es, solche
Umstände zu machen."

"Dazu ist das Team da.", sagte Marim. "Kannst du tippen, wenn ich
diktiere? Dann kann ich packen und wir müssen nicht ganz so zum
Bahnhof hetzen. Ich schaffe aber auch beides."

"Ich kann.", sagte Nurek zurückhaltend.

Er wartete mit dem Diktieren, bis sie am Zelt waren, Nurek im Gras
saß und er das Zelt sperrangelweit geöffnet hatte. Als sie
die Nachricht verschickt hatte, drückte er ihr Gumbol in den
Arm. Dabei mussten sie beide sogar lächeln.

"Anonym41 kommt in einer Viertelstunde.", teilte sie mit. "Passt
das?"

"Locker.", sagte Marim. Er bewunderte dieses Team.

---

Marim war gerade fertig, als Anonym41 auftauchte. Es war wie Magie. Bis
eben war er völlig angespannt und innerlich gehetzt gewesen. Nun, da die
Sachen gepackt waren, und dieser gelassene Ork einfach dastand
und bereit war, Nureks Rucksack zu tragen, war er plötzlich ruhig. Anonym41
schulterte gleich beide Rucksäcke, über jede Schulter einen, und begleitete
sie über das Gelände zum Bahnhof. Durch ihre Körpergröße vielleicht, oder
auch wegen der Weste, die sie dieses Mal trug, die sie als Teil des
Securiteams auswies, ergab sich ein Personen-leerer Raum um sie
herum. Es hinterließ ein seltsam wehmütiges, aber auch befreiendes
Gefühl in Marim, als sie das Gelände hinter sich ließen und
die Treppe zum Bahnhof hinabstiegen. Anonym41 wartete mit ihnen sogar
auf den Zug und trug das Gepäck im Zug bis zu ihrem Platz -- dieses
Mal natürlich kein Sonderabteil, aber trotzdem ein bisher leeres. Erst
jetzt fiel Marim ein, dass er Linoschka noch gar nicht informiert
hatte, sondern sich das nur vorgenommen hatte. Er schrieb ihr.
Soweit er das sah, war es allerdings nun ziemlich unmöglich, dass sie vorher
noch eine Fähre hinüber bekommen würde.

---

Aber sie war da. Sie erblickte sie, als Marim den ersten
Rucksack aus dem Zug wuchtete, die er zuvor an die Tür gebracht hatte, und
eilte zu ihnen. Sie machte auch Anstalten, beide zu nehmen, aber Marim
hielt sie davon ab und trug seinen eigenen.

"Du bist nass.", informierte Nurek Linoschka.

Sie hatte recht. Linoschkas Kleidung war durchweicht. "Meine Mitfahrgelegenheit
war abenteuerlich.", sagte sie. "Ich bin denn wohl auch mal gesegelt. Es
hatte ein Segelkurs aus Geesthaven einen Ausflug nach Klit gemacht, auf die
Seite der Insel, die Fjärsholm zugewandt ist. Ich bin also zur Insel
mit der Fähre übergesetzt und habe gefragt, wann sie wieder fahren und
ob mich wer mitnähme. Das hat funktioniert, aber war eben sehr nass." Sie
betrachtete Nurek und Marim gründlicher. "Oh my, seht ihr fertig aus. Nurek,
soll ich dich vielleicht tragen?"

Nurek schüttelte den Kopf. "Aber du hast recht. Ich kann mir gerade nicht
einmal vorstellen, den Weg von Fjärsholm nach Hause zu radeln."

"Dann leihen wir uns eine Rikscha.", schlug Linoschka vor.

Und auch das taten sie.

---

Es war fast physisch spürbar, wieviel Anspannung von Nurek abfiel, als sie
wieder in ihrer WG waren. Von Marim allerdings auch. Es war einfach gewohnt. Wände
waren da, wo sie immer gewesen waren. Es roch vertraut. Nurek verzog sich
aber nicht sofort, sondern setzte sich in die Sofaecke in den
Gemeinschaftsraum.

"Zu neugierig zum Ausruhen?", riet Linoschka.

Nurek nickte. "Und die Chancen sind hier höher, dass ich bald Monua trinke."

"Weil ihn eine Person vorbeibringt?", fragte Ivaness, das gerade
aufgetaucht war.

"Ihr tut alle viel zu viel für mich.", sagte Nurek. "Ich dachte schon
daran, dass ich mich vielleicht hier leichter selbst aufgerafft bekomme."

"Ich war noch nicht dran mit verwöhnen heute. Ich mache eine Kanne.", sagte
Ivaness.

Bevor as zurück war, fühlte Tjaren vor, ob es ihnen zu viel würde, wenn Mø
und Tjaren sich auch dazusetzten, und als sie befanden, dass es das
nicht wäre, kam schließlich auch Mø dazu.

Es war gemütlich. Die Terrassentür stand offen und herbstlicher
Nordwind wehte herein. Wobei, Marim wusste gar nicht, welche
Windrichtung es war. Er nannte den Wind Nordwind, weil es für den
Norden typischer, nicht so warmer Wind war. Sie tranken warmen Monua. So
richtig hatte er das Getränk erst in dieser WG kennengelernt. Es
tat gut. Und schließlich beantworteten Nurek und Marim die Fragen, wie
es war. Über die Overloads erzählten sie erstmal nicht so ausführlich. Eigentlich
hatten sie für das Konzert auch nicht ausreichend sinnvoll beschreibende
Worte. Es fühlte sich trotzdem gut an, es zu teilen.

Nurek wartete nur gerade so das Abebben der Euphorie vom
Nacherzählen ab, bevor sie das Thema wechselte. "Linoschka?"

"Wir hatten Training.", begann sie, ohne Nachzufragen, was Nurek
meinte. Es war wohl klar. "Dieses Mal war Emotionstraining dran. Das
ist dazu da, mit emotional schwierigen Situationen umgehen zu lernen. Oder
eben zu lernen, dass wir sie vermeiden müssen. Hat irgendwer von uns
ein Problem mit dem Thema fesseln, wehrlos sein oder sowas?" Linoschka
unterbrach die Erzählung, bis sie das Kopfschütteln und Verneinen
aller explizit registriert hatte. "Wir haben also reihum jeweils einige
von uns gefesselt und allein gelassen. Mit einem Notfallorter in der
Hand, und mit Überwachung der Vitalparameter natürlich, also safe. Aber
es war trotzdem aufregend, zwei Stunden irgendwo zu liegen, sich zwar
ein wenig drehen zu können, aber nicht weg zu können. Wir haben
das auch nachgespielt, wenn jemand uns jeweils bewacht. Unsere Gruppe
aufgesplittet, sodass wir gegeneinander gespielt haben, damit
das Gefühl von Feindlichkeit mehr mitschwingt."

"Das klingt so krass!", meinte Tjaren.

Mø nickte zustimmend. "Also, dass das für mich nichts wäre, glaube ich,
brauche ich nicht dazusagen. Wie war es für dich?"

Linoschka antwortete nicht direkt. Interessanterweise lächelte sie
ein bisschen. Sie zog die Beine an und umfasste den Becher mit den
Händen. "Ich darf Namen nennen, hat sie gesagt, also Spielnamen. Und
ein bisschen erzählen.", leitete sie ein. "Ærenik hat mich bewacht. Sie
hat die Rolle der fiesen anderen Spielgruppe sehr gut ausgespielt. Ich
glaube, sie mag Schauspiel sehr gern, aber lebt dann auch die
Rolle. Sie hat sehr fiese Sachen gesagt. Ich gehe hier nicht ins
Detail. Ich glaube, sie würde im Spiel dann nicht so fiese Sachen
sagen, aber zwischen uns war das abgesprochen."

Marim grinste. Nicht des Inhalts wegen -- obwohl, vielleicht ein
wenig --, sondern vor allem, weil Linoschka mehr und mehr lächelte. "Kannst
du eine Richtung andeuten?", fragte er. "Irgendwas Abwertendes, oder
Bedrohungen?"

"Alles.", sagte Linoschka. "Ich..." Sie unterbrach sich selbst direkt, verhedderte
ihre Finger ineinander und kleckerte dabei mit der Tasse.

Mø reichte ihr ein Tuch.

Als Linoschka sich wieder sortiert und
tief durchgeatmet hatte, setzte sie neu zu sprechen an: "Ich
mochte die Bedrohungen. Was sie mir alles antun würde. Und auch
irgendwie gefesselt sein. Sie hat es gemerkt. Und mich gefragt,
ob ich am Abend eine Bondage-Session machen würde. Wir
teilen uns ja die Wohnung." Linoschka wirkte sehr aufgeregt und
grinste immer noch. "Ach ja, falls du dich sorgst, Nurek, es ist
nichts mit Körperflüssigkeitsaustausch passiert, darum ging
es nicht."

Nurek nickte. Marim konnte nicht so richtig ausmachen, ob
sie Linoschkas Erzählung auch so spannend fand wie er, aber
sie wirkte zumindest nicht verstört.

"Ich meinte, wir müssten direkt nach eurem Konzert fertig sein, womit
sie einverstanden war. Aber dann hat alles länger gedauert.", sagte
Linoschka. "Sie hat mich gefragt, ob ich abbrechen will. Und dann
habe ich mir gedacht, ich bin sehr weit weg und könnte wenig
tun von dort aus. Und ich habe einfach gehofft, ihr packt das schon. Es
tut mir so leid, dass ich nicht erreichbar war."

Nurek grinste nun. "Dir braucht nichts leidtun.", sagte sie. "Du
hättest sonst etwas Wichtiges verpasst, und Ivaness hat gut
helfen können."

"Eigentlich nicht.", sagte Ivaness. "Ich habe meine Erfahrungen
mit dir geteilt. Mehr konnte ich nicht tun."

"Das war sehr viel wert für mich.", sagte Marim. "Danke!"

"Du, Marim, sag mal: Fesselst du auch gern?", fragte
Linoschka. "Ich hatte den Eindruck, Ærenik ist vielleicht ein
bisschen wie du. Sie ist jedenfalls auch asexuell und hat
spontan irgendwelche Sessions mit Leuten, weil sich das gerade
ergibt."

"Ich habe noch nie gefesselt.", sagte Marim. "Also, ja,
schon."

"Bis du es kennst und langweilig findest?", fragte Nurek.

"Genau.", antwortete Marim grinsend. "Also, wahrscheinlich. Manchmal
mag ich Dinge danach auch noch, aber oft ist dann halt auch gut."

"Würdest du es auch unsexuell machen?", fragte Linoschka. "Also,
einfach aus ästhetischen Gründen?"

"Ja, auf jeden Fall!", antwortete Marim. Die Situation fühlte sich
ein wenig seltsam, aber auch aufregend an. "Wobei:", überlegte er.
"Ich wäre wahrscheinlich in einem entspannten, konzentrierten
Headspace, den ich sehr mag. Wäre das zu viel?"

"Nein, das klingt prima.", erwiderte Linoschka.

"Bedrohst du sie dann auch?", fragte Nurek.

Wirkte sie skeptisch? "Bis jetzt ist das nicht abgesprochen. Ich
bin nicht sicher, ob das mein Stil wäre, das müsste ich ausprobieren,
wenn du, Linoschka, das wölltest."

"Tatsächlich würde ich gern einfach Bondage ausprobieren. Ohne
andere Elemente und am liebsten in Kleidung.", sagte Linoschka.

"Habe ich je gesagt, dass ich Lust hätte, dich zu verschnüren?", fragte
Mø.

"Nee, hast du nicht!", rief Linoschka überrascht.

"Zu dritt?", fragte Mø. "Also, ich will mich nicht aufdrängen. Wenn
du dich mit Marim sicherer fühlst, go for it."

"Nurek, ich fühle mich gerade so, als sollte ich dich fragen, ob
das für dich okay ist. Immerhin wohnen wir alle zusammen und
so.", sprach Marim aus, was ihn untergründig eine Weile nervös
gemacht hatte. Es war albern. Nurek hatte öfter gesagt, dass
sie ihre Grenzen und Bedürfnisse selber kommunizieren würde.

"Macht was euch glücklich macht.", sagte sie. "Stöhnt nicht
zu laut, wenn ich im Haus bin."

Sie kicherten. Das war eine etwas andere Wendung des
Gesprächs, als womit Marim gerechnet hätte.

"Du, Marim, bist in Seilen vermutlich auch schön.", fügte Mø hinzu.

Marim versuchte, zu vermeiden, sehr breit zu grinsen und
nickte.

---

Sie gingen früh ins Bett, aber schliefen lange noch nicht. Es
war alles zu aufregend gewesen. Außerdem sah ihr derzeitiger
Tagesrhythmus schlafen noch nicht vor. Aber sie waren zu
erschöpft für alles andere, als in Nureks Schlafnische am gekippten
Fenster in weiche Decken und ineinander gekuschelt zu
liegen und leise zu reden.

"Macht es dir wirklich nichts aus?", fragte Marim.

"Nein, Marim, gar nichts.", sagte Nurek, klang dabei eine
Spur gereizt. "Aber ich habe eine andere wichtige Frage, die
ich fast übergriffig finde, zu stellen, aber es trotzdem
tun möchte: Macht es dir nichts aus? Fühlst du dich damit
wohl und sicher?"

Marims Gedanken stolperten und seine Hand, die damit beschäftigt
gewesen war, ihren Gesichtsrand zu streicheln, verharrte mit. "Klang
es für dich, als wäre ich bedrängt worden?", fragte er.

"Nicht direkt.", widersprach Nurek. "Aber es war ein
Anliegen der anderen, was für jene Begeisterung oder Freude auslöst, und
du kannst Leuten Begeisterung und Freude nicht gut ausschlagen,
glaube ich. Das würde dir schwerfallen."

Marim dachte darüber nach und kam zu dem Schluss, dass Nurek
grundsätzlich recht hatte. Damit hätte er Schwierigkeiten. Aber
war es in dieser Situation der Fall? Und falls ja, hätte
es schlimme Auswirkungen, oder würde er auch ohne die Schwierigkeiten ähnlich
entscheiden?

"Und es kam außerdem häufiger vor, dass du zum Reflektieren, warum
du dich in einem Moment schlecht gefühlt hast, einen Anstoß und
einen Tag Abstand gebraucht hast. Manchmal auch erst dann
überhaupt das schlecht fühlen realisiert hast.", sagte
Nurek. "Glaube ich. Vielleicht erinnere ich mich auch falsch. Jedenfalls
kann ich mir vorstellen, dass du mit der Fesselsache viel Spaß hast
und das super für dich ist. Aber mir war es doch ein Anliegen,
dich dazu anzustupsen, dass du in dich gehst und schaust, ob
du dabei über deine eigenen Grenzen gehen würdest."

"Danke, dass du das sagst.", sagte Marim. Er zog sie sehr
sanft noch etwas fester in die Umarmung. Ihr weicher Körper lag
dieses Mal mit der Rückseite an seinem. "Ich denke darüber nach. Ich
glaube, ich mag das gern mit Linoschka und Mø machen, aber
ich glaube auch, dass es Situationen gibt, wo du mich
mit diesem Anstoßen in eine wichtige Denkrichtung schiebst. Du darfst das, wenn
dir danach ist, gern wieder tun."

"Ich habe dich sehr lieb.", sagte Nurek sanft und strich
ihm über den Unterarm.

"Ich dich auch.", murmelte Marim in ihr Haar.
