Astgedanken
===========

\Beitext{Nurek}

"Jedes Mal.", fluchte Nurek. "Ich kann noch so sehr versuchen, mich auf ein Treffen
mit einer anderen Person vorzubereiten. Ich bin dabei einfach überfordert
und fühle mich hinterher jedes Mal scheiße!"

Linoschka nickte. Dazu musste Nurek genau hinsehen, weil Linoschka
gleichzeitig damit beschäftigt war, eine Reihe an Klimmzügen an
einer dafür montierten Stange auf der überdachten Terrasse zu machen. Es
war ihr erstaunlich wenig anzumerken, dass es sie anstrengte. Sie
wirkte auf beruhigende Art gelassen dabei. Diese Gelassenheit war, was
Nurek brauchte.

"Entweder, seine Virtualität passt automatisch Größen an, oder er ist ein
ziemlich kleiner Elb. Er ist ungefähr so groß, wie ich.", fügte
Nurek hinzu.

Linoschka ließ sich auf die nackten Füße fallen, landete dynamisch und ließ sich
in eine Liegestützposition herab. Nurek hatte Schwierigkeiten mit dem
Zählen, aber glaubte, dass so ungefähr 30 davon nun dran wären, wenn
Linoschka der täglichen Routine folgte. "Vielleicht ist er Waldelb. Waldelben
sind durchschnittlich so groß wie Lobbuds.", informierte
Linoschka, bevor sie anfing.

"Ich weiß. Ich bin ein Lobbud.", sagte Nurek.

"'Tschuldige. Das war wohl unsensibel.", sagte Linoschka. "Ich habe
erst kürzlich mehr über verschiedene Elbenvölker gelernt. Mir war
vor einer Woche noch nicht bewusst, dass und wie Leute so
daherkategorisieren. Da habe ich Eigenschaften noch
einfach hingenommen und nicht in Zusammenhang mit Durchschnittswerten oder Normen
gebracht." Sie senkte den Körper ab, um mit den Liegestützen zu beginnen. Hielt aber dann
noch einmal inne. "Ich bin nicht sicher, ob ich die Änderung in meinem
Kopf mag, oder ob weiterhin keine Zusammenhänge zu kennen nicht eigentlich vorteilhafter
gewesen wäre."

"Ich fand dich nicht unsensibel.", sagte Nurek. "Ich hatte eher Angst, unsensibel
zu Marim gewesen zu sein, weil ich einen Wortwitz mit Waldkontext gemacht
habe: Studiengestrüpp."

Während Linoschkas Routine würde sie nicht sprechen, das wusste Nurek. Sie
selbst hätte es nie geschafft, den Körper zu bewegen, einfach nur um ihn
zu bewegen. Es brauchte für sie immer einen Zweck, etwa Landschaft
ansehen oder Quallen.

"Wahrscheinlich mache ich mir zu viele Gedanken.", sagte Nurek. "Wahrscheinlich
wüsste ich einfach über die Dinge, die absolut sicher unsensibel wären, nicht, dass
sie es sind, weil ich nun mal keinen Subtext verstehe, und mache mir
deshalb zu jedem einzelnen Satz Gedanken."

Linoschka nickte und lächelte sogar ein bisschen.

"Du meinst, das ist eine perfekte Selbsteinschätzung?", fragte Nurek.

Linoschka nickte noch einmal. Dann schwiegen sie. Mitten in dieses
Schweigen hinein trat Ivaness, Nureks Geschwister, leise in den Türrahmen zur
Terrasse und lehnte sich mit verschränkten Armen daran an.

"Mein allerliebstes Schwesterherz!", begrüßte as Nurek.

"Mein bestestes Geschwisterherz!", grüßte Nurek leidenschaftlich zurück.

"Und gemochtestes Linoschkaherz!" Ivaness runzelte die Stirn. "Der
Superlativ von gemocht verwirrt überraschend.", murmelte as
und fragte dann wieder in normaler Lautstärke: "Wenn du fertig bist, hast
du Lust Terrorium zu zocken?"

Linoschka nickte.

"Ich würde gern auf andere Gedanken kommen. Darf ich mitmachen?", fragte
Nurek.

"Hui.", machte Ivaness, und sagte dann erst einmal ein paar Momente
nichts. As blickte Linoschka fragend an. Linoschka nickte wieder. "Wir sind ziemlich
am Ende der Welt.", gab Ivaness zu bedenken. "Weit weg vom Spawn. Da müsstest du erst einmal
hinkommen."

"Es ist okay, wenn du 'nein' sagst.", versicherte Nurek. "Wenn das
zu stressig ist, treffe ich mich online mit anderen. Oder befasse
mich mit regenerativen EM-Anzügen."

"Regenerativen EM-Anzügen?", fragte Ivaness, betonte alles davon
und wirkte belustigt. "Kurz REM-Anzüge, Anzüge, in denen eins
besonders entspannt schlafen kann?"

Nurek schnaubte. "Das wäre schön. Ich habe nicht so gut
geschlafen. Aber die Eigenschaft regenerativ bezieht sich
auf den Anzug und nicht auf die Person darin."

"Wir haben zu viele Äste.", sagte Ivaness.

Nurek nickte. Sie spürte es auch, dass die Verzweigung ihres
Gesprächs komplex wurde. Ein Ast war die unausgesprochene Frage, was
regenerative EM-Anzüge waren, und einer die Frage, ob
es doch okay wäre, wenn Nurek mitspielte, oder eben nicht. Außerdem
wollte Ivaness bestimmt wissen, wie es gelaufen war. As war gerade
erst aufgestanden und lehnte in einem flauschigen, pinken Bademantel
da.

"Es sind EM-Anzüge in der Entwicklung, noch nicht einmal im
Studien-Status, die gefüttert werden, und sich dann selbst
ausbessern.", erklärte Nurek. "Das heißt, wenn sie gerade
nicht getragen werden, dann werden sie sozusagen auf Nährboden
gelegt, ganz normalen, der auch für Minzpflanzen gut wäre," -- warum
musste sie schon wieder die Minz-Referenz bringen? -- "und daraus
ziehen sie dann Nährstoffe, um sich selbst zu reparieren, wie Haut
das zum Beispiel tut. Das hieße, dass sie nicht mehr zum Recycling
geschickt werden müssten. Und sie sind dann noch mehr Bio als vorher."

"EM-Anzüge, die Erde fressen?", fragte Ivaness leicht ironisch.

Ein Grinsen schoss über Linoschkas Gesicht, mitten in der
Bewegung.

Nurek betrachtete kurz den Schweißfilm, der sich
auf Linoschkas Haut bildete und richtete die
Aufmerksamkeit dann wieder auf ihr Geschwister. "Sozusagen. Und
Genaueres weiß ich auch noch nicht.", schloss
Nurek die Erklärungen ab. "Wie gesagt, ich will mehr dazu lesen." Sie grinste
ebenfalls. Sie wusste, dass ihr Geschwister die Witze machte, weil
Linoschka und sie, und auch der Rest der WG, sie alle witzig
fanden, und nicht, weil as nicht daran
glaubte, dass aus den Fortschrittsideen die Zukunft entstehen könnte. Es
ging iem nicht darum, sich über die Entwicklungen oder über Nurek
lustig zu machen.

"Du kannst in Terrorium mitspielen.", versicherte as, als
nächstes diesen Ast abarbeitend. "Es wird halt nur mindestens eine
halbe Stunde brauchen, bis du bei uns bist, und wir müssen dich
gut lotsen. Das macht mir Spaß, und Linoschka hat da nichts
gegen. Ich weiß nicht, ob es dich stresst, in Terrorium erst
einmal auf dich gestellt zu sein."

"Magst du ein bisschen erzählen, was auf mich zukommt?", fragte
Nurek. "Ich habe mal ein Let's Play und ein Tutorial dazu
angesehen, immerhin stand ich bei beidem direkt in der
Handlung und es war nicht von außen zugesehen."

---

Terrorium war ein graphisch wunderschönes Spiel, fand Nurek. Und ein
grauenvoll gefährliches. Nah am Spawn standen viele selbst
zusammengebaute Häuser, alle leer. Linoschka und Ivaness
leiteten Nurek aus der Ferne zu einem sehr versteckten, das sie
am Anfang zusammengebaut und darin viel Ausrüstung platziert hatten, mit
der sich Nurek nun ausstatten konnte. Es war kein
Spiel, das darauf Wert legte, möglichst realistisch physikalischen
Gesetzen zu folgen, wie, dass Dinge ein Gewicht hatten, und
trotzdem versuchte, ansatzweise den Anschein zu erwecken. Nureks Inventar war in
Taschen eines langen, dunkel-blaugrünen Mantels verstaut, der
sehr viele davon hatte. Taschen mit einer Art Verkleinerungszauber: Wenn
sie das Langschwert in eine davon steckte, dann wurde es dabei klein
und leicht wie ein Streichholz. Linoschka riet ihr allerdings, es
in die Scheide am Gürtel zu stecken, die Nurek anfangs gar nicht
bemerkt hatte, weil sie so klein war. Linoschka riet dazu, weil
das Schwert sonst womöglich Löcher in die Manteltaschen stechen
könnte.

"Du hast quasi null Erfahrung in Schwertkampf, richtig?", fragte
Ivaness.

"Auf was für einer Skala?", fragte Nurek.

"Auf einer von -1 bis 9?", schlug Ivaness vor. "Dabei
deuten negative Zahlen auf Ungeschick hin."

"Ja, null kommt hin.", sagte Nurek. "Ich schwanke tagesformabhängig
zwischen -0.3 und 0.3 würde ich sagen. Mit Bögen kann ich besser
umgehen, aber eigentlich verstecke ich mich lieber."

"Verstecken geht auch, aber dann bekommst du keine
Monsterdrops. Das macht Vorankommen langsamer.", erklärte Linoschka.

"Außer, du gehst zufällig einen Weg, den vorher schonmal Leute
gegangen sind, die dir eine Spur wertvoller Gegenstände in
Hütten zurückgelassen haben.", meinte Ivaness. "Und mit Leute, meine
ich Linoschka und mich. Wobei ich
nicht mehr weiß, was in den ersten Hütten so liegt. Bogenmaterial
kommt definitiv erst später. Aber das kriegen wir alles hin. Beeil dich
nur mal, es wird Nacht."

"Ist es nachts nicht gefährlicher?", fragte Nurek irritiert.

"Im Single-Player, auf Servern mit Anti-Grinding-Regeln
oder weiter weg vom Spawn schon.", erklärte Ivaness. "Vielleicht
ist dir aufgefallen wie sehr unser Haus versteckt war, und wie
gezielt mit Pausen und so wir dich von dort, wo du gespawnt
bist, dahingelotst haben. Auf
diesem Server gehört am Anfang dazu, dass andere Spielende
dich ausrauben -- das nennt sich auch grinden --, und
das passiert öfter tags, weil es tagsüber
ja ungefährlicher ist. Vor allem ohne Ausstattung. Aber
du hast ja Ausstattung."

Nurek sagte nichts weiter dazu und ließ sich noch ein bisschen
in die Ausstattung einweisen. Das Schwert war verziert. Die
magischen Heiltränke wirkten, als wären sie Glitzer oder
Wolken in Flaschen abgefüllt. Das Fernglas hatte einen
Steampunk-Look. Unter ihrem Mantel trug Nurek ein sehr
fein gewebtes Kettenhemd aus schwarz-glitzerndem Material mit
eingearbeiteten silbrigen Spiralen. Binnen kurzer Zeit war
sie eine fabulös schöne Tötungsmaschine, wenn auch eine
schlecht ausgebildete, die bevorzugte, lieber nicht
zu töten.

"Später kommen Zauber in Flaschen dazu, die dich für je
ein paar Milli- bis hin zu Zentistunden unsichtbar
machen, oder die Monster verwirren, oder du kannst
damit fliegen.", zählte Ivaness auf.

"Ich mag die Netze.", sagte Linoschka.

"Nee, damit machst du mich immer noch nicht glücklich.", erwiderte
Ivaness, und erklärte Nurek: "Es gibt Klebemasse, die sich, wenn sie richtig geworfen
wird, zu sehr feinen, ekligen Spinnweben auffächert und verklebt. Es
löst sich nach einer Weile auf. Damit kannst du Monster auch
vorübergehend fixieren."

Die letzten Sonnenstrahlen vergingen, und Ivaness leitete
Nurek vorsichtig aus der Höhle, in der das Haus stand, hinaus,
den Hang hinab. Nurek übertrug dabei, was sie sah, an
die beiden anderen. Linoschka war aufmerksam. Ihr entging nicht, als
ein Elb weit entfernt hinter einem Baum hervorlugte, während er
Nurek unauffällig zu folgen versuchte. Ivaness riet dazu, wegzulaufen und
führte Nurek geschickt einen Pfad zwischen pechschwarzen Bäumen mit hellblau
leuchtender Maserung und blauen Blättern hindurch in ein Tal, in
das von anderen Seiten orange Lavabäche flossen. Es handelte sich um einen
Sumpf mit Kröten, die halb so groß waren wie Nurek. Der Weg war ein
Labyrinth, das Ivaness in- und auswendig kannte, wahrscheinlich weil
as es am Anfang sehr oft gegangen war. Der Elb tauchte erst
nach einer Weile wieder auf. Hier im Sumpftal war weite Sicht
möglich. Er versuchte Nurek wieder einzuholen, aber versank im
Moor, bevor er sich wesentlich genähert hatte, und despawnte.

"Nurek, das ist deine Wahl: Willst du zurückgehen und das gedroppte
Inventar einsammeln, oder lieber schneller bei uns sein?", fragte
Ivaness.

"Ich würde vermuten: Auf der einen Seite, je weiter ich bei euch
bin, desto unwahrscheinlicher sehe ich eine Spielperson ihr
Inventar verlieren." -- sie mied das Wort 'sterben' und wusste
nicht so genau, warum -- "Auf der anderen Seite wirkt die
Spielperson nicht, als hätte sie in diesem Spiel schon viel
gesehen, weshalb ich vermute, dass das Inventar nicht so
interessant ist?"

"Außer, die Person hat vorher andere Schätze geplündert.", überlegte
Ivaness.

"Wie lange brauche ich?", fragte Nurek.

"Wenn du zügig bist, eine Achtelstunde."

---

Nurek brauchte am Ende etwas länger als eine halbe Stunde bis zu
Linoschka und Ivaness. Sie kam durch viele kreative Wälder, durch
eine Gebirgslandschaft mit Steinriesen, die Geröll rollten, und
Schnergen -- Tieren ähnlich riesiger Schnecken mit Häusern
aus Stein, die es auch im Outernet gab. Sie durchquerte eine Steppe, die
voll mit Blumen und Schmetterlingen war, obwohl die virtuelle
Sonne darauf niederbrannte und blendete. Und überall wich sie
gezielt wunderschönen, riesigen Monstern aus, die es sich zum
Ziel gesetzt hatten, sie zu fressen.

Es war, wie sie schon gewusst hatte, ein optisch wunderschönes und
nervlich extrem stressiges Spiel. Als sie bei Linoschka und Ivaness angekommen
war, war sie erschöpft. Sie war tatsächlich auf andere Gedanken gekommen. Sie
hörte auch noch nicht direkt auf zu spielen, weil Ivaness und Linoschka
ja extra auf sie gewartet hatten. Aber länger als eine weitere
halbe Stunde hielt sie es nicht aus.

"Wie schlimm ist es für dich, wenn wir weiterspielen?", fragte
Ivaness, als sie sich verabschieden musste. "Du hast gemerkt, wir
kommen viel langsamer voran, als du am Anfang, weil wir alles kennenlernen
müssen. Wenn du wiedereinstiegst, dann ginge das Aufholen schneller, es
sei denn, du steigst erst in zwei Wochen wieder ein."

"Es ist überhaupt nicht schlimm.", versicherte Nurek. "Ich hoffe
nur, ich habe euch nicht genervt, weil ihr am Anfang warten musstet, und
nun kann ich schon nicht mehr."

"Nein, das war ja halt auch vorher klar.", widersprach Ivaness. "Es
ist immer noch nicht das erste Mal, dass du mit uns spielst. Ich
weiß doch vorher, dass du nicht lange durchhältst, und das
ist auch okay. Daher habe ich mir eher Gedanken gemacht, ob
es für dich mies ist, wenn die Hälfte deiner Spieldauer fürs
Hinterherkommen draufgeht. Die Frage nun aber ist, brauchst du mich jetzt?"

All die Worte traten in Nurek einen Haufen Gefühle los. Astgefühle. Astgedanken, weil
sie in viele Richtungen gleichzeitig verzweigten und Nurek keine
Reihenfolge wusste, in der sie sie hätte abarbeiten können oder wollen. Da
war die Wut darauf, nicht länger zu können. Sie mochte die Beschränkung
nicht. Da war die Angst, die sie gehabt hatte, dass sie Linoschka und
Ivaness Spaß rauben könnte, die völlig unbegründet gewesen war. Warum
hatte sie die überhaupt? Das ergab keinen Sinn. Und dann die
Frage, ob sie Ivaness gerade brauchte.

"Ich glaube schon.", murmelte Nurek zurückhaltend. "Nicht genau jetzt, aber
im Laufe des Tages. Mir geht es mies."

"Du kannst mich auch genau jetzt haben. Einfach falls du Angst hast, dass
du mir was wegnimmst. Terrorium läuft nicht weg.", versicherte Ivaness.

Linoschka nickte einfach.

"Ich würde gern duschen und ein bisschen allein draußen die Vögel
angucken. Vielleicht eine Runde schwimmen.", beschloss Nurek. "Aber
am frühen Abend wäre ich froh über Mental Support." Sie merkte, wie sie
fast weinte.

Ivaness merkte es auch und nahm sie in der Virtualität
fest in den Arm. "Ich bin dann da."

"Ich auch, wenn du magst.", sagte Linoschka.

"Mag ich. Auch der Rest der WG, wenn gewünscht."

---

Nurek setzte die frisch gewaschenen Füße auf den Stein der Schären. Es
war nicht unbedingt dreckiger Stein, aber am Ende wären ihre Füße
sicher wieder dunkelgrau von unten. Natur eben.

Nurek mochte es sehr, Lobbudfüße zu haben. Große Füße mit
dicker Haut darunter, die jegliches Wetter abkonnte, Schuhe
hinfällig machte und zugleich sensibel fühlte.

Die WG, -- eine kleine Hack-Kommune derzeit bestehend aus fünf Personen --, bewohnte
eines von acht Häusern in Röbersjard, eine Viertelstunde mit dem
Rad von der Kleinstadt Fjärsholm entfernt. In Fjärsholm gab es einen
Bahnanschluss, Fährverbindungen zur Insel Klit oder rüber nach Geesthaven, der
Sportstadt auf der anderen Seite der großen Bucht, einen kleinen Sporthafen,
Cafés und Restaurants, ein gewisses Angebot an Kursen oder kleineren
Konzerten. Nicht viel, aber absolut ausreichend.

Röbersjard war ruhig und lag mitten zwischen Bäumen und Schären nah am
Wasser. Nurek mochte die Stille. Wobei es vielleicht seltsam war,
den Geräuschpegel, den die Möwen, Amseln, Krähen und Insekten von sich gaben, als
Stille zu bezeichnen. Nurek kletterte über den weichgespülten Stein
über ein paar Hügel im Wasser zu einer guten Badestelle. Etwas weiter
draußen von hier sichtbar stand der alte, schwarz-weiß geringelte
Leuchtturm im Wasser, dessen Ballustrade voll von Körmöränen war. Am
Fuß des Leuchtturms lag ein Motter und schlief. Die Wolkenbänder
warfen wandernden Schatten über die Schären. Nurek wartete ab, bis die
nächste scharfe Schattenlinie an ihr vorübergezogen war und sie
für eine Weile Sonne haben würde, bis sie sich langsam ins
kalte Wasser hinabließ. 

Sie schmeckte Salz auf den Lippen, und liebte es. Sie zog die Schnorchelmaske
an, die sie am Ufer bereitgelegt hatte und schwamm knapp unter der Wasseroberfläche
nah am Stein entlang. In den Ritzen hausten schwarze Seeigel. Ansonsten waren nur
Seesterne langsam genug, dass Nurek sie selbst mit dieser für sie angepassten Schnorchelmaske
klar erkennen konnte. Alles andere war eigentlich zu schnell. Allerdings
war Nurek gewohnt, etwas eingeschränkt zu sehen und genoss das, was sie sah,
trotzdem. Manchmal fragte sie sich, ob es wie ein Widerspruch wirkte, dass
zu ihren größten Interessen gehörte, Natur und Lebewesen zu beobachten. Aber
Interessen mussten eben nichts sein, worin Leute gut waren, sondern was
sie begeisterte.

Heute allerdings sah sie lediglich ein paar Seenadeln, einen Schleimfisch, der
an einem Fels nuckelte, und einen kleinen Tausendflössling, - eine Art
Wurm mit sehr vielen Flossen an beiden Seiten entlang des Körpers, mithilfe
derer er in jeglichen Krümmungen stationär schwimmen konnte. Es war ein
wunderschönes Wesen.

---

Als Nurek zum Haus zurückkehrte, hatten die anderen Mitbewohnenden den
Tisch im Garten gedeckt. Linoschka stellte gerade einen großen Kardamalkuchen
in die Mitte, als Nurek eintraf.

"Du hast Kuchen gedruckt!", rief Nurek ihr zu.

"Ja!", rief Linoschka über den Tisch hinweg zurück.

"Was machen wir dagegen?", fragte Nurek.

"Essen?", fragte Linoschka.

"Meinst du wirklich, dass das eine sinnvolle Methode ist, mit
der Situation umzugehen?", fragte Nurek.

"Ja!", antwortete Linoschka überzeugt.

Sie hatten häufiger solche nicht viel sagenden Konversationen. Irgendwie
gehörte es zu Nureks Mustern. Muster beruhigten. Sie war immer noch
nervös.

Mø und Tjaren hatten statt Stühle eine Liege an die eine Seite des Tischs
positioniert. Tjaren hatte sich auf Møs dicken Bauch gekuschelt, den
sie vor allem dazu sehr gern hatte. Sie streichelte Tjarens
faltiges Gesicht und Tjaren entspannte. Tjaren hatte oft Schmerzen, was
es zu einem noch entspannenderen Anblick machte, dass das wohl
gerade nicht der Fall war.

Nurek half dabei, Becher herauszutragen, pflückte weitere Zweige
der Minze für den Minzaufguss und setzte sich zuletzt dazu.

"Diese Forschperson will dich nie wiedersehen!", riet Ivaness
scherzhaft dramatisch.

"Marim hat sich heute morgen gemeldet, dass er Kopfschmerzen hätte, es
wirklich nicht an mir läge, aber er frühestens heute Abend wieder
in der Lage sein wird, mit mir zu reden. Und dass ihm das wirklich
leid tut.", berichtete Nurek und fragte sich, ob sie etwas
Wesentliches vergessen hatte.

"Hast du Angst, dass es irgendwie doch an dir liegen könnte?", fragte
Ivaness.

As war durchaus oft gut darin, zu raten, was für Sorgen Nurek sich
machte, aber dieses Mal lag as nur so halb richtig.

"Kurze Antwort: Nein.", sagte Nurek. "Lange Antwort: Ich kenne von
mir, dass ich in bestimmten Zeiten nur sehr bestimmte Leute
um mich haben mag."

"Wir haben dich auch sehr gern!", warf Mø warm lächelnd
ein.

"Das ist wunderschön! Aber es geht nicht nur um gern haben.", sagte
Nurek. "Es geht auch um Berechenbarkeit." Und das war exakt so
eine Situation: Wenn sie in fremden Runden gesagt hätte, dass sie
sich gestresst fühlte, weil sie die anderen in der Runde nicht
vorhersehen konnte, weil sie nicht berechenbar genug wären, dann
fühlten sie sich beleidigt und sagten sonst etwas. Ob das
nicht langweilig wäre, dass sie keine Versuchsobjekte wären, oder
sie fühlten sich irgendwie reduziert. In dieser Hack-Kommune
war das anders. Sie wussten, was damit gemeint war, und fanden
es verständlich und okay. "Jedenfalls, würden Marim und ich
uns besser kennen, und wäre ich zufällig eine Person, die so
zu ihm passt, wie ihr zu mir, dann wäre es vielleicht anders. In
dem Sinne kann es durchaus an mir liegen, oder Voraussetzungen
im Zusammenhang mit mir. Aber es ist okay.", führte sie
die Argumentation zu Ende. "Ich muss nicht aktuell perfekt für
Marim sein, und es ist auch okay, wenn ich es nie bin. Ich
will doch auch gar nicht mit allen Wesen der Welt hier zusammenwohnen. Also, rein
faktisch könnte es sein, dass es an mir liegt. Deshalb fühle
ich mich aber nicht schlecht oder falsch, und glaube nicht, dass
ich unbedingt was falsch gemacht habe."

"Was ist dann das Problem?", fragte Ivaness. "Was stresst dich?"

"Zum einen, das Übliche.", antwortete Nurek. "Hätte ich diesen
oder jenen Witz nicht machen sollen? War ich zu aufgeregt? War
ich zu albern?" Nurek überlegte, dass dies doch Gedanken waren, ob sie
etwas falsch gemacht haben könnte, und konnte deshalb nicht zum
'zum anderen' kommen, das ohnehin viel komplizierter war. Ein
komplexerer Ast im Astdenken. Und als sie nicht fortfuhr, tat
es Ivaness stattdessen.

"Du machst dir ja doch Gedanken, ob du was falsch gemacht
haben könntest.", sagte as. "Das ist internalisierter Ableismus. Du
hast -- wie immer -- alles durchdacht, was du in der Zeit durchdenken
konntest. Du hörst zu. Du argumentierst, dass andere nicht auf
dich zukommen würden, um dir zu sagen, was du falsch gemacht hast, und
du deshalb selber suchen müsstest, wenn du es rausfinden willst. Aber
das ist nicht deine Aufgabe. Warum schießt du die Leute nicht ab, die
diesen Job nicht übernehmen, klar mit dir zu kommunizieren? Sprich,
wenn da was ist, und Marim sagt das nicht, dann ist das sein
Problem. Wenn du sowas im Nachhinein rausfindest, streich den Kontakt. Du
hast genügend coole Kontakte."

Ivaness war schon immer recht radikal gewesen. Nurek mochte das so
an sich, aber gerade stresste es. "Marim ist auch neuroatpyisch."

"Ja und?", fragte Ivaness. "Es ist ein Spektrum. Auch unter
neuroatypischen Leuten gibt es welche, die nicht klar kommunizieren, oder
wo Kommunikation eben eine Hürde ist, bei der die Verantwortung, mit
ihr umzugehen, nicht nur deine ist. Wirklich."

"Ich weiß." Nurek seufzte. "Ich habe mich einfach schon seit
Monaten vorbereitet, dieses Treffen zu haben."

"Du hast schon ein bisschen gefanhypet.", meinte Linoschka
leise.

"Ja!", grummelte Nurek. "Ich hype diese Person, und dafür fühle ich mich
auch beschissen. Der Forschungsstil ist so cool wirr! Es ist alles so
Jetzt, so konsequent. Nach dem Motto: Oh, wir haben hier eine Information
gefunden, die nicht zu unseren bisherigen Methoden passt. Ich mache
ab jetzt sofort nicht so weiter wie bisher. Es ist so flexibel." Nurek
merkte, wie der Hype sie wieder erwischte, diese innere Begeisterung, dieses
Brennen. "Und dann das Forschungsziel selbst! Eskapismus hat schon immer
Leuten geholfen. Es wird mit der steten Verbesserung der Lage und
dem weiteren Dekonstruieren toxischer Kapitalismus-Strukturen, die
immer noch in den Köpfen verankert sind, zunehmend weniger
notwendig, dass Leute Eskapismus brauchen. Aber er geht halt daran
und sagt: Wer zum Slik sagt, dass wir gute Dinge nur als Ausgleich
haben dürfen? Wieso sollten wir diese Dinge, die uns durch schlechte
Zeiten gebracht haben, nicht erforschen, ob eine gewisse Dosis nicht
einfach per se völlig gesund und gut ist?"

"Hey, langsam!", unterbrach Mø ihren Redefluss. "Ich habe mich
auch etwas mit dieser Forschperson und den Thesen auseinandergesetzt. Diese
letzten Sätze sind von dir, nicht von ihm!"

"Stimmt.", sagte Nurek. "Ich denke eben auch schon lange darüber
nach und fange an, zu vermischen, was ich gelesen habe, und was
ich mir selbst dazu weitergedacht habe."

"Warum fühlst du dich beschissen für den Hype?", fragte Linoschka.

Nurek atmete bewusst aus. Die Begeisterung ließ etwas nach, und das
Schamgefühl kehrte zurück. "Weil ich noch nie einen Hype auf eine
bestimmte Person gehabt habe. Ich fühle mich dadurch aufdringlich.", erklärte
sie.

"Weil du das Bild von lauter Jugendlichen im Kopf hast, die ein
Gesangsidol feiern und alle dieses Gesangsidol daten wollen, aber
das Gesangsidol kann unmöglich alle Fans daten?", fragte Ivaness
ungewohnt ernst. "Und entsprechend ist die Hoffnung jedes einzelnen Fans, die
Person sein zu können, die das Idol am Ende tatsächlich datet,
unrealistisch, woraus sich so ein wirres Dilemma ergibt, dass
der Traum für die meisten zum Scheitern verurteilt ist? Sodass
sich die Personen abgrenzen wollen, aber nicht können und sich
hinterher dafür schämen, weil sie den Eindruck haben, dass sie
was dafür könnten. So etwa?"

Nurek nahm sich ein Stück Kuchen, und weil sie damit anfing, folgten
Linoschka und Ivaness ihrem Beispiel. Sie wusste, dass das
Wartenlassen auf eine Antwort dazu führte, dass Ivaness glaubte, dass
as nicht so sehr falsch lag. Was überraschenderweise auch ein
wenig stimmte. "So etwa.", gab Nurek also schließlich zu. "Außer, dass ich eigentlich
nicht glaube, dass so viele Leute Marim Präsenz hypen, wie
das bei Gesangsidolen der Fall wäre. Ich meine, es geht um sehr spezielle
Forschung und der Account ist nicht einmal sehr groß, obwohl
das Projekt selbst immerhin eine gewisse Bekanntheit bekommen
hat. Und außerdem hat mein Hype nicht so sehr
Verliebtheitsnatur. Zumindest nicht im romantischen Sinne. Es
ist eher diese krasse Begeisterung für das Thema. Aber schon
irgendwie auch für die Person dahinter."

"Ich verstehe, glaube ich.", sagte Ivaness. "Es ist was
völlig anderes, aber es hat genügend Parallelen zu der
Gesangsidol-Situation, dass sich dieses Schamgefühl
daraus überträgt."

"Ja, ich glaube schon.", sagte Nurek. Sie steckte sich ein Stück
Kuchen in den Mund und kaute ausgiebig darauf herum. Er schmeckte nach pfeffrigen
Gewürzen, war weich und nussig. Sie lächelte Linoschka für
dieses fantastische Werk an und Linoschka verstand. "Nun ja, und
es ist eben ein seltsames Gefühl, eine Person zu treffen, von der
ich mir über lauter Kurznachrichten, Replies, Threads, Blogartikeln
und sowas auf und über Shortspread ein klares Bild machen konnte, die
aber mich noch überhaupt nicht kennt. Das ist einfach surreal."

"Das verstehe ich, glaube ich.", sagte Tjaren. "So ging es
mir, als ich euch über das Internet kennengelernt habe, bevor
ich euch auf der *Drei Hackselnüsse* in Fjärsholm kennengelernt
habe. Das erste Mal mit euch war ich sehr nervös, aber das hat
sich schnell gelegt. Ich bin so froh, dass ich bei euch sein
darf."

"Das klingt so, als wärest du ein Anhängsel, aber du bist vollständig
Teil von uns.", versicherte Mø und strich dabei liebevoll durch
Tjarens kurzes, graues Haar.

"Das ist auch irgendwie surreal.", murmelte Tjaren. "Und schön!"

Tjaren war in jeweils verschiedenen Wortsinnen jüngstes und
zugleich ältestes Mitglied der WG. Tjaren war mit 69 etwa
doppelt so alt wie das nächst älteste Mitglied Mø, und Tjaren
wohnte erst seit zwei Jahren hier. Es fühlte sich länger
an, als wäre Tjaren schon immer da gewesen.

"Du bist geliebt.", erinnerte auch Nurek und der Rest
stimmte leise zu.

Tjaren sagte nichts dazu. Nicht mit Worten. Tjarens Arme
schlangen sich um Mø stellvertretend für den Rest der WG
und Mø erwiderte die Umarmung. Dieses Paar zu betrachten
machte Nurek immer wieder glücklich.

---

Als es dunkel wurde, räumten sie den Tisch wieder ab. Hunger
auf Abendessen hatten sie nach dem Kuchen noch nicht. Sie aßen
auch nicht immer zusammen, oft war es mehr gestaffelt.

Nurek versuchte sich zunächst mit dem Lesen über REM-Anzüge
zu beschäftigen, aber es wollte nicht so recht klappen. Es war ja nun
Abend. Sie erwartete, dass sich Marim wieder melden könnte. Und
allein, dass diese Möglichkeit bestand, führte dazu, dass sie sich
nicht konzentrieren konnte, wie das bei allen Terminen oder
Plänen der Fall war, die weniger als einen Tag in der
Zukunft lagen, und noch einmal mehr, wenn die Startzeit nicht
genau bestimmt war. Sie ging rüber in Linoschkas Zimmer. Die
Regel war, wenn die Tür offen stand, dann durfte Nurek jederzeit
hineinkommen. Sie legte sich quer über Linoschkas Bett auf den
Bauch.

"Darf ich auf deinem Bett liegen und leise wimmern?", fragte
sie.

"Ja.", sagte Linoschka mit dieser einladenden Gelassenheit.

Nurek stimmte leise damit ein, "oh je" wiederholt vor sich hinzumurmeln. Nach
einer Weile setzte sich Linoschka neben sie aufs Bett und
schaukelte Nurek im Lendenwirbelbereich. Dann klopfte sie ihr
über den Rücken. Beides war sehr gut und half, solange es
anhielt. Nurek kämpfte dagegen an, sich für so viel Zuwendung
schlecht zu fühlen. Aber Linoschka hatte schon sehr oft
erzählt, dass sie das gern machte und schon wieder zurück an
ihre Projekte gehen würde, wenn sie keine Lust mehr hatte. Dann
hörte Nurek endlich das unaufdringliche Ping über die
Hörimplantate. Sie freute sich, dass sie sich nun voll an diese
Hördevices gewöhnt hatte. Bis vor ein paar Monaten hatte
sie Hinterohrhörer benutzt, eine Erfindung, die sie vor
langem getestet hatte, als Methoden in der Herstellung gesucht worden
waren, sie massentauglicher herstellen zu können, und nicht
mehr nur für Personen, die besonderen Bedarf
dafür hatten. Jene übertrugen Ton über Knochenschall
und Überlagerung hinter dem Ohr, sodass die Ohren frei blieben. Implantate
gingen da noch ein Stück weiter: Sie waren überhaupt nicht spürbar und speisten
sich über Körper- und Bewegungsenergie, aber es war trotzdem
anfangs ungewohnt gewesen, kein Hördevice abzulegen. Das
Geräusch jedenfalls kündigte eine Nachricht von Marim an. Sie
wartete noch einen
Moment ab, bis Linoschka eine Pause machte, und stand auf.

---

Sie schrieb auf dem Weg in ihr Zimmer eine Nachricht und fragte, ob
er sie in einer ihrer Virtualitäten treffen wollen würde. Sie
hatte sich das länger überlegt. Vielleicht wäre es besser gewesen, wenn
sie ihm mehr Führung überlassen hätte. Oder sie reinen Textaustausch
angeboten hätte. Aber es fühlte sich einfach den ganzen Tag untergründig
doch richtig an, diese Frage zu stellen und die Antwort einfach abzuwarten.

*Marim Präsenz: Gerne! Ich bin ein bisschen matschig, ich hoffe
das stört nicht.*

*EnDe: Ich auch.*

Sie startete die Virtualität. Sie hatte sie selbst gebaut, einst, lange
bevor sie Marims Studie überhaupt kennen gelernt hatte. Sie war hier oft. Sie
musste ständig an sie denken, wenn sie über Marims Studie las.

Sie befand sich im Inneren eines verlassenen, hohen Steinbaus mit
Säulen und Spitzbögen. Die Atmosphäre war angenehm kühl und feucht und
die hallende Akustik beruhigte. Es fiel nur fahles Spätabendlicht
durch die hohen, schmalen Fenster, wahlweise auch Frühmorgenlicht, je
nach Fantasie. Der Boden stand bis zu ihren Knöcheln unter Wasser. Irgendwo
schwamm vielleicht der leise Fischschwarm kleiner Fische, den sie
kreiert hatte, aber sie hatte ihm Freiheit gegeben. Er konnte auch
aus dem Gebäude herausschwimmen, nicht weit allerdings, weil die
Virtualität dort aufhörte. Beziehungsweise, es war starker Nebel
außen, und wenn sie durch das Wasser waten würde, bis die Mauern
außer Sicht wären, würden sie auf der anderen Seite wieder
auftauchen. Es war eine periodische Virtualität, wie eine Kugel, auf
deren Oberfläche sie im Kreis gehen könnte, nur dass die Krümmung
nicht spürbar war, obwohl die Kugel sehr klein war. Technische
Details. Sie wollte ohnehin nicht aus dem Gebäude heraus.

Marim tauchte ein paar Meter vor ihr auf und blickte sich um. "Wow!", sagte
er. "Das passt sehr."

Nurek grinste. Sie wusste nicht genau, was er meinte, aber die
Reaktion erst einmal war positiv. Das freute sie. Sie wollte
nicht sofort fragen.

"Ist die Virtualität neu?", fragte er. Dadurch, dass er sprach, war
der Hall noch mehr wahrnehmbar. Seine Stimme klang schön darin.

"Nein.", entgegnete Nurek. "Ich komme hier seit bestimmt fünfzehn Jahren
immer wieder her, perfektioniere dann oft eine Kleinigkeit, aber im Wesentlichen
höre ich hier Musik und entspanne. Meditiere vielleicht, ich bin
nicht ganz sicher, ob der Begriff passt." Sie selbst sprach eher
selten hier. Sie sang öfter. Auch wenn nicht unbedingt gut, aber das
machte ja nichts.

"Sie ist schon sehr detailliert.", stellte Marim fest. "Ich hätte mir
vielleicht denken können, dass sie alt ist. Aber dann wieder wäre
dir gut zuzutrauen, dass du so etwas schnell hinbekommst."

Nurek lächelte geschmeichelt. Sie sah zu, wie Marim das Wasser
beobachtete, als er langsam zu einer Wand schritt. Sie beobachtete
seine Finger, wie sie über die Steine strichen. Über die Ritzen, über
die Wölbungen und an den Kanten entlang. "Wow.", wiederholte
er. "Die Feuchtigkeit auf den Steinen ist perfekt. Und der zarte Geruch!"

"Was meintest du mit 'Das passt sehr'?", fragte Nurek endlich. "Dass
es gut zu mir passt, oder dass es zu deiner Studie passt, oder noch
was anderes?"

"Dass es zur Studie passt.", antwortete Marim lächelnd. "Es
passt vielleicht auch zu dir, aber ich kenne dich noch nicht
gut genug. Ich meinte gerade vor allem, dass ich in die Virtualität
kam, und sofort voll präsent, voll im Hier war." Er ließ die Wand los
und kehrte zu ihr in die Mitte der Virtualität zurück, blieb vielleicht
zwei Meter vor ihr stehen. "Sie überfordert nicht mit Reizen. Aber
sie hat genügend davon, in die ich mich hineinfühlen kann. Ich
fliehe also nicht in einen Headspace, weil es nicht genug gäbe, woran
meine Aufmerksamkeit sich verhaken könnte, und auch nicht, weil es
so viel wäre, dass ich nicht filtern könnte. Es ist genau die
richtige Menge. Verstehst du, was ich meine?"

Nurek nickte. "Das Spannende ist, dass es nach fünfzehn Jahren
immer noch so ist.", fügte sie hinzu, korrigierte sich dann
aber noch einmal: "Wobei, vielleicht nicht ganz. Ich habe
eben über all die Jahre immer noch ein bisschen perfektioniert."

"Woran zuletzt?", fragte Marim.

"Am Wasser!", sagte Nurek lächelnd. Sie setzte sich in einen
Schneidersitz. "Setz dich auch, wenn du magst. Das Wasser ist
hier nicht so nass."

Marim grinste einen Moment sehr breit mit halb geschlossenen
Augen. "Das habe ich schon gemerkt.", sagte er und nahm ihr
gegenüber Platz. Ebenfalls ein Schneidersitz, mit etwas weniger
Abstand als sie gerade noch zueinander gestanden hatten.

Natürlich hatte er es schon bemerkt. Er trug keine hohen Schuhe
oder so etwas. Er hatte wieder bunte Ringelstrümpfe an, die sich
eben nicht voll Wasser gesogen hatten, wie es bei nasserem
Wasser der Fall gewesen wäre. Sie endeten in
der Mitte der Oberschenkel. Zwischen Strumpf und Hose war ein Abschnitt
graubrauner Haut mit dunkelbrauner dünner Behaarung zu sehen, sowie
Strumpfbänder, die unter der kurzen, grauen Hose verschwanden. Die
Hose war aus stabilem Stoff und am Hosenbein einmal
umgekrempelt, aus Modegründen, nicht zu irgendeinem anderen Zweck. Marim
trug eine ärmellose, hellblaue Bluse mit einem Muster aus
rosa und lila Quallen und tiefem Ausschnitt. Nun, als er saß,
strich er sein langes, dunkelblondes, weiches Haar über die linke
Schulter nach vorn. Die Spitzen schwammen im Wasser. Er saß
sehr gerade da und beobachtete es. Er war schön, fand sie. Sie
verknüpfte keine Sympathie mit optischer Schönheit. Es war einfach
gerade mehr oder weniger Zufall, dass sie eine Person schön fand, die
sie sympathisch fand. Sie fand auch sehr unsympathische Personen
schön und sehr sympathische Personen, nun, nicht hässlich. Sie
fand eigentlich niemanden hässlich. Neutral wohl dann.

"Es tut mir so leid, wie das gestern gelaufen ist.", sagte Marim
schließlich.

"Muss es nicht.", widersprach Nurek. "Overloads passieren. Es war
für mich merkwürdig, dass du dich erst über Bewunderung und Begeisterung
gefreut hast, und dann war es plötzlich zu viel. Aber so etwas
passiert halt."

"Darf ich eine persönliche Frage stellen?", fragte Marim.

Nurek nickte. "Wenn mir Fragen zu persönlich sind, sage ich das, wenn
du sie bereits gestellt hast. Oder ich antworte eben oberflächlich
darauf. Ich kann meine Grenzen selber stecken." Sie überlegte einen
Moment, ob das arrogant aufgefasst werden könnte. "Also, ohne, dass
ich Leute dafür angreifen oder shamen möchte, die das nicht können. Ich
kann es eben. Und ich mag, wenn mir das überlassen wird."

"Wir können das für mich auch so halten.", sagte Marim, statt direkt
die persönliche Frage zu stellen. "Ich kann es nicht immer. Aber
ich würde es trotzdem gern selbst lernen und übernehmen."

"Sind für dich Fragen in der Richtung okay? Sowas wie 'Es wirkt, als
wäre diese Grenze eine, die du gebrauchen könntest. Möchtest du?'", fragte
Nurek.

"Damit kann ich gut leben.", stimmte Marim zu.

Sie lächelten sich einen Moment an, aber Marim wich ihrem Blick aus und sah
stattdessen ins Wasser vor sich, in dem er mit der Hand sehr vorsichtig
Wellen schlug. Er nahm sie aus dem Wasser, und sie tropfte nicht, war
sofort trocken.

Nurek versuchte, ebenso gerade zu sitzen wie er. Sie
erinnerte sich daran, dass er auch in dieser Virtualität ungefähr so groß
war wie sie. Vielleicht sollte sie mutig fragen, ob er Waldelb war. Aber
vielleicht war er auch aus ganz anderen Gründen klein.

"Hast du dir den ganzen Tag Gedanken gemacht, ob du etwas falsch
gemacht haben könntest?", fragte er.

Das war wirklich eine persönliche Frage, fand sie. Was nicht hieß, dass
sie ihm böse wäre. Sie hatte es erlaubt. Sie mochte es sogar. Sie
fände angenehm, wenn ihr Austausch rasch dahin ginge, dass sie so
etwas klären könnten. Das war allerdings genau, was andere an ihr
oft gruselig fanden, und wovon sie sich hätte vorstellen können, dass das das
Hauptproblem gewesen wäre. "Schon irgendwie, ja.", sagte
sie. "Ich habe versucht, mich abzulenken. Und es liegt nicht an dir. Ich
habe einfach diese Ängste, dauernd. Neue Personen kennenlernen ist
einfach der Shit. Es ist so nervenaufreibend. Uffz, wie fühlt
sich das für dich an, was ich sage."

"Vertraut.", sagte Marim. "Klassische Anxiety neuroatypischer
Personen." Er machte eine kurze Pause, in der er anfing, leicht
mit dem Oberkörper zu schaukeln, ganz sachte. "Und deshalb tut mir
das leid, wie das gelaufen ist. Ich musste weg, da ließ sich nichts
machen. Ich dachte mir aber, wenn du auch nur ein bisschen
bist wie ich, dann hattest du einen sehr anstrengenden Tag. Wie
kommst du mit Verabredungen zurecht, die nicht genau planbar aber in
naher Zukunft liegen?"

Nurek schnaubte. "Nun ja. Ich habe mich kaum auf das fokussieren
können, worauf ich mich eigentlich habe fokussieren wollen.", gab
sie zu. "Aber auf der anderen Seite war das eingeplant. Ich habe
damit gerechnet, dass, wenn ich an der Studie teilnehme, die nächsten
Tage für mich erstmal gelaufen und nicht sinnvoll für irgendwas
projektartiges anderes verwendbar sind."

"Kann ich es irgendwie besser für dich machen?", fragte Marim.

"Uffz, ich weiß es nicht.", sagte Nurek. "Ich hatte nicht
eingeplant, dass du das rausfindest. Oder dass du dich auf was
anderes Großes einlassen würdest, als deine Studie." Sie
atmete einmal tief ein und aus und schüttelte ihre Hände
aus, weil es sich richtig anfühlte. "Ich hatte es nicht
eingeplant. Aber ja, ich hätte gern mehr mit dir zu tun, weil
du coole Sachen machst, und ich mehr davon mitbekommen
möchte. Und es fühlt sich seltsam an, weil ich daherlaufe und
dich gefühlt schon ein Stück kenne, also so oberflächlich, wie das
über Blog-Artikel und kurze Mitteilungen und so geht, und du mich aber
gar nicht."

"Das ließe sich wohl ändern, nicht?", fragte Marim.

Nurek beobachtete, wie sich sein Körper anspannte. "Hast du
auch Angst?", fragte sie.

"Ja, ziemlich.", bestätigte Marim. "Ich kann das, was du beschreibst, gut
nachempfinden. Neue Personen und so. Die Angst, unangenehm zu sein. Diese
Frage, warum mir deine Begeisterung im einen Augenblick gefiel, ich mich
so gefreut habe, wirklich! Und im nächsten Augenblick war sie mir
gruselig. Die Frage, ob dich das nicht vollkommen verunsichern muss, habe
ich mir auch den ganzen Tag gestellt. Und ich wollte dir irgendwie
Sicherheit geben, aber mit den Kopfschmerzen hätte es keinen
Sinn ergeben."

"Mist-Kopfschmerzen.", sagte Nurek. "Die können allgemein weg. Ist
das bei dir ein typisches Overload-Symptom?"

"Ja.", sagte Marim. "Ich hasse das. Ich habe dabei dann auch solche
Gedankenzustände, in denen ich Realität und Traum vermische, Ängste
ganz schlimm werden, und einfache Aufgaben, wie Lebensmitteldrucker
bedienen, nicht mehr gehen, wenn denn Aufstehen überhaupt eine Option
ist. Das ist alles gar nicht mal so gut."

Nurek schüttelte den Kopf. "Das tut mir leid.", sagte sie. Sie
fühlte mit den Händen ins kühle Wasser. "Ich habe dir übrigens
einfach geglaubt.", fügte sie hinzu. "Also, dass du Kopfschmerzen
hast, und es einfach nicht geht. Aber ich wusste nicht, dass
du alles erklären und einordnen würdest und bin gerade dankbar
darum."

"Noch eine persönliche Frage, und ich möchte dazu sagen, dass ich
mich bei einem 'nein', auch ohne Begründung, absolut nicht schlecht
oder vor den Kopf gestoßen oder so etwas fühle:", leitete Marim
ein. "Ich habe bei dem Versuch festgestellt, dass wir ständig
zu anderen Themen abgedriftet sind, weil sie spannend waren. Die
ganzen Äste fand ich interessant und spürte den Drang dazu, mich
mit dir viel mehr auszutauschen. Möchtest du das?"

"Ja." Nurek überlegte gar nicht lange. "Das wäre ein Traum!" Dann
überlegte sie doch noch einmal einen Augenblick, ob sie
irgendetwas hinzufügen sollte, und falls ja, was. Die
Welle an Angst, irgendwie zu stören, war kurz abgeebbt und rollte
nun wieder über sie hinweg. "Sollten wir irgendwelche
Grenzen abstecken?", fragte sie. "Kannst du sagen, wann dir Hype
oder Begeisterung zu viel ist?"

Marim betrachtete eine Weile nachdenklich die Wasseroberfläche, in
die er mit den Händen wieder Wellenmuster wedelte. Dann blickte er
einen Moment auf, nur kurz, und ließ sich nach hinten ins Wasser fallen. So, wie
es Leute eher nur in Virtualitäten taten, weil diese den Fall abdämpften, sodass
es nie weh tun würde. Es spritzte. "Dein Wasser ist unrealistisch. Das
wollte ich dir vorhin schon sagen. Aber dann kam mir in den Sinn, dass
du das sicher selber weißt."

Nurek lachte auf. "Selbstmurmelnd.", bestätigte sie. "Willst du
liegen bleiben? Wäre es für dich in dem Fall okay, wenn ich mich
neben dich legte, damit ich dich besser hören kann?"

"Ja, komm her, wenn du willst. Das ist okay.", stimmte er zu.

Nurek stand auf, suchte sich einen Platz neben Marim mit
einem sinnvollen Abstand aus und legte sich neben ihm
ins Wasser. Es war so angenehm kühl, schwappte leicht am
Körperrand. Es hatte keinen Einfluss auf das Hören. Vielleicht
war es das, was ihm aufgefallen war, was zu seiner Feststellung
geführt hatte. Wobei, das hätte er ihr vorhin noch nicht
sagen können. Aber vielleicht war es gerade hinzugekommen.

"Wie lange hast du an dem Wasser gearbeitet?", fragte Marim.

"Die Frage ist unmöglich zu beantworten!", rief Nurek. Der
Hall ließ sie fast böse klingen, fand sie. Sie fühlte sich
gar nicht böse, nur emotional. "Vor fünfzehn Jahren war es
klassisches Wasser, der Standard in Virtualitäten. Ich habe
mich zügig entschieden, es etwas dunkler zu färben. Und dann
kamen eben ständig Details dazu, aber ich habe zwischendurch auch
mal gar nicht daran gearbeitet, sondern an den Wänden, dem Hall,
dem Nebel draußen, dem Licht, dem Geruch, der Freiheit. Moment,
Freiheit ist indirekt, das ist keine Komponente, woran ich
schrauben könnte."

"Ich verstehe, was du meinst.", warf Marim ruhig in einer
Redepause ein.

Einer Redepause, die Nurek gemacht hatte, weil
sie den Faden verloren hatte. Mit der Unterbrechung
durch den Einwurf überlegte sie, dass sie ihn auch
gar nicht zu Ende führen musste und verzichtete
darauf, ihn wiederzusuchen. "Du hast die Frage
von vorhin noch nicht beantwortet.", erinnerte sie
stattdessen. "Was voll okay ist, wenn du nicht willst. Aber
wenn es an Vergesslichkeit oder sowas liegt, hier ist die
Erinnerung."

"Vergessen nicht. Und Ausweichen wollte ich eigentlich
auch nicht.", sagte Marim. "Die Frage ist schwierig. Ich
wollte mich deshalb zum Denken hinlegen, und dann war das
Wasser so interessant, da bin ich abgedriftet." Er hob
die Hand aus dem Wasser, die ihr zugewandter war. "Wieso
tropft sie nicht? Aber wieso spritzt es, wenn ich mich
ins Wasser fallen lasse?"

Nurek grinste. "Ich habe lange daran gearbeitet. Meine
ersten Ideen waren, dass Personen, die die Virtualität
betreten, einfach sehr wasserabweisend sind. Aber es
steckt inzwischen viel mehr If-Then-Else-Abfrage drin, die
mit Materialeigenschaften spielen. Wir können gern durch
den Code gehen, wenn du willst." Nach kurzem Zögern fügte
sie hinzu: "Aber er ist ziemlich unprofessionell. Eine
Bauklotzsprache. Ich habe das nie in einem Kurs oder
so gelernt. Tutorials haben mich gelangweilt. Ich habe
das Learning-by-Doing gemacht."

Marim grinste sie an. "Das ist so cool!", sagte er. "Ich
habe prinzipiell programmieren gelernt, mit Kursen und
allem. Aber ich bezweifle, dass ich deswegen viel mehr
kann als du."

Nurek grinste kurz zurück. Aber beurteilen konnten sie
das beide nicht. Also sagte sie nichts dazu.

"Thema Grenzen.", sagte er schließlich. "Ich glaube, ich muss
dir nachher schriftlich darauf antworten. Es ist komplizierter und
ich muss mir dazu Gedanken machen, aber hier und mit dir denke ich
dauernd an andere Dinge." Marim fuhr wieder mit den Händen
durch das Wasser. Einige Strähnen seiner Haare, die er nicht
plattlag, folgten der erzeugten Strömung. "Ich glaube allerdings, dass
ich dir gar keine Grenze setzen möchte, sondern eher erklären möchte, was
in mir dabei vorgeht. Vielleicht kannst du damit mehr anfangen."

"Gern.", sagte Nurek.

Marim schwieg eine Weile, in der Nurek ihn beobachtete. Marim
starrte währenddessen ins Gewölbe. Das machte Nurek auch gern. Aber
gerade beobachtete sie lieber, wie das schwach durch die Fenster beleuchtete
Wasser sachte zappelndes Licht auf sein Gesicht warf. Er
trug vielleicht braunen Lippenstift, vielleicht auch nicht,
Nurek war sich da nicht ganz sicher.

"Ich versuche gerade, meine widersprüchlichen Gedanken dazu
in Worte zu fassen und es klappt einfach nicht.", sagte Marim
schließlich. "Ich schreibe es dir, sobald ich mich verstanden
habe."

"Das ist in Ordnung.", sagte Nurek lächelnd.

"Ich finde so schön und überweltigend, dass du auch mehr mit
mir zu tun haben magst. Obwohl gestern!", betonte Marim und
klang so begeistert, wie sich Nurek fühlte, wenn sie über
seine Studie nachdachte.

"Ich bin allerdings seltsam.", warnte Nurek. "Das kann zu
Irritationen führen."

"Das ist okay, damit rechne ich. Ich versuche, mitzuhalten
oder adäquat zu konkurrieren.", erwiderte Marim lächelnd.
"Zum Beispiel schreibe ich Überweltigen gerne
mit 'e', weil es sich so anfühlt, wie, wenn die Welt über
einen hinwegrollt und es gleichzeitig gut, aber auch einfach
sehr viel davon ist. Zählt das?"

"Das ist wunderschön!", rief Nurek. "Auch diese Form 'obwohl
gestern'! Ich mag alternative Grammatik. Auch alternative
Plüräle zum Beispiel."

"Ohja! Oder alternative Könjünktive!", begeisterte sich Marim. Allerdings
fügte er nichts hinzu. Vielleicht fiel ihm keiner ein.
