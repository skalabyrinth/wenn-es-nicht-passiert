Einladen
========

\Beitext{Marim}

Marim las die kurze Liste, die sie erstellt hatten, um gemeinsam
auszuprobieren, ob Marim für Nurek eine gute Begleitung für ein
Live-Konzert war. Falls sie sich am Ende dafür entschieden, diente
sie auch gleich als Vorbereitung für den Ernstfall.

- Über Umgang mit Overloads und Melts reden.
- Über Umgang mit Needs reden.
- Trockenüben.
- Treffen im Outernet.
- Kleine Feier mit mehreren vertrauten Personen.
- Größere Party mit überwiegend nicht vertrauten Personen.
- Zelten.
- Absprachen mit Notfall-Ansprechpersonen.

Die ersten drei Punkte waren außerdem schon abgehakt. Das
hatte gar nicht mal so wenig Zeit gebraucht. Nicht zuletzt, weil
sie immer wieder inhaltlich abgedriftet waren. Unter anderem
hatten sie eine Virtualität ausprobiert, die komplett
aus Federn in nahezu Schwerelosigkeit bestand. Das war ziemlich
anders gewesen, als sie es sich vorher vorgestellt hatten, dabei
hatten sie sich im Vorfeld gar keine so genauen Vorstellungen gemacht.

Der erste Punkt war noch relativ schnell gegangen. Er wusste, was
Overloads und Meltdowns waren. Bei vielen neuroatpyischen Personen
führten für andere alltägliche Reize zu stärkeren Stressreaktionen, wie
in der Ecke sitzen und weinen -- die Reaktion
wurde häufig als *Overload* bezeichnet --, oder zu
Ausbrüchen, die wie Wut wirkten, aber nicht unbedingt
mit der Emotion verbunden waren, sondern eher
mit Überforderung und Überlastung. Die Reaktion
war meist nicht gegen Personen
gerichtet oder zweckgebunden, und Ruhe und Rückzug
war dann häufig das vorrangige Bedürfnis. Diese zweite Reaktion
wurde oft als *Meltdown* bezeichnet^[*Meltdowns* und *Overloads* sind
unter vor allem autistischen Menschen gängige Begriffe mit ungefähr
dieser Definition. Je nach Umfeld schwanken die Definitionen
etwas und nicht jede autistische Person kann den Worten ein
eigenes Erleben zuordnen. Es sind keine Fantasy-Begriffe]. Nurek
schleuderte zum Beispiel
Dinge, die sie gerade zur Hand hatte, durch die Gegend. Das
waren Nureks Reaktionen, hatte sie erzählt. Meltdowns hatte
sie vielleicht einmal in drei Jahren, wenn sie sich sehr
übernahm. Aber genau das hatte sie ja für das Live-Konzert
vor. Bewusst, gezielt, und trotzdem möglichst sicher.

Marim hatte vielleicht einmal einen Meltdown gehabt. Er
war sich nicht sicher. Overloads hatte er häufiger. Er
bekam dann Kopfschmerzen und musste sich zurückziehen. Aber
meistens setzten sie verzögert ein, und wenn er dringend
handlungsfähig bleiben musste, ging das.

Es hatte sich aus dem Gespräch mit Nurek
ergeben, dass er mit Situationen wie Konzerten erheblich
besser zurechtkam als sie. Er war dann erst danach richtig fertig
und brauchte davor viel Ruhe, aber die Konzert- oder
Event-Situation selbst stand er im Normalfall
durch. Er war auf einem Hackevent gewesen
und zusammen mit Anuka auf zwei Live-Konzerten und
einem Festival. Das hatte brauchbar funktioniert. Mit
viel Anleitung traute er sich zu, in so einer Situation
zusätzlich auch auf Nurek achtzugeben und sich zu kümmern, falls
sie nicht mehr konnte.

Generell sahen Meltdowns und Overloads relativ individuell
aus. Für manche gab es sogar einen fließenden Übergang
dazwischen. Für manche war nicht so klar, wo ein
Overload anfing. Letzteres war für Nurek
der Fall. Sie war in der Lage, recht präzise zu beschreiben,
was dann mit ihr passieren würde und was Marim tun müsste, wenn er
bestimmte Anzeichen bei ihr beobachtete.

Der zweite Punkt hatte lange gedauert und war vielleicht
eher ein Punkt, der trotzdem immer wieder aufkommen würde, auch
wenn sie nun schon ausführlich darüber gesprochen hatten.

Nurek hatte einen Haufen alltäglicher Dinge, die sie
tat, oder die wichtig für sie waren. Das fing bei einfachen
Dingen an, wie, was sie nicht essen konnte und was nur, wenn
sie nicht gestresst war, und ging über dazu, dass sie
unter Hochanspannung überhaupt regelmäßig daran erinnert
werden musste, zu essen und zu trinken. Nicht nur daran
erinnert werden. Denn die Schritte waren für sie eine
zu große Hürde, sich selbst darum zu kümmern, wenn
zu viel Party um sie herum war. Wenn sie einfach nur
gefragt würde, ob sie etwas essen wollte, würde sie
ablehnen, auch wenn sie etwas bräuchte, weil es ihr
unmöglich vorkäme, die Schritte dafür selbst vorzunehmen, oder
sich auch nur in ihrem overloadeten Kopf vorzustellen, was
dafür noch getan werden müsste. Die Option erschien
ihr dann nicht vorhanden zu sein. Also musste die
Frage gleich mit einem Angebot verbunden sein, ihr
etwas zu bringen.

Nureks WG kannte sie dahingehend gut genug, dass es bereits
Routine für die Gruppe war. Es gab keine Liste, auf der all
ihre Hürden standen. Da sie für sie so fest zum Alltag
gehörten, fielen sie Nurek nur nach und nach ein.

Es erinnerte Marim daran, wie es für ihn war, einen Fragebogen
für eine Diagnosestellung auszufüllen. Bei so einer Frage, ob seine
Füße manchmal einschliefen oder so etwas, kreuzte er oft 'nein'
an, aber nach einem Tag fiel ihm ein, dass ihm das oft
passierte, aber es so sehr zum Leben gehörte, dass er
es vergessen hatte. Solche Dinge eben. Glücklicherweise
konnte es in so einem Fragebogen auch Tage später noch nachgetragen
werden und die KI-Auswertung spuckte dann eine neue
Diagnose aus, konnte entsprechend besser helfen. Im
Fall von Nurek half so etwas nicht. Wenn sie erstmal auf
dem Konzert waren und es passierte etwas, dann passierte
das, und dann war die Veranstaltung potenziell gelaufen.

Für den dritten Punkt hatten sie mit Virtualitäten
Situationen durchgespielt, die eintreten könnten. Nurek hatte
sich natürlich nicht bewusst und absichtlich in einen Overload
begeben. Sie hatten die Situationen ohne Overload als Rollenspiel
nachgespielt, sich daran langsam herangetastet.

Nun stand der vierte Punkt, sich im Outernet
treffen, allmählich an. Und für
Punkt fünf hatte er sogar schon eine vage Idee. Eine kleine
Feier mit mehreren für Nurek vertrauten Personen. Ob
es zu viel wäre, beides zu verknüpfen? Er
öffnete *Diner*, die Chat-Anwendung, auf die sie
umgezogen waren, und tippte in ihren gemeinsamen
General-Chat:

*Marim: Was machst du Neujahr?*

Er schaute auf den anderen Bildschirm, um ein weiteres
Protokoll für seine Studie zu schreiben, aber dann sah
er bereits, dass sie tippte und konnte sich nicht
auf sein Protokoll konzentrieren.

*Nurek: Kleine WG-Feier.*

*Nurek: Oh!*

*Nurek: Du meinst, ich soll dich einladen für Punkt 6?*

*Marim: \*5. Und \*4.*

Sie vertat sich öfter bei Zahlen und fand es in Ordnung, dass
er sie korrigierte. Darüber hatten sie gesprochen.

*Marim: Außerdem: Genau!*

*Nurek: Ich frage die anderen.*

Dieses Mal brauchte es wirklich lange, bis sie wieder
antwortete. Er schaffte es in der Zeit, die innerliche
Aufregung in den Griff zu bekommen, sich darauf einzustellen, dass
sie auch 'nein' sagen könnte, und sich schließlich doch auf
sein Protokoll zu fokussieren. Er bekam nicht mit, als sie
wieder schrieb, weil er so vertieft darin war, aber als er
fertig war, war die neue Nachricht noch nicht sehr alt.

*Nurek: Du hast dich erfolgreich eingeladen .)*

Marim grinste. Oh, wie er sie für so etwas mochte! Den
Smiley mochte er auch, von dem er inzwischen wusste, dass
es ein einäugiges Grinsegesicht sein sollte.

Die Neujahrsfeier nutzte Marim traditionell, um neue Gruppen
kennenzulernen. Er feierte jedes Jahr bei einer anderen
Person aus seinem Umfeld, von daher passte das gut.

Das Gefühl von Freude kam etwas verspätet an, dann aber
um so heftiger. Die Hack-Kommune hatte sich immer
schön angehört, wenn Nurek darüber geredet hatte. Er
freute sich auch aufs Reisen und stellte bei dem Gedanken
fest, dass er gar nicht wusste, wohin.

*Marim: Wo wohnst du eigentlich?*

Nurek schickte ihm einen Link auf eine Karte. Er schrieb noch:

*Marim: Ach ja, ich freue mich so!*

Bevor er sich die Karte ansah. Norden. Nördlich noch von
Geesthaven, einer Stadt, die für Wassersport bekannt war. In
den Schären Nord-Maerdhas, die früher einmal sehr
schlecht zu erreichen gewesen waren, weshalb die Gegend immer
noch wenig besiedelt war.

*Marim: Ist das schon nördlich genug für Nordlicht?*

*Nurek: Gelegentlich .)*

*Marim: Wow!*

*Nurek: Ja, ist cool. Aber ich bin auch schonmal in einer Virtualität
mit Nordlicht aufgewacht und habe es für Outernet-Nordlicht gehalten. Es
gibt viel relevantere Naturphänomene hier, die Virtualitäten noch nicht
ganz präzise hinkriegen. Böigen Wind mit Nieselfeuchte zum Beispiel.*

*Marim: Romantisch <3*

*Nurek: \*nickt*

---

Damit sie Punkt 4 zeitlich vor Punkt 5 abarbeiten konnten, reiste Marim schon ein paar
Tage vor dem Vorneujahrsabend in Röbersjard an. Er nahm den Zug über
NOCity nach Geesthaven und von dort aus die Fähre -- einen Trikieler mit
monströser Segelfläche -- nach Fjärsholm, wo er von Nurek und Linoschka
abgeholt wurde. Er hätte auch bis Fjärsholm mit dem Zug durchfahren
können, aber er hatte Lust gehabt, den eisigen Wind durch seine Haare pusten
zu spüren und, wie Nurek beschrieben hatte, den feinen Nieselregen. Das
Wetter passte zufällig zu ihren Beschreibungen. Die See war nicht
wenig aufgewühlt, aber die drei Rümpfe des Trikielers schnitten geradewegs
hindurch. Dabei war es ein vergleichsweise kleiner Trikieler. Die
transkontinentalen Trikieler waren um einiges größer.

Auf halber Strecke fing es dann doch an zu schütten. Marim schnürte seine
Regenjacke dichter. Das Wasser perlte an seinen dicken, wasserabweisenden
Ringelleggins ab. Dass das Wetter nicht unbedingt für die Ringelstrümpfe
ausgelegt war -- oder umgekehrt -- hatte er vorher gewusst und den Umstand
eingeplant.

Als er in Fjärsholm von Bord ging, war er auf angenehme Weise andurchgefroren, also
nur das erste kleine Stückchen von durchgefroren. Er mochte das ganz leichte Brennen
auf den Armen und Beinen. Es war perfekt.

Er erkannte Nurek und Torf sofort. Nicht am Gesicht. Er war zwar für
eine neuroatypische Person etwas überdurchschnittlich gut darin, Gesichter
wiederzuerkennen, aber beide hatten die Gesichter ihrer Repräsentationen
in Virtualitäten so verändert, dass sie nicht daran wiedererkennbar
gewesen waren. Diese Vorkehrung war aus Anonymitätsgründen gar nicht
mal so unüblich. Er erkannte sie, weil Nurek der einzige Lobbud war, der
dort im Regen stand, und Torf in Figur, Größe und Kleidungsstil
passte. Sie trug eine lange Regenhose, aber darunter durchnässte, weiche
Socken in Sandalen. Außerdem hatten sie drei Fahrräder. Das dritte
hatten sie hier geliehen. Das hatten Nurek und er abgesprochen und
Nurek hatte ihm während seiner Überfahrt noch eine Nachricht geschrieben, dass
es geklappt hatte.

"Ich bin Linoschka!", rief Torf gegen den starken Wind an, als er vor
ihr stand. "Du kennst mich als Torf, aber im Outernet bin ich lieber
Linoschka. Geht das?"

Marim nickte. "Pronomen bleiben?", fragte er.

Linoschka nickte.

"Wobei ich bestimmt noch genug mit Namen überfordert sein werde.", sagte
er. "Aber ein neuer müsste gehen."

Nurek sagte gar nichts. Auch das war, nun, nicht abgesprochen, aber
sie hatte gesagt, dass sie häufig nicht in der Lage wäre zu reden, wenn
gerade zu viele neue Dinge auf einmal passierten, und das war bei
einem ersten Treffen mit einer Person im Outernet, mit neuen
Fahrrädern, vor einer Fähre, in einer nicht völlig vertrauten
Stadt und mit vielen Personen in der Umgebung der Fall. Trotzdem
sortierte sie ihm eines der Fahrräder wortlos zu und stellte es
für ihn in der Höhe richtig ein. Sie drückte ihm, um die
Hände dafür frei zu haben, außerdem seinen Fahrradhelm in
die Hand. Es war ein Helm mit einer Mechanik, bei der sich vieles
der Schale ineinander verschränken und dann fixieren ließ, sodass
er auf fast jede Kopfform anpassbar wäre. Er kannte diese
Technik, aber bisher nur in automatisiert. Hier musste er
den Helm selber zusammenschieben, aber die Hinweissymbole, wie
das ging, waren ausreichend hilfreich. Marim war trotzdem
nervös. Seine Finger zitterten etwas. Niemand sprach, bis sie
aufbrachen. Sie schoben die Fahrräder noch aus der
kleinen Personenmenge heraus, die den Trikieler verlassen
hatte, und fuhren erst dann. Der Radweg führte etwas hügelig
über die felsigen Schären, die weiter innen lagen. Dunkelgraue
Wollken zogen über den Himmel -- er schrieb Wolken mit zwei L, wenn
sie den Eindruck von Wolle machten -- und die kleineren
Schäreninselchen neben ihm im aufgewühlten Meer waren nass
von Regen. Er bekam mehr und mehr das Gefühl, freier atmen
zu können, aber auch von der Fahrt völlig erschöpft zu
sein.

Röbersjard bestand nur aus wenigen Häusern. Auf einem Grundstück
stand ein kleines Windrad, schon von Weitem sichtbar. Marim
tippte darauf, dass es zur Hack-Kommune gehören könnte, und behielt
recht. Der Garten war groß, hatte verwilderte Stellen, sortierte
Bepflanzung an anderen Orten und eine Terrasse. Die Tischplatte des
Tischs war gegen Wind und Regen abgekippt und festgebunden. Sie
stellten die Räder in einen Schuppen, der voll interessantem
Gerümpel war, aber für die Fahrräder doch ausreichend Platz
bot.

Ein hochgewachsener, dicker Elb mit langen, pinken Haaren
und einem freudig wirkenden, etwas schiefen Gesicht empfing
sie mit Handtüchern. Es roch nach Tee. "Ich bin Mø. Sie,
ihr, ihr, sie.", stellte sie sich vor. "Ich mache meistens
Einführungen und versuche dabei auch, deine aktuellen
Bedürfnisse möglich zu machen. Tee mit anderen? Tee alleine?
Nahrung? Ein Zimmer mit Rückzug? Eine Führung? Weniger
Vorschläge?" Sie hielt bei jedem Vorschlag einen weiteren
Finger in die Luft.

Marim musste beim letzten Vorschlag lachen. "Moment.", sagte
er. Er fühlte sich besonders mutig, als er Mø ein mittelgroßes
Handtuch abnahm. Ein kleines hätte gereicht, aber er überlegte, es
zum Duschen weiterzuverwenden. Er zog seine Jacke aus und schüttelte
sie vor der teilüberdachten Haustür, dem Beispiel der anderen beiden
folgend, kurz aus. Dann nahm Mø sie ihm mit fragendem Blick ab und
hängte sie an eine Garderobe.

"Es ist hier eindeutig zu voll.", war das erste, was er Nurek
sagen hörte. "Ich setze mich ins Gemeinschaftszimmer und
warte da." Und weg war sie.

Auch darüber, dass so etwas passieren konnte, hatten sie
gesprochen.

"Mach dir keine Sorgen. Sie braucht einfach etwas Rückzug.", erklärte
Mø.

Marim nickte. "Ich weiß.", sagte er. "Ich bin Marim. Er, sein, ihm
ihn. Ich würde eine kurze Führung nehmen, so etwas wie Hausregeln, und
dann hätte ich gern eine Viertelstunde Rückzug oder so."

"Das ist herrlich konkret. Damit kann ich dienen.", sagte Mø. Ihr
Blick wanderte einmal an ihm hinab. "Was für ein schönes Kleid!"

Marim sah ebenfalls an sich hinab. Dabei wusste er eigentlich, was für ein
Kleid er trug. Er machte sich für Ankunftstage stets sehr
genaue Gedanken, welche Kleidung er sich aussuchen würde. Er
hatte ein blaugrünes mit Farbverlauf gewählt, an dessen einer Schulter eine
Qualle schwamm, deren Tentakeln sich um seinen Körper schlangen. Er
lächelte. "Danke!", sagte er.

"Ich glaube, du passt hier gut her.", sagte Mø, bevor sie ohne
weitere Verzögerung mit der Führung anfing.

Die Führung hatte ein gutes System. Sie priorisierte danach, was Marim
dringend brauchen könnte. Mø zeigte zuerst die Badezimmer, den
Lebensmitteldrucker, zeigte, wo das Gemeinschaftszimmer war und dann
zeigte sie Marim den Besuchendenraum. Sie fragte, ob er den Rest
sehen wollte, aber Marim beschloss, erst einmal ein paar Momente allein
zu verbringen.

Der Raum hatte ein Fenster mit Wald- und Felsblick, an das der
Regen prasselte. Er war insgesamt gar nicht mal so klein. Es
gab Bett, Schreibtisch, Schrank, ein Regal und Platz auf dem
Boden, um gegebenenfalls Sachen zu sortieren. Außerdem war
es ein Hybridraum, der auch als Spielzimmer nutzbar war. Mit
elektromagnetischen Feldern konnten
hier sowohl Virtualitäten mit den entsprechenden Anzügen fühlbar
gemacht, als auch durch entsprechendes
Tuch, das auf die EM-Felder reagierte, Mobiliar und
ähnliches in das Zimmer integriert werden.

Als erstes packte Marim Gumbol aus dem Rucksack, damit das
Krokodil Luft bekäme, und legte es ins Bett. Er packte nur
die wichtigsten Dinge aus, bevor er sich aufs Bett dazulegte
und versuchte, die Reiseanspannung loszuwerden.

Er hätte einfach gefühlt für den Rest des Tages und der Nacht
schlafen können, aber da war die Neugierde auf das Unbekannte, die
anderen Personen, und Nurek, die vielleicht im Gemeinschaftsraum
wartete, deshalb wurde daraus nichts. Das würde ein Tag
werden, an dem er mehr Energie ausgab, als er hatte, wofür er
die Rechnung hinterher irgendwann begleichen müsste. Mit
unter anderem Kopfscherzen wahrscheinlich.

Als er so ruhig war, wie es eben ging, zog er sich doch
die Leggins aus und stattdessen Ringelstrümpfe an, die
er wieder mit glitzernden Strumpfbändern befestigte, weil
er sich in der elastischeren, dünneren Kleidung viel
wohler fühlte. Das senkte das Stresslevel sehr. Es war nicht
mehr einfach 70% seiner Denkkapazität damit belegt, seine
Beinkleidung wahrzunehmen und bewusst auszublenden.

Dann ging er in den Gemeinschaftsraum. Das Gefühl des Dielenbodens durch
die dünneren Strümpfe fühlte sich in den ersten Momenten nach
der langen Reise so sehr erleichternd an, dass er fast
nur deshalb breit gegrinst hätte. Oder geweint. Sein Körper
konnte sich da nicht so gut entscheiden.

Im Gemeinschaftsraum saß derzeit nur Nurek. Sie drehte an
einem bunten Gegenstand, der ein Logik- und Geschicklichkeitsrätsel
darstellte. Sie war vertieft darin und erschreckte sich
sehr, als sich Marim vorsichtig in einiger Entfernung räusperte. Das
war ein Problem, von dem sie herausgefunden hatten, dass es
quasi nicht umgehbar war. Nurek erschreckte sich einfach
sehr stark, wenn irgendein unerwarteter Reiz ihren vollfixierten
Hyperfokus unterbrach. Sie grinsten beide. Und sagten beide
nichts.

Marim setzte sich in einen Sessel neben ihr, nahm die
Beine mit auf die unbeschreiblich weiche Sitzfläche und strich sich
den nassen Zopf hinter die Schulter. Erst dabei merkte er, dass sie
noch zu einem Zopf geflochten waren. Er trug sie lieber offen, also
holte er sie wieder nach vorn und löste das Zopfband.

"Das kannst du doch nicht machen.", meinte Nurek.

Marim runzelte die Stirn. "Haare entflechten?"

"Haarporn.", sagte Nurek. "Also, Porn ist vielleicht ein bisschen
viel gesagt. Haare ziehen einfach meinen Blick auf sich, jede
Bewegung von ihnen. Ich habe halt Angst, dass du dich gleich
in den ersten Momenten hier unwohl fühlst, weil ich dich
dabei anstarre. Und ich bin so gestresst, dass ich mein
Starren nicht kontrollieren kann."

Marim ließ die Hände sinken. "Du darfst starren.", erlaubte
er.

Sie blickten sich einige Momente gegenseitig an. Nurek
wirkte durchaus nervös, fand er. Er fühlte sich auch nervös, schon, aber
er hätte mit viel mehr Nervosität gerechnet.

"Klammer auf, wenn du die Haare in halb entflochtenem Zustand ohne
Haarband lässt, dann ist das so etwas Unfertiges, was meine
Aufmerksamkeit noch mehr anzieht.", sagte Nurek.

Marim nickte, schmunzelte etwas, runzelte dabei auch ein wenig
die Stirn. Allerdings machte er nicht weiter. Aus Gründen, die
er noch nicht verbalisiert bekam.

"Klammer zu. Aber nur zur Beruhigung.", fügte Nurek hinzu.

Nun grinsten sie beide. Es beruhigte tatsächlich. Nicht
wieder geschlossene Klammern verhinderten einfach effektiv, voll
aufzunehmen, was in ihnen stand.

"Ich will dich echt nicht unwohl fühlen lassen.", sagte Nurek. "Und
ich habe den Eindruck, genau das tue ich gerade. Und außerdem, dass
ich die Schuld dafür auf dich schiebe, obwohl es eigentlich
spielerisch sein sollte, aber das sollte ich trotzdem nicht tun."

"Nurek, mir geht es gut. Du machst nichts falsch.", sagte
Marim. Dann holte er tief Luft und fasste in Worte, was
er sich überlegt hatte. "Wenn du Haare so sehr magst, möchtest
du sie gern entflechten, oder ist das im Moment zu viel?"

Nurek schloss die Augen und wirkte einen Moment an einem Rand
von Unsicherheit, bei dem Marim anfing, sich zu sorgen, ob
nicht einmal das Angebot zu machen, eine okaye Idee gewesen
war.

"Du würdest mich an deine Haare lassen?", fragte sie.
"Gern?"

Marim nickte. "Ist ja auch nicht so, als hättest du beim
Kuscheln in den Virtualitäten meine Haare nie angefasst.", sagte
er. "Ich mag das Kribbeln auf der Kopfhaut sogar. Ich weiß, dass
du jegliche Grenzen akzeptierst."

Nurek blieb noch eine Weile regungslos sitzen, aber nun wieder
mit geöffneten Augen. Dann atmete sie tief ein und aus und
stand auf. Sie war nicht sehr groß, aber andere in der WG
schon, Mø vor allem. Deshalb wahrscheinlich waren die Sitzmöbel größtenteils
auch für größere Personen vorgesehen, beziehungsweise
kam es bei diesen Sitzmöbeln nicht so sehr darauf an, wie groß
die Leute waren.

Nurek trat hinter seinen Sessel und er sortierte für sie den Zopf
wieder nach hinten. "Wie zärtlich darf ich mit dir sein?", fragte
sie.

Marim fragte sich, wie sie es immer wieder schaffte, dass
ihm von ihren Fragen oder Verhaltensweisen so unkontrolliert
heiß wurde. "Gesetzt den Fall, andere WG-Mitglieder kämen herein, wie
gut kämen sie damit zurecht, wenn sie mich schmelzen sähen?", fragte
er.

Nurek kicherte. "Gut.", sagte sie. "Ich glaube, sie wären einfach
glücklich, dass ich eine Person zum Schmelzen gefunden habe."

"Beliebig.", beantwortete er die Frage von vorher. "Bis ich
*Stop* oder *rot* sage. Wie sonst auch."

Dann fühlte er Nureks zarte, große Hände an seinem Nacken, kurz
darauf in seinen Haaren. Es kribbelte. Ein Kribbeln, dass er
sehr gern hatte, und ihm war gerade völlig gleich, dass er es
vielleicht später mit Kopfschmerzen bezahlen werden müsste. Er
schloss die Augen und fühlte sich darin hinein, wie sie den
Zopf entflocht und hinterher mit den Fingern durch die Haare glitt, bis
sie sortiert über die Sesselkante hingen. Anschließend umarmte
Nurek ihn von hinten. "Ich bin schon ein bisschen hart verliebt, glaube
ich.", murmelte sie.

*Ich auch*, dachte er. Aber er sagte es nicht. Es kam ihm zu
platt vor. Warum auch immer. Er legte stattdessen einen Arm
auf ihren, mit seiner Hand ihren Arm nachfahrend, bis sie auf ihrer
Schulter lag. Und dann sagte er es doch: "Ich in dich auch."

Er überlegte, ob es ihm zu viel war, nun auch noch
zu kuscheln. Mehr, als sie schon taten. Aber als sie ihre Wange
an seine Hand legte und er ihr Haar an seinem spürte, war
ihm das plötzlich egal. Es war vertraut. In den Virtualitäten
wurde Nurek durchaus gut abgebildet. Er drehte sich weiter
zu ihr um, sodass er mit der Hand auf ihren Rücken reichte, und
kraulte ihren Nackenbereich, weil er wusste, dass sie
das mochte.

Irgendwann, vielleicht ein paar Augenblicke später, aber vielleicht
auch erst nach einiger Zeit -- er hatte das Zeitgefühl verloren, kniete
er auf dem Sessel, sie hatten die Sessellehne zwischen ihnen, und sich
darüber hinüber gereckt im Arm.

"Wir haben auch ein Sofa.", informierte die etwas eckige Stimme, die
er vorhin schon kennengelernt hatte. Møs Stimme.

Nurek erschreckte sich wieder, aber nicht so sehr, wie vorhin.

"Ich dachte, ich informiere euch darüber.", fügte Mø hinzu.

Marim und Nurek lösten sich und kicherten beide ein bisschen.

"Ich will gar nicht stören.", sagte Mø. "Ich dachte, ich schaue, ob
ihr Gesellschaft mögt, und wenn nicht, gehe ich gern wieder."

"Ich glaube, es ist ganz gut, dass du störst.", widersprach
Nurek. Dann blickte sie Marim mit leicht schockiertem Ausdruck
im Gesicht an. "Oh, das wirkt sehr missverständlich.", murmelte
sie.

"Ich empfinde ähnlich.", beruhigte er. "Ich weiß nicht, ob aus
denselben Gründen. Es war schön, aber ist mir eigentlich auch
zu viel."

Nurek nickte. "Ich fühle mich sehr am Rand von einem
Overload."

Nurek löste sich vom Sessel und setzte sich stattdessen in
die ihm zugewandte Ecke des Sofas, das daneben stand, ebenfalls
auf ihre Beine, und schaukelte leicht mit dem Oberkörper, mit
sehr geradem Rücken.

"Soll ich wirklich bleiben? Sicher?", fragte Mø.

Nurek nickte. Dann schaute sie Marim an und runzelte die
Stirn.

"Bleib ruhig.", sagte er. "Ich bin sehr erschöpft von
der Reise, aber noch viel zu aufgeregt für Ruhe. Und
etwas essen muss ich irgendwann auch noch."

Mø setzte sich auf einen Schaukelstuhl Nurek gegenüber. "Ich
bin die smalltalkigste Person in diesem Haushalt.", sagte
sie. "Das bedeutet, ich
habe das Bedürfnis, eine Menge oberflächlicher, persönlicher Fragen
zu stellen, bin es aber auch gewohnt, dass das stresst oder nicht
gut kommt, und habe Verständnis. Oder auch dafür, dass so ein
Gespräch nur von meiner Seite Smalltalk ist, und die andere
Seite eben auf ihre Art reagiert. Wie ist da dein Vorzug?"

"Du möchtest mir jetzt gern eine Reihe oberflächlicher, persönlicher
Fragen stellen?", brach Marim herunter, was er verstanden hatte.

"Genau. So welche wie: 'Wie geht es dir?', 'Wie war die
Fahrt?', 'Woher kommst du heute?'", präzisierte Mø.

"Stell sie, aber hetz mich dabei nicht.", forderte Marim auf. Er
grinste vorsichtshalber, damit sie nicht ausversehen einen
genervten Tonfall in seine Aufforderung las.

Mø nickte, und zwar nicht nur mit dem Kopf, sondern machte
dabei auch eine tanzende Bewegung mit dem Oberkörper, die sehr
fröhlich wirkte. "Alles gut?", fragte sie.

"Nie alles.", sagte Marim. Er kannte diese Frage. Er konnte
sie nie sinnvoll einordnen und hatte daher eine Antwort
zurecht gelegt. Er wunderte sich außerdem, wie wenige
Leute irgendeinen Plan hatten, warum er mit 'nie
alles' reagierte, wenn er es nicht erklärte. "Allein
schon dadurch, dass es widersprüchliche Bedürfnisse gibt, kann
nicht alles gut sein. Aber ich nehme an, du möchtest gar nicht
wissen, ob alles gut ist. Magst du die Frage so formulieren, dass
sie wörtlicher meint, was du wissen willst?"

Mø grinste und schüttelte langsam den Kopf. "Ich sage
doch, du passt hier rein.", meinte sie. "Ich versuch's: Wie
fühlst du dich gerade, und wie fühlt sich die Umgebung
gerade für dich an? Fehlt dir etwas?"

Marim stellte fest, dass er keine Lust auf eine ausführliche
Antwort hatte. Er hatte etwas Angst, dass eine knappe
Antwort unhöflich aufgefasst werden könnte, aber auf
der anderen Seite war diese WG wahrscheinlich einer
der besten Orte, um zu üben, er selbst zu sein. "Müde und
erschöpft.", sagte er. "Neu, aber durchaus gemütlich
bisher. Einladend und weich. Ich mag das Fremdheitsgefühl
tatsächlich." Er war doch etwas mehr ins Detail
gegangen. "Nahrung."

"Das kriegen wir hin.", meinte Mø. "Willst du allein essen, oder
mit uns zusammen."

"Durchaus gern letzteres.", sagte Marim.

"Ach ja.", sagte Mø. "Ich erteile dir hiermit Zugriff auf den
Lebensmitteldrucker im Haus. Falls ich es vorhin vergessen habe. Ich
kann mich gerade nicht daran erinnern."

Es reichte üblicherweise so eine ausgesprochene Erlaubnis, wenn
eine zuständige KI gerade dazu eingestellt war, zuzuhören, damit
Marim dem Lebensmitteldrucker sogar ferngesteuert und nicht nur
an der Maschine selbst Aufträge schicken konnte.

Er nickte. "Hattest du tatsächlich noch nicht. Danke!", sagte
er. "Ist es üblich, dass jede Person ihren eigenen Kram druckt?"

"Meist ja.", sagte Mø. "Wir haben hier einfach zu viele Leute
im Haushalt, deren Geschmäcker sich nicht so vertragen."

---

Marim erkundigte sich noch, wie gemeinsames
Essen hier gehandhabt wurde, und nach ein paar
anderen Routinen. Irgendwann -- es war schon längst
dunkel und der Regen floss weiterhin beruhigend schön
die Scheiben hinab -- saßen sie tatsächlich alle zusammen am Tisch und
aßen. Marim war so aufgeregt, dass er vergaß, zu pusten, bis
sein Essen abgekühlt genug wäre. Dann hatte er viel zu
heißes Essen im Mund und musste darum herum atmen, es
im Mundraum ja nicht zu viel Haut berühren lassen. Mø
stellte erst wieder Fragen an ihn, als sie fertig gegessen
hatten. Zunächst einiges über die Fahrt und
später über sein Leben, ob er einer einzelnen Arbeit
nachging, mehrerer oder keiner. Dabei erfuhr Marim, dass
es bei der WG gemischt war. Einige machten Technik-Support, wenn
es gerade passte, Tjaren verwaltete ein Forum und
Linoschka arbeitete gar nicht. Nurek befasste sich mit verschiedensten
Studien, hatte aber insgesamt alles mögliche schon
gemacht, über Bedienung an Eisständen, Leiten einer kleinen
lokalen Offline-Jugend-Hack-Gruppe, Entwicklung von
verschiedensten Virtualitäten, etwas Forschung -- das, wovon
sie schon einmal erzählt hatte --, bis hin zu Mitentwicklung
barrierearmer Bauwerke in Hinblick auf neurologische
Behinderungen. Sie hatte auch schon Rezepte für Lebensmitteldrucker
entwickelt, ein Drehbuch für ein chaotisches Theater geschrieben,
sich an einer Renaturierungsaktion beteiligt und sie
testete Hardware auf Bugs.

"Ich stelle die Frage sehr selten.", leitete Marim schließlich
ein. "Was kannst du eigentlich nicht?"

Nurek grinste. Vielleicht hatte sie die Frage wirklich schon viele
Male gehört. Jedenfalls konnte Marim sich das gut
vorstellen. "*Diese* Frage" -- sie machte eine
Kunstpause -- "kann ich nicht beantworten."

---

Die Zeit bis zum Neujahrsfest verging -- nicht unerwartet -- nicht
gerade langsam. Es gab noch keine Routinen und dauernd diese
stressige Frage, wie viel Zeit er mit den anderen verbringen
wollte. Ebenso nicht unerwartet hatte er am nächsten Tag erst einmal lange
Kopfschmerzen und blieb im Bett. Linoschka brachte ihm
zwischendurch eine Suppe dahin. Sie war sehr ängstlich gewesen, ob
er es überhaupt in Ordnung fand, wenn sie klopfte, um ihm
selbige anzubieten. Die Suppe tat gut.

Am Abend saß er wieder im Gemeinschaftsraum, geschafft
und ermattet, als habe er eine Woche Fieber gehabt. Das war immer
die Nachwirkung von so einem Overload mit Kopfschmerzen, die sich
anfühlten, wie Nägel, die in seine Augenbrauen und Nase und durch
seinen Augenhintergrund gehämmert worden wären. Wenn jedes
bisschen Licht, dass er wahrnahm, ihm so weh tat, dass die
Atmung blockierte.

Er beobachtete Nurek, Ivaness, Tjaren und Linoschka, wie sie
ein Kartenspiel spielten. Er selbst war zu schlapp dafür, aber
die anderen störte nicht, dass er nur zusah. Nicht
einmal, als er in Nureks Blatt linste und fragte, ob es ein
gutes Blatt wäre mit den drei Assen darin. Vielleicht kam
ihm dabei aber auch zu Gute, dass das Deck kein einziges
Ass enthielt.

Am folgenden Tag ging er mit Nurek spazieren. Es war alles noch
sehr nass, aber es regnete nicht mehr. Tatsächlich gab es für die
kommende Nacht Glatteiswarnung, und irgendwann in den kommenden
Tagen sollte es schneien.

Ein weiterer Tag verging, an dem er zwei weitere Versuchspersonen
hatte. Und einer, der tatsächlich einfach sehr ruhig und gemütlich
war. Sie saßen zusammen im Gemeinschaftszimmer jeweils an ihren
eigenen Faltrechnern. Es ergab sich das ein oder andere
kurze Gespräch, aber sie alle hatten gar nicht so viel zu
sagen, also war es auch viel ruhig. Mal zogen sich Linoschka und Nureks
Geschwister zurück, um ein Abenteuerspiel in einer Virtualität zu
zocken, mal waren Tjaren und Mø in einem ihrer Zimmer und
Nurek vermutete, dass sie dort Sex hatten.

Als Nurek und Marim allein waren, nutzten sie die Zeit, um
sich in seine Studie zu vertiefen. Es war immer wieder ein
Genuss mit Nurek darüber zu reden. Eskapismus im historischen
Kontext, wie die genaue Bedeutung davon war, ob sich
die Bedeutung gewandelt hätte, und vor allem darüber, warum
immer wieder über Notwendigkeiten von Eskapismus gesprochen
wurde, statt über positive Auswirkungen, auch ohne dass diese
notwendig wären.

---

Und dann kam der Vorneujahrsabend. Bis zum Mittag waren vier Personen
zur Feier gekommen, die der Rest sehr gut kannte, als hätten sie mal
zusammen gewohnt. Es stellte sich heraus, dass eine der Personen
tatsächlich mal in Marims aktuellem Zimmer gehaust hatte, aber
dann zu einer anderen der Personen gezogen war.

Sie saßen gemeinsam am Tisch und erzählten sich alte Anekdoten, teils
mit Insidern gespickt, sodass Marim Schwierigkeiten hatte, mitzukommen. Ihm
machte es nicht so viel aus, an sich. Er hatte ein wenig das
Gefühl, in einen Personenkreis hinzugekommen zu sein, der
zwar sehr lieb wirkte, aber in den er nicht so richtig gehörte.

"Kommen irgendwann noch weitere Personen?", fragte er nach mehreren
Stunden schließlich in einer Gesprächspause.

"Nope!", sagte Ivaness. "Falls das deine Frage war: Du hast
dich da in so eine schon lange bestehende, kleine Gruppe eingeladen."

Das Gefühl von eben verstärkte sich, überrollte ihn und war
zugleich altvertraut. Das Gefühl, nicht hier herzugehören, nun auch vermengt
mit dem Gefühl, nicht mitbekommen zu haben, dass
ihm das eigentlich längst mitgeteilt worden war. Subtil
vielleicht. Vielleicht meinte Ivaness genau das.

"Du bist willkommen!", versicherte Mø. "Natürlich bist du das."

"Oh, klang das ausladend?", fragte Ivaness.

Auf einmal sahen ihn alle an. Und Marim fehlten die Worte.

"Es tut mir leid.", sagte Ivaness. "Es gibt keine Regel, dass
sich die Gruppe nicht erweitern darf. Wenn mein Schwesterherz
sagt, du kannst dazu, dann kannst du dazu."

"Ich glaube, es ist einfach meine Angst, nie dazuzugehören.", murmelte
Marim. "Ich habe noch nie in einer Gruppe dazugehört. Vielleicht
Mal bei Einzelpersonen."

"Du gehörst dazu.", versicherte Mø noch einmal. "Ich habe das am
Anfang schon zweimal gesagt und ich sage es auch gern wieder: Du
passt einfach."

"Können wir etwas tun, damit das Gefühl weggeht?", fragte
eine der fremden Personen, ein Zwerg mit einem in dicke
Zöpfe geflochtenem Bart und Glatze.

"Woher kennt ihr euch?", fragte Marim, statt zu antworten.

"Die meisten von uns aus der Lerngruppe, wir waren
in derselben.", antwortete Mø. "Tjaren
kam später dazu, erst vor zwei Jahren. Über mich. Das lief
vielleicht ein bisschen ähnlich wie bei dir jetzt. Nurek war nur ein halbes Jahr
in unserer Lerngruppe, aber wir haben Kontakt gehalten. Tissan
war mal mit Ivaness zusammen und ist darüber reingekommen."

Marim folgte Ivaness' Blick, der sich auf einen anderen Lobbud
richtete. Sie lächelten sich gegenseitig einmal an. Marim schloss, dass
der andere Lobbud dann wohl Tissan sein musste. Er hatte sich
die Namen bei der Vorstellung so schnell nicht alle merken
können.

"Seid ihr ungefähr oder genau im gleichen Alter?", fragte Marim
und blickte Nurek und Ivaness abwechselnd an, aber sie schüttelten
den Kopf.

"Ich bin fünf Jahre jünger.", sagte Ivaness. "Aber Lern-Inhalte
passieren ja nicht zwangsläufig linear. Ich war mit lernen immer
viel schneller als andere, Mø langsamer, und Nurek etwas chaotisch. Wir
hatten gehofft, dass es ihr nicht langweilig bei uns in
der Gruppe wird, aber das ist doch passiert, also ist sie
irgendwann gegangen."

"Schule, beziehungsweise Lerngruppen waren nichts für mich.", sagte
Nurek.

Einige Momente sagte oder fragte niemand etwas und Nurek bekam
Aufmerksamkeit. Vielleicht fuhr sie deshalb fort: "Ich
habe viele Lerngruppen und einige Schulformen ausprobiert,
sogar zwei physische. Das letzte war das Ehrenberg-Internat. Aber
egal wie, Schule ist halt einfach nicht meins. Ich
habe den Quatsch abgebrochen."

Marim blickte sie nachdenklich an. Vielleicht hatte sie
üble Erfahrungen gemacht und er sollte nicht bohren. Auf der
anderen Seite hatte sie mal gesagt, dass sie ihre Grenzen
selber ziehen wollte. "Vom Ehrenberg-Internat habe ich an sich
Gutes gehört. Also, klar, okay, wenn
das nichts für dich ist, ist das in Ordnung. Aber wenn du erzählen magst: Woran
lag es beim Ehrenberg-Internat."

"Das Ehrenberg-Internat *ist* durchaus gut.", betonte
Nurek. "Ich konnte in eine Lerngruppe, die
abends und nachts unterrichtet wurde, was mehr so meinem Tagesrhythmus
entspricht. Weißt du ja schon. Sie haben sogar die Stundenlänge an unsere Bedürfnisse
angepasst. Aber wenn mein Gehirn sich entschloss, dass nun Mathematik
dran ist, aber im Plan stand Geschichte, dann ist das mit fünf anderen
Kindern, deren Gehirn auch sowas beschließt, aber eben nicht zufällig immer
gleich, nicht drin. Das ist einfach null realisierbar. Deshalb lerne ich
mit KI und autodidaktisch und habe etwas kurios geskillt."

Tjaren und ein paar der anderen kicherten. "Kurios geskillt trifft
es echt gut bei dir.", kommentierte Tjaren. "Unsagbar vielfältig und
insgesamt sehr viel."

"Es ist auch ein Haufen fragwürdiger Skills dabei.", wandte
Nurek ein. "Ich mein', ich kann in Desertclimber wirklich,
wirklich gut Wasserquellen finden. Aber stürze fast immer ab. Ich kann
bei diesem vintage Minenspiel, das auf jedem System anders
heißt, die leeren Minenfelder in
Rekordzeit finden, aber nur ohne Fähnchen zu setzen. Ich kann Elektronik in
*Ender-Village* viel besser bauen als in real, obwohl sie in dem
Spiel furchtbar unpraktisch ist, Unmengen Sonderregeln hat und
viel Platz braucht. Ich kann mit den Füßen voran schneller
schwimmen als die meisten -- die das für gewöhnlich nie
geübt haben --, würde damit vielleicht Wettschwimmen gewinnen. Nicht
auf Landesebene oder sowas. Ich kann beim Luft einsaugen pfeifen, auf Kommando
niesen, mit beiden großen Zehen in der Nase bohren, also, wenn meine Nase
größer oder die Zehen kleiner wären, und Frösche perfekt imitieren."

"Aber Elekotronik in *Ender-Village* macht auch einfach
Spaß!", kommentierte Tissan.

"Es macht dir ja auch generell Spaß, Tools zu benutzen, wofür
sie eigentlich nicht gedacht sind.", fasste Linoschka
zusammen.

"Marim, wie geht es dir jetzt?", unterbrach Mø. "Wir sind
vom Thema abgekommen. Vielleicht willst du auch gar nicht
im Mittelpunkt stehen, aber mir liegt schon daran, dass
wir dich nicht ausschließen."

Marim fühlte, wie sich eine Angst in ihm löste, nur
ein bisschen. "Mir hat es eigentlich nicht viel ausgemacht, vieles
nicht so sehr zu verstehen. Ich glaube, gegen das Gefühl, nicht
dazuzugehören, können wir akut nichts tun. Dazu ist das zu alt. Aber
ich hatte auch Angst, dass ich störe, und ich glaube, die lässt
gerade nach."

"Du störst nicht.", betonte Ivaness recht energisch. "Ufz,
es tut mir so leid."

"Du hast gar nichts getan.", versuchte Marim zu beruhigen. "Es
gab eine schlimme Auslegung, aber die hast du gar nicht gemeint. Das
ist jetzt klar."

Ivaness hatte schulterlange, gewellte Haare und sehr dunkle
Augen. As wirkte energiegeladen und hatte insgesamt vielleicht
ein eher loseres Mundwerk. As hatte im Gespräch zuvor einige
sarkastische Sprüche gebracht, bisher welche, die die anderen
aufzubauen schienen. Auch in den vergangenen Tagen gelegentlich. Er
konnte as nicht so gut einschätzen, aber sein Eindruck von iem
war bisher sympathisch gewesen.

"Du darfst jederzeit fragen, wenn du etwas nicht verstehst.", sagte
Mø. "Ich glaube, wir haben irgendwann mal unseren eigenen
Sprech entwickelt und es kann schwer sein, sich dareinzufinden, oder
für uns, es überhaupt zu bemerken, dass wir das schon wieder
tun. Aber dahinter stecken lustige Geschichten, die wir uns
auch gern immer wieder gegenseitig erzählen. Also fühl dich
eingeladen, nach diesen Geschichten zu fragen."

Marim nickte. Die Aufmerksamkeit blieb nicht lange bei ihm, sondern
schwappte tatsächlich über kurze Umwege wie Essensplanung, oder
wer zum Neujahr rausgehen würde, wieder zu Gesprächen, die auf
alten Gemeinsamkeiten und Erlebnissen aufbauten. Marim fragte nicht, sondern
lauschte einfach auf die Stimmen und Gefühle, und
fühlte sich nun trotzdem weniger verloren. Manchmal
erklärte Nurek nun eine alte Geschichte von sich aus. Er wusste, dass
ihr Kopf selbst in diesem bekannten Umfeld völlig überlastet
war, und es berührte ihn sehr, dass für ihn Kontext zu schaffen
darin eine solche Priorität hatte.

Gegen Mitternacht gingen die meisten raus. Ivaness, Tissan und
Tjaren war es draußen zu hektisch und das geplante Lichterspiel
zu grell, und sie zogen sich in die Küche zurück. Sie wollten
etwas aufräumen, etwas interessanten Knabberkram drucken und
sich im Wesentlichen ausruhen.

Das Lichterspiel erzeugten sie mit LED-Technik auf einer
Hauswand. Es war wunderschön, fand Marim. Einige aus der WG hatten
jeweils etwas Programm vorbereitet. Nureks Programm war
kurz, und wirkte so, als würde sich Nordlicht in Wasser
auf der Hauswand spiegeln. Marim hatte das Bedürfnis, sie
in den Arm zu nehmen, aber sie lehnte ab. Stattdessen ergab
sich unter einigen anderen ein Gruppenkuscheln. Es passierte, weil
eine Person "Amöbe?" fragte. Er wurde mit der selben Frage
in das Kuscheln eingeladen. Und als er sich dazu entschied
und sich Arme um ihn schlangen, war es ihm gleichzeitig
zu viel und genau das, was er brauchte. Er fühlte sich für
einen Moment nicht mehr störend.

---

Das Gefühl kam nicht allzu viel später wieder stark
zurück. Sie kehrten grüppchenweise wieder ins Haus, er
in der Gruppe, in der Nurek mitging. Er hatte die ganze
Zeit vermieden, aufs Klo zu gehen, einfach, weil der
Stress so groß gewesen war, dass er sich lieber gar nicht
vom Fleck gerührt hatte. Vielleicht hatte auch ein
bisschen hineingespielt, dass er sich in dem Sessel weit weg
von der Tür, in dem er gesessen hatte, wohlgefühlt
hatte. Er hatte Angst gehabt, dass er besetzt werden würde, ginge
er kurz weg. Ein Gedanke, der sich auch egoistisch angefühlt
hatte, und zugleich wäre es eben ein Stressproblem gewesen,
hätte er sich an einen neuen Ort mit mehr Bewegung im
Rücken gewöhnen müssen.

Jetzt jedenfalls nutzte er die Gelegenheit, das Bad
aufzusuchen. Er blieb dort etwas länger als nötig. Die
neuen Stimmen von vorher hallten in seinem Kopf nach und
redeten darin durcheinander unverständliches Wirrsal. Einige
sprachen gemischt Niederelbisch und Kadulan, und sein
Kopf versuchte die Muster abzuspeichern. Es war wie
ein Ohrwurm. Heute würde er wohl lange zum Einschlafen
brauchen.

Kurz machte er sich Sorgen, ob er wirklich so eine
geeignete Person wäre, um Nurek beim Wunschtraum erfüllen des
Live-Konzertes zu unterstützen. Aber sie hatten bereits
darüber gesprochen. Aus dem Grund war es ja auch nur
ein Plan zum Kennenlernen und Herausfinden, ob es funktionierte,
und nicht bereits der Beschluss, dass sie es tun
würden.

Und als er ins Wohnzimmer zurückkehrte, saßen oder standen
die anderen in kleineren Gruppen aus zwei bis vier Personen. Er
hätte sich zu Nureks Gruppe gesellt, aber sie hatte sich
alleine mit einer Kuscheldecke an eine Wand gekuschelt und
hatte die Augen geschlossen. Der Gedanke, sich dazuzulegen,
fühlte sich seltsam an. Er konnte nicht so genau erklären, warum.
Und bei der Vorstellung, sich zu irgendeiner anderen Gruppe zu setzen
oder zu stellen, fühlte er dieses Gefühl, dass er störte,
sehr intensiv. Er versuchte, sich zu überwinden, es
trotzdem zu tun. Es war ein Gefühl aus der Vergangenheit. Es
hatte nichts mit dieser Situation zu tun. Damit es wegginge, müssten
ihm vermutlich alle anderen drei- bis fünfmal in der
Stunde aktiv mitteilen, dass er dazugehörte. Oder er
musste eben selbst versuchen, gegen das Gefühl vorzugehen. Aber
dafür hatte er gerade keine Kraft.

Er setzte sich auf ein unbesetztes Sofa in eine Ecke
und wartete ab, lauschte auf die Stimmen, betrachtete
das flackernde Kerzenlicht einer großen Kerze auf
dem Tisch. Sie musste hier platziert worden sein, als
sie draußen gewesen waren. Ihm gegenüber saßen Linoschka,
Ivaness und Tissan. Sein Blick fiel auf Linoschkas
Kettenanhänger, der das Kerzenlicht reflektierte. Es
war das Zeichen der Noldafin. Er hatte den Anhänger
schon bei ihrem Kinoabend in der Virtualität bemerkt. Ihn
zu sehen, erfüllte ihn mit einer Art Freude, von der
er nicht sicher war, ob sie sinnvoll war, oder projiziert:
Er vermutete, dass Linoschka den Anhänger aus einer
bestimmten Verbundenheit und vielleicht mit einem
Pride-Gefühl trug. Pride übersetzte sich einfach nicht
so gut ins Kadulan. Stolz traf es einfach nicht. Es
ging dabei auch um so etwas wie trotzen, darum, sich
nicht unterkriegen zu lassen. Sollten sie sich besser
kennenlernen, würde er sie vielleicht danach
fragen, was es für sie bedeutete.

Linoschka hatte in dem Dreiergespräch den größten
Redeanteil. Das überraschte Marim. Linoschka war insgesamt
eigentlich sehr zurückhaltend gewesen. Aber vielleicht brauchte
es nur das richtige Thema. Marim wusste nicht welches, weil
andere Geräusche im Raum seine Fähigkeit, Wörter selbst in
dieser Distanz zu verstehen, zerstörte. Er konnte aber beobachten, dass
das Gespräch mehr eines zwischen Tissan und Linoschka wurde und
Ivaness zunehmend weniger interessiert wirkte.

As stand schließlich auf und kam zu ihm herum. "Möchtest
du lieber allein sein?", fragte as.

Marim schüttelte den Kopf.

Ivaness setzte sich zu ihm. "Zu laut?"

"Ein bisschen.", sagte Marim, korrigierte sich aber
dann: "Mindestens. Doch, schon."

"Hast du NC-Hörtechnik?", fragte Ivaness.

Marim runzelte die Stirn und nickte. "Aber dann höre ich potenziell
gar nichts mehr."

"Nein, das geht viel präziser.", informierte as. "Diese Technik
hat für Virtualitäten schon lange drauf, die Mikros der anderen
im selben Outernet-Spielraum beim Noice Canceln
mitzubenutzen. Damit funktioniert, dass die
Stimme einer anderen Person, mit der du im selben Spielraum bist, nur
dann herausgefiltert wird, wenn sie in der Virtualität spricht, sodass
du sie dann über deine jeweilige Hörtechnik hörst, falls ihr
in selbiger dicht beieinander seid, und sonst eben gar nicht. Wenn sie aber
irgendwodurch kennzeichnet, dass sie gerade direkt mit dir
sprechen möchte, wird ihre Stimme für dich nicht herausgefiltert."

Marim nickte. "Ich dachte, in solchen Fällen wäre vorübergehend
gar kein Noice Cancelling aktiv."

"Warst du noch nie in einer Spielhalle mit zehn Leuten oder so?", fragte
Ivaness.

Marim schüttelte den Kopf. Und nickte kurz darauf. "Ah.", sagte
er. "Dann müssen einzelne Stimmen gefiltert werden."

Ivaness nickte. "Wir halten das hier in der WG aus, weil wir
ein bisschen Software zusammenprogrammiert haben, die das in
der WG immitiert. Du kannst die gern mitnutzen. Dann kannst du
alle Stimmen einzeln lauter oder leiser regeln."

"Wie cool!" Marim fühlte eine innere Begeisterung für diese
Idee, die sich in einem sanften Lächeln zeigte. Er merkte, dass
sein Masking nachließ. Er hätte sich sonst deutlicher und
lauter visuell gefreut, damit Leute nicht interpretierten, dass es
ihn gar nicht interessierte. Aber er war zu müde, es klappte
nicht mehr.

Ivaness erklärte ihm, wie es funktionierte. Außerdem klärte as
ihn über Datenschutzproblematiken dabei auf. Wenn er anderen
ermöglichen wollte, dass sie ihn herausfiltern könnten, dann
musste er auch damit einverstanden sein, dass Aufnahmen von
ihm entsprechend übertragen wurden. Es war nicht leicht, sie
für etwas anderes zu verwenden als fürs Filtern, aber das
Potenzial, dass es gehackt werden konnte, erhöhte sich
dadurch. Allerdings ging es nur um Ton, der ohnehin in Hörweite
war, aber eben üblicherweise nicht zwangsläufig verstanden
wurde, sondern wahrscheinlich eher Hintergrundrauschen wäre.

Als sie gemeinsam alles eingerichtet hatten, ging es ihm
besser. Er hörte Ivaness, das ja immerhin auf dem gleichen
Sofa mit ihm saß, nun viel entspannter und besser. Seine
halb unterbewusste Auto-Ergänzung musste nicht mehr so
viel arbeiten. Es war, als würde ein großes Gewicht von
ihm abfallen.

"Ich wollte noch einmal sagen, dass es mir leid tut.", sagte
Ivaness schließlich. "Und sag nicht, es wäre nichts passiert. Es
ist etwas passiert. Alle von uns kennen diese Ängste mehr oder
weniger stark. Daran hätte ich denken können."

Es kam für Marim nach diesem eher technischen Gespräch so
aus dem nichts, dass er Tränen in seinen Augen spürte. Vielleicht
spielte mit hinein, dass auch die aus Zukunftsreserven angezapfte
Energie allmählich aufgebraucht war. Vielleicht auch, weil
es ihn manchmal besonders traf, Verständnis für seine
Ängste zu bekommen.

"Vielleicht mache ich den Emotionskram nun wirklich schlimm, aber
ich möchte es dir gern sagen: Ich mag dich.", fügte
Ivaness hinzu.

As hatte recht mit dieser Vorannahme. "Wenn ich weinen darf, bin
ich okay mit dem Emotionskram.", sagte Marim schnell. Dann
versagte ihm die Stimme, weil seine Atemwege
zuschwollen und seine Augen ein wenig ausliefen.

"Darfst du.", sagte Ivaness. "Brauchst du etwas? Eine
Umarmung? Ein Taschentuch?" Wie schon Mø am Anfang hielt as
dazu erst ein, dann zwei Finger hoch.

Er schüttelte den Kopf und holte sein eigenes Taschentuch
hervor.

---

Später in der Nacht, als die ersten schlafen gegangen
waren und Tjaren und Mø eng
zusammengekuschelt auf einem Sofa lagen, ergab sich die
nächste Situation, in der Marim in keiner Gesprächsgruppe war. Aber
es fühlte sich nicht mehr schlimm an. Irgendwas hatte sich
gelöst. Er überlegte, ins Bett zu gehen, und blickte noch
einmal in die Ecke, in der Nurek lag. Sie hatte die Augen
wieder geöffnet und beobachtete ihn. Er stand auf und hockte
sich zu ihr, ohne etwas zu sagen.

"Machen wir uns hier ein Nest?", fragte sie schläfrig.

Marim lachte kurz leise -- mehr durch die Nase -- und
lächelte. Er nickte und holte sich alles an Kissen und
Decken, was es in seinem Zimmer gab. Linoschka brachte ihnen noch
mehr Kissen, bevor sie sich dann selbst in ihr Zimmer zurückzog.

Eingekuschelt und Nurek umarmend endete der Tag für ihn, aber er
brauchte noch lange, bis er eingeschlafen war. Es war das
erste Mal, seit dem einen Mal als er frisch angekommen war, dass
sie kuschelten. Aber es fühlte sich so ruhig und vertraut
an, als wäre es schon alt.
