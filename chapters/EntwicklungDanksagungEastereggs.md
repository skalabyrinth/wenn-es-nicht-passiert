Entwicklung, Danksagung, Eastereggs und Referenzen {.unnumbered}
==================================================

Ich schreibe eigentlich an meiner Romanreihe *Myrie Zange*. Wenn euch dieser
Roman gefallen hat, könnte *Myrie Zange* vielleicht auch was für euch sein. Die
Romanreihe ist länger und komplexer, aber das Universum ist das gleiche, und
der Hauptcharakter ist auch autistisch (und asexuell übrigens, aber das
ist nicht ganz so zentral).

Jedenfalls bin ich derzeit psychisch erkrankt, habe wenig Fokus, wenig
Konzentration, weiß teils nicht, was ich in den letzten Sätzen geschrieben
habe. In dieser Verfassung kann ich meine Romanreihe nicht weiterschreiben. Deshalb
habe ich *Wenn es nicht passiert* dazwischen geschoben. Das Buch liegt, wie
immer, in aktueller Fassung auf meiner Homepage:

[Link zur Seite des Buchs auf meiner Homepage.](skalabyrinth.org/books/WennEsNichtPassiert.html)

<!--Dieses Mal auch in einer
einmalig überarbeiteten Fassung auf Belletristica:

[Link zur Belletristica-Fassung.](belletristica.com/de/books/36732-wenn-es-nicht-passiert)-->

Auch wenn ich Schwierigkeiten mit dem Fokus habe, ist vielleicht etwas zusammengekommen, was
sich zu veröffentlichen lohnt. Ich nehme sämtliche Kritik gerne entgegen.

---

Meine größte Schreibmotivation passiert, wenn meine Werke gelesen werden und
ich Feedback erhalte. Daher hat mir das Schreiben umso mehr Spaß gemacht, weil
Kián jede Szene fast direkt nach dem Veröffentlichen mitgelesen und kommentiert
hat.

Ich danke jenen sehr, die mich in Sachen Sensitivity Reading beraten, damit
mein Werk möglichst keine verletztenden Stereotypen oder unzutreffende und dadurch
unangenehme Beschreibungen reproduziert. Das sind Katherina Ushachov und
Britta Redweik. Von letzterer stammt auch die initiierende Idee, wie in der
Widmung angedeutet.

Es ist außerdem für mich immer wieder eine Freude, Bücher als pdf
mit der Software *SPBuchsatz*, die Karl-Heinz Zimmer entwickelt, zu
erstellen. Danke auch für die vielen beantworteten Fragen dazu.

---

Ich verbastele gern *Eastereggs* in Bücher. Dabei handelt es sich um zum Beispiel
Muster oder versteckte Informationen. Damit niemand ausgeschlossen wird, verrate
ich hier, was in diesem Buch für Eastereggs und Refernzen stecken:

Das Buch hat drei Teile und jeder Teil vier Kapitel. Die Titel der Teile bestehen jeweils
aus einem Substantiv im Nominativ und einem Substantiv im Genitiv. Die
Kapiteltitel folgen in jedem
der Teile jeweils einem Muster. Im ersten Teil sind sie alle Fantasiewörter, vielleicht
philosophische, die etwas mit Pflanzen
zu tun haben. Im zweiten Teil stecken die Zahlen eins bis vier in der Reihenfolge
indirekt in den Titeln. Die Titel im dritten Teil fangen alle mit F an und drei
davon sind interessant buchstabiert.

Im Buch kommt eine Band mit Namen *Die Fenster* vor, die Vintage-Dinge
machen und Musik mit Festplatten. *Die Fenster* sind eine
Anspielung auf das Betriebssystem Windows. Desweiteren kommt ein
Hackkongress mit Namen ForKaos vor. Als größter Hackkongress in der
Welt bildet er eine Parallele zum Chaos Communication Congress, dem
größten Hackkongress Deutschlands, wenn ihr eine Vorstellung haben
möchtet, was damit gemeint sein könnte.

Die erwähnte Stadt Rumpf ist an Kiel orientiert. In Kiel gibt es tatsächlich
ein "Studentendorf" (bisher, soweit ich weiß, aber keine Ansinnen, es umzubenennen) und sowohl
in der Mensa I als auch in der Mensa II haben Uni-Bälle mit Live-Musik
stattgefunden.

Das Buch nimmt durchaus Referenz auf die Romane *Myrie Zange*, ohne sehr
zu spoilern. Es setzt gegen Ende von Band 1 ein und hört gegen Mitte von
Band 3 auf. Hauptreferenz ist das Spiel, ein Wettkampf, der in der Welt
relativ zentral ist. Charaktere, die auch in *Myrie Zange*, oder auch
in den Kurzgeschichten *Tee im Schnee* oder *Jurin Raute - Windschwinge*
vorkommen, sind: Anonym41, Ærenik, Bjork, Gabriane, Linoschka und Rosa Pride-Away. Anonym41
ist ein recht zentraler Charakter in *Myrie Zange* und *Tee im Schnee* (mit
anderem Namen), der in diesem Roman nur am Rande vorkommt. Rosa Pride-Away ist
der Hauptcharakter in *Tee im Schnee*. Umgekehrt ist
Linoschka ein zentraler Charakter in diesem Buch, der in *Myrie Zange*
nur am Rande vorkommt.
