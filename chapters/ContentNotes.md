### Anmerkungen zu den Content Notes

Ich versuche hier eine möglichst vollständige Liste an Content Notes
zur Verfügung zu stellen, aber weiß, dass ich nicht immer alles
auf dem Schirm habe. Hinweise sind willkommen und werden ergänzt. Über
die Content Notes hinaus darf mir gern jede Frage nach Inhalten gestellt
werden und ich spoilere in privaten Konversationen nach bestem
Wissen. Es bedarf dafür keiner Begründung oder Diskussion. Ich mache das
einfach.

Ich nehme außerdem teils sehr seltene Content Notes für Personen mit auf, die
ich kenne, weil sie sich für meine Kunst interessieren.

### Für das ganze Buch, mit Hinweisen, für welche Kapitel

- Kleine Spoiler für die Myrie-Zange-Buchreihe.
- Essen ist ein häufiges Thema.
- Behinderungen im Zusammenhang mit Augen.
- Overloads, mehrere leichtere, und ein sehr starker.
- Übergriffige, gaslightende und toxische Beziehungen (Rückblenden).
- Schmerzen, Kopfschmerzen vor allem, mit drastischen Bildern umschrieben.
- Medikation und Einnahme davon gegen Kopfschmerzen.
- Erwähnung von Sex.
- Romantische Kuschelszenen.
- Direkt beschrieben sind weder Küsse noch Nacktheit.
- Wirbellose Tiere häufig erwähnt.
- Spinnweben [Kapitel 2 - Astgedanken].
- Körperverformungen werden beschrieben [Kapitel 4 - Sitznesseln].
- Reclaimter Slur: Schlampe [Kapitel 4 - Sitznesseln].
- Sexualisierte Gewalt, emotionale Erpressung, erwähnt (Erinnerung, Trauma) [Kapitel 8 - *Revier*].
- Inkontinenz [Kapitel 9 - *Fanfahren* und Kapitel 12 - *Futur II*].
- Erwähnung bespuckt werden [Kapitel 10 - *Funkenfrequenz*].
- Füße, Fußfetisch erwähnt, gespieltes Machtgefälle [Kapitel 10 - *Funkenfrequenz*].
- Erwähnung von politischer Verfolgung [Kapitel 10 - *Funkenfrequenz*].
- Starker Overload [Kapitel 10 - *Funkenfrequenz*].
- Erwähnung von Fesseln, BDSM [Kapitel 11 - *Fordergrund*].
- Überflutungen und Explosionen, aber ohne echte Bedrohung [Kapitel 12 - *Futur II*].
