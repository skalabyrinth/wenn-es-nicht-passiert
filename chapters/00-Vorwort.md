Vorwort {.unnumbered}
=======

Dieses Buch steht unter Creative Commons Lizenz:

[http://creativecommons.org/licenses/by-nc-nd/4.0/](http://creativecommons.org/licenses/by-nc-nd/4.0/)


![image of the by-nc-nd 4.0 licence](/home/kaulquake/Fischtopf/Geschichten/Myrie/license.png){width=0.8 height=0.4}

Das Buch entstand im Sommer 2021 in etwa einem Monat Schreibzeit, unter anderem
während einer psychosomatischen ReHa. Es wird, wie all meine Werke, im Laufe der Zeit bearbeitet.

Das Cover basiert auf einer Grundlage, die ich mit Aquarell-Stiften in einer Ergo-Therapie
während besagter ReHa gemalt habe. Ich habe es anschließend mit gimp überarbeitet.

Ich behandele in wörtlicher Rede Punkte gleichberechtigt mit anderen Satzzeichen. Ich setze
mich damit über gängige Grammatik-Regeln hinweg, einfach, weil es mir so besser
gefällt und konsistenter vorkommt.

![map of Maerdha in grayscale](/home/kaulquake/Fischtopf/Geschichten/WennEsNichtPassiert/MaerdhaBW.png)
