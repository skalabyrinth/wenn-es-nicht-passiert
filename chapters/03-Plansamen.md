Plansamen
=========

\Beitext{Marim}

"Magst du mich testweise 'Mein Mädchen' nennen?", fragte Anuka. Er saß auf
einem hohen Hocker im virtuellen Supermarkt -- einem Laden, in dem Druckresultate
von Rezepten für Lebensmitteldrucker ausgestellt waren, sodass sie vorm
Drucken angesehen und berochen werden konnten.

Für Marim war so ein Supermarkt reine Entspannung. Deshalb trafen sie
sich hier. Er sah sich die Speisen ausführlich an, las die Listen der
Inhaltsstoffe. Er pinnte den Zettel, den er gerade gelesen hatte, zurück
ans Regal, um seine Aufmerksamkeit auf Anuka zu richten. "Von hier aus, oder
mit viel Liebe in deiner Nähe?", fragte er.

"Uh, ja, komm auf mich zu!", sagte Anuka. "Make it sexy!"

Marim grinste breit. "Okay!", meinte er, wandte sich in Anukas Richtung
und ging möglichst elegant auf ihn zu. Er hatte die Art zu gehen oft vorm Spiegel
geübt, bis sie ihm gefiel. Direkt neben Anuka lehnte er
sich lässig an die Wand. "So etwa, mein Mädchen?", fragte er.

Anuka lehnte sich seinerseits im Sitzen gegen die Wand und
legte den Kopf in den Nacken. "Mist.", sagte
er sachlich. "Es funktioniert einfach nicht so gut, wenn es geplant ist. Ich
stelle mich dann darauf ein, dass etwas ungewohnt ist, wodurch es nicht
zufällig einfach gut sein kann."

Marim nahm wieder etwas mehr Abstand, um die Sexyness aufzulösen. "Ich verstehe
das, glaube ich.", sagte er. "Also, möchtest du, dass ich dich die Tage mitten
in irgendeinem Text so nenne?"

Ankua nickte und lächelte. "Das wäre prima. Auf der anderen Seite erwarte ich
von dir sowas dann vielleicht. Es ist schwierig."

"Warum überhaupt?", fragte Marim. "Willst du darüber reden?"

"Ich habe mich daran erinnert, dass ich es früher nicht schlimm fand, 'Mädchen' genannt
zu werden, und dass ich mich darin lange wiedergefunden habe. Obwohl ich
männlich bin.", erklärte Anuka.

Das stimmte. Sie hatten früher, als Kinder viel Zeit miteinander verbracht,
vorwiegend in der Badewanne oder in irgendwelchen Bächen draußen in
Gummistiefeln, wenn sich ihre Eltern besucht hatten. Damals hatte Marim
gern Latzhosen getragen. Und als sich ihre Körper anfingen, weiterzuentwickeln,
hatte Anuka festgestellt, dass er trans war, und gehofft, dass es Marim
ähnlich ginge, weil sich bei Marim in der Zeit seine bevorzugte Mode auf
Kleider und Strümpfe umgestellt hatte. Aber bei Marim war es wirklich nur
die Mode gewesen. Sie hatten viel darüber gesprochen und Marim hatte dabei herausgefunden,
dass er wohl cis war und sein liebstes Herzwesen eben trans.

Er nickte. "Möchtest du herausfinden, ob du dich in dem Begriff immer
noch wiederfindest?", fragte Marim.

"Ja.", sagte Anuka. "Ich erwische mich manchmal in meinem eigenen Kopf
dabei, wie ich mich Mädchen nenne. Etwa 'Das kannst du nicht so machen, Mädchen.', oder
ähnliches."

"Glaubst du, dass sich etwas verändert hat? Also, dass du so etwas wie
long term fluid bist? Oder dass du mit dem Begriff kein Geschlecht verknüpfst?", fragte
Marim. "Sind das sinnvolle Möglichkeiten? Gibt es noch mehr? Oder rede ich Unsinn?"

"Du redest keinen Unsinn. Ich habe mich das eben auch gefragt.", sagte
Anuka. "Mein Schluss soweit: Ich bin long term fluid, aber ich war nie weiblich und
bin es immer noch nicht. Ich
hatte mal wenig Geschlecht und habe nun mehr Geschlecht." Anuka machte eine
kurze Redepause, in der er sehr grinste. "Aber Mädchen ist für mich nicht
neutral. Ich war eben ein männliches Mädchen. Der Begriff *männliches Mädchen*
macht was sehr Positives mit mir!"

"Euphorie?", fragte Marim und grinste mit.

Anuka hörte als Antwort lediglich nicht mit dem Grinsen auf. "Geschlecht ist
kompliziert.", sagte er nur.

Marim nickte. "Ich finde immer noch manchmal erstaunlich, dass du mit mir
darüber so viel reden magst, obwohl ich die Erfahrung nicht habe."

"Du hast sie halt doch!", widersprach Anuka. "Du bist cis. Aber du hast
dich wirklich sehr hinterfragt, weil du ein kleines Umfeld hattest, nicht
viel Kontakt, und ausgerechnet die andere Person -- ich also --, mit
der du viel zu tun hattest, ist trans. In einer Zeit, in der
Geschlechtszuweisung bei der Geburt und auch sonst inzwischen
fast gar nicht mehr passiert, und der Begriff trans sich neu und
schwammiger definiert. Du warst von Anfang an dabei."

"Vielleicht stimmt, dass es dazu führt, dass ich ein paar Erfahrungen stärker habe, wie das
Hinterfragen.", sagte Marim. "Aber einige Erfahrungen teilst du mit
mir, die ich definitiv nicht habe."

"Und das Großartige ist, dass ich sicher sein kann, dass du mich immer
voll ernst nimmst, du trotzdem Verständnis hast und ich dich
damit nie nerve.", sagte Anuka ernst. "Ich glaube, es gibt einige
trans maskuline Personen, denen allein die Vorstellung, sich
Mädchen zu nennen, so unangenehm ist, dass es schwierig ist, darüber
zu reden. Was okay ist. Wir sind eben individuell und haben
verschiedene Erfahrungen. Wie du und ich eben auch."

Es kam Marim nach wie vor seltsam vor, dass Anuka seine Erfahrungen
so sehr ähnlich einschätzte. Aber er konnte es nicht besser
beurteilen. Und letztendlich war ja schön, dass sie sich so
sehr verstanden, auch auf der Ebene. Marim war froh über diesen
Austausch. Denn ja, als nächstes würde er sich wohl Gedanken
machen, wie sich die Bezeichnung 'Mädchen' für ihn anfühlte. Oder
auch die Bezeichnung 'männliches Mädchen'. Er musste grinsen. "*Männliches
Mädchen* ist irgendwie schön. Es fühlt sich ein wenig so
an, als bräuchte ich von irgendwo eine Erlaubnis für das Label, aber
wenn ich es haben dürfte, würde ich es durchaus auch mögen. Ich
mag Kind sein. Ich mag den Klang von 'Mädchen'."

Anuka lächelte und wartete noch etwas ab.

"Aber so ganz meins ist das Label nicht. Es fühlt sich
dann als Label für mich so an wie eine Verkleidung oder ein
Beruf. Ich glaube, das Wort ist vielleicht nicht
dafür gedacht.", kam Marim zum Schluss. "Rede ich Unsinn? Sollte
ich etwas entlernen?"

"Ich glaube, wir reden hier über solche Feinheiten, dass, wenn
du hier noch weiter dekonstruierst, die Wörter gar keine
Bedeutung mehr haben.", widersprach Anuka. "Was auch nicht
schlimm wäre. Aber hier geht es um persönliche Nuancen
und Gefühle. Du darfst dich damit nicht ausreichend
wohl fühlen, oder eben auch doch. Das ist beides okay."

Marim nickte verwirrt. "Geschlecht ist kompliziert.", wiederholte
er, was Anuka vorhin gesagt hatte.

"Warum brauchst du eigentlich wieder einen Supermarkt?", wechselte
Anuka das Thema. "Was ist passiert?"

Es musste an sich nichts passiert sein, damit Marim einen
Supermarkt betrat. Er tat es eben einfach gern. Er tat es
wöchentlich mehrmals. Er suchte sich sehr bedacht aus, was
er druckte und aß. Nicht, weil er so an sich sehr wählerisch
gewesen wäre, -- vielleicht ein bisschen. Sondern weil es
eine Routine war, die ihn beruhigte, die einfach zur
Woche dazu gehörte und ihn entspannte.

Aber es war ungewöhnlich, dass er Anuka nicht woanders traf, sondern
dass er ein Treffen an so einem Ort mit ihm vereinbarte. "Ich habe mich
vermögt.", sagte er.

Fosh, sie hatten die halbe Nacht hindurch noch geredet. Sie hatten
erst über das Wasser geredet. Nurek hatte ihm tatsächlich den Code
gezeigt, und dann -- vor seinen Augen -- festgestellt, wie es viel
eleganter gelegt werden konnte und es umgebastelt. Sie war so derbst
frei davon, dass er sich irgendwelche Urteile bilden könnte. Oder
sie war nicht frei davon, aber sie konnte nichts dagegen tun, sie
selbst zu sein. Es war entspannend.

Dann hatten sie sich erschöpft wieder ins Wasser gelegt, die kleine
Änderung untersucht, die sie eingebaut hatte. Sie waren beide eigentlich
völlig durch gewesen, weil soziale Interaktion mit neuen Personen
nun einmal sie beide sehr stresste, aber sie hatten nicht aufhören
gekonnt. Dann waren sie zum eigentlichen Zweck der Virtualität
gekommen. Sie hatten versucht, sie wahrzunehmen, sich fallen
zu lassen und zu entspannen, was echt nicht einfach gewesen war, zu
zweit, mit so vielen spannenden Themen auf dem Stapel. Sie hatten
sich geeinigt, gemeinsam Musik zu hören, und festgestellt, dass sie
zufällig die gleiche Lieblingsband hatten. Das war ein Moment gewesen, der
sie beide gegruselt hatte. Sie waren sich nicht vollends ähnlich, aber
sie hatten auch noch nichts gefunden, worin sie völlig verschiedener
Ansicht wären. Ihre Vorgehensweise mit Neuem passte zusammen. Und
dann auch noch die Musik.

Sie hatten zwei der sechstel-Stunden-langen Stücke der Band
*Schabernakel* in Stille gehört. Dann hatte sie ihn angeblickt.
Glücklich, hatte er gedacht. Und sich verabschiedet, weil sie
nicht mehr konnte.

Marim atmete langsam ein und aus, während er bemerkte, wie Anuka
ihn nachdenklich beobachtete.

Das Ende dieses Zusammenseins hatte ihn genau so unsortiert und
verunsichert zurückgelassen wie beim ersten Mal. Es war nichts
Schlimmes passiert, aber es fühlte sich so an. Es war so ein
Unsinn. Immerhin schrieben sie jetzt gelegentlich kurze Nachrichten
hin und her. Und seine versprochene Nachricht bezüglich
potenziell zu viel Bewunderung stand auch noch aus.

"Klingt spontan voll schön!", meinte Anuka.

Er fragte nicht von sich aus nach mehr Details. Aber Marim wusste, dass
er neugierig war. "Sie hat vorgestern an meiner Studie teilgenommen. Sie
ist mir auch vorher schon durch Likes und ein paar Replies
aufgefallen. Sie schrieb mir abends. Ich darf leider keine Details über
die gewünschte Virtualität erzählen. Wir haben uns darin jedenfalls kennen
gelernt. Oberflächlich. Beziehungsweise, irgendwie haben wir
uns eben gut verstanden."

"Dann ist sie wohl auch irgendwo auf dem neurodiversen Spektrum, schätze ich.", sagte
Anuka.

"Ähm, ja!", bestätigte Marim und erinnerte sich daran, dass das mit
zu seinen Gedanken gehört hatte, als sie sich kennengelernt hatten.

Anuka war nicht auf dem neuroatypischen Spektrum, er war neurotypisch. Aber
dadurch, dass sie sich so lange kannten und Anuka
mitbekommen hatte, wie es bei Marim zur Diagnose gekommen
war, was für Fragen gestellt worden waren, wie das
abgesteckt worden war, und was sich daraus
im Umgang für Konsequenzen ergeben hatten, die beiden helfen
würden, kannte er sich damit recht gut aus. Trotzdem erschien
Marim sein spontaner Schluss nach seiner so kurzen Zusammenfassung
voreilig. Es war typisch für Anuka, zu schnell zu schließen, und
oft wehrte sich Marim, aber heute hatte er keine Lust.

"Meinst du, sie würde auf die Idee kommen, dass es sich um ein Date
handeln würde, wenn du sie ins Kino einlädst?", fragte Anuka.

"Ich würde nicht davon ausgehen.", antwortete Marim. Er wandte sich
von Anuka ab und ging wieder auf eines der Regale zu. Die direkte
Nähe wurde ihm zu viel und er brauchte etwas, um den unruhigen
Teil seines Gehirns zu beschäftigen. Gerüche taugten dazu. "Und außerdem,",
fügte er hinzu, "würde ich Leute üblicherweise
aus anderen Gründen ins Kino einladen als für Dates. Bei Dates würde
ich mich gern unterhalten."

"Du könntest sie trotzdem einladen.", meinte Anuka. Er kam
Marim hinterher, nicht zu dicht, aber so, dass er Marim
ausreichend im Blick haben konnte und nicht rufen
musste. "Das Hirnissennest hat einen
neuen Film rausgebracht, den ich mir gern ansehen wollte, in
Begleitung, denn mit der Surrealität hinterher allein zu sein, ist
nicht so meins."

"Also, willst du eigentlich einen Kinoabend machen, möchtest
sie gern kennenlernen und dachtest, du schmeißt mal einfach
alles zusammen?", schloss Marim.

"Genau!" Anuka grinste.

"Ich frage sie.", antwortete Marim schlicht.

---

Zum Nachmittagessen setzte Marim sich auf eine gepolsterte
Kiste auf die Terrasse. Er hatte sich außerdem eins der
Kissen von seinem Bett mitgenommen, um seinen Rücken gegen
die Steinwand hinter ihm abzupolstern, und sein
Plüschkrokodil. Es hieß Gumbol. Dieses Krokodil hatte
Charakter für ihn. Es war mit ihm in den letzten
zwanzig Jahren überall mitgereist, und er war durchaus
viel gereist. Es war ein Begleit- und Reisekrokodil. Es
hatte die Blicke auf sich gezogen, statt auf ihn, Leute
hatten es sofort gemocht und der Einstieg in Gespräche
war dadurch leichter geworden. Der grauenhafte Smalltalk
hatte weniger 'Alles gut?' oder andere komplexe Floskeln
enthalten und mehr 'Darf ich es streicheln?' und 'Wie
heißt es denn?'. Das waren einfachere Fragen.

Er hatte ein Waldmeister-Reisgericht mit Muschecken
gedruckt. Es war vielleicht experimentell, aber es hatte ihn im Supermarkt
sehr neugierig gemacht. Dann hatte er erst ein kleines Probierhäppchen
gedruckt, und als er fand, dass er davon mehr haben könnte, die
ganze Speise.

Er aß sie langsam, im Schneidersitz auf der Kiste sitzend. Es
war eine Gemeinschaftsterrasse. Mehrere Dachgeschosswohnungen
teilten sie sich. Aber derzeit waren die anderen nicht von
Leuten belegt, die viel draußen gewesen wären, also hatte er
die Terrasse für sich. Eine Terrasse mit Meerblick, im weitesten
Sinne. Die Großstadt Minzter fiel vor ihm hinab, sandfarbene
Häuser mit weißen Flachdächern, die wirkten, als würden sie sich
aufeinanderstapeln und wären teils durcheinander gewürfelt. Dazwischen
wuchsen Harzpalmen, Landpappeln, sowie verschiedenste Weiden- und
Lindenarten, einige mit Früchten. Außerdem gab es fast
in jedem Haus irgendeine Partei, die
die Dächer bepflanzte, sodass sich ein Blumen- und Rankenmeer über
die Stadt ergoss. Es war hübsch. Dazwischen fuhr die Magnetbahn, aber
das war derzeit ein Chaos -- ein überschaubares immerhin. An sich
hatte Minzter ein gut funktionierendes, relativ frisch
modernisiertes Stadtbahnsystem gehabt, als die Metropole und
Hauptstadt Maerdhas Fork mit dem Kopplungssystem
gestartet war: Dem Magnetschwebebahn-System, bei dem die Kapseln
jener direkt in die Kapseln der Fernzüge einfuhren oder eingelassen
wurden, was ein Umsteigen im Hauptbahnhof unnötig machte. Das war
erheblich barriereärmer für viele, effektiver und schneller. Ein
Gutteil der Fernzüge hielt nicht einmal mehr, wenn der Nahverkehr
in ihn hineindockte, und weniger Beschleunigung hieß weniger
Energieaufwand und weniger Zeitverwarten.

Für Minzter war der Zeitpunkt völlig falsch gewesen. Es waren damals
allerlei Szenarien simuliert worden, aber alle waren zum Ergebnis gekommen, dass
Barrierearmut anders anzugehen und die damals noch modernisierten,
aber nicht ans Fernverkehrssystem koppelnden Züge ein paar
Jahrzehnte weiterzuverwenden die sinnvollste Möglichkeit in Bezug
auf Klima und Ressourcenverbrauch war. Diese Zeit war nun abgelaufen und
der Umbau Minzters hatte begonnen. Immerhin hatte es für diesen Umbau
über zwanzig Jahre Zeit gegeben, ihn sinnvoll zu durchdenken. Im Moment
fuhren noch beide Systeme. Das alte wurde nach und nach abgebaut und
neu verwertet. Das hieß aktuell, dass manchmal mehr Umstiege oder mehr Umwege in Kauf
genommen werden mussten, wollte eins irgendwohin. Es lagen
alle Informationen zum Umbau in einer riesigen Abhandlung transparent
bereit. Sie konnten im Ganzen gelesen werden, was auch einige
neugierig taten, nicht zuletzt, um es zu diskutieren. Aber das
Lesen hätte mehrere Tage gebraucht. Natürlich
gab es auch wie immer KIs, die die Informationen, die
eine Person gerade jeweils brauchte, für diese in der Ausführlichkeit
herausfilterte, die ihr am besten weiterhalf.

Jedenfalls erstreckte sich diese beblümte Häuserlandschaft mit den
Schienen dazwischen hügelig bis zum Ufer, nur, dass die Häuserhügel die
Sicht größtenteils abschnitten, sodass Marim effektiv nur einen
Schnipsel Horizont mit Meer sehen konnte. Es war trotzdem befriedigend,
genau diesen Schnipsel beim Essen nicht aus den Augen zu lassen.

Marim strich Gumbol über den Rücken. Das flusige Fell hatte
eine augenblicklich beruhigende Wirkung auf ihn, dabei fühlte
er sich gar nicht mehr so unruhig. Oder war er es doch? Er
hatte eben auf der einen Seite Energie genug, eine neue Speise
auszuprobieren, von der er noch nicht wusste, was sein Körper
so genau dazu sagen würde, aber fühlte sich auf der anderen immer noch
nervös wegen Nurek. Passend zum Gedanken vibrierte sein
Faltrechner sachte neben ihm. Er wollte eigentlich beim Essen nicht
gestört werden, aber er hatte Nachrichten von
Nurek priorisiert. Er faltete den Rechner auf und las:

*Nurek: Heute Abend könnte ich wieder.*

Er musste unwillkürlich grinsen. Er schrieb direkt zurück, dass es
ihm auch passte, dann klappte er den Rechner wieder zu. Für nach
dem Essen nahm er sich vor, endlich die Nachricht zu schreiben, die
er sich überlegen wollte. Hoffentlich bekam er es irgendwie hin, sich
darauf zu fokussieren. Nun erst einmal schloss er für ein
paar Momente die Augen, um das Essen zu schmecken, und verlor sich
wieder in Gedanken über das Hier, allerdings nicht unbedingt
über das Jetzt.

Marims Wohnung war klein und er hatte sie möbliert übernommen. Er
schaffte sich keine eigenen Möbel an, weil er etwa alle zwei bis
drei Jahre umzog. Eine Kiste auf der Terrasse reichte zum
Sitzen auch vollkommen. Minzter war schön, aber eigentlich zu groß für
seinen Geschmack, und zu warm. Er war hier auch schon seit zwei
Jahren. Es hatte ihn vor einigen Monaten angefangen, zu
beschäftigen, und war nun wieder ein dauerpräsenter Gedanke. Es
wurde Zeit, zu ziehen.

Nachdenklich blickte er auf sein Handgelenk. Windschwingen hatten
die Tradition, sich zwei unscheinbare Bögen dorthin zu tätowieren, --
natürlich taten es nicht alle Windschwingen. Das Zeichen war
Kinderzeichnungen nachempfunden, die fliegende Vögel häufig auf
diese Art vereinfacht darstellten. Manchmal dachte Marim darüber
nach, ob er das auch wollte, aber so ganz richtig kam es ihm nicht
vor.

*Windschwinge* war ein Label, dass Personen trugen, die aus verschiedensten
Gründen keinen festen Wohnort hatten, sondern vor allem auf Reisen
lebten. Aber das traf auf Marim ja nicht so ganz zu. Zwar verbrachten
die meisten Windschwingen durchaus immer wieder mal ein paar Monate
am selben Ort, aber zwei Jahre wären schon viel. Außerdem zog es
ihn nicht unbedingt weg, weil er gern auf Reisen war, sondern weil
er Orte einfach irgendwann über hatte und dann einen neuen brauchte. Und
langsam hatte er Minzter über.

Ihm fiel nun erst auf, dass auch in Minzter das Wort Minze steckte. Wie
ironisch zufällig.

Sein Essen schmeckte immerhin nicht nach Minze. Er aß die letzten
Bisse mit Fokus aufs Essen und legte anschließend den schwarzen, leichten
Teller zur Seite. Er war praktisch, weil er leicht abwaschbar war und
nicht so laut dabei. Diese Art Geschirr war inzwischen fast überall in Maerdha
Standard, aber hier in der Gegend entstanden, und sie gehörte ebenfalls zur
Einrichtung der Wohnung. Dann
nahm er seinen Faltrechner zwischen Knie und Brustkorb und
versuchte sich auf das Tippen einer Nachricht zu fokussieren. Direkt
klappte das nicht. Sein Blick schweifte wieder auf das Fitzelchen
Meer, das er sehen konnte.

Minzter lag am Nachtmeer. Es hieß so, weil hier Quallen zu Hause waren, die
nachts leuchteten, abhängig von Größe und Jahreszeit. Marim liebte
es und hatte hier viele Nachtspaziergänge
am Wasser unternommen. In der Stadt selbst taten das nur leider viele. Deshalb
war er dabei nie so richtig zur Ruhe gekommen, hatte lieber dazu
die Stadt verlassen, aber seltener die Energie dazu gehabt.

Die größeren Quallen
schwammen tiefer und die kleinen Babyquallen oft näher an der Oberfläche, was
dem Meer durchaus einen Eindruck von leuchtendem Sternenhimmel
gab, aber eigentlich auch nicht so richtig. Marim fand es
mehr für sich genommen schön. Die Quallen sammelten sich nur hier, in der Bucht, in der die
Lun nicht allzu weit entfernt ins Meer mündete. Die Küste war hier
so geformt und die Bucht gerade passend schmal, dass sich durch Strömung fast Süßwasser
am Ufer von Minzter bildete, das für diese Quallen günstigen Lebensraum
darstellte. Sie hatten in der Menge noch keinen anderen Lebensraum auf dem
Planeten Arda erschlossen.

Er schickte dem Lebensmitteldrucker noch einen gewürzigen, anregenden
Tee-Druckauftrag, -- wobei Drucken das Erwärmen und
Eingießen von Flüssigkeiten wenig präzise beschrieb --, und machte sich dann an
die Nachricht.

*Begeisterung*, schrieb er zunächst. Er konnte das Wort auch wieder löschen, aber
es stand dort erstmal, um ihn daran zu erinnern, worum es ging. *Warum war
sie erst sehr gut und dann, Momente später, zu viel?*, schrieb er dazu.

Er versuchte sich in das gehabte Gefühl zurückzuversetzen, erinnerte sich aber stattdessen
zunächst an den Moment, in dem er beschlossen hatte, nicht zu erklären, wann
es zu viel wäre, sondern was in ihm in dem Moment vorgegangen war, und auch
das half etwas dabei, zu fokussieren. Er hatte schließlich schon einmal
entschieden, dass diese Richtung der Gedanken gut wäre.

*Als du dich mitbegeistert hast, war es zuerst, wie, miteinander für eine
Sache zu brennen. Das gleiche recht spezielle Interesse haben und darin
aufgehen. Ich glaube, das ging dir auch so, und das ist für mich einfach
ein sehr schönes Gefühl. Eine Option, mit einer Person ins Detail
zu gehen, durch die Fremdperspektive Ideen zu bekommen, auf die
eins selbst nicht gekommen ist, aber für die der Austausch mit lediglich
oberflächlich interessierten Personen nicht weit genug geht. Und
auch einfach diese Freude, mit der riesigen Begeisterung für
die Sache nicht allein zu sein. Das ist wunderschön.*

Marim las den Abschnitt noch einmal und fragte sich, ob er ihm zu
persönlich wäre. Oder ob er ihr zu persönlich sein könnte. Er
las ihn noch einmal. Und ein weiteres
Mal, dieses Mal daran denkend, dass Nurek durchaus sehr brachial offen
war. Brachial in einem positiven Sinne, vielleicht
im Sinne von positiver Schonungslosigkeit, oder unapologethisch. Mit
dem Gedanken im Hinterkopf überlegte er, aus seiner Comfort Zone
etwas herauszugehen und die Nachricht so stehen zu lassen.

Und dann fügte er doch einen neuen Absatz vorneweg hinzu: *Das folgende
ist sehr ehrlich und direkt aus meinen Gefühlen niedergeschrieben. Das
mache ich generell meist eher erst, wenn ich Leute besser kenne, und
dann durchaus sehr gern. Aber
bei dir fühlt es sich gerade richtig an. Vielleicht, weil du
das auch machst, oder weil du auf eine Weise kommunizierst, durch die ein Ort
zu zweit mit dir ein Safe Space ist. Ich habe dabei hauptsächlich
Angst, dich zu bedrängen. Allein das 'bei dir fühlt es sich richtig
an' mit einem Herausstellen, dass du eine Ausnahme bist, fühlt sich
für mich so nach einer Floskel an, die in Gesprächen fällt, um einer
Person ein besonderes Gefühl zu geben, aber im Gegenzug etwas
zu bekommen. Vielleicht wäre das neurotypisch? So
ist das jedenfalls nicht gemeint. Du sagtest, du möchtest Grenzen selber
setzen. Und ich werde jede davon ohne schlechte Gefühle
akzeptieren.*

Marim durchdachte noch einmal, ob er da wirklich eine All-Aussage stehen
haben wollte oder ob er doch eine Situation konstruieren könnte, in
der das Grenzensetzen selbst dazu führen könnte, dass er schlechte
Gefühle hätte, und nicht, dass er sie bereits überschritten hätte. Beim
erneuten Lesen stieß ihm immer wieder das 'zu zweit' auf. Das klang
irgendwie falsch, nach mehr als es war. Aber es wegzulassen, wäre
nicht präzise gewesen. Ein Ort mit ihr und einer anderen Person, die
er nicht kannte, wäre kein Safe Space gewesen. Er überlegte 'du bist
ein Safe Space' zu schreiben, aber das war auch keine gute Formulierung. Er
beließ es schließlich dabei und schrieb dann den nun dritten Absatz:

*Der Moment, in dem es für mich innerlich kippte, war, als
du meintest, dass irgendwas der eigentliche Grund gewesen wäre, warum
du die Studie so gut fändest.*, schrieb er. Er blickte dazu
noch einmal in sein Protokoll, um zu schauen, ob er sich richtig
erinnerte. *In dem Augenblick flammte in meinem Kopf die Idee
auf, du könntest nicht vorwiegend als Versuchsperson da sein, sondern
andere Gründe könnten mindestens genau so wichtig sein. Für mich
war in dem Augenblick unangenehm, dass es nicht vorher kommuniziert
war (aber eigentlich war es das, du hattest ja gelegentlich mal etwas
in der Richtung repliet, aber das hatte ich da nicht auf dem
Schirm), und dass ich nicht wusste, um was für Gründe es sich
handelte. Ich werde gelegentlich mal bedrängt. Das hast du nicht, aber
ich hatte da wohl Angst und das Bedürfnis, mich vorsichtshalber
abzugrenzen. Abzugrenzen gegen etwas, was passieren könnte, weil
es mir ein paar Mal zu viel als Abzweig aus einer ähnlichen
Situation passiert ist. Verstehst du, was ich meine?*

*Es wäre von meiner Seite
besser gewesen, das zu erkennen und
es nicht so seltsam untergründig zu machen, aber ich wusste es selber
in dem Moment nicht besser. Eigentlich weiß ich es erst
klar mit diesem Aufschrieb, währenddessen
die Selbstanalyse geschieht. Jedenfalls hat es nicht mit
dir direkt etwas zu tun, du hast gar nichts falsch gemacht. Ich
möchte deiner Begeisterung keine Grenze setzen, die ist schön. Ich
war in dem Moment durch schlechte Erfahrungen, die nichts mit dir
zu tun hatten, nur unsicher, und das wird vielleicht nicht zum letzten
Mal zum Vorschein gekommen sein. Ich hoffe, ich erkenne es beim
nächsten Mal besser.*

Er las auch diesen Absatz ein paar Mal, änderte einzelne Worte, und
änderte sie dann wieder zurück, weil der nächste Absatz schon
das Wort oder den Inhalt enthielt. Er fügte hinter 'sondern andere Gründe
könnten mindestens genau so wichtig sein' mit einem Gedankenstrich
noch hinzu '-- was vollkommen okay gewesen wäre, beziehungsweise
ist'. Dann, nach erneutem Lesen, kam er zum Schluss, dass es brauchbar
war. Etwas Angst hatte er, dass Nurek trotzdem Schuld bei sich
suchen könnte. Der Anfang las sich vielleicht heftig. Aber mit dem übrigen
Absatz sollte es eigentlich aufgelöst und erklärt sein. Hoffte er. Oder, dass
Nurek im Zweifel nachfragen würde.

Zum Abschluss löschte er seine anfänglichen Notizen, las alles
noch einmal zusammen und drückte nach einigem Zögern auf den
Versenden-Knopf. Die gewohnte Angst überrollte ihn, die Nachricht
ausversehen nicht Nurek geschickt zu haben, sondern einer
ganz anderen Person. Er kontrollierte
es. Er hatte es richtig gemacht. Er las die Nachricht noch einmal
und kontrollierte es wieder. Manchmal tat er Dinge auch falsch, obwohl
er es immer wieder kontrollierte, weil sein Fokus ein Detail nicht
mitschnitt, oder in seinem Kopf gerade zwei Namen zu einem
verschmolzen. Ganz fokussiert fühlte er sich gerade nicht.

Er trank ein paar Schlucke Tee, fühlte die Anspannung abfallen, die
er aufgebracht hatte, um sich auf diesen Text zu fokussieren und aus
seiner Comfort Zone herauszugehen, und wollte die Nachricht gerade wieder
lesen, als Nureks Reaktion kam.

*Nurek:*

Er bemerkte sofort, dass sie sich für ihn umbenannt hatte. Er
überlegte, ob er sich für sie auch in nur *Marim* ohne Nachnamen
umbenennen sollte.

*Ich antworte dir später oder morgen ausführlicher. Ich möchte dir
nur schnell schon einmal eventuelle Unsicherheiten nehmen.*

*Punkt 1: Deine Grenzen gehen vor. Wenn du dich in einer Situation unsicher
fühlst, weil sie dich zum Beispiel an etwas Bedrohliches erinnert, und du hast
die Wahl zwischen Abgrenzung, aber mit schlecht gewählten, seltsamen,
unpräzisen Worten bis hin zu unstimmigen Argumenten, oder
keine Abgrenzung, dann ist ersteres immer
die bessere Wahl. Alles Weitere lässt sich hinterher klären, und
wenn nicht, wäre ich ganz schön scheiße.*

*Punkt 2: Ich fühle mich nicht bedrängt.*

*Ich sehe durchaus Stellen in dem, was du schriebst, in die viele
Leute üblicherweise einen bestimmten Subtext lesen würden, vielleicht
einen romantischen, oder irgendwelche Erwartungen. Ich kenne mich
nicht aus, was genau, ich weiß nur, dass da meistens einer ist, bei
manchen der Formulierungen, wenn sie von anderen kommen. Ich
lese da aber keinen solchen hinein. Wenn
ich doch irgendwann einen Subtext für wahrscheinlich halten
sollte, dann werde ich brutal offen erfragen, ob du ihn so
meinst, und erst mit einer Bestätigung aus der Möglichkeit
in meinem Kopf eine Tatsache machen. Klingt das gut für dich?*

Marim hatte am Ende von Punkt 1 schon geweint. Leise. Es schoss
einfach heiß in seine Augen. Es war als würde sich an seinen
negativen Erfahrungen ein kleiner Teil auflösen. Das
verinnerlichte Victim Blaming -- sich selbst an Übergriffigkeit
gegenüber ihm die Schuld
geben, statt Personen, die sich scheiße verhielten --, zerlegte
sich. Es würde zurückkommen, klar. Aber jedes Mal mit so
deutlich aufgeschriebenen Worten wurde es weniger und er erlaubte
sich selbst mehr, zu sein.

Der zweite Punkt war diese Sicherheit, die er schon länger
bei ihr wahrnahm, und von der er ihr geschrieben hatte. Eine
Sicherheit die sich wie Atmen anfühlte. Wo er
sich sonst immer darum kümmern musste, was für Subtext
Leute so in seine Aussagen interpretieren könnten, und
durch präzisere Ausdrucksweise versuchen musste, dagegen
vorzubeugen, hatte er hier einfach zugesichert bekommen, dass
er diese Leistung nicht ständig von sich abfordern musste. Sie
war mit Nurek nicht seine Verantwortung allein. Nun, sie war
ja ebenfalls neuroatypisch, aber auch neuroatypische Personen
waren nicht gleich. So, wie sie es schrieb, fühlte er in
das Weinen hinein eine Auflösung von kaum wahrgenommener, weil
so permanent in Kommunikation angebrachter Anspannung, sodass
er tatsächlich freier Atmen konnte. Das tat er dann auch.

---

Am Abend, dieses Mal schon etwas früher als die bisherigen
Male, trafen sie sich wieder in Nureks Virtualität. Es
war ein guter Ort, ein wirklich guter Ort für Treffen. Er
spürte eine innere Freude, als er sie
betrat. Nurek lag bereits auf dem Boden im unrealistischen Wasser
und grinste die Decke an. Er
legte sich wortlos neben sie und tat dasselbe.

"Außerdem leite ich gern Konversationen so ein, als wären sie
schon eine Weile am Laufen.", begrüßte Nurek ihn.

Marim überlegte, wie er in Sachen Seltsamkeit kontern konnte, wie
er es sich vorgenommen hatte. Das erste, was ihm einfiel, fand
er zwar durchaus seltsam, aber auch gewagt. Er probierte
es trotzdem. No risk, no fun, oder so. Und außerdem
fiel ihm nichts anderes ein, solange er diesen Gedanken
als Reserve speicherte. "Mein Herzwesen möchte dich daten."

"Was?" Nurek beließ es bei der Reaktion.

Marim konnte nicht schließen, ob sie nur Irritation widerspiegelte oder
auch so etwas wie Abscheu. Er befürchtete, nun doch aufdringlich gewesen zu sein, und das
war gar nicht mal so ein entspannender Gedanke. Außerdem
vielleicht nicht völlig unrealistisch. "Ich versprach, ich
würde versuchen, adäquat zu konkurrieren, bezüglich seltsam
sein.", erklärte er, brachte dabei möglichst doch
Ernsthaftigkeit in seinen Ausdruck. "Ist das eine
Kategorie von seltsam, die in eine sehr
unkomfortable Richtung driftet?"

"Wäre es mit etwas mehr Details weniger seltsam?", fragte Nurek, statt
zu antworten.

"Ja.", versicherte er.

"Dann wäre ich jetzt bereit für Konkurrenzreduzierung." Sie
musste nur Momente nach ihm auch kichern.

Marims Nervosität legte sich eine Spur. Wenn sie so reagierte, dann
war das wahrscheinlich entweder kein Problem, oder ein lösbares. "Er
würde gern einen Kinoabend machen und schlug vor, ich könnte
dich dazu fragen. Er fragte außerdem, ob du das wohl als Date
auffassen würdest, was ich für unwahrscheinlich hielt."

"Aber dann wollte er eher uns miteinander daten und
nicht er mich?", korrigierte Nurek seine ursprüngliche
Aussage der Seltsamkeit.

Marim wurde für einen Augenblick sehr heiß. Er hätte damit rechnen
müssen, dass Nurek das fragte. Es war ihm kurz peinlich. Dann
sammelte er sich zusammen und dachte sich, dass es eigentlich
keinen Grund dafür gab. Sie konnten sicher sachlich darüber
sprechen. Hoffte er. Und wenn nicht, dann war das eben so. "Ja, das
wäre eigentlich präziser."

"Wäre es für dich ein Date?", fragte Nurek.

Gleich die nächste dieser Fragen. Dieses Mal war der
Augenblick des Schamgefühls sogar noch kürzer. "Ich
hatte das nicht geplant. Für Dates bevorzuge ich eigentlich
andere Aktivitäten. Aber wenn du von dir aus gern hättest, dass
es eines wäre, würde ich vielleicht nicht 'nein' sagen."

"Wie definierst du Date?"

Marim musste schmunzeln, weil er sich tatsächlich schon oft
Gedanken darüber gemacht hatte, wie er die Frage beantworten würde, aber
noch nie gefragt worden war. Und seine Antwort trotzdem
nicht saß. "Ich empfinde Date als einen Umbrella-Term. Ein
Date ist also dann ein Date, wenn alle Beteiligten sich darüber
einig sind.", antwortete er. "Dafür braucht es natürlich trotzdem
schonmal eine Richtung, in die es gehen soll. Ich würde von
mir aus als Date eine Verabredung bezeichnen, bei der die
Option auf eine neue Beziehungsebene eruiert wird." Marim
runzelte die Stirn. "Ich habe ein Fachwort benutzt. Herausgefunden
wird? Sowas. Etwas in Richtung erforschen, erheben."

Nurek ging nicht weiter auf die Extra-Erklärung ein. "Beziehungsebene
ist ziemlich vage.", meinte sie. "Ist besagtes Kino-Gedöns
dann nicht ein Date, weil wir
dabei herausfinden, ob wir beide zusammen gemeinsam einen Film
ansehen können oder ob wir uns dabei auf die Nerven gehen oder
so etwas?"

"Vielleicht ist es das. Beziehungsebenen lassen sich sicher auch
nicht scharf trennen. Vielleicht ist das aber auch noch zu stark
die gleiche, die wir schon haben.", antwortete Marim. "Ganz, wie
du magst."

"Für die Kino-Sache ist mir das dann egal. Solange es diese
Beziehungsebene ist, über die wir eruieren, und nicht eine
andere. Wobei manche anderen sicher auch okay wären, kommt
jeweils drauf an.", sagte Nurek. "Aber ich hätte gern ein
Date, hm, eine Art Date? Ein Date-Gedöns mit dir. Ich würde
gern eruieren, ob wir gemeinsam auf ein Konzert gehen
könnten. Outernet, live. Das kann natürlich bereits
scheitern, wenn das für dich jetzt schon einfach nicht
in Frage kommt. Dann wäre im Nachhinein dies ein Date
gewesen, richtig? Ich bin verheddert, hier sind schon wieder zu viele
Gedankenäste."

"Schabernakel?", fragte Marim, bevor er irgendeine
der anderen Fragen, die Nurek gestellt hatte, in seinem
Kopf sortiert hatte.

"Ja.", sagte Nurek. "Es ist ein Wunschtraum von mir, sie
irgendwann live zu sehen. Aber es gibt so viele Barrieren. Und
die erste, an der es scheitert, ist, dass ich das nicht
alleine packe, aber keine Person kenne, die mich begleiten
kann oder mag."

"Hui.", sagte Marim sachlich. Er hatte schon Lust dazu, überlegte
er. Es war noch kein Gedanke, der so wirkte, als wäre er
durchdacht. Der Gedanke hatte sich zunächst einfach in Worten gebildet
und wollte raus. Er überlegte, wie viel von der Einordnung, was
ein Date sein könnte und was nicht, er noch vornehmen wollte, oder
ob das nicht eigentlich auch egal wäre. Aber es fühlte sich
unsortiert an, nicht abgeschlossen. Er wollte zumindest
die Möglichkeit einräumen, dass sie die Dinge definierten. Aber
gerade konnte er nichts anderes sagen, als: "Ich habe schon
Lust dazu." Er wusste nicht, wie viel Zeit vergangen war, bis er
das gesagt hatte.

Nurek neben ihm lächelte. "Es wird kompliziert.", meinte
sie, und wirkte dabei unsicherer als sonst. "Ich brauche einen
sehr genauen Plan. Ich brauche mehr Vertrauen zu dir, wenn das
klappen soll. Ich habe Angst, dass du jetzt denkst, das wäre eine
Sache, die nicht scheitern kann, wenn wir nur genau genug planen, aber
sie kann scheitern."

Marim nickte. Es störte ihn nicht, er verstand das. "Das ist
in Ordnung.", sagte er vorsichtshalber. "Wir sähen nun sozusagen
diese Plansamen und schauen, was sie brauchen, um zu wachsen, und
ob wir haben, was sie brauchen."

"Und wenn das Gestrüpp daraus nicht so wachsen kann, wie
es für diesen Plan müsste, dann kommt
es in den Kompost und wir verwerten es zu was anderem?", fragte
Nurek.

"Das ist ein schönes Bild.", stimmte Marim zu.

Sie schwiegen ein paar Momente. Marim wusste nicht, ob sie das
taten, weil es gerade nichts zu sagen gab oder sie einfach
beide zu verheddert in nicht so ganz verbalisierten Gedankenranken
waren.

"Ich kann bald nicht mehr.", kündigte Nurek an. "Aber ich
mag das Kino-Gedöns noch zu Ende besprechen und habe Fragen."

"Stell sie!", forderte Marim auf.

"Wie gut funktioniert der Film in 2D?", fragte Nurek. "Ich kann kein
3D sehen. Es gibt ja die diversesten Ansätze, Filme zu gucken. Die
mit auf eine Leinwand schauen haben meistens gute 2D-Versionen. Das
würde meine Version der Virtualität dann auto-ersetzen. Prinzipiell
kann ich natürlich 2D sehen, was andere Leute in 3D sehen, wie
im Outernet ja auch. Aber es gibt auch Filme, die nur
in 3D gut funktionieren. Und
anders als in interaktiven Situationen kann ich das in einem
Film nicht ausgleichen."

"Ich dachte mir das beinahe schon, dass du blind auf
einem Auge mit 3D Schwierigkeiten haben könntest.", sagte
Marim. "Die Produzierenden nutzen meist keine besonders
speziellen Effekte, aber ich erkundige mich. Wäre es okay, wenn
ich in 2D mitschaue, oder wäre das unsensibel? Ich habe das
lange nicht mehr gemacht."

"Das kannst du gerne mitmachen.", sagte Nurek und kicherte. "Ich bin
auf dem Glasauge blind."

Marim blickte sie an, weil er meinte, sich daran zu erinnern, dass
die Augenhöhle leer gewesen wäre. Aber Nurek sah an die Decke des
Steinbaus, und lag mit der anderen Seite zu ihm. "Stören dich Fragen
dazu?", fragte er.

"Nein.", sagte Nurek. "Kommt natürlich immer ein bisschen drauf
an, welche, aber im Zweifel sage ich das dann schon."

"Hast du in der Virtualität eine leere Augenhöhle, während du
im Outernet ein Glasauge hast?", fragte er.

"Ich trage im Outernet etwa vier Stunden am Tag das Glasauge. Eigentlich
nicht aus Glas, sondern ein haptisch ähnliches Material, etwas leichter,
etwas weniger Wärmeleitfähigkeit. Es kann passend gedruckt werden, nachdem
die Augenhöhle mit Lasetechnik ausgemessen worden ist, und wird
dann stark erhitzt bis die Oberfläche flüssig ist, die dann
mit Magnetfeld und Luftdruck so getrocknet wird, dass sie keine
noch so feinen Kanten oder Risse mehr hat. Das
Material hat einen komplexen Namen, aber wir
nennen es alle Glas. Es heißt ja auch Plexiglas und so.", erklärte
Nurek. "Es sorgt dafür, dass die Augenhöhle gedehnt bleibt. Aber
das ist auch unangenehm, egal wie passgenau es ist. Daher trage ich
es nur, wenn ich muss. Ich mag mich optisch eigentlich auch ohne gern, daher
habe ich mich entschieden, in der Darstellung meiner Person in Virtualitäten
keines zu tragen."

Marim nickte. Er wusste nicht, was er dazu sagen sollte. Er fand es
irgendwie schön, dass Nurek das offen zeigte. Das war ja in Virtualitäten
umgehbar. Seine Repräsentation in Virtualitäten trug zum Beispiel keine
Brille. Allerdings nicht aus ästhetischen Gründen, sondern
einfach, weil er sie hier nicht brauchte und er
fand, dass authentischere Darstellung die Nachteile eines
Brillengestells, das manchmal im Weg war, nicht aufwog.

Nur ein Auge zu haben, war allerdings wohl etwas sehr anderes, als
eine Brille zu brauchen. Was Nurek dazu bewog, es offen
zu zeigen, wusste er natürlich nicht. Und eigentlich
fühlte er sich auch nicht, als wäre er in der Position, dazu
eine eigene Meinung zu haben, weil es ihn nicht betraf. Also sagte
er erst einmal nichts und fühlte sich seltsam deswegen, weil er ja
die ausgehende Frage gestellt hatte, und eine Reaktion nun vielleicht
dran war.

"Der Grund, warum ich überhaupt auf das Glasauge zu sprechen kam, war
eigentlich, dass ich hier und auch gerade keines trage.", sagte
Nurek. "Und damit ist es unpräzise zu sagen, ich wäre auf einem
Auge blind. Das eine Auge ist nicht blind und ein zweites, das
blind sein könnte, habe ich nicht. Und gleichzeitig fühlte ich
mich seltsam, zu kritisieren, weil es so klingen könnte, als würde
ich dich auf eine verletzende Ausdrucksweise hinweisen, dabei
hat der Drang, zu präzisieren, vielleicht eher was damit zu tun, dass
ich neuroatypisch bin. Ich kann oft unpräzise Aussagen nicht
so stehen lassen!" Nurek warf die Hände in die Luft, wie
in einer 'I can't even'-Geste, und das Wasser spritzte dabei.

Marim lächelte. "Aber du hast ja recht, das stimmt!", sagte
er. "Eigentlich war mir das unterbewusst tatsächlich auch
so halb aufgefallen, irgendetwas hatte mich an meiner Aussage
gestört."

"Weiter im Text.", sagte Nurek. "Du hast ein Herzwesen dabei. Darf
ich auch eines mitbringen?"

"Natürlich!", stimmte Marim zu.

"Dann hätte ich nur noch die Fragen, wann und wo und solches
Gedöns. Aber
am besten schickst du mir damit eine separate Nachricht. Sowas
habe ich lieber schriftlich."
