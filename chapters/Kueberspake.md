Küberspake
==========

Nurek stand vor dem Druckertisch, die Beine viel weiter als
Schulterbreit auseinander, so weit, dass sie fast die Dehnung
spürte, weil der Tisch zu niedrig war. Sie knickte außerdem
am Becken mit geradem Rücken ab und stützte die Ellenbogen
auf. Das war vielleicht keine sehr gewöhnliche Haltung, aber
die entspannteste, bis die Ellebogen anfangen würden, sich gegen
ihr Gewicht zu wehren. Es war der untere Teil eines
Doppeltischs. Auf die obere Platte hätte sie mit gerecktem
Körper gerade so gucken können. Da sie nicht sonderlich
groß war, hatte sie den Druckauftrag an den unteren Drucker
gedruckt, und sah den dampfenden und zurückhaltend
zischenden Düsen dabei zu, wie sie flüssige Metallinien auf die
filigrane Platine malten. Sie holte eine Brille aus ihrer
Brusttasche. Das linke Glas vergrößerte, das rechte hatte ihr
kleines Geschwister mit einem Bild bemalt, das Nurek immer
mit einem warmen Gefühl betrachten musste, bevor sie die
Brille in ihrem Gesicht platzierte. Das Glas war auch in
die neue Brille umgezogen, nachdem ihr Auge gelasert worden
war und sie statt des konkaven nun ein konvexes Glas brauchte, weil
ihr manche Dinge eben zu klein waren und sie vergrößert
werden mussten. Insgesamt eine Verbesserung, aber Nurek
hatte sich eigentlich mehr erhofft.

Das Bild, dass ihr Geschwister auf das nicht gebrauchte
Brillenglas gemalt hatte, weil Nurek geburtlich kein rechtes Auge
hatte, war nicht abgesprochen gewesen und Nurek war sich erst
nicht sicher gewesen, ob sie sich hätte ärgern sollen. Aber
ihr Geschwister hatte einen Mondaufgang gemalt, mit dem
C-Zeichen für die komplexen Zahlen als Mond, und hatte auch
sonst nicht mit versteckten mathematischen Symbolen gespart. Nurek
hatte nie anders gekonnt, als das Bild wertzuschätzen.

"Du machst viel dein eigenes Ding.", riss sie eine Stimme
aus dem Gedanken, die zu einer Person gehörte, die sich
neben sie gestellt hatte.

Nurek erschreckte sich heftig. Das passierte immer, wenn ihr
Fokus auf etwas anderem haftete und jemand sich zu leise näherte. Wobei, es
passierte auch, wenn Leute sich laut näherten.

"Oh, das tut mir leid, das wollte ich nicht.", sagte die
Person, "Störe ich? Soll ich lieber wieder gehen?"

Nurek wandte ihren Blick vom Druckvorgang ab und dem Ork neben
ihr zu. Er trug eine Latzhose und darunter ein rot-schwarz-kariertes,
weiches Hemd. Er war nur etwa einen halben Kopf größer als sie
und trug das dunkelbraune Haar, dass vorwiegend in der Schädelmitte
wuchs, etwa schulterlang. Es verteilte sich nicht in irgendeiner
beabsichtigt dahindrappierten Weise über seinen Kopf. Freundlicherweise
trug der Ork ein Namensschild:

> *Tissan*
>
> *er/sein-/ihm/ihn*

Nurek trug keines. Vielleicht sollte sie es nachholen, sich eines
zu drucken. Direkt am Eingang des Hackspaces stand ein Label- und
Buttondrucker.

"Verstehst du mich?", fragte Tissan.

Nurek nickte. Dann erinnerte sie sich zurück, was er vorher gefragt
hatte. Der Grund, warum sie nicht direkt darauf geantwortet hatte, fiel
ihr auch wieder ein. Also, der Grund, neben dem, dass sie zu abgelenkt
von seinem Erscheinen war. Neue Personen belegten bei den ersten Begegnungen
immer ihren vollen Arbeitsspeicher einfach nur, indem sie irgendwie
aussahen, sich irgendwie bewegten, eine Stimme hatten oder
irgendwie rochen.

Das erste, was Tissan gesagt hatte, war lediglich eine Feststellung
gewesen: Sie machte tatsächlich viel ihr eigenes Ding. Was er damit
zum Ausdruck hatte bringen wollen, neben der offensichtlich zutreffenden
Aussage, also, warum er ihr das mitteilte, keine Ahnung.

Die Frage, ob er störe, deutete darauf hin, dass er mehr interagieren
wollte, aber sie war sich nicht sicher. Vielleicht wollte er auch einfach
genau da stehen. Das würde sie nicht stören. Solange sie wusste, dass es
das war, was er wollte. Es störte sie, nicht zu wissen, worauf er
hinaus wollte.

"Möchtest du etwas von mir?", fragte sie, um der Antwort näherzukommen.

"Nicht unbedingt.", sagte Tissan und grinste. Es war eine absolut unhilfreiche
Reaktion, aber immerhin beließ Tissan es nicht dabei. "Wenn du einfach
dein eigenes Ding machen möchtest, ist das absolut akzeptiert. Wenn du aber
mehr dazugehören möchtest, möchte ich mich anbieten, zu unterstützen, falls
du dabei Hilfe brauchst. Ich kenne fast alle hier. Ich höre gern zu und
setze mich gern für Leute ein."

Das Hilfsangebot brachte Nurek aus dem ohnehin bereits angeknacksten
Konzept. Sie hatte eigentlich dem Druckvorgang für die Platine zusehen
wollen. Sie hatte ein längeres Bastelprojekt vor. Es ging um eine
Orientierungshilfe für blinde und sehbehinderte Personen, oder solche, die
einfach Spaß an Experimenten mit Sinnen
hatten. Die Platine konnte unter die Schuhsohle
in einen Schuh gelegt werden und sollte durch Vibration oder Wärmeimpulse
verschiedener Stärke ein Gefühl dafür geben, wie hoch Stufen
wären, die sich auf einem Weg auftaten. Nurek hatte keine
Ahnung, ob das Device jemals diese Verwendung finden, oder
ob es völlig unpraktikabel sein würde. Sie hatte vor allem viel
Spaß, selber daran herumzubasteln, und nachdem sie es in einer Virtualität
ausprobiert hatte, machte sie sich nun an die technische, reale
Umsetzung. Dazu hatte sie vor einer Woche begonnen, das nächste Hackspace
zu besuchen, um den Platinendrucker mitzubenutzen. Es war ihre
vierte Platine. Sie hoffte, dass sie nicht mehr Versuche brauchen
würde. Sie testete alles vorher in Virtualitäten für möglichst
wenig Verbrauch, aber die Umsetzung in die Realität klappte bei
so komplexen Ideen trotzdem selten direkt, vor allem bei Personen, die
bisher wenig Realitäts-Erfahrungen hatten. aber Nurek
war ehrgeizig.

Tissan stand immer noch vor ihr und wartete geduldig. Ob er
es schon kannte? Ob er häufiger Personen Hilfe anbot, die ihn
dann verloren ins Gesicht starrten, ohne es richtig
wahrzunehmen? Er ließ sich jedenfalls nicht anmerken, dass ihn
irgendetwas störte.

"Ich bin neuroatypisch.", erklärte Nurek sich, "Kognitive
Hyperaktivität, autonomer Fokus, Reizfilterschwäche, aus
der Ecke."

"Damit bist du hier nicht allein.", sagte Tissan grinsend.

Nurek überraschte das im Prinzip nicht. 
