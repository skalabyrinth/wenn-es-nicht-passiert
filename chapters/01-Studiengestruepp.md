Studiengestrüpp
===============

\Beitext{Marim}

*EnDe: Ich möchte mich gern in ein riesiges Minzblatt einwickeln.*

Der Satz tauchte kurz vor Mitternacht in Marims Direktnachrichten
auf. Er hatte eigentlich ins Bett gehen gewollt, aber nun snappte sein Fokus
darauf und er verschob alle anderen Pläne. Er kannte
den Zusammenhang, aber stellte sich vor, er täte es nicht. Er liebte
die Vorstellung, gewisse Sätze außerhalb des ursprünglichen Zusammenhangs zu lesen. Eine wohlige
Verwirrung machte sich in seinen Gedanken breit. Es war außerdem die
erste Nachricht überhaupt von *EnDe* (oder vom *EnDe*?), weshalb
sich zum Out-Of-Context-Verwirrungsgefühl ein lustiges Chaosgefühl
gesellte. Dieser Kontakt fiel vielleicht gern
mit Türen in Häuser, schloss Marim schmunzelnd. Da könnte er womöglich
mithalten.

*Marim Präsenz: Wann?*

Die Antwort ließ nicht lange auf sich warten.

*EnDe: .)*

Kryptisch und kurz, eine feine Ergänzung zur ersten Nachricht. Aber nur Momente später
schrieb der Kontakt konkret:

*EnDe: Ich bin flexibel, aber wenn nicht jetzt, dann hätte ich gern 1 Tag
Vorlauf.*

Marim haderte kurz mit sich, dass er nicht über die andere Person
entscheiden sollte, was für jene zu viel Stress sein könnte. Typischerweise
meldeten sich vorwiegend Leute bei ihm, die genaue Planung bevorzugten
und die Spontanität stresste, selbst wenn sie zunächst
anderes behaupteten. Aber es war nicht an ihm, das für andere zu
entscheiden. Und *EnDe* hatte ohne Begrüßung und ohne
viel vorher zu fragen direkt einen konkreten Plan vorgelegt. Vielleicht
war *EnDe* tatsächlich spontan.

*Marim Präsenz: Wenn jetzt nicht zu stressig ist und du kein Problem mit Vorbereitungschaos
hast, gern jetzt.*

Er schickte *EnDe* gleich einen Zugangslink in eine Virtualität mit.

*EnDe: \*zieht EM-Anzug an.*

EM-Anzüge -- EM stand für elektro-magnetisch -- waren diese perfekt auf
den Körper angepassten Catsuit-artigen Anzüge, die heutzutage fast alle hatten, mit
einem sehr feinen Drahtgeflecht darin. Sie konnten damit ein elektromagnetisches Feld um den
Körper herumbauen, das als Gegenfeld zum elektromagnetischen Feld im Spielraum
Virtualitäten physisch spürbar machte. Stabile Zustände konnten in den Materialien
eingerastet werden, sodass für Leute mit Anzug eine Treppe da sein konnte, die
keine Energie für ihre Existenz sog, die für Personen ohne Anzug nicht da war. Wie
Magneten eben, nur sehr rasch umschaltbar.

*EnDe: \*ist in ca 0.1h da.*

Marim nahm sich die Zeit, noch einmal zu schmunzeln und sich voll darin hineinzufühlen. *EnDe*
verwendete diese beschreibende Form für im Augenblick ausgeführte Handlungen für sich, deren
Namen er vergessen hatte. Das taten manche Leute erst, wenn sie sich
länger kannten, aber manchen war es auch
egal. Marim war es sympathisch. Er war gespannt, was für eine Person ihn erwartete.

*Marim Präsenz: Ich warte dort auf dich.*

Er klappte den Faltrechner zu und legte ihn zwischen die zwei kleinen Kopfkissen, auf
die er anschließend das große Kopfkissen platzierte, das zuvor seinen gegen
die Wand gelehnten Rücken abgepolstert hatte, und kroch vom Bett, das er zugleich auch
als Sofa nutzte. Als er seine vergrößernde Brille mit der VR-Brille austauschte, kam
der Chat wieder in sein Blickfeld.

*EnDe: Ich bin etwas nervös.*

Das war er auch. Jedes Mal. Es hatten nun schon irgendetwas in der Größenordnung
von 500 Personen an seiner Studie teilgenommen. Mit jeder der Personen hatte
er sich über die vergangenen drei Jahre persönlich unterhalten und Protokolle
geschrieben. Aber er war immer noch jedes Mal nervös.

Marim beschloss, erst die 0.1h abzuwarten und nicht vorher noch einmal zu
antworten. Er wusste nicht was. Vielleicht hätte er 'Etwas?' fragen
können. Oder über seine Nervosität reden können. Aber dafür hätte er
sich, wenn, mehr Zeit nehmen wollen.

Er begab sich in das Nachbarzimmer und startete die Virtualität. Minze. Das
war ein interessanter Wunsch. Nun erst drang der eigentliche Wunsch
zu ihm durch. Sich in ein Minzblatt einwickeln, war ein wirklich schöner Wunsch.

In der Studie ging es darum, in einer Situation sehr präsent zu sein, voll im
Hier und Jetzt, und sich gleichzeitig fallen zu lassen. Aber statt das in
Umgebungen zu tun, die realistisch waren, -- was schon seit Jahrhunderten
als Technik galt, die sich positiv auf die meisten Psychen auswirkte --, hatte
Marim die Idee gehabt, zu untersuchen, wie es sich mit völlig unrealistischen
Umgebungen oder Ereignissen um Personen herum verhielt. Es war komplex
zu erforschen. Zunächst betrachtete er Umgebungen, die sich die
Teilnehmenden selbst als welche ausdachten, die sie außerhalb von
Virtualitäten nie erleben würden, von denen sie sich aber einen guten Effekt
vorstellen könnten. Die Ideen waren sehr individuell. Mit
diesen wenigen Vorgaben war es schwierig, ein Auswertungsskelett zu
formen, sodass aus den Versuchen eine sinnvolle Studie würde. Marim wusste,
dass er sich irgendwann ein konkreteres Forschungsziel von nur einem Teil der Idee setzen, oder
die Idee konkretisieren sollte. Aber auch das wollte er mit System machen: Indem
er erst einmal den Versuch so schwammig immer wieder mit verschiedenen
freiwilligen Individuen ausführte, um herauszufinden, was es für Muster gab,
was er beim Konkretisieren bedenken musste. Das tat er nun seit etwa
drei Jahren. Er konnte sich keine spannendere Arbeit vorstellen. Die
sich ergebenden Unterhaltungen und Kontakte begeisterten ihn. Eine Begeisterung, die
oft Atem raubte, Pläne durcheinander schmiss, ihn wuselig und hibbelig
machte und dieses wunderschöne Gefühl von starkem Fokus und
Klarheit mit Glückseligkeit in seinem Kopf fabrizierte. Und
als Nebenprodukt Nervosität.

Statt selbst zu entscheiden, welche Minze wohl am angenehmsten zum
Einwickeln war, lud er einfach eine komplette Datenbank verschiedener Minze und
pflanzte automatisiert je ein Büschel jeder Minze in seine Virtualität. Das Ergebnis
war unpraktisch. Die Minze erstreckte sich einfach in alle Richtungen zum Horizont. Die
Datenbank war zu groß und zu fein unterteilt. Ein Schnauben hinter ihm ließ ihn
herumfahren.

"Es tut mir leid.", sagte *EnDe*. "Ich bin in furchtbar alberner Stimmung und lache
wahrscheinlich über alles. Ich hoffe, ich bin trotzdem irgendwie erträglich."

Marim grinste. "Ich mag Albereien, also gehe ich davon aus." Seine Antwort
kam ihm unbeholfen vor. Er wollte *EnDe* an sich Sicherheit geben. Aber heute
schien sein Talent dafür im Urlaub zu sein. "Marim Präsenz. Er, sein, ihm, ihn. Aber
an sich wusstest du das schon."

"Nurek. Sie, ihr, ihr, sie.", stellte sich Nurek vor. Sie stand barfuß
auf dem Erdboden, was nicht untypisch für Lobbuds war -- und ein Lobbud
war sie. "Ich bin zwar
relativ klein, aber um mich in eins von diesen Minzblättern einzuwickeln, müsste
ich trotzdem noch sehr schrumpfen, oder die Pflanzen müssten wachsen. Machst du dafür
Regen? Oder sind die Pflanzen vielleicht weiter hinten größer?" Nurek
blickte Richtung Horizont und drehte sich im Kreis.

"Ich habe mir noch keine Gedanken gemacht, ob ich uns skaliere oder eine
Pflanze. Ich war mehr bei der Frage stecken geblieben, welche Minze, und
wie ich dir eine gute Möglichkeit geben kann, zu wählen.", erklärte Marim. Ein
ziemlich starker Teil von ihm wollte mitalbern. Er reflektierte, dass
es daran lag, dass Personen sich oft sicherer fühlten, wenn andere
Personen bei ihrem Verhalten in gewissem Umfang mitmachten. Machten
sie zu viel mit, war es gruselig. Machten sie gar nicht mit, dann
passierte es leichter, dass die Personen glaubten, mit ihrem Verhalten
zu nerven.

Nurek war in die Hocke gegangen und hatte angefangen, die Minzblätter
zu befühlen, etwas zu reiben und an ihren Fingern zu riechen. Marim
arbeitete, -- in Gedanken immer noch halb abwesend --, daran, dass
die Pflanzen, dünn wie sie waren, wuchsen, sodass sich Nurek nicht
bücken oder hinhocken müsste. Aber eigentlich war seine Reflexion von eben noch
nicht durch.

Vielleicht waren jene Überlegungen und Reflexe bezüglich Mitmachen
von Albernheit Masking. Er war neuroatypisch. Bei ihm gehörte
dazu, dass er Schwierigkeiten damit hatte, Subtext zu verstehen, oder
Emotionen von neuen Gesichtern abzulesen. Viele Leute kommunizierten nach
bestimmten Regeln, die für ihn nicht erfassbar waren, weshalb er
es durch so etwas wie Vorsichtsverhalten ausglich, bestimmtes
Kopieren, Nachahmen von Verhaltensmustern, die für ihn nicht viel
Sinn ergaben, aber ohne die es regelmäßig zu Missverständniskatastrophen
kam.

Allerdings hatte Nurek von Anfang an auf ihn nicht unbedingt
einen neurotypischen Eindruck gemacht. Er sollte so etwas nicht
schnell schließen und vor allem nicht fremddiagnostizieren. Aber
er lag schon sehr oft richtig mit seinen Einschätzungen. Er überlegte
zu fragen.

"Für eine systematische Abwägung über die gesamte Datenbank hinweg haben
wir wohl nicht die Zeit, aber ich habe mir diese Minze hier
ausgesucht.", sagte Nurek. "Die Unterschiede der jeweiligen
Optima innerhalb der letzten 20 Testungen waren sehr gering, während
sie bei den ersten 20 noch sehr groß waren. Wenn es also keine Minze gibt, die
sich doch noch sehr stark von lokaler Minze unterscheidet, dann ist
auch nicht mehr viel rauszuholen."

"Forschst du auch?", fragte Marim.

"Hm. Ja? Nein?", antwortete Nurek, vielleicht von sich selbst
verwirrt. "Mich hat theoretische Physik mal fasziniert
und ich habe mal an einem Paper mitgeschrieben. Wie
typisch in theoretischer Physik ging es um
Energieminima von irgendwelchen Zuständen. Ich habe mich
für die Methode und die Mathematik dahinter mehr begeistert, als
für die eigentlichen Zustände, und es ist Jahre her, also kann
ich nicht einmal mehr genau sagen, worum es überhaupt ging. Was
mit Spinstrom. Das war mein stärkster Berührungspunkt zu
Forschung."

Marim lächelte und nahm im Augenwinkel wahr, wie Nurek sein
Gesicht studierte, während er mit Gesten die Virtualität
veränderte. Er entschied sich dazu, außer der einen
Minzpflanze, die Nurek ausgesucht hatte, die Umgebung durch
einen Wildgarten zu ersetzen, ein paar Minzblätter auf den
Boden zu verteilen und Nurek und sich neben diesen
herunterzuskalieren, sodass Nurek sich ganz in eines der
Minzblätter einwickeln könnte.

"Warum frugst du?", fragte Nurek schließlich.

Marim lächelte noch etwas mehr, weil er die alternative
Vergangenheitsform mochte. "Du warst systematisch. Und
dein System erinnerte, nun ja, an Annäherungsmethoden
von Minima oder Genauigkeit." Er ärgerte sich einen
Moment, weil das nicht die Fachbegriffe dafür waren, die
er eigentlich gelernt hatte, und beschloss, es einfach
dazuzusagen. "Ich drücke mich nicht mehr so eloquent
aus, wie kurz nachdem ich das studiert hatte. Mein
Gedächtnis ist schlecht."

"Ich halte viel von mangelder Eloquenz. Dann haben fachfremde
Leute mehr Chancen, hinterherzukommen und zu verstehen.", entgegnete
Nurek grinsend.

"Die beste Eloquenz wiederum ist die, die präzise und zugleich
leicht verständlich ist.", widersprach Marim.

"Gibt es die?", fragte Nurek.

"Ich dachte schon." Marim überlegte einen Moment. "Oder ich bin
vom Elternhaus schon verschiedene Fachsprachen gewohnt, sodass
es mir nicht auffällt. Aber ich kenne diesen Drang mancher
Leute, mit Fachbegriffen um sich zu werfen, während es allgemein
bekannte Begriffe gibt, die eins zu eins das Gleiche bedeuten. Und
ich mag den Trend, auf letztere zurückzugreifen."

Nurek blickte ihn an und nickte dann sehr sachte, als wäre sie sich
nicht sicher, ob das gerade eine sinnvolle Geste war. "Ich glaube, ich
auch. Ich stecke nicht tief genug drin.", sagte sie schließlich. "Wollen
wir die Blattsache machen?"

Marim nickte und grinste wieder.

"Muss ich etwas beachten?", fragte Nurek. "Oder wickele ich mich jetzt
einfach ein und fühle."

"Wenn dir die Umgebung zusagt, dann letzteres.", ermutigte Marim. Als
Nurek sich unsicher umblickte, vielleicht nicht so glücklich wirkte, fügte
er hinzu: "Ich kann sie beliebig umkrempeln. Wir haben Zeit."

Nurek holte tief Luft und ließ sie wieder entweichen. "Ich
glaube, frische Minzblätter fallen nicht einfach so auf den Boden. Wenn
sie gepflückt werden, dann vergehen sie da. Was okay ist, aber nicht
so eine positive Vorstellung. Ich hätte sie lieber neben einer
Teekanne, wo sie vielleicht nach dem Einwickeln zu Tee verarbeitet
würden."

Marim musste unwillkürlich warm lächeln. "Ich mag dein Gehirn. Bis
jetzt zumindest.", sagte er leise und begann, die Umgebung umzubauen.

"Ich deins auch, glaube ich.", sagte Nurek, auf einmal viel spürbarer
unsicher als bisher. "Ich folge dir schon eine ganze Weile auf
*Shortspread*, vor allem wegen dieses Projekts. Es ist großartig."

"Danke!", sagte Marim. Das Grinsen, das nun in sein Gesicht
trat, fühlte sich fast wie ein leichter Krampf an, ein positiver
allerdings. "Ich glaube, ich werde ein paar Tage grinsen müssen."

"Bekommst du selten dafür Komplimente?", fragte Nurek.

"Nicht selten. Und auch nicht oft. Aber Komplimente zu meinem
Herzprojekt bringen mich jedes Mal tagelang mindestens
innerlich zum Grinsen.", erklärte Marim.

Inzwischen standen sie auf einer Tischplatte neben einer Teekanne. Marim
eröffnete Nurek einfach das Menü, sodass sie mit auswählen konnte, welche
Maserung der Tisch haben sollte. Nurek schien zunächst überfordert, aber
als Marim ihr etwas Zeit ließ, begriff sie überraschend schnell. Sie
wählte eine Glaskanne, in der noch ein Rest Tee vom letzten Aufguss
war und die noch etwas Wärme abstrahlte. Marim musste ihr nicht
einmal zeigen, dass es Einstellungen für Temperatur und
Geruch gab. Sie fand es von selbst. Dabei war es kein
Standardmenü. Unpraktischer Weise teilten sie die Eigenschaft, Erforschen
zu müssen. Das Zimmer, in dem der Tisch stand, war am Ende sehr
individuell eingerichtet für eine Virtualität, die eigentlich nicht lange
halten würde, und zwar nicht, weil Nurek genaue Vorstellungen hatte, sondern weil
sie den Umfang der Einstellmöglichkeiten ausprobieren wollte.

Marim ließ sie machen. Er war müde, aber ihr zuzusehen war auch zu
schön. Und im Zweifel würden sie die eigentliche Minzblattsache eben
verschieben. Er fragte sie vorsichtshalber, ob das in ihrem Sinne
war. Sie nickte abwesend. Sein Verdacht, dass Nurek auch irgendwo
auf dem neuroatypischen Spektrum sein könnte, verhärtete sich.

"Siehst du eigentlich zu, während ich mich ins Minzblatt wickele?", fragte
Nurek.

"Das darfst du dir aussuchen.", antwortete Marim. "Für die Studie ist es
vorteilhaft, wenn ich Daten bekomme, aber du darfst auch jederzeit entscheiden, dass
du doch nicht willst. Nichts ist verschwendet, mach dir da keine Sorgen um
mich. Die meisten entscheiden sich dazu, dass ich dabei bleibe und währenddessen
Fragen beantworte, Hinweise gebe, daran erinnere, dass sie sich in die Situation
fühlen möchten, und Fragen stelle. Aber es steht dir frei, das anders
zu entscheiden."

Nurek wirkte nachdenklich und kniff dabei die Augen etwas zusammen. "Hmm.", machte
sie. "Es reizt mich ja, Dinge anders zu machen als andere. Aber ich glaube, ich
fände einfach interessanter, wenn du dabei bleibst. Kannst du denn noch?"

Marim nickte nach kurzem Zögern. Es würde ihm ohnehin leichter fallen zu
schlafen, wenn dieser Versuch abgeschlossen wäre. Selbst, wenn er also
jetzt eigentlich etwas zu müde war, würde er am Ende nicht mehr Schlaf
bekommen, ob er es durchzog oder nicht.

Nurek ging ein weiteres Mal ins Menü und ließ einen altrosa Ohrensessel entstehen. Einen,
der in Marims Standardauswahl aufgelistet war, und in dem er auf seinem Profilbild saß. Nurek
grinste ihn an, als sie die Schablone in für ihn passender Größe neben Teekanne und
Minzblatt materialisierte, wie so ein Haus in einem Zivilisationsaufbauspiel, und er
grinste zurück. Er streckte den Rücken durch und nahm auf elegante Weise
darauf Platz, etwas in das linke Sesselohr gelehnt, das eine Bein über die rechte
Lehne baumelnd, das andere über das eine geschlagen, Füße gestreckt. Er zog die
langen, bunten Ringelstrümpfe noch einmal glatt, um das Manöver zu vollenden. Er
mochte durchaus seine eigenen Beine ansehen. Gerade wollte er allerdings eigentlich
ausschließlich auf Nurek und den Versuch aufmerksam sein. Aber bei der Erinnerung
an sein Profilbild hatte er die gleiche Haltung wie eben darauf einnehmen
wollen, und hoffte, dass Nurek das nicht störte. Sie grinste immer noch, auf
eine Weise, die sehr aufmerksam und warm wirkte.

Während Nurek sich nun mit dem Minzblatt auseinandersetzte, -- zunächst, indem
sie es anblickte und sich Gedanken machte --, funktionierte der Versuchsaufbau
bei ihm bereits. Er wurde sich sehr bewusst über das Hier und Jetzt. Es war ein
guter Versuchsaufbau. Er saß in einem gemütlichen Sessel auf elegante Weise neben
einer Glasteekanne mit einem Rest Tee darin. Einer Glasteekanne, die ihn um einiges
überragte und in der sich sein Sessel sachte spiegelte. Er hatte
die unbehandelte Tischmaserung in den Füßen gespürt. An sich
kannte er es, in Räumen zu sein, in denen er ungewohnt klein war, manchmal auch
ungewohnt groß. Das war keine seltene Idee von Versuchspersonen. Aber ein Minzblatt
neben einer Teekanne gab dem Raum eine wunderschöne Behaglichkeit. Auch das
Zimmer an sich: Nurek hatte keine Einrichtung gewählt, die Stereotyp auf Gemütlichkeit
ausgelegt gewesen wäre, wie es in vielen rustikalen Virtualitäten oder
Märchenvirtualitäten der Fall war, sondern eine,
die eher an ein echtes Szenario erinnerte, in dem
eine Person ein Heim mit Restemöbeln quer aus der Verwandtschaft, was bei jenen jeweils
so übrig war, eingerichtet hätte.

Nurek legte sich auf ein Ende des Minzblattes und wickelte sich endlich
darin ein. Sie wickelte sich auch direkt wieder aus und probierte es
noch zwei oder drei Mal, bis sie zufrieden war, der Kopf bis zum Mund
herausschaute, es ihr nicht zu fest und nicht zu lose war. Abschließend
nahm sie die obere Kante des Minzblattes in den Mund. Nur
ein paar Momente. "Ich bin nervös.", sagte sie, was sie vorhin schon gesagt hatte.

"Das verstehe ich, das geht vielen so.", sagte Marim. Er änderte die
Anspannung in der Körperhaltung auf eine Weise, auf die er schon oft
erlebt hatte, dass sie entspannenden Einfluss auf andere hatte. Wieder
musste er an Masking denken. "Hast du eigentlich eine EM-Zahnspange?"

Nurek nickte. "Ich teste Aroma-Devices. Es gibt dafür noch nicht einmal
einen coolen etablierten Namen. Viele halten das für Unfug und sagen
so Dinge wie: Dann könnten wir gleich einen winzigen Lebensmitteldrucker in
den Mund bauen. Dass es keinen Sinn ergäbe, Nahrung mit Gefühl und
Geschmack zu simulieren, weil wir ja auch einfach echtes Essen mit
in die Virtualität mitnehmen könnten, das von der Virtualität nachgebildet
wird, während wir es essen. Das käme immer näher an ein realistisches
Gefühl heran, wenn wir in Virtualitäten mit anderen essen wollten, als
ein simulierter Essvorgang es je könnte." Nurek machte eine
kurze Sprechpause, während jener sie Marim
über den Rand des Minzblatts anblickte. Keine
allzu gemütliche Haltung vermutlich. "Aber Kauen, und Dinge im Mundraum
untersuchen, oder auch schmecken, ist mein bester Stimm. Ich möchte
Holz zum Beispiel im Normalfall nicht essen."

"Ich verstehe das total.", sagte Marim, gar nicht mal so wenig
emotional. Ein kleiner Teil seines Gehirns meldete ihm leise
'Sie hat Stim gesagt.' "Ich sollte das auch ausprobieren und mich
an Studien beteiligen. Mindestens schon, um es mit in diese Studie hier
aufzunehmen, aber auch, weil Kauen, wie du sagst, einfach ein
guter Stim ist." Und dann fragte er es doch noch einmal laut, einfach
vorsichtshalber: "Du bist auch neuroatypisch, oder?"

"Ja!", rief Nurek nun wieder breit grinsend. "Was hat mich
verraten? Dass ich *EnDe* heiße? Wie ND für neurodivers?"

Tatsächlich hatte sich Marim bereits gefragt, wie der Username
zustande kam. Aber auf ND war er nicht gekommen. "Zuletzt war es
das Wort Stim." Und einen kurzen Moment, nachdem er geantwortet
hatte, fragte er sich, ob die Frage rhetorisch gewesen war.

Nurek sagte nichts dazu und grinste einfach weiter. Sie strich mit den
Fingern, die nahe ihres Kopfs aus dem Minzblatt lugten, über die weiche,
zackige Kante des Blatts. "Ich frage mich, ob ich Dinge richtig mache. Und
denke nun über die Zahnspange nach. Und darüber, ob ich dir auch
sagen sollte, dass ich das Gefühl von 'Ende' mag und auch deshalb
so heiße. Oder auch, weil sogar altmodische Screenreader es richtig
vorlesen, nur etwas betonter, wie so ein brutal
hervorgehoben artikuliertes 'Ende'.", gab sie zu. "Außerdem frage ich mich,
ob meine Idee wohl originell ist, oder ob viele Leute solche
Ideen haben wie, sich in ein Minzblatt einzuwickeln. Und ob ich
überhaupt vergleichen sollte. Und all dies lenkt mich davon ab, mich
in der Situation präsent zu fühlen. Das soll bestimmt nicht so, oder?"

"Die ersten Millistunden bis hin zu einer Zentistunde ist das häufig
der Fall.", widersprach Marim. "Vielleicht sollte ich darüber im
Vorfeld informieren, aber ich habe es bisher auch nicht so gemacht. Ich
finde gerade diesen Aspekt auch spannend, wie lange es braucht, dass
Versuchspersonen sich darauf einlassen." Marim machte eine kurze
Redepause, nicht nur, weil er zur nächsten Frage wechseln
wollte, sondern auch, um sein langes Haar hinter den Rücken zu
sortieren, das seinen Oberarm kitzelte. "Es ist schwierig, einzuordnen, wie
originell Ideen jeweils sind. Neben der Tatsache, dass es einfach
Geschmackssache ist. Es ist spannend, wie verschieden konkret die
Wünsche sind. Häufig kommt es vor, dass Personen sehr klein sein wollen, so
etwas wie, sich in einem Mauseloch verkriechen, oder sich an eine
Katze kuscheln, während sie selbst so groß wie eine Maus sind."

"Wünschen sich die meisten, dass die Katze ungefährlich für sie
ist?", fragte Nurek.

"Tatsächlich nicht nur die meisten, sondern bisher alle", konkretisierte
Marim. "Jedenfalls, hat diese Virtualität etwas sehr Eigenes, Individuelles
an sich. Das macht sie vielleicht originell?" Er dachte einen Augenblick
nach. "Aber so originell, wie eine Vielzahl Individuen nun mal alle
für sich genommen sind? Hilft dir die Antwort?"

"Sehr!", sagte Nurek und wirkte tatsächlich erleichtert. "Es
reduziert auch das bohrende Gefühl, dass ich eigentlich was
Besonderes sein will, aber mich schlecht fühlen würde, über andere
zu denken, dass sie weniger besonders wären. Oder die Angst, dass
ich dich unter Druck setzen könnte, eine Bewertung vorzunehmen, die
mich irgendwie anderen vorzieht oder umgekehrt. Es bestätigt, ohne
das zu tun."

Sie lag einige Momente still in dem Minzblatt eine Körperlänge
von seinem Sessel entfernt und schloss das Auge. Erst dadurch
wurde Marim bewusst, was er die ganze Zeit schon halb unterbewusst
bemerkt hatte, -- dass ihre zweite Augenhöhle leer war, bis
gerade nur fast geschlossen. Es waren anfangs mit neuen Personen
einfach immer so viele Eindrücke, dass er nicht wusste, wie er
sortieren sollte und solche Auffälligkeiten untergingen. Nun war
sein Kopf nur noch mit zwei Dingen maßgeblich beschäftigt: Mit
der Frage, die gerade auf dem Stack lag, die er aber verschieben
sollte, falls Nurek es gerade schaffte, im Hier und Jetzt
anzukommen, und mit dem Wort 'Burrito'. Personen in seinem
Umfeld, die sich in etwas einwickelten, bezeichneten diese
Pose inflationär so. Deshalb dröhnte dieses eine Wort nun alle
paar Momente ungefragt durch seine Gedanken. Burritos waren
im Wesentlichen Nahrung eingerollt in Fladen, sodass sie leichter
transportiert werden konnten, oder auch für Resteessen. Sie waren
eine Zeitlang durch gedruckte, essbare Brotdosen halb verdrängt worden,
aber dann, als Lebensmitteldrucker
begannen, Teigfladen ordentlich hinzubekommen, und als eine
Vintage-Community angefangen hatte, nostalgisch
Serien von früher wieder hervorzukramen, in denen Burritos
ein Thema gewesen waren, waren sie zurückgekehrt. Sie waren vorübergehend
ein Meme geworden, auch als Personenburritos. Marim
fragte sich, wie kulturübergreifend das
Prinzip von Burritos wohl war.

"Wie viel veröffentlichst du hiervon eigentlich? Und
in welcher Form?", fragte Nurek.

"Nichts, ohne dein Einverständnis.", versicherte Marim. "Ich schreibe
ein Protokoll, das ich dir gebe, in dem du Dinge ergänzen oder
korrigieren kannst, sollte ich etwas missverstanden oder vergessen haben. Das Protokoll
speichere ich privat, solange du es erlaubst, um später Daten daraus
erheben zu können. Dabei geht es erstmal nicht um statistisch
relevante Daten, sondern darum, Möglichkeiten für Abfragen
abzustecken. Wenn ich dann soweit bin, daraus einen Fragebogen
abzuleiten, fange ich mit den Versuchen von vorne an. Parallel
verteile ich Fragebögen an alle, die bisher teilgenommen
haben. Als zwei getrennte Versuche, einen sauberen und einen, in
dem die bisherige Arbeit eingeordnet wird. Für all diese nachträglichen
Auswertungen und Fragen erfrage ich getrennt dein Einverständnis
bezüglich Veröffentlichung."

"Ich kenne, dass ich mit einer KI allgemein eingerichtet habe, was
für Daten ich gerne unter welchen Bedingungen weitergebe und welche
nicht. Ist das mit deinen Methoden abgleichbar?", fragte Nurek.

"Das macht es zumindest einfacher. Wahrscheinlich reicht das aus. Ich
würde dich dann nur für irgendwelche abstrusen Sonderfälle
extra fragen." Marim kannte diese Systeme. Fast alle verwendeten
so etwas in vereinfachter Form, wenn es um generelles Datensammeln für
Experience-Verbesserung bei Software oder Virtualitäten ging. Speziell für Studien
war es seltener, dass Leute sich so etwas eingerichtet hatten, weil
die Einstellungen viel feiner waren. "Nimmst du häufig
an Studien teil?"

"Ja!", sagte Nurek. "Ich liebe Studien! Ich liebe auch dein unvollständiges
Studiengestrüpp und hatte mich schon gefragt, ob es bei diesem eben dazu
kommen könnte, dass die Voreinstellungen bezüglich Datenfreiwilligkeit
nicht ausreichen."

Sie waren schon wieder bei anderen Themen als bei der Minzblatt-Sache, stellte
Marim fest. Dann konnte er auch die Frage stellen: "Wie kamst du eigentlich
auf die Idee mit dem Minzblatt?" Nach kurzem Zögern fügte er hinzu: "Das
ist sogar eine Frage, die ich allen stelle, also eine, die wahrscheinlich
studienrelevant ist."

Nurek blickte ihn einen Moment lang über den Rand des Minzblattes hinweg
an, wieder in dieser Haltung, die auf Dauer ungesund für den
Nacken sein würde. Solange sie das EM-Feld nicht unterstützend
hinzunähme, woran die meisten in so einer Haltung nicht dachten, aber
zu Nurek könnte es passen, dass sie es täte. "Ich denke über deine Studie schon
seit einem halben Jahr nach und finde sie gut. Ich wollte eine Idee
haben.", gab sie zu. "Und dann saßen wir heute Mittag draußen im Garten,
neben den drei verschiedenen Minzsorten. Nur drei!" Nurek hielt
die drei mittleren Finger der einen Hand unter der Kante des Minzblattes hervor. Die
Ellenbogen blieben zugedeckt. Es sah gemütlich aus. "Und ein Mitbewohn
meinte, die eine Minze wäre so weich mit dem zarten Haarflaum
darauf. Da kam mir der Gedanke 'Ich möchte mich am liebsten in
einem Minzblatt einwickeln!' Also habe ich dir geschrieben, als
ich das nächste Mal Ruhe hatte."

Marim spürte dieses Gefühl von Glückseligkeit, das ihm für ein paar
Momente die Luft abschnürte. Es kam, weil Nurek sich so für seine
Arbeit begeisterte, seine Faszination so sehr teilte. Es war selten
so intensiv. Die Bewunderung durch eine fremde Person kam ihm aber
auch ein wenig seltsam vor, vielleicht auch eine Spur unbehaglich,
nur eine Spur. Sie kannten sich erst so kurz, und es war so intensiv. Er
mochte Chaos, aber hier war auch viel Potenzial für Missverständnisse
bei starken Gefühlen. Immerhin lediglich fachliche gemeinsame
Begeisterung.

"Was ist jetzt an der Reihe?", fragte Nurek in die Stille hinein, die
entstanden war.

"Eine Zentistunde Schweigen.", erklärte Marim. "Wobei du sie abbrechen
darfst, wenn es dir irgendwie unbehaglich wird. Auch abbrechen und
wann anders wiederholen." Er lehnte sich in den Sessel zurück
und schlug beide Beine seitlich unter den Körper, weil mit mehr oder
minder fast ausgestreckten Beinen das Denken
schwer fiel. "Dabei ist die Idee, dass du dich in
die Situation hineinfühlst, die Umgebung genau wahrnimmst, und beobachtest, was
es mit dir macht. Ob es entspannt, begeistert, verwirrt, was für Emotionen
es hervorruft. Und ob du loslassen kannst, dich fallen lassen kannst, und
zugleich präsent bleiben kannst. Hast du noch Fragen?"

"Was, wenn ich einschlafe? Und auch sonst, woher weiß ich, dass die Zentistunde
um ist?", fragte Nurek.

"Ich stelle mir einen Wecker und sage dir dann Bescheid." Marim überlegte, dass
vielleicht wahrscheinlicher wäre, dass er einschliefe.

"Dann kann das losgehen.", sagte Nurek.

"Ich leite es mit einem leisen 'Pling' ein. Ist das recht?", fragte
er noch.

Nurek nickte, wobei sich der Rand des Minzblattes faltete.

Er ließ das Pling-Geräusch sachte durch die Virtualität erklingen. Er
mochte es. Er hatte es sich von Entspannungs-Sessions abgeschaut, die
sich zuerst im Südosten Maerdhas entwickelt hatten. Er hatte auch über
die Art des Forschens versucht zu lernen, die aus denselben
Kulturzusammenhängen hervorgegangen waren. Er war in einigen
virtuellen Seminaren gewesen, in denen er die verschiedenen kulturellen
Einflüsse in die Art und Weise zu forschen schätzen gelernt hatte.

Das helle Pingen war wie eine Trennung zweier mentaler Zustände
voneinander und löste für ihn dadurch schon beim Hören eine
entspannte Wachheit aus. Diese Konditionierung war beabsichtigt
und sehr alt.

Nurek hielt die Augen geschlossen und lag ganz still da, die
Kante des Minzblatts im Mund. Sie hatte eine weiche, mittelbraune
Kurzhaarfrisur und hellbraune Haut. Marim rief sich ihre Kleidung
in Erinnerung, was ihm gar nicht mal so leicht fiel. Sie war
dunkel, vielleicht schwarz, oder ein beliebiger Farbton in einer
dunklen Schattierung, wahrscheinlich nicht braun. Ein Stoff mit
einem Dino-Muster oder war es ein Drachen-Muster? Er konnte sich
nicht einmal daran erinnern, ob es unten ein Rock oder eine weite
Hose, und ob es an der Taille geteilt in zwei
Kleidungsstücke gewesen war. Er wollte
unbedingt genau nachschauen, wenn sie sich wieder auswickelte.

Das Minzblatt um sie herum folgte ihren Atembewegungen. Es wirkte
sehr entspannt. Der Eindruck konnte völlig täuschen. Ihm zumindest
sahen nur Leute an, dass er unentspannt war, wenn er es war, die ihn
sehr gut kannten. Er bemerkte es an sich selbst oft genug zu spät.

Er hörte auf, sie zu beobachten, stellte für sie seinen Ton ab und
befasste sich damit, sein Gedankengestrüpp zur Virtualität in für ihn gut
sortierte Worte zu übersetzen, bis der Wecker klingelte. Es war gut, dass er für sie
den Ton abgestellt hatte, denn er erschreckte sich sehr. "Wenn du
magst, kehre langsam in die Wirklichkeit der Virtualität zurück.", sagte
er leise und möglichst sanft.

Nurek prustete. "Die Wirklichkeit der Virtualität. Du hättest auch
sagen können: 'Wenn du magst, kehre langsam in die Unwirklichkeit
zurück.' Und irgendwie wäre das richtig gut gewesen!"

Marim lächelte. "Ich habe es anfangs von Entspannungskursen
übernommen, die ungefähr so enden. Es hat nie so richtig gepasst. Und
nun fühlt es sich falsch an, etwas anderes auszuprobieren, aber vielleicht
sollte ich."

"Wenn, dann nimm vielleicht doch nicht meinen Vorschlag. Der
führt auch zu lachen.", riet Nurek.

"Magst du einmal, solange es noch frisch ist, von deinen Gedanken
im Minzblatt berichten?", bat Marim. "Wir können mehr auch später
protokollieren, oder du schreibst mir einen Text dazu, wenn du später
noch einmal darüber nachdenkst. Aber das werden meistens andere Gedanken
sein als die frischen."

Nurek nickte. "Abgesehen davon, dass der Satz 'Es geht hier eigentlich
um Eskapismus' sich versucht hat, in meinem Kopf breitzumachen, habe
ich es tatsächlich genießen können.", berichtete sie. "Es ging viel besser,
im Hier und Jetzt zu bleiben, als bei Bodyscans, die ich häufiger
versucht habe, weil das Minzblatt meine Präsenz eingefordert hat. Ich
habe dann so etwas gedacht wie: Du bist hier in einem Minzblatt
eingewickelt. Ich mag den Geruch, ich wünschte mir Geschmack, ich
mag die Haptik im Mund. Um den Körper hat es sich nicht viel interessanter
als eine Decke angefühlt, das hätte ich mir beeindruckender
vorgestellt. Vielleicht muss ich demnächst Minzblattkleidung direkt
auf der Haut ausprobieren."

Nurek machte eine nachdenkliche Pause im Redefluss, in der Marim
schmunzelte, weil er sich gerade kurz zuvor Gedanken über ihre Kleidung
gemacht hatte.

"Ich konnte ruhig atmen.", fügte Nurek hinzu, langsamer redend
als vorhin. "Eigentlich möchte ich mich wieder auswickeln und
kann deshalb gerade nicht nachdenken."

"Wickel dich aus!", forderte er sie auf.

Sie folgte der Anweisung, betrachtete die Spuren, die sie
im Minzblatt hinterlassen hatte, stand auf, streckte und dehnte
sich. Sie hatte einen weichen, runden, gelenkigen Körper. Bei der Kleidung
handelte es sich um ein nachtblaues, figurbetontes Kleid mit
Dinos und Drachen. Die Drachen waren nur ein paar Ausnahmen
zwischen den Dinos. Nun erinnerte er sich an die leichte
Irritation, die er vorhin bei der Betrachtung der Kreaturen
gehabt hatte, für die er sich nicht die Gehirnkapazität genommen
hatte, zu analysieren, um was für eine es sich gehandelt hatte, weil
andere Gedankendinge vorher dran gewesen waren.

"Besser.", sagte sie. Sie drehte den Kopf zur Seite und roch
an dem eng anliegenden Träger des Kleides, den sie dazu mit
drei Fingern der Nase entgegen dehnte. Sie lächelte. "Das
kriegen olfaktorische Emitter natürlich auch nicht so
gut hin. Punktuelle Gerüche. Es gibt noch viel zu tun. Aber
nach Minze riecht das wohl noch."

"Ich frage mich, wenn lokale Gerüche umgesetzt werden, wie
das wohl gemacht wird.", sagte Marim nachdenklich. "Aromenmischer
brauchen eben schon Platz, das ist nicht so einfach, sie überall
hinzuplatzieren."

"Aktuelle Entwürfe arbeiten mit nur wenigen Aromenmischern, an die
jeweils ein dünnes, nicht störendes EM-Tuch befestigt ist, in das wiederum
ein Netz an sehr feinen Leitungen verarbeitet ist.", erklärte
Nurek. "Sowas wie Vorstufen der Gerüche werden dann mit im Netz
gespeicherten Aromen schonmal vorkreiert, und zwar sehr in der
Nähe der Nase, sehr lokal, und dann wird mit etwas Zeitversatz
ein präziserer Duftstoff aus dem Emitter nachproduziert und
hinterhergeschoben. Die Fließgeschwindigkeiten durch die
Leitungen sind das Bottleneck. Fast im wahrsten Wortsinn."

"Das ist spannend. Steckst du irgendwie in der Forschung?", fragte
Marim neugierig.

"Ich nehme an Studien teil. Sonst nicht.", informierte
Nurek.

"Bekommst du dann Geräte zugeschickt und testest es zu Hause
aus? Oder fährst du an Orte dafür?", fragte Marim.

"Ersteres.", antwortete Nurek. "Wir sind bei uns ziemlich
gut ausgestattet. Das meiste dockt gut an."

"Cool!" Marim hielt sich davon ab, sich spontan in das Thema
hineinvertiefen zu wollen. Er neigte dazu, sich mit zu
vielen Dingen gleichzeitig auseinandersetzen zu wollen und
zu viele Projekte anzufangen. Es war an sich nicht schlimm, aber
eine Nacht drüber zu schlafen, bevor er so eine Entscheidung
fällte, räumte mit seinen Ideen in einer Weise auf, dass es
ihn dann weniger überforderte.

Als nächstes stellte er sich vor, wie Nurek auch jenen
Personen, die Studien zu olfaktorischen Devices machten, ihre
Begeisterung so mitteilte wie ihm. Wenn jene sich etwa so freuen
würden wie er, wünschte er es ihnen.

"Du bist sehr motivierend, weißt du das?", fragte er.

Nurek fing zu grinsen an und blickte ihm ins Gesicht. "Das habe
ich noch nicht sehr oft gehört. Und es freut mich sehr!"

Marim beschloss, die Fragen wieder aufzunehmen. "War für dich
das Präsenzgefühl positiv, eher unangenehm zum Beispiel anstrengend,
oder etwas anderes?"

"Ich fand es gut. Es war ein Mindset, das ich mag. Sich real
fühlen, in einer nicht realen Situation. Eine leichte, geistige
Herausforderung, die ich sehr mag.", beschrieb sie. "Der
eigentliche Grund, warum ich diese Studie so gut finde, glaube
ich."

Sie hatte es schon wieder gesagt. "Du gibst fast gruselig
viel positives Feedback zur Studie.", sagte Marim schließlich,
und hoffte, sie damit nicht zu erschrecken oder zu beleidigen. "Bist
du mehr hier für das Präsenz-Erlebnis oder mehr, weil du die Studie
magst?" Ihm wurde plötzlich sehr heiß. Hatte er nicht gerade noch vor wenigen
Sätzen quasi das Gegenteil gesagt? Dass sie motivierte?

"Ist das eine Frage, die du anderen stellst?", fragte Nurek zurück.

Marim schüttelte den Kopf.

"Ich finde die Frage schwierig. Ich denke, das eine bedingt jeweils das
andere.", antwortete sie. "Ich mag das Präsenz-Erlebnis und deshalb
die Studie."

Marim nickte langsam. "Das ergibt Sinn. Ich glaube, ich bin müde und
unkonzentriert und dadurch lenkt mich diese Vermischung etwas
ab. Aber auf der anderen Seite ist es auch sehr schön, dass du
die Begeisterung teilst."

"Hm.", machte Nurek nachdenklich. "Mir wird manchmal gesagt, dass ich
zu schnell persönliche Sachen erzähle oder persönliche Gefühle
teile. Dass das Leuten manchmal unangenehm ist. Ich habe da kein
so starkes Gefühl für wie andere. Könnte dir etwas davon irgendwie
unangenehm sein?"

Marim antwortete eine Weile nicht. In seinem Kopf verknoteten sich
die Gedankenanfänge zu einem Gedankensalat, die für ihn nicht
mehr lesbar und auch nicht mehr enthedderbar waren. Er mochte das
nicht, besonders jetzt nicht. Er hatte das Gefühl, eine bisher
freundliche Stimmung zerstört zu haben. "Ich glaube, das ist
nicht das Problem.", sagte er schließlich. "Ich habe heute auf
meine Grenzen nicht so gut geachtet und gerate in einen Overload." Er
spürte es nun, da er es aussprach, deutlicher. Er wurde fahrig,
nichts wollte mehr richtig sein, er wollte allein sein, und
auch wenn Nurek lieb war, sie war fremd und sie und die
Umgebung zu viel.

"Nicht gut.", stellte Nurek sachlich fest. "Möchtest du Pause machen
und schlafen und wir reden einfach weiter, wenn du dich wieder bei
mir meldest?"

Marim nickte zögernd. Er fand das alles sehr unbefriedigend. Er
wollte Nurek eigentlich nicht so stehen lassen.

"Mach dir keine Gedanken um mich.", sagte sie, als wüsste sie genau, was
los war. "Ich bin geduldig. Für mich spielt Verzögerung in Gesprächen
keine Rolle. Wenn du dich morgen meldest oder in einer Woche, ist das
alles in Ordnung. Und wenn du dich gar nicht meldest, auch."

"Ich melde mich morgen.", versprach er.

"Okay.", sagte sie. "Brauchst du sonst noch etwas? Wird hier aufgeräumt, oder
startest du jedes Mal frisch?"

"Ich starte frisch.", antwortete er.

"Dann kannst du aus der Virtualität und hast deinen Raum?", fragte
sie weiter.

Marim nickte.

"Dann reden wir uns morgen.", sagte sie sanft und lächelte.

Sie blickten sich noch einige Augenblicke an, er sah ihr dabei nicht
direkt ins Gesicht. Das wäre zu stressig gewesen. Wieso waren Overloads
so viel schlimmer, nachdem eins realisiert hatte, dass es sich um
einen Overload handelte. "Danke.", sagte er schließlich noch. "Bis
morgen. Wirklich: Danke."

"Bis morgen. Pass auf dich auf."

---

In seinem Bett, ohne VR-Brille, ohne EM-Anzug, fühlte er sich immer noch
schlecht. Es war Unfug, das zu tun. Es war ein Overload und
Nurek hatte darauf entsprechend reagiert. Wahrscheinlich kannte sie
Overloads.

Es brauchte eine ganze Weile, bis sein Gehirn die überfließenden Eindrücke
so weit verarbeitet hatte, dass es leise genug zum Schlafen war. Und
dann dachte er: Ich möchte Nurek eigentlich nicht nur wegen des
Abschließens des Versuchs wiedersehen. Sie hatten so viele interessante
Themen angerissen.
