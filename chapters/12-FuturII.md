Futur II
========

\Beitext{Nurek}

"Manchmal sind Beziehungen besonders gut, weil Dinge darin nicht passieren
und auch nie passiert sein werden.", sagte Nurek.

"Futur II?", fragte Marim. "Warum ausgerechnet Futur II?"

"Ich hatte Lust dazu.", sagte Nurek schlicht.

"Verständlich.", überlegte Marim. "Du wirst dir dazu schon deine
Gedanken gemacht haben."

Nurek schnaubte belustigt und blickte aus der geschlossenen Terrassentür. Eine
dünne feine Schneeschicht bedeckte das Gras, das aber noch hindurchschaute, sodass
es Chancen hatte, Marim nicht zu sehr zu blenden.

"Was ist denn freundlicherweise nicht passiert?", fragte Marim. "Und wovon
glaubst du, dass es auch in Zukunft nicht passiert sein wird?"

"So ein simpler Umstand, dass du mich einfach nie gefragt hast, ob
ich dich nicht vielleicht doch küssen wollen würde.", war das erste, was
Nurek einfiel. Eigentlich hätte sie gern ein besseres Beispiel gehabt, aber
ihr würde keins einfallen, solange dieses noch nicht ausgesprochen war.

Marim blickte sie verwirrt an. "Warum sollte ich das fragen?"

"Es ist einfach schon zweimal passiert, dass ich nach einem Monat, im
anderen Fall zwei Tagen Beziehung das nochmal gefragt worden bin.", sagte
Nurek. "Das waren beides nicht sehr langfristige Beziehungen. Die mit
dir ist nun meine längste."

"Ich fände es übrigens angenehm, wenn wir nicht versuchten, zu sagen, dass
die Beziehung für immer halten würde.", sagte Marim.

"Dann passiert das nicht.", sagte Nurek. "Sowas lässt sich für mich einfach
auch nie absehen. Wir werden in einer Beziehungskrise schon rausfinden, was
wir einsetzen wollen, um sie zu halten, oder ob sie dann eben auch
einfach fällig ist."

"Danke.", sagte Marim lächelnd. "Ich bin dann wohl froh, dass so
ein Ewigkeits-Versprechen nicht passiert."

Marim war gerade zwei Wochen weg gewesen, mit seinem Herzwesen Anuka. Unter
anderem hatten sie das ForKaos besucht, einen riesigen Hackkongress in Fork, dem
größten Maerdhas. Nurek hatte im Prinzip auch Interesse, jenen mal zu
besuchen, aber sie war immer noch ausgelaugt vom Funkenfest und hatte
das Gefühl, es wäre besser, das zu tun, wenn sie wieder mehr Energie hätte. Nächstes
Jahr vielleicht.

Der Kongress war allerdings ein Hybridkongress. In ähnlicher
Weise, wie Linoschka sie als EM-Anzug besucht hatte, konnten Personen
den Kongress remote besuchen, also, von zu Hause aus. Und auch in einer
weniger Körper-repräsentativen Form. Zum Beispiel konnten Personen
von außerhalb einen kleinen Roboter auf Rollen über das Veranstaltungsgelände
fahren und in begrenztem Maße waren Drohnen okay.

Diese Optionen hatte Nurek an
einem der Tage ausprobiert, aber nicht lange, weil auch
das sie stresste. Sie hatte danach einfach die
Zeit für sich genossen. Und nun war sie froh, dass Marim wieder
da war.

"Ich finde außerdem schön, dass Eifersuchtsdrama nie passiert
sein wird.", fügte Marim hinzu. "Eigentlich war mir das von Anfang
an klar, dass es unwahrscheinlich ist, aber zwischendurch hatte
ich ein bisschen Angst, dass meine Art Freiheit dir doch unangenehm
sein könnte. Ich hätte dann auch was daran geschraubt. Du
bist mir wichtig. Aber es war nie nötig."

Nurek lächelte. Sie saßen sich gegenüber, jeweils in gemütlichen
Sitzmöbeln. Nurek hatte Lust, die Kerze zwischen ihnen anzuzünden. Also
tat sie es.

"Ich fand von so vielen Schwierigkeiten schön, dass sie nicht passiert sind.", sagte
Nurek. "Dass meine Art zu kommunizieren nie zu schlimmen Konflikten
geführt hat."

"Deine Art zu kommunizieren ist sehr klar.", sagte Marim. "Aber
ich verstehe, was du meinst."

"Ich mag, dass du mich immer ernst genommen hast.", sagte Nurek. "Seltsamerweise
passt das für mich auch ins Muster, dass etwas nicht passiert. Ich
konnte ohne Angst sagen, als ich inkontinent war zum Beispiel. Angst
vor Reaktionen und Dingen, die bei dir eben nicht passieren."

"Ich würde dich in den Arm nehmen, es sein denn, du willst, dass es
nicht passiert.", antwortete Marim und lächelte.

"Du hast mich wohl schon wieder lieb.", stellte Nurek ebenso lächelnd
fest. Aber sie kam nicht um den Tisch herum und die ausbleibende
Kommunikation vermittelte ausreichend, dass Umarmung gerade nicht
dran war.

"Ich würde mich gern wieder mehr meiner Studie widmen.", sagte
Marim. "Das ist in letzter Zeit nicht passiert, und das ist
in Ordnung. Ich hatte auch keinen Kopf dafür. Aber nun
würde ich gern wieder mehr."

"Ich habe mir auch eine Studie ausgedacht. Aber ich weiß
nicht so genau, ob ich fähig bin, sowas durchzuziehen.", sagte Nurek.

"Ist anfangen, und dann eventuell ein ewig unbeendetes Projekt
haben, eine Option?", fragte Marim.

Nurek grinste. "Davon habe ich sehr viele. Und ich glaube, es
ist eine Option. Aber ich glaube, ich würde gern vorher
einigermaßen festlegen, wie wichtig mir Durchziehen wäre. Ob ich
mich einfach treiben lasse, oder ob ich mir das doch etwas
fester vornehme, sodass ich mich auch über kleinere Hürden
pushe."

"Worum geht es denn?", fragte Marim.

Diese Neugierde, mit der er fragte, fühlte sich wunderschön
an. Nurek kam aus dem Grinsen nicht mehr raus.
"Empathie-Virtualitäten.", sagte sie. "Stell dir vor, du
wärest eine Hummel und fliegst dauernd gegen Fensterscheiben. Wie
kannst du einer Person dieses Gefühl, das du dabei hast, näher
bringen.", leitete sie ein. "Ich dachte dabei an eine Virtualität, in
der du automatisch sehr schnell vorankommst, auch wenn du nur
vorsichtig gehst, und überall sind unsichtbare Wände."

Marim schmunzelte. "Ich mag die Idee. Das Beispiel ist
witzig. Wollen wir das basteln?", fragte er. Und
fügte leiser murmelnd hinzu: "Macht es dir was aus, dass
du nicht die erste Person mit der Idee bist?"

"Ich weiß, dass so etwas nicht wenig erforscht ist.", ging
Nurek auf die zweite Frage ein. "Daher ist das auch nicht
meine Studienidee. Sondern als Vorbereitung dafür gedacht. Ich
fände eine Meta-Studie spannend: Ich würde Personen fragen, deren
Lebensrealität eine andere ist als die der meisten, wie sie
in Virtualitäten diese für andere am ehesten nachempfindbar
machen würden. Dann hinterher erforschen, wie das klappt, um
herauszufinden, ob es Konzepte gibt, die übertragbar sind. Und
vielleicht auch, ob diese Virtualitäten wiederum abhängig
vom Publikum verschieden sein sollten, je nachdem in was
eben jene sich mit ihrer Lebensrealität hineinfühlen können."

"Uh, das ist komplex.", sagte Marim. "Aber unglaublich
spannend. Und so hilfreich!"

"Ich überlege, das Projekt mit anderen zusammen zu machen.", sagte
Nurek. "Mit Leuten, die ebenso dafür brennen würden, und es
weitermachen würden, sollte ich aussteigen."

"Dachtest du dabei an mich?", fragte Marim. "Es ist okay, wenn
nicht."

"Ich habe da länger drüber nachgedacht.", antwortete
Nurek wahrheitsgemäß. "Und ich hätte dich unglaublich gern dabei, wenn
du das magst. Ich kann mir vorstellen, dass dir ein Projekt auch
reicht, das musst du wissen. Aber mir wäre es lieb, wenn
mein Projekt nicht zu zweit mit dir ist, sondern dann auch
noch andere dabei sind."

"Das finde ich gut. Auch gerade, weil ich gern mein Projekt
als Hauptprojekt behalten mag.", sagte Marim.

"Wir sind uns zu einig.", murrte Nurek kichernd. "Themenwechsel. Hast
du Lust, eine Runde bergauf zu schwimmen?"

"Wie meinen?", fragte Marim.

"Ich hatte schon eine Virtualität und habe mich immer gefragt, ob
ich noch eine haben darf.", sagte Nurek. "Aber im Zweifel ist
die dann einfach nicht für die Studie. Ich würde gern einen
See haben, der so um 30% gekippt ist. Die Schwerkraft
fürs Wasser soll eine andere sein, als die für mich, sodass
ich durch ein nicht fließendes Gewässer bergauf schwimme."

Marim grinste. "Du bist so großartig! Das machen wir!"

---

Sie vermisste Linoschka und Linoschka sie. Das fühlte Nurek
später am Tag intensiv.

Marim und sie hatten außer der
Schwimmvirtualität auch noch eine mit einem unterirdischen
Kanalsystem gebastelt, in dem die Wände aus Sternenhimmel
bestanden und sie sich fast lautlos mit einem Boot hindurchbewegen
konnten. Als sie genug davon hatten, hatte sich Marim
zu einem Spaziergang aufgemacht und Nurek war allein im
derzeit leeren Haus geblieben. Mø, Tjaren und Ivaness
unternahmen einen Ausflug zu einer Ausstellung und
würden heute Abend wiederkommen. Linoschka war am
Ehrenberg-Internat, Nureks alter Schule. Es war eine
große Schule, die außerdem traditionell ein großes
Neujahrsfest ausrichtete, das zumindest in Teilen einen
Hackkongress-Charakter hatte. Linoschka hatte von
Lockpicking erzählt -- Schlösser knacken --, von
RoboCups, wo kleine, möglichst simpel kreierte
Roboter versuchten, sich von Tischen zu schubsen, allerlei
Wettbewerben in Geschicklichkeitsspielen, aber auch
viel künstlerischem Angebot. Vom Ehrenberg-Internat nahm
eine Spielgruppe am Spiel teil. Deshalb gab es
unter den Vorstellungen dieses Mal auch einiges, was
mit dem Spiel zusammenhing. In dem Rahmen bot Linoschka
zusammen mit Bjork einen Workshop an. Das war schon länger
geplant. Linoschka war aufgeregt. Aber wahrscheinlich würde
sie deshalb Neujahr nicht in der WG sein, und das war das erste
Mal, seit sie die Tradition eingeführt hatten.

Linoschka hatte außerdem Liebeskummer. Sie hatte sich
ausversehen in Ærenik verliebt. Ærenik hatte ihr, als sich
Linoschka getraut hatte, das anzusprechen, allerdings gesagt, dass
sie auch aromantisch wäre, und auch an dieser Art Beziehungen
kein Interesse hatte. Durchaus einfühlsam, hatte Linoschka
versichert. Und sie machten immer noch gelegentlich
Fesselspiele. Aber Linoschka fühlte sich deshalb im Moment
sehr traurig und wäre gern in Nureks Nähe gewesen.

Nurek ging langsam durchs Haus. Vielleicht hätte es rastlos
gewirkt, aber es war ja keine Person da, auf die sie irgendwie
hätte wirken können. Sie war eher ruhig. Sie genoss den
Raum, für sich zu sein, sich selbst wahrzunehmen. Sie wollte
am liebsten etwas sortieren. Perlen zum Beispiel. Aber sie besaß
keine eigenen. Sie spazierte durch den Garten und fegte die
Terrasse, nicht, um sie gefegt zu haben, sondern fürs Fegen, bis
sie sehr gründlich schneefrei war. Die Minze war auch etwas
von Schnee bedeckt. Nächstes Jahr würde sie sicher auch
neu wachsen.

Schließlich legte sie sich auf eine Kuscheldecke in den Gemeinschaftsraum
aufs Sofa und las sich erste Grundlagen an, wie Studien sinnvoll
angegangen würden.

Sie wachte davon auf, dass Marim die Haustür wieder öffnete. Es
war schöner Schlaf gewesen, aber nun wieder wach zu sein, war
auch schön. Nurek mochte die Stimmung kurz vor Neujahr. Wenn
alles kalt war, Kuscheldecken gemütlich. Und wenn Marim besonders
weiche Ringelstrümpfe trug. Sie musste bei dem Gedanken grinsen.

Sie hörte, wie Marim die Jacke weghängte. Es war eine
Multifunktionsjacke. Marim hatte immer noch so wenige Dinge, dass
er innerhalb eines Tages hätte beschließen können, komplett
auszuziehen. Zumindest, wenn er keine Kopfschmerzen hatte.

Marim schaute zu ihr aufs Sofa.

Nurek vermutete, dass er
sich auf sein Zimmer verziehen würde, wenn sie nicht zu
erkennen gab, dass sie wach wäre und Gesellschaft mochte. "Hast
du mir einen Schnürsenkel mitgebracht?", war das erste, was
ihr Gehirn an Worten bildete. 'Schnürsenkel' war auch
einfach ein sehr gutes Wort.

"Nein.", antwortete Marim. Die Stimme klang warm und
nur minimal verwirrt. Er war sie wohl einfach zu
sehr gewöhnt.

"Das verstehe ich.", antwortete Nurek. "Hätte ich
auch nicht."

Wie erwartet betrat Marim den Gemeinschaftsraum. Nurek
rückte auf dem Sofa etwas zur Seite, also legte er sich
dazu.

"Du bist so angenehm kalt.", murmelte Nurek behaglich.

---

Am Abend trafen sie Linoschka in einer Virtualität. Sie nahmen dazu
Nureks dunklen Säulen- und Bögenbau, der ein Stückweit unter
unrealistischem Wasser stand. Marim hatte der Virtualität vor einer
Weile einen Schwarm winziger Enten hinzugefügt, nur etwa so groß wie
ihre großen Zehen. Sie waren unaufdringlich und leise, schwammen mal
hier hin und mal dahin und waren unbeschreiblich niedlich.

"Morgen ist der Workshop.", sagte Linoschka. "Bjork erwartet, dass
die Spielgruppe auftaucht, die in sein Zuhause
eingebrochen ist, um dort zu verwanzen. Das macht mich einfach immer
noch wütend."

"Uffz ja!", stimmte Nurek zu.

Linoschka hatte davon kürzlich erzählt: Zum Spiel gehörte es durchaus
dazu, wenn sich Teilnehmende über den Weg liefen, dass sie sich
gegenseitig ausspionierten und dazu vielleicht sogar Wanzen benutzten. Teils
wurden auch die oft speziell fürs Training selbst errichteten
Stützpunkte überwacht oder verwanzt, -- weil es als Spielelement im Vorfeld
zur eigentlichen Phase durchaus Spaß machen konnte. Als Stützpunkt
konnte eine verlassene Hütte irgendwo in der Botanik dienen, oder
etwas komplett selbst Gebautes. Aber
Linoschka war sich mit vielen einig, dass Privathaushalte zu verwanzen
zu weit ging. Vor allem, wenn darin noch andere Personen lebten, die
nicht teilnahmen, und zwar unabhängig davon, ob die Wanzen so eingestellt
werden konnten, dass sie Nicht-Teilnehmende gar nicht abhörten oder nicht. Das war
privat.

Sie hatten wegen des Vorfalls einmal sehr gründlich in ihrer Hack-Kommune
nach Wanzen gesucht, aber keine gefunden.

"Jedenfalls bin ich sehr aufgeregt.", sagte Linoschka. "Und ich glaube, danach
fahre ich doch wieder zu euch."

"Juhu!", rief Marim.

Nurek grinste.

"Marim, ich habe da nochmal eine Frage.", sagte Linoschka.

"Sprich!", forderte Marim sie auf.

"Darf ich auch an deiner Studie teilnehmen?", fragte Linoschka.

Marim lachte auf. "Aber natürlich! Ich würde mich riesig freuen!"

"Diese Virtualität hat mich inspiriert. Und der Segelausflug nach
Geesthaven, als ich euch abgeholt habe. Und Träume.", sagte Linoschka. "Ich
träume manchmal von Naturkatastrophen. Vor allem Überflutungen. Aber
in den Träumen sind sie nicht bedrohlich, sondern wunderschön.", berichtete
Linoschka. "Ich bin dann im oberen Stockwerk von Häusern und bis unterhalb
des Balkons ist Wasser, Sturm peitscht und Wellen schlagen ans Haus. Ich
finde es unbeschreiblich ästhetisch. Können wir das mal nachbauen?"

"Wow, das ist schön.", sagte Nurek. "Oder auch Explosionen. Ich mag
die Vergänglichkeit in den Bildern. Ich mag die Bilder und schäme
mich manchmal dafür."

"Genau, ich auch.", stimmte Linoschka zu. "Die Bilder fühlen
sich so entlastend und erlösend an. Aber es wäre gut, wenn das nicht
passiert."
