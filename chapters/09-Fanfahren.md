Fanfahren
=========

\Beitext{Marim}

"Was magst du noch für ungewöhnliche Dinge?", fragte Marim.

"Dich?", antwortete Nurek.

"Point taken.", sagte Marim. Wie seltsam warm es sich anfühlen konnte,
als Ding bezeichnet worden zu sein, wenn das hieß, von Nurek gemocht
zu werden. Wie eben jene ungewöhnlichen Dinge. Pflanzennamen zum
Beispiel.

"Ich verwirre gern Leute.", fügte Nurek grinsend hinzu.

"Als ob du mich noch verwirren könntest.", kommentierte
Marim, ebenso grinsend.

"Ich werde es drauf anlegen, wenn du willst.", bot Nurek an.

"Ich bitte darum." Marim erinnerte dieser Austausch an einen
der ersten, die sie überhaupt gehabt hatten. Unter anderem hatte
Nurek in so einem Zusammenhang offenbart, dass sie Konversationen
gern so anfing, dass sie wirkten, als wären sie mitten drin. "Warum
verwirrst du eigentlich gern Leute?"

"Viele Gründe.", sagte Nurek. "Und alle durch Backtracking
gefunden. Das heißt, ich habe mir nicht gedacht, aus diesen
Gründen könnte ich mal Leute verwirren, sondern ich habe
angefangen Leute zu verwirren, und versuche nachträglich
rauszufinden, warum."

"Mit Erfolg?", fragte Marim.

"Teils.", antwortete Nurek. "Einmal ist es das Übertragen
des Konzepts, was Smalltalk für mich faktisch ist: Bei
Smalltalk kommen Leute auf mich zu, reden mit mir und
verwirren mich. Ich habe keinen Plan, was sie wollen. Also
denke ich mir, ich mache das auch: Ich gehe auf Leute zu
und sage Dinge, die verwirren."

"Das klingt für deine Verhältnisse ungewöhnlich nach
Rache.", überlegte Marim. Er glaubte nicht daran, dass
er mit dem Gedanken recht haben könnte. Er hoffte viel mehr,
dass Nurek durch seinen Einwand erläutern würde, warum
er unrecht hatte.

"Interessant.", sagte Nurek nachdenklich. "Es fühlt sich
nicht so an, aber du hast recht, dass es so klingt." Sie
dachte so angestrengt nach, dass ihre Hand aufhörte, seinen
Nacken durch seine Haare hindurch zu kraulen. "Zu Rache gehört aber
ein Wunsch, anderen Böses zu tun. Den habe ich nicht.", hielt
sie fest. "Ich frage mich, ob das ein Nebenresultat ist. Weil
ich nicht nachempfinden kann, was an Verwirrung schlecht
sein soll, und von mir auf andere schließend dahingehend keine
Rücksicht nehme." Nach kurzem Zögern fügte sie noch
hinzu: "Auch keine Rücksicht nehmen kann. Selbst, wenn
ich mir Mühe gäbe: Ich bin gewohnt, durch meine pure Existenz
zu verwirren. Ein anderer Grund, warum ich es tue, ist nämlich, dass
ich es dann ja auch gezielt und halbwegs kontrolliert
tun kann. Sodass ich wenigstens selbst verstehe, was
an mir nun verwirrt."

Marim musste unweigerlich glucksen und versenkte
seinerseits seine Hand liebevoll in ihr Haar. Dazu musste er
sich nach hinten überstrecken. Sie lagen halbwegs in Löffelhaltung
ineinander, sein Rücken ihr zugewandt. Aber sie lagen
nicht besonders eng. Dazu war es zu warm. Sie hatte seine
Haare sortiert und kraulte ihn, solange das Gespräch jeweils
nicht zu beanspruchend war, am Rücken oder im Nacken.

Das, was sie erklärte, knüpfte an einem anderen Punkt an, über
den er sich lange Gedanken gemacht hatte, schon etwa seit sie sich
kennengelernt hatten. "Bei unserem ersten Kennenlerngespräch wirktest
du auf mich so forsch, wie ich vermutlich nie sein werde, wenn
ich eine Person neu kennenlerne.", leitete er ein. "Ich dachte
damals, dass du vielleicht einfach mutiger bist. Aber ich
habe nun eine neue These. Magst du anhören und mitüberlegen?"

"Ich denke auch, dass ich nicht einfach mutiger bin.", sagte
sie. "Gern! Leg sie dar, die Gedanken!"

"Die Sache ist die, ich habe eigentlich wenig Plan davon, welches Verhalten wann doch
sozial akzeptiert ist. Ich mache eben deshalb viele Dinge vorsichtshalber nicht
vor neuen Leuten.", erklärte er. "Du hingegen weißt über so
wenig Dinge, ob sie sozial akzeptiert werden würden oder nicht, dass
dir nichts mehr übrig bleibt. Deshalb musst du zwangsläufig dauernd Dinge tun, die
sozial nicht akzeptiert sein könnten, bist also sozusagen freier,
fühlst dich dabei aber trotzdem gegebenenfalls scheiße. Ich weiß nicht, ob
ich richtig liege, und falls ja, welche der Varianten die Traufe ist."

Nurek murmelte zustimmend. "Ich glaube, das passt einigermaßen.", sagte
sie. "Ich habe oft den Eindruck, dass du in sozialen Situationen wie
auf dem Ball oder, als Bjork aufgetaucht ist, doch noch etwas
mehr begriffen hast, was eigentlich passiert, als ich."

Marim überlegte gerade, im Astgespräch, das sie schon wieder führten, nun
bis zur letzten Gabelung zurückzugehen, um Nurek weiterzufragen, warum
sie gern verwirrte, oder an noch eine Gabel zuvor, was sie
noch für ungewöhnliche Dinge mochte, als
ein zartes Ping von seinem Hördevice ihm ankündigte, dass Linoschka
sie anrief. Sie beide. Er drehte sich zu Nurek um, und nahm das Gespräch
an, als diese nickte.

"Ich habe mich ein bisschen eingelebt.", sagte Linoschka
statt einer Begrüßung.

Sie hatte sich entschieden, in die
Spielgruppe einzutreten, und Bjork hatte sie direkt
mitgenommen. Sie wohnte nun mit der Person, die die Koordination
der Gruppe übernahm -- eine andere als Bjork -- zusammen
in einer Herberge in Geesthaven, der Sportstadt, von der
aus die meisten Fähren zu ihnen übersetzten. Sie trainierten alle gemeinsam
im Nordwesten Maerdhas. Bjork wohnte auch nicht allzu weit
von der Gegend entfernt. Linoschka vermisste ihre Hack-Kommune
und rief oft an oder schrieb Nachrichten.

"Wir sind ein Liebesgedöns!", unbegrüßte Nurek zurück. Sie
zog das 'ie' besonders lang und strahlte dabei.

Linoschka gab ein glucksendes Geräusch von sich.

"Weil wir uns lieb haben.", erklärte Nurek. "Und Liebespaar
klingt irgendwie zu einschränkend, finde ich."

Marim verschlug es ein bisschen die Sprache. Er fühlte
sich in diesem Moment sehr glücklich und strich ihr
über die Wange.

"Wir liegen gemütlich im Bett. Wir sind noch gar nicht
aufgestanden.", sagte Nurek.

"Ich merke das.", erwiderte Linoschka. "Ich bin im
Gemeinschaftsraum."

Nurek schnaubte. "Ich komme gleich.", versprach sie und
kroch aus dem Bett.

Dem gemütlichsten aller Betten, wie
Marim immer noch fand. Es gab eben keinen besseren
Ort für ein Bett, als eine Fensternische.

Linoschka war nicht in Person da. Sie experimentierten mit
einem alten EM-Anzug. Für Linoschka wurde die WG in eine
Virtualität übersetzt und ein EM-Feld in der echten WG bewegte
einen EM-Anzug so durch die Wohnung, wie sie in der Virtualität
es tat. Sie hatten den EM-Anzug bisher noch nicht
angekleidet, sodass er vielleicht etwas ulkig wirkte. Sie
hatten aber einen fast gesichtsgroßen Bildschirm eines alten
Faltrechners über den Kopfteil gerollt und festgeknotet, der
ihr Gesicht abbildete und ihre Mimik zeigte. Es gab
für solche Zwecke Masken, wenn wirklich eine Person von
woanders ins quasi Outernet oder in die Augmented Reality
anderer Personen kopiert werden sollte. Aber es war
dazu schon alles mögliche getestet worden, und da sie
es nicht dringend brauchten, wäre es Ressourcenverschwendung gewesen. Sie
hätten sich ja auch alle in der Kopie des Hauses in der
Virtualität treffen können. Aber der wandelnde EM-Anzug, der
quasi Linoschka abbildete, hatte etwas Privateres.

EM-Linoschka saß auf dem Schaukelstuhl, als sie sich angekleidet
dazusetzten. Sie überlegten, Karten zu spielen. Einfach, um
den EM-Anzug herauszufordern. Aber im Prinzip konnte der
EM-Anzug alles, was ein gewöhnlicher Körper auch konnte. Und
in der Theorie noch viel mehr, sich am Ellenbogen lecken
zum Beispiel. Fast, es mangelte an einer Zunge. Dieser
Anwendungsfall half niemandem. Aber es machte ziemlich
viel Spaß, sich Gedanken über dieses Szenario zu
machen:

Während EM-Linoschka zwar mangels Zunge nicht mit der
Zunge an den Ellenbogen kam, aber sehr wohl mit der
Nase, würde Linoschka in Geesthaven nicht
plötzlich eine neue Anatomie bekommen, dass
sie es dort auch könnte. Linoschka in Geesthaven
allerdings befand sich in einer Virtualität, sodass bei der Bewegung
des Ellenbogens in die Gegend der Nase der Arm in der
Virtualität verkürzt dargestellt werden konnte. Solange
sie sich nicht unvirtuell selbst berührte, konnte also
das Berührungsgefühl zwischen ihrer Nase und ihrem Ellenbogen
virtuell hergestellt werden. In Wirklichkeit würde dabei ihr
Ellenbogen die virtuell dargestellte Nase und ihre Nase den
virtuell dargestellten Ellenbogen berühren. So ähnlich
wie, wenn zwei Personen an verschiedenen Outernet-Orten
sich in einer Virtualität berührten. Im Prinzip würde
es optisch auffallen, wenn die Nase auf einmal länger und die
Arme auf einmal kürzer waren, sodass die Berührung
auch visuell wahrgenommen werden konnte, aber es waren
durch Tricks optischer Täuschung schon Personen
in Virtualitäten vorübergehend davon überzeugt
worden, dass sie mit der Nase an ihren Ellenbogen
kommen könnten. Das war also keine sehr neue Idee. Besonders
war hier, dass es ja auch EM-Linoschka gab, das Anzug-Abbild, das
gewissermaßen mit Linoschka aus der verdrehten Virtualität übereinstimmte.

Sie alberten eine ganze Weile herum, was so die Vor- und Nachteile
von EM-Linoschka wären und allein dafür lohnte sich der
Aufbau. 

---

Der ulkige WG-Abend war wunderschön und außerdem eine effektive
Ablenkung. Nurek war in den letzten Tagen zunehmend aufgeregt
gewesen. In der Nacht hatten sie sich zum Einschlafen passend
zum Thema gegenseitig vorsichtig mit den Ellenbogen gestreichelt. Am
folgenden Morgen brachen sie Richtung Funkenfest
auf.

"Es fühlt sich an, als würde ich in eine neue Welt aufbrechen, in
eine völlig neue Ordnung.", erklärte Nurek, als sie in Fjärsholm
in den Zug stiegen. Sie hatten überlegt, dass weniger Umstiege
weniger stressen würden und hatten sich deshalb gegen die Fähre
entschieden. "Das ist albern."

"Es ist nicht albern.", beruhigte Marim sie. "Es ist eine große
Sache, und es kann sein, dass sich dadurch in deinen Erfahrungen
etwas ändert, sodass es nicht mehr ist, wie vorher."

"Das Gefühl ist extremer.", widersprach Nurek.

"Dann ist das eben so.", sagte Marim. "Gefühle sind erstmal
einfach. Daran ist nichts albern. Zumindest nichts lächerlich."
Er wusste nicht, ob er die richtigen Worte fand. Nurek sagte erst
einmal nichts mehr. Aber das konnte auch daran liegen, dass sie
nun ihr Abteil suchten. Ein Abteil zu zweit. Das hatten sie
zuvor angemeldet, als Sonderwunsch. Zur Schonung von Ressourcen
waren die meisten Kapseln der Züge für mehr Personen ausgelegt, aber
falls Personen ein kleines Abteil für sich brauchten, wurde
das im Normalfall auf Anfrage akzeptiert, aber musste dann für
die Planung je nach Strecke ein bis zwei Tage zuvor gebucht werden.

Sie verteilten ihr Gepäck auf den niedrigen Gepäckablagen
neben den Sitzsesseln. Sie hatten bei der Vorherplanung
ihre Größe angeben dürfen und hatten beschlossen, sie
heimlich zu groß anzugeben. Deshalb hatten die
Sessel nun Sitzflächen, auf denen sie beide gut ihre
Beine verknoten konnten.

"Vorsicht, schlechter Wortwitz incoming.", sagte Nurek.

Marim grinste sie an.

"Wir sind ja jetzt Fans, die fahren. Das Verfahren
nennt sich dann Fanfahren.", sagte Nurek.

Uh, der war wirklich angenehm schlecht, fand Marim. Er
lachte. Und beobachtete Nureks Gesicht, das erst mitlachte
und dann wieder sehr nervös wirkte.

"Du sagst wahrscheinlich wieder, es wäre nicht albern.", sagte
sie. "Aber ich gehe nun aufs Klo. Und ich war im Bahnhof doch
schon. Also fühlt sich das irgendwie albern an."

"Um dich nicht zu enttäuschen: Das ist nicht albern.", sagte
Marim und versuchte ein tröstendes oder wenigstens sanftes
Lächeln.

Nurek hantierte an ihrem Gepäck, als sie losging, um
eine kleine Tasche daraus mitzunehmen und als sie wiederkam,
wirkte sie, als wäre ihr sehr unwohl. Sie setzte sich nicht
direkt. Marim musterte sie aufmerksam.

"Ich bin inkontinent, weil ich gestresst bin.", murmelte
sie.

"Shit.", sagte Marim. "Hast du Dinge dabei, die helfen?"

"Ich habe Stoffbinden, die eigentlich für Menstruation
gedacht sind.", berichtete Nurek. "Ich mag eigentlich überhaupt
nichts, was über Unterhosen hinausgeht, an meinem Körper
spüren. Ich nehme die Reize die ganze Zeit wahr. Es ist
mir so unangenehm."

Marim nickte. Aber etwas anderes fiel ihm gerade auch nicht
ein, um das Problem besser zu lösen. "Shit.", wiederholte er.

"Ich habe die Unterhose gewechselt und trage nun eine.", sagte
sie. Sie verstaute, die kleine Tasche wieder im Rucksack und
setzte sich dann wieder neben ihn. "Ich falle auseinander!", nölte
sie laut und ausgiebig. Nölen half ihr manchmal.

Sie ließ ihren Körper gegen seinen fallen und er legte sanft
einen Arm um sie.

"Magst du Puzzle?", fragte sie.

Marim brauchte einen Moment, um die Frage auf ihre Klage
zu beziehen und zu verstehen, warum sie nun wieder
kicherte. Coping-Humor. Nurek-Humor. "Sehr gern.", sagte
er.

---

Zu ihrer Strategie beim WG-Abend hatte gehört, dass sie
spät ins Bett gegangen waren. Dadurch war der
Aufbruch anstrengender gewesen, aber
wahrscheinlicher, dass Nurek auf der Fahrt
schlafen können würde. Wie gut es funktionierte, konnte Marim nicht
einschätzen. Nurek schlief relativ leise, aber murmelte
auch immer Mal wieder etwas. Die Fahrt dauerte etwa einen
Vierteltag. Manche sagten auch eher halber Halbtag. Das
System, um die Daten für Feiertage sich wiederholender
Feste zu bestimmen, richtete sich meistens nach Halbtagen, weil
von Neumond bis Neumond relativ genau 29.5 Tage lagen. Monate
hatten entsprechend mal 29 und mal 30 Tage, und in jenen
Grenzfällen, wenn ein Fest in einem Jahr in einem Monat
auf dem 30. Tag gewesen war, und der selbe
Monat im Folgejahr aber nur 29 Tage hatte, wurde der
Tag für das Fest daran festgemacht, mit welchem der
Halbtage das Fest verbunden war. Aus der Tradition heraus
rechneten manche Leute eher mit Halbtagen und würden entsprechend
eher von einem halben Halbtag reden als von einem Vierteltag. Es
gab da verschiedene Tendenzen, Regions- und Volks-abhängig.

Nurek war, ähnlich wie bei ihrem Campingausflug, über
irgendwelche halbwegs gut wegsteckbaren Grenzen hinaus
erschöpft, als sie ankamen. Eine Urukene empfing sie am relativ leeren Bahnhof. Sie
waren früher als der Hauptstrom an
Festivalbesuchenden angekommen. Der große Ork trug einen der klassischen, schwarzen
Kampfanzüge, die zu Orkando gehörten, mit einem weißen Gürtel. Die
anderen Farben kannte Marim nicht auswendig. Er wusste nur, dass weiß für
so etwas wie eine vollständige Ausbildung
stand und das Ausbildungssystem noch nie linear
gewesen war. Der Ork wirkte freundlich aber auch
distanziert, sprach nicht viel, und führte sie in ein Calmp zu einem
Zelt. Calmp hießen die Camping-Regionen auf dem Platz, in denen
Ruhe, keine zu raschen Bewegungen und wenig Licht vorgesehen
war. Es standen schon Zelte. Es durften auch Zelte mitgebracht werden,
aber viele Regionen waren schon vollständig mit aufgebauten
Zelten ausgestattet, um Besuchende zu entlasten, für die
das viel Stress bedeutete. Sie bekamen ein Zelt, wie abgesprochen, nicht
sehr dicht am nächsten Sanitärhäuschen, weil es viele Campende
mit einem dringenderen Anliegen gab, möglichst nah an einem dran zu sein, aber
mit einem schnurgeraden Weg dorthin.

Der Ork erklärte eine Sammlung Kärtchen, von denen sie eines aussuchen
konnten, das in einer dafür am Zelt angebrachten Hülle dann gut zu
sehen wäre. Sie konnten damit kommunizieren, ob das Zelt schon belegt war, und
wie viel oder wenig Kontakt sie mit anderen wünschten, sprich, ob
das Zelt privat bleiben sollte, oder "anklopfen" willkommen wäre, so
gut das eben bei Zelten ging. Sie entschieden sich kaum verwunderlich
dafür, nicht gestört werden zu wollen.

"Fühlt ihr euch mit Gabriane wohler?", fragte der Ork.

Marim fiel erst jetzt auf, dass er sich überhaupt nicht vorgestellt
hatte. Nurek nickte. Das, fand Marim, war ein gutes Beispiel für
Kommunikation, bei der er sich gehemmt fühlte, weil er wusste, dass
es zwar okay war, zuzustimmen, aber irgendwie für viele dazugehörte,
etwas Relativierendes oder etwas zur Einordnung dazuzusagen, bei der
Nurek aber einfach direkt war.

"Sie wird euch noch schreiben oder eine Nachricht schicken.", sagte
der Ork. "Ab heute spät abends gibt es kleine künstliche Feuer um das
Securiteam-Camp herum. Geplant ist gemeinsames
Essen mit Personen, die lieber kleine Runden haben, und noch
keinen Anschluss. Eines davon koordiniert sie. Wenn ihr nicht
zu erschöpft seid, dann ist das was für euch."

Marim fühlte sich leicht unkomfortabel dabei, dass dieser
Ork zu wissen glaubte, was gut für sie wäre. Aber gleichzeitig
klang es so, als habe er recht. Und bisher war sein Eindruck
der Person sympathisch gewesen. Sie war fast doppelt so
groß wie er. Die Gesichtszüge verrieten, dass
sie wahrscheinlich unter 20 war, vielleicht irgendwas zwischen
fünfzehn und siebzehn, schätzte Marim.

"Wie viele?", fragte Nurek.

"Acht bis zehn. Zehn ist die Grenze.", antwortete der Ork.

"Danke.", sagte Nurek.

Der Ork lächelte zum ersten Mal und deutete eine Verbeugung an. "Ich
wünsche euch eine schöne Zeit.", sagte er, drehte sich um und
verschwand.

Marim hätte irgendwie damit gerechnet, dass er noch so etwas fragen
würde, wie, ob er ihnen bei etwas Weiterem behilflich sein könnte. Aber
er glaubte auch, dass es der perfekte Zeitpunkt war, nun allein
zu sein. Sie wussten ausreichend, sie hatten Kontaktmöglichkeiten
und Nurek wirkte, als ob sie dringend den Rückzug bräuchte. Sie
krochen ins Zelt. Die Matratze war gemütlicher als die geliehene, die
sie bereits wieder in Fjärsholm abgegeben hatten. Es gab einen
Vorzeltbereich, wo sie ihre Rucksäcke zurückließen. Nurek wischte
sich die nackten Füße ab und legte sich direkt auf die Matratze.

"Ich glaube, ich will zu diesem Kunstfeuer.", sagte sie. "Ich
mag Feuerlicht. Und ich mag nicht schon wieder die ganze
Zeit halb liegen und halb schlafen, das strengt auch an. Ich
glaube, so eine kleine Sache heute ist gut."

Marim strich ihr über den Rand des Gesichts und lächelte.

"Streichelst du mir den Rücken?", fragte Nurek.

Marim nickte.

Nurek drehte sich dazu auf den Bauch. Sie trug ein dünnes
Hemd mit dünnen Trägern in hellblau mit vielen Haien darauf, aber
hin und wieder dazwischen ein Narwal. Es war vielleicht noch
eine Spur krasser als das Oberteil mit den Dinos und den paar
Drachen dazwischen von damals aus der Virtualität, denn
Drachen gab es immerhin wirklich und sie hatten eine gewisse
Verwandtschaft mit Dinos. Narwale dagegen waren
Fantasy-Säugetiere^[Anmerkung des Schreibfischs: In diesem
Fantasy-Roman ist das so herum. In  unserer Welt gibt
es Narwale sehr wohl.].

"Ich bin gesprächig.", sagte Nurek.

"Sprich so viel du willst.", sagte Marim. "Ich mag deine
Stimme."

Nurek machte ein paar behagliche Geräusche, während er
ihr über den Rücken streichelte, dann an der Wirbelsäule
rieb und mit beiden Händen sachte über die Haut klopfte.
"Meinst du, du schaffst es, innerhalb des Zelts kein
Feuer zu spucken?", fragte sie.

"Ich denke, das lässt sich einrichten.", antwortete er.

"Fühlst du dich dadurch eingeschränkt?", fragte sie.

Marim schmunzelte. "Nicht besonders.", antwortete er.

"Magst du Tannennadeln?", fragte Nurek.

"Sie riechen gut. Aber sie nerven manchmal unter
den Füßen.", antwortete Marim.

"Findest du Tannennadeln so insgesamt besser oder schlechter
als Nähnadeln?", fragte Nurek.

"Darf ich auch Stecknadeln mitvergleichen?", fragte Marim

Nurek erlaubte es. Sie blieb jeweils nur kurz bei den
einzelnen Themen, sprang unvorhersehbar von einem zum
anderen und insgesamt war darin durchaus eine angenehme
Verwirrung. Im Wesentlichen ging es ums Reden, darum die
Stimme zu nutzen, und das Gehirn zu stimulieren, aber
auch nicht zu doll.

---

Später am Abend folgten sie Gabrianes
Einladung zum künstlichen Lagerfeuer. Es waren kleine, chaotisch
geformte, orangerote Stoffbahnen, die von unten sachte beleuchtet
wurden, sehr leicht waren und sich in einem Aufwind bewegten, der
thermisch in einer dafür vorgesehenen Vorrichtung entstand. Sie
hatte dazu wohl vorher den Tag über in der Sonne stehen
müssen. Am beeindruckendsten war aber der Funkenflug, der auf
die oben sehr durchsichtigen, dunklen Stoffenden projiziert
wurde. Es war für ein unechtes Feuer beeindruckend schön.

Gabriane setzte sich reihum zu verschiedenen kleinen Grüppchen und
schließlich waren sie irgendwann dran. Sie erkundigte sich, ob
sie alles hatten und es ihnen so gut wie möglich ging. Es
war eine gute Frage. Denn sie bemerkten dadurch, dass sie
etwas Bewegungsdrang hatten und vorm Schlafengehen noch
einen Spaziergang machen wollten. Sie planten gemeinsam eine
Route, sodass sie ein bisschen aber auch nicht zu viel
vom Festivalgelände sehen würden.

"Hast du eigentlich auch irgendwann mal Zeit für dich, so gut, wie
du dich um alle kümmerst?", fragte Nurek.

Garbriane lächelte und nickte. "Eine meiner wichtigsten Aufgaben
in der Leitung des Securiteams ist es, mir nicht zu viel zuzumuten und
genügend Pausen zu machen.", erklärte sie. "Ich habe eine Vorbildfunktion. Und
ich habe die Aufgabe, Grenzen zu kommunizieren. Wenn das Funkenfest
das Securiteam überlastet, dann muss das Angebot verringert werden, damit
weniger Leute kommen, oder es muss das Securiteam vergrößert werden. In
den letzten zehn Jahren ist die Größe des Festivals eigentlich recht stabil. Wir
sind gut aufgestellt, es läuft prima. Also, abgesehen von den
Barriere-Problemen bezüglich mangelnder EM-Felder, über die wir bereits
sprachen."

"Das ist gut.", sagte Nurek. "Ich schaue mir eben allein an, was du
für uns tust, und das würde mich schon über den Rand meiner Grenzen
bringen." Nurek wandte den Blick von Gabriane ab und beobachtete
nachdenklich das Feuer, vielleicht etwas melancholisch. "Ich
würde auch gern irgendwo helfen. Eigentlich."

"Meine Belastungsgrenze ist überdurchschnittlich hoch. Und
deine vermutlich niedriger als die der meisten Leute.", sagte
Gabriane vorsichtig. Als ob sie Angst hatte, damit negative
Gefühle hervorzurufen, vermutete Marim. "Ich kann mir vorstellen,
dass auch das eine Belastung ist. Helfen zu können, ist etwas
Schönes, aber selbst für mich gibt es Grenzen und das fühlt
sich manchmal nicht gut an. Inzwischen bin ich es gewohnter. Ich
bin froh, dass du es hergeschafft hast. Ich bin nicht sicher, ob
es das irgendwie besser macht, aber du machst Mut. Und wenn du
gut auf dich Acht gibst, hilft das auch vielen, und zwar nicht
nur aus dem Grund, den viele sofort nennen, weil dann
weniger Einsätze nötig sein werden, sondern auch, weil du uns damit
Erfahrungen schenkst, wie wir das Angebot verbessern
und inklusiver gestalten können, sodass sich am Ende mehr
Personen hertrauen. Indirekt beeinflusst du mit dem Mut, alles
im Vorfeld so genau abzusprechen und zu erklären, Teilhabe
für andere."

Nurek nickte nur ein wenig, aber schwieg ansonsten, den
Blick weiter ins Feuer gerichtet.

"Heißt das, ein guter Beitrag könnte sein, hinterher
einen Bericht darüber zu schreiben?", fragte Marim.

"Wenn ihr das gerne machen mögt, auf jeden Fall.", bestätigte
Gabriane.

"Hast du dich viel mit Psychologie auseinandergesetzt?", fragte
Marim. Das hatte für ihn ein bisschen aus ihrer Art, mit
Nurek umzugehen, herausgeklungen.

Gabriane bestätigte und berichtete ein bisschen davon, womit
sie sich auseinandergesetzt hatte, aber auf einmal war
Marims Aufmerksamkeit nicht mehr in der Lage zu folgen, weil
Nurek sich verabschiedet hatte, sie würde sich um Nahrung
kümmern. Sie musste dazu nicht weit weg gehen. In der Nähe
des Pseudofeuers in Sichtweite befand sich ein längerer Tisch
mit einer Reihe Lebensmitteldruckern. Er beobachtete, wie
sie einen zu bedienen anfing und ihren Taschenrechner
in der Wartezeit des Druckens hervorholte. Kurz darauf
hatte er eine Nachricht. Da er ohnehin nicht zuhören konnte,
unterbrach er Gabriane, um die Nachricht anzusehen, weil
es ihm wichtig vorkam.

*Nurek: Soll ich dir was mitbringen?*

Marim überlegte, dass an Lebensmitteldruckern immer das
einfachste war, den Druckauftrag zu wiederholen.

*Marim: Das gleiche wie du.*

Nurek reagierte rasch mit einem bestätigendem Smiley. Anschließend
versuchte Marim sich wieder auf das Gespräch zu fokussieren und
sich angemessen für die Unterbrechung zu entschuldigen, die
Gabriane aber gar nichts ausgemacht hatte. Es fiel ihm
sehr schwer. Gabriane merkte es und verabschiedete sich, als
Nurek wieder zurückkam.

"Ich wünschte, ich hätte bessere Worte für dich.", sagte
sie noch.

"Die Worte sind in Ordnung.", widersprach Nurek. "Es ist mein
Problem. Das musst nicht du lösen."

Marim runzelte die Stirn. Ihm kam es nach einem seltsam unbefriedigenden
Abschied vor. Gabriane war nichts anzumerken, aber ihn wühlte es
auf.

Sie schwiegen, während sie aßen. Nurek aß wie immer relativ langsam
aber für ihre Verhältnisse schnell. Und als sie fertig waren und
das Geschirr gespült und zurückgestellt hatten, brachen sie auf.

"Können wir die Route andersherum gehen?", fragte Nurek.

Marim stimmte zu. Sie hatten sich überlegt, auf dem Rückweg den direktesten,
ruhigsten Weg von außerhalb des Festivalgeländes zu ihrem Zelt zu gehen, und
auf dem Anfangsweg etwas mehr vom Gelände zu erkunden. Nun verließen
sie das Gelände stattdessen rasch. Marim konnte sich vorstellen,
dass sie auf dem Rückweg noch einmal umplanen und insgesamt wenig
Festivalgelände sehen würden. Nurek fühlte sich aus irgendwelchen Gründen
nicht gut. Im besten Fall war es einfach Überlastung und
Schlaf würde helfen. Und vorher etwas Bewegung.

Sie gelangten über einen breiten Weg, der sich durch das
Gelände zog und auf dem um diese Uhrzeit noch relativ viel aber
auch nicht schlimm viel los war, auf einen ausgetretenen Wanderweg durch
die trockeneren Gegenden des Moors. Soweit er die Landschaft überblicken
konnte, war sie sehr schön und ein Typ Landschaft, den er selten
sah, aber es war schon dunkel. Abgedeckte Lichter waren im Boden
montiert, sodass der Weg sachte beleuchtet war und sie sehen konnten, wo
sie ihre Füße hinsetzten, aber ab Becken aufwärts und um sie herum hüllte
angenehm entspannendes Dunkel den Raum.

"Wir müssen reden.", sagte Nurek.

Marim versuchte die Panik niederzuringen, die dieser Satz
in ihm auslöste. Leider war er damit nicht schnell genug. Er
hatte "Oh, liegt es an mir?" gefragt, bevor er sich sortiert
hatte. Als Nurek antwortete, hatte er immer noch nicht ganz ergründet,
warum es ihn überhaupt panisch machte. Dann mussten sie eben
reden.

"Was?", fragte Nurek irritiert. "Also, was ist mit 'es' gemeint."

"Es tut mir leid, ich habe zu schnell reagiert.", sagte Marim. "Ich
merke seit ein paar Stunden, dass du -- vorsichtig gesagt -- mies
gelaunt bist. Oder gereizt oder angespannt. Was in Ordnung ist."

Nurek wandte ihm für ein paar Momente schweigend den Kopf zu. "Ich
bin wütend.", teilte sie ihm mit, und so klang sie auch. Als ob
sie davon noch eine Menge zurückhalten würde. "Das hast du nicht
verdient. Ich will nichts an dir auslassen."

"Dann weiß ich das jetzt. Und du kannst mich anschreien und
alles, was du halt gerade brauchst.", versprach Marim. "Wir
können das hinterher alles sortieren. Mach dir gern erstmal
Luft."

Nurek nahm sich keine Luft. Marim hörte sie nicht einmal atmen. Sie
blickte ihn einfach weiter an und stolperte im nächsten Augenblick
über eine der am Rand des Wegs platzierten Leuchten, weil sie
nicht bemerkt hatte, wie sie ausversehen nicht mehr so ganz geradeaus
gelaufen war. Marim fing den Sturz ab. Er konnte sie nicht auffangen, aber
er konnte sie so stützen, dass ein Ausfallschritt und in die
Hocke gehen den Sturz im Wesentlichen vermied.

"Ich hasse alles!", sagte sie schließlich, als sie wieder standen. "Du
hast mich schon wieder aufgefangen. Du lässt zu, dass ich dich
anschreie. Du nimmst den Weg andersrum mit mir. Du isst das Gleiche, wie
ich, damit ich mich weniger um Dinge kümmern muss. Oder? Das war
doch der Grund, oder?"

Sie wurde tatsächlich lauter. Und Marim war doch der Grund für die
schlimmen Gefühle. Das war, wovor er Panik gehabt hatte, aber nun fühlte
er sich nicht panisch. Es machte ihn nicht fertig. Zumindest nicht
so sehr, dass es in irgendeinem sinnvollen Verhältnis mit der Panik stand. Das war
interessant. "Ja, war es.", sagte er niedergeschlagen.

"Und das geht so, seit wir losgefahren sind.", sagte Nurek. Wieder
ruhiger, als müsste sie dabei bleiben, damit sie den Ausdruck
der Wut aufrecht erhalten könnte. "Das Fiese ist, es ist abgesprochen,
das war der Plan, und ja, ich hätte es vielleicht geschafft, dir
Essen zu drucken, das nicht ein Wiederholungsdruck gewesen wäre, aber ja, es
hätte mich auch ein bisschen gestresst. Du kümmerst dich gut, und
ohne das Kümmern könnte ich nicht. Ich hasse das." Sie holte Luft. Sie
war tatsächlich wieder lauter geworden. "Ich hasse diese Abhängigkeit. Ich
hasse, mich nicht um mich selber kümmern zu können. Verstehst
du?"

Marim verstand. Er nickte, und weil er sich nicht sicher war, ob sie
das sah, stimmte er auch verbal zu. Er hätte sie am liebsten fest
in den Arm genommen, aber er hielt sich noch davon ab. Wenn, dann sollte
er vorher fragen. Und vielleicht würde sie sich nicht mehr trauen
wütend zu sein, wenn er es täte, also wartete er noch ein bisschen.

"Richtig schlimm war für mich der wahrscheinlich nach außen
unscheinbare Moment, als du Gabriane frugst, ob ein Bericht helfen
würde.", sagte sie.

"Weil ich ihn am Ende schreiben würde, weil du es vor dir herschieben
würdest, bis es nicht passiert?", fragte Marim.

Nurek lachte mitten in die Wut. "Ja.", sagte sie. Sie fand aber
dieses Mal genauso rasch zurück. "Weil ich ins Bedauern gerate, dass
ich nicht viel beitragen kann und damit klarkommen muss. Und dann
kommst du an, und verstehst überhaupt nicht, dass es darum geht, dass
*ich* es nicht kann, sondern schlägst vor, wie du stattdessen wieder
*für* mich beitragen kannst."

"Das war unachtsam.", stimmte Marim zu. "Es tut mir leid."

"Das ist Unfug!", rief Nurek. "Du bist mega achtsam. Die ganze
Zeit. Du bist mit deiner Achtsamkeit sehr perfektionistisch. Du
hast meine Wut nicht verdient."

"Es geht nicht ums Verdienen.", sagte Marim. "Du hast Wut. Die
Wut ist verständlich. Sie muss raus. In der Wut ist auch Kritik an mir, aber
vor allem an den Umständen. Das ist in Ordnung, wie du das machst."

"Ich habe keine konkrete Kritik an dir.", widersprach Nurek. "Ich
bin das in meinem Kopf durchgegangen. Immer wieder. Was ich
mir anders wünschen könnte. Wie ich dir sagen könnte, dass ich
mich mit der Menge Raum und ständigem Dasein, die du mir gibst,
unwohl fühle, aber in konstruktiv. Also, mit einer Idee, was du
ändern könntest, sodass es mir besser ginge. Aber ich habe keine. Ich
brauche dich auch. Ich habe dich derbst lieb, aber ich hasse dieses
Brauchen gerade sehr." Dieses Mal steckte weniger Energie in ihrer
Wut, weil sie langsam weniger wurde, glaubte er, und nicht, weil
sie Wut unterdrückte. "Deshalb wirkst du in meiner Darstellung
irgendwie wie die Ursache der Wut, bist sie aber nicht. Du verhältst
dich ungefähr bestmöglich, du hast nicht viel Spielraum."

"Wenn wir versuchen, den Spielraum auszureizen, ist das dann
produktiv, weil wir vielleicht erreichen können, dass
du dich eine Spur wohler fühlst, oder kontraproduktiv, weil wir
darüber reden, was ich noch besser für dich machen kann?"

"Letzteres.", sagte Nurek.

"Trotzdem ein Vorschlag, nicht genau in die Richtung, aber
vielleicht mit Zusammenhang zum Thema, wenn du magst?", fragte
Marim vorsichtig.

"Okay.", sagte Nurek.

"Wir hatten über einen langen Zeitraum abgesprochen, was du alles
brauchst. Wir haben herausgefunden, dass ich kaum etwas, was
dir dieses Gefühl gibt, abhängig zu sein, reduzieren kann.", leitete
er ein. "Und zugleich hast du das Bedürfnis nach mehr Eigenverantwortung oder
Eigenständigkeit. Würde es dir helfen, wenn du mehr Zeit ohne mich
verbringst? Soll ich dich quasi manchmal irgendwo abgeben
und hinterher wieder einsammeln? Sodass du mich in der Zeit nicht
um dich herumwuseln hast, aber trotzdem die Sicherheit, dass
du an dem jeweiligen Ort zurechtkommst, und mich später wieder
hast?", fragte
Marim. "Vielleicht weniger ein Ansatz um mit dem Problem selbst
umzugehen, aber vielleicht hilft es, wenn du dann mehr Raum hast."

Marim fühlte sich nicht so wohl mit der Frage. Er hatte solche
in der Vergangenheit mit Nurek zu stellen gelernt. Anfangs war
es ihm sehr schwergefallen. In der Frage steckte die Abhängigkeit,
über die sie sprachen, implizit drin. Sie mochten sie ja beide
nicht. Aber es ging nun mal nicht anders und war der Preis
für Teilhabe.

Nurek schüttelte den Kopf, was Marim nur gerade so in der
Dunkelheit ausmachen konnte. "Manchmal frage ich mich, ob wir
zu sehr aneinanderkletten. Aber ich habe hier auf diesem Festival
definitiv das Bedürfnis dazu. Ich habe höchstens Angst, dich
einzuschränken."

"Tust du nicht.", sagte Marim. "Ich genieße jeden Moment mit
dir. Selbst die Wut. Sie hat mir Angst gemacht am Anfang, aber
nun auch erleichtert." Er fragte sich, warum. Wie er es Nurek
begründen konnte. Aber so richtig klar war es ihm nicht. Es erklärte, was
los gewesen war, aber besser wäre doch eigentlich gewesen, wenn kein
Problem vorgelegen hätte.

---

Auf dem Rückweg nahmen sie doch den längeren Weg über das Festival-Gelände, den
sie eigentlich für den Hinweg geplant hatten. Es war noch relativ ruhig. Sie
waren ja schließlich früh angereist. Es gab einen Bereich des Camps mit
Holzhütten. Einige Bauten waren noch nicht
abgeschlossen und die zukünftigen Wände lagen noch auf dem Boden rum, Werkbänke
mit Werkzeugen daneben. Sie beobachteten einige Personen, die dabei waren,
Zelte oder Wände mit LED-Lichtern zu überspannen, auf die sie verschiedene
Lichtmuster spielten. Sie faszinierten, taten Marim aber auch in den
Augen weh. Nurek hielt ihn bei einem nicht so grellen Häuschen auf. Es
war schon fertig eingerichtet und über der Tür stand *AG Betriebssysteme*.

"Das ist der Verein, aus dem auch die Band *Die Fenster* stammt, eine
Art Rechner- und Technik-Vintage-Verein. Kennst du den?", fragte Nurek.

"Ich habe davon gehört.", sagte
Marim. "*Die Fenster* spielen hier auch, oder?"

"Oh, das war mir nicht klar!", rief Nurek. Sie hopste ein paar Mal auf
der Stelle und ihre Hände wanderten als Ausdruck von Freude an ihr
Gesicht. "Wollen wir auch das Konzert ansehen?"

Marim musste breit grinsen. "Gern!", sagte er.

"Ich würde gern jetzt noch einen Blick ins Museum werfen.", sagte sie. "Wenn
es sich um eines handelt. Zumindest
schrieb Rosa Pride-Away, dass die Räume der *AG Betriebssysteme* ein
bisschen wie ein interaktives Museum sind. Magst du?"

Marim nickte. "Auch das gern.", sagte er.

"Was würdest du tun, wenn ich nicht da wäre?", fragte Nurek. Ein Teil der
Niedergeschlagenheit von vorhin kehrte dabei zurück in ihre Stimmung.

"Ich wäre daran vorbeigegangen, ohne zu merken, dass es neben mir steht.", sagte
Marim. "Vielleicht würdest du oder eine andere Person mir morgen davon erzählen
und ich würde denken: 'Cooles Museum, das möchte ich mir ansehen. Ich muss morgen
mal daran denken, danach zu schauen, wenn ich da lang spaziere.' Aber wenn ich
dann am nächsten Abend wieder hier lang käme, wäre ich so sehr in meinem Headspace
bei irgendeinem wunderschönen Problem -- zum Beispiel bei meiner Studie und
einem Virtualitäts-Design --, dass ich nicht einmal so richtig verstehen würde,
dass ich eigentlich gerade gehe oder auch nur eine physikalische Repräsentation
auf dieser Welt habe. Ich würde im Zelt ankommen und nicht
wissen, dass ich gerade schon wieder am Museum vorbeigelaufen bin. Irgendwann
mitten in der Nacht würde ich dann vielleicht kurz aus dem Schlaf fahren und
denken, ich sollte mir eine Notiz oder so machen. Aber ich würde es die
ganze Zeit hier nicht hinkriegen, den Weg so aufmerksam zu gehen, dass ich
nicht vorbeilaufe. Einfach weil mein primäres Ziel Schabernakel
ist, und das Museum ein Nebenplan. Vielleicht käme ich soweit, im Nachhinein eine
Idee zu haben, welches der Bilder meiner Umgebung, die
ich in dem Moment aufgenommen aber nicht verarbeitet
habe, das Museum hätte sein können." Marim pausierte seinen Frustmonolog
nur einen kurzen Moment, holte nicht einmal ausgiebig Luft. "Im
Nachhinein würde ich mir denken: 'Nun ja, wieder einmal eine Gelegenheit
verpasst. Eigentlich so vier bis fünf. Was soll's.' Aber eigentlich auch
nur, weil ich es nicht ändern kann. Ich bin dann etwas resigniert. Und
wünschte mir irgendwie doch, hineingegangen zu sein."

"Komm mit!", sagte Nurek einfach lächelnd. Sie hielt ihm die Hand hin, und
er nahm sie an. Sie war weich und eigentlich viel zu warm, aber fühlte
sich tröstend an, und als hätte sich wieder etwas zurechtsortiert.
