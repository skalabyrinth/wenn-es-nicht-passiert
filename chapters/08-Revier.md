Revier
======

\Beitext{Nurek}

Wie sich diese Haare vom Kopf in ihrem Schoß aus ins
unrealistische Wasser um sie herum ergossen und sich
darin verteilten. Das könnte doch noch mehr übertrieben
werden. Vielleicht sollte sie sich eine Virtualität
mit flüssigen Haaren bauen oder wünschen.

Es waren zwar sehr schöne Gedanken, aber welche, die
sie nicht jetzt ausbreiten wollte. Marim hatte sich
gerade erst beruhigt. Nurek wusste noch nicht einmal
im Detail, warum er geweint hatte. Musste sie natürlich
auch nicht. Sie hatte das Gefühl, dass Marim nichts
dagegen hätte, sie einzuweihen, aber das im Moment
erst einmal Weinen und gestreichelt werden dran gewesen
war.

Gerade klappte das mit der Empathie ja mal gar nicht. Da
lag eine Person in ihrem Schoß, die sie eigentlich sehr
lieb hatte, und litt, und sie fühlte: Resteuphorie
von der Triangulierung, Lust, sich mit physikalischen
Eigenschaften von Haaren auseinander zu setzen, sowohl
realistischen als auch der angenehmen Verwirrung, die
unrealistische Eigenschaften auslösen würden. Ihr blieb
nicht viel anderes übrig, als Empathie zu masken, so
gut es ging, irgendwo nach dem Gefühl zu suchen, das
Zuneigung war, aber gerade nicht in den Fokus wollte, und
wenigstens physisch da zu sein, so gut es ging.

Marim drehte sich auf den Rücken und blickte zu ihr auf. Er
wirkte nun wieder erstaunlich gelassen, nur müde. "Wie
geht es dir eigentlich?", fragte er.

Ausgerechnet diese Frage. Nurek strich die durch die
Drehaktion entsprechend verdrehten Haare wieder glatt. Sie
schluckte. "Falsch."

"Soll ich besser nicht fragen?", fragte Marim.

Nurek atmete tief ein und aus. "Ich wäre gerade gern für
dich da. Aber ich bin es nicht."

Marim runzelte die Stirn. "Mein Eindruck ist ziemlich anders.", sagte
er. "Was meinst du damit?"

"Ich habe dich gerade nicht lieb.", sagte Nurek. "Oder ich fühle
es zumindest nicht. Weil mein Kopf mit anderen Dingen voll
ist."

"Temporär?", fragte Marim.

Nurek nickte. "Ich glaube, das gehört mit dazu, dass ich
neuroatypisch bin. Manchmal fühle ich nicht, dass ich Leute
mag, weil mein Gehirn mit anderem beschäftigt ist. Es
kommt wieder. Aber es ist ein denkbar ungünstiger Zeitpunkt
gerade."

Marim lächelte sanft. "Dafür habe ich dich gerade umso mehr
lieb."

Das Lächeln steckte an. Und für einen Moment drang sanft das
Gefühl zu ihr durch, wie wichtig Marim ihr war. Dann war
es wieder halb bei Triangulierung. "Danke, dass du so
eine verständnisvolle Person bist."

"Fühlst du dich nun weniger falsch?", fragte Marim.

Nurek grinste. "Nein. Es ist so, so falsch!", sagte sie. "Du
weinst in meinem Schoß, brauchst offensichtlich Support, und
am Ende tröstest du mich!"

"Du hast gut getröstet.", sagte Marim. "Ich mag die Dunkelheit
und Ruhe in dieser Virtualität, und die Vertrautheit deiner
Anwesenheit. Das war alles sehr gut."

"Oh, das ist auch so ein Gedanke, den ich oft habe:", fiel
Nurek ein. "Ich verknüpfe mit Dunkelheit so viel Positives. Es
bedeutet für mich Ruhe, Entspannung, Friedlichkeit, Rückzug. All
die guten Dinge. Leute finden sogar Nächte romantisch. Aber in
allen möglichen Sprüchen oder Geschichten ist Licht die Analogie
für Positives und Dunkelheit für Schlechtes."

"Jop, nervig.", bestätigte Marim sachlich. "Licht tut mir in
den Augen weh. Das ist bei manchen Filmen immer besonders toll,
Klammer auf, Sarkasmus, Klammer zu, wenn das Ende mir als
positiven Abschluss die Netzhaut zerschießt."

Nurek nickte und strich Marim über das Gesicht. "Mit
wem rede ich." Und trotzdem fühlte es sich angenehm an, mit
ihm darüber einig zu sein.

"Dränge ich mich dir auf?", fragte Marim. "Soll ich gerade
eigentlich am besten eine andere Person nach Support fragen?"

Nurek schüttelte den Kopf. "Ich bin gern mit dir hier. Ich
fühle nur nicht so stark, wie ich gern möchte. Wenn du mehr
Support brauchst, kann ich gerade nicht. Aber es gilt, wenn
mir etwas unangenehm oder zu viel ist, grenze ich mich selber
ab." Sie fühlte sich awkward, als sie das sagte. Vielleicht,
als träfe es nicht genau den Punkt. Sie überlegte, dass es
aber auch nichts weiter dazu zu sagen gäbe, und wechselte
das Thema ein wenig. "Willst du erzählen, was los war?"

"Ich habe mich in den letzten Tagen überlastet.", sagte
er und versuchte dann möglichst kurz zusammenzufassen: "Ich habe mit
Linoschka an Plänen gearbeitet, wie eine fiese, ableistische,
widerwärtige Spielgruppe ausgetrickst werden kann, damit diese nicht
weiter Personen psychisch zerstören kann. Einige haben prinzipiell
auch geklappt, aber einige auch nicht, und einiges bisher
zumindest nicht so gut wie gehofft. Das
hat mich emotional aufgewühlt und geistig gefordert und deshalb
bin ich einfach fertig. Nichts Schlimmes."

"Das klingt übel.", sagte Nurek sachlich. "Ich glaube, ich
habe dich noch nie solche Worte in den Mund nehmen gehört."

"Ich habe noch schlimmere.", sagte Marim grinsend, hörte
dann aber wieder auf damit. "Es sind so widerliche Dinge passiert. Ich
kann dir gern einen Artikel, Vlog oder eine Doku
zeigen, worin das gut zusammengefasst wird. Aber eigentlich will ich
lieber Abstand haben."

Nurek nickte. Sie machte den Mund auf und schloss ihn wieder. In
ihrem Sichtfeld blendete sich die Nachricht ein, dass eine Person
im Outernet an der Tür stand und noch niemand sie reingelassen
hatte. Sie erwarteten sogar eine Person. "Taugt Bjork als
Ablenkung?", fragte sie.

Marim nickte. Dann kicherte er plötzlich. "Wie das wohl
kommt, wenn ich begrüße mit: 'Guten Tag, ich bin die eine
der beiden Personen, die euch während der vergangenen
Woche mit Strategien versorgt hat, aber ich habe gerade
keine Lust, darüber zu reden.'?"

"Probier es aus!", schlug Nurek vor. Vielleicht war es kein
guter Vorschlag. Vielleicht war es einer, der eher zu
ihr passte, weil sie nach all den Jahren immer noch keinen
Plan hatte, wie sie sich denn bitte sonst verhalten sollte. Es
kam einfach alles, was sie sagte, infrage, als unhöflich
eingeordnet zu werden. Jedes Einschränken bezüglich Dingen,
nach denen ihr zumute war, sie zu sagen, wäre willkürlich
oder vollständig gewesen.

---

Bjork war ein sehniger, kräftig wirkender Ork, etwas
mehr als einen Kopf größer als Nurek, also kein allzu
großer Ork, mit dunkelbrauner Haut. Er trug eine hellbraune,
kurze Hose, deren Taschen halb von dem breiten Beckengurt
des schweren Wanderrucksacks überdeckt wurden, und ein
für das Wetter zu warmes, cremeweißes Oberteil mit hochgekrempelten,
weichen Ärmeln und Kapuze.

"Linoschka schläft.", begrüßte ihn Nurek.

"Linoschka?", fragte Bjork. "Soll ich den Namen schnell wieder
vergessen, weil du Torf meinst?"

"Oh. Mist!", fluchte Nurek. "Ja, bitte."

"Glücklicherweise ist mein Namensgedächtnis schlecht.", sagte
Bjork. "Obwohl hier zwei Faktoren mies reinspielen: Zum einen
die Sache mit dem rosa Schnorch, an den wir nicht denken sollen, und
zum anderen, dass ich für das Spiel angefangen habe, Gedächtnistraining
zu machen. Das ist aber immerhin nur so semierfolgreich." Bjork
versuchte sich die Ärmel mit womöglich vergleichbar wenig
Erfolg noch weiter hochzukrempeln. "Moin!", sagte er. "Überhaupt."

"Ach richtig, Begrüßungsgedöns.", sagte Nurek. Das hatte sie
noch nie so richtig verstanden. "Wenn du das brauchst, bemühe
ich mich gleich, das nachzuholen. Ich nehme an, du willst Sachen
abstellen, dich teilentkleiden und eventuell kühl duschen?"

Bjork schüttelte den Kopf, aber es wirkte sonderbarerweise
nicht wie eine ablehnende Geste. "Das wäre unwahrscheinlich
wunderprächtig und viel besser als eine Begrüßung.", sagte
er. "Ich wollte eigentlich nur sagen, dass ich da bin, und mich
nicht aufdrängen. Draußen campen ist für mich fein."

"Wenn du Dinge aus Zurückhaltung ablehnst, würde ich dir
ans Herz legen, dich umzuentscheiden.", sagte Nurek und
setzte an, weiterzusprechen, dass sie aber auch nichts aufdrängen
wollte, aber Bjork hob eine Hand.

"Das war ein Missverständnis.", sagte er. "Irgendwie hatte
ich direkt ein gewisses Vertrauen zu dir und würde die
Angebote gern annehmen."

Eine ausgesprochen höfliche Person, und vielleicht dadurch auch
verwirrend, befand Nurek. Sie öffnete die Tür noch weiter, was
ebenso lediglich eine Geste war wie das Ärmelhochkrempeln, weil
Bjork längst mühelos hindurchgepasst hätte.

Sie war erleichtert, als
Marim die Einführung übernahm. Ihr Gehirn war noch damit
beschäftigt, nachträglich zu versuchen, herauszufinden, woher
das Missverständnis gekommen war. Und dann mit der Frage, ob
die Ähnlichkeit der Wörter Ork und Bjork zufällig war. Bjork
war, genauso wie Torf, ein Online-Name. So etwas war durchaus
oft nicht Zufall. Aber Nurek überlegte, vielleicht besser
abzuwarten, ob irgendwann von anderer Seite ausgehend
eine Geschichte dazu
erzählt würde, und nicht selbst nachzufragen.

Bjork duschte höchstens eine zehntel Stunde, eher weniger
lang und setzte sich in frischer dünnerer Kleidung zu
Marim und Nurek in den Gemeinschaftsraum. "Zu wievielt
wohnt ihr hier?", fragte er.

"Sechs.", sagte Nurek. Es fühlte sich immer noch schön an, nicht
mehr mit 'fünf' zu antworten. "Zwei sind aber diese Woche auf
Reisen. Es bleiben mein Geschwister, Torf, er und ich." Sie
deutete auf Marim und sich. "Ich bin Nurek. Sie, ihr, ihr, sie."

Bjork stellte sich noch einmal mit Pronomen vor, die Nurek bereits
wusste, und Marim tat dasselbe. Dann schwiegen sie ein
paar Momente bis Bjork wieder das Wort ergriff. "Ich will
wirklich keine Umstände machen. Wie mache ich am wenigsten? Indem
ich mich nun einfach draußen mit mir selbst beschäftige, bis
Torf aufwacht?", fragte er. "Und bitte täuscht keine
Höflichkeiten vor. Ich komme prima allein zurecht, und der
Grund, warum ich hier sitze, ist, falls euch Konversation
und Kennenlernen lieber ist."

"Dir ist das unangenehm, dass Torf noch nicht wach ist,
richtig?", analysierte Marim.

"Bitte nicht wecken.", bat Bjork, statt zu antworten.

Nurek hätte gern gewusst, ob Marim mit der Einschätzung
recht gehabt hatte. Bjork machte auf sie spontan einen
sympathischen, aber leider auch sehr überfordernden
Eindruck, indem er sich nicht klar genug für sie
ausdrückte. Vielleicht ging es sie nichts an. Oder er
hatte seine eigene Sprache, die sie mit der Zeit lernen
würde. Gerade fühlte sie sich hochgradig angespannt.

"Ich hatte das eigentlich ohnehin nicht vorgehabt.", sagte
Marim. "Sie hatte in den letzten Tagen Schlafprobleme. Ich
wecke auch mal Leute, aber in diesem Fall hat sogar sie sich
selbst gewünscht, dass wir sie nur zu ganz wichtigen Dingen
wecken sollten. Falls das mit dem Schlaf mal klappt."

Und es musste gut klappen, überlegte Nurek. Auch Linoschka hatte
im Normalfall Alarm für die Klingel eingerichtet. Für
Bjork hätte sie dieser zuerst erreicht haben müssen.

"Ich bin auch früher als erwartet.", sagte Bjork.

"Nein.", widersprach Nurek verwirrt. "Du hast mitgeteilt, wann
deine Fähre in Fjärsholm anlegen würde. Das passt mit der
Entfernung." Er hatte nicht abgeholt werden wollen, was
zu den Charaktereigenschaften passte, die Nurek bis
jetzt kennen gelernt hatte. Aber vielleicht schloss sie zu schnell.

"Ich wandere oft schneller, als Leute erwarten.", räumte
Bjork freundlich ein.

"Du hast über nun fünf Spielphasen hinweg öffentliche Daten über
deine Bewegungsmuster, deine Sportlichkeit und ähnliches ins Internet
gestreut, und besuchst eine Hack-Kommune. Unsere Estimated Time
Of Arrival, kurz ETA, für dich wäre sogar etwas vor deiner
Ankunft gewesen. Aber vielleicht hast du so lange draußen
warten müssen, weil Li", Nurek unterbrach sich. "Mist. Torf
noch schläft und es so lange gedauert hat, bis deine Ankunft zu
mir durchgemeldet worden ist."

Bjork blieb ein paar Momente ruhig, als würde ihn das überhaupt
nicht berühren. Dann breitete sich ein Lächeln in seinem
Gesicht aus und er gluckste warm. "Ich habe bereits acht Spielphasen
hinter mir, aber die jeweils ersten sind nicht mitgezählt, weil
aus ihnen keine Daten öffentlich werden?", fragte Bjork.

Endlich verstanden sie sich. Dachte Nurek, aber fand
den Gedanken gleich darauf albern. "Exakt.", bestätigte sie.

"Wir haben dabei diskutiert, ob das ethisch okay ist.", räumte
Marim ein und fing an, das gemeinsame Gespräch dazu wiederzugeben, aber
Bjork hielt ihn wieder höflich auf.

"Es ist vollkommen in Ordnung.", sagte er. "Es ist mir
absolut bewusst, dass diese Daten über mich frei zugänglich
sind, und ich bin im Spielkontext gewöhnt, dass Leute sie
auswerten. In anderen Kontexten wäre das vielleicht
uncool. Aber macht euch hier keine Sorgen."

"Wir werden zusammen campen.", sagte Nurek. Sie fragte
sich, ob sie damit das Thema wechselte, warum ihr
so wichtig war, das ausgerechnet jetzt zu sagen, und ob
ihm das nicht längst klar wäre.

"Ah, ihr seid das.", sagte Bjork und lächelte. "Torf
hatte lediglich gesagt, zwei Herzwesen. Nicht einmal, dass
sie aus der gleichen WG sein würden. Wir hatten uns
verabredet, dass ich Torf hier abhole. Mehr Details wollte
sie ohne Absprache mit euch nicht sagen."

"Oh.", machte Nurek. Ein plötzlicher neuer Gedankensalat entstand
in ihrem Kopf, den sie versuchte, in verbale Sprache
runterzubrechen. "Hat es dich deshalb nervös gemacht, in
einem fremden Haushalt zu sein, in dem du die Leute eventuell
gar nicht länger kennen wirst? Warst du überhaupt nervös?"

Bjork blickte sie länger an und sein Lächeln wurde
zu etwas Geduldigem, das keine spontane Reaktion auf
irgendetwas mehr war. Er setzte an, etwas zu sagen, ließ
sich aber davon ablenken, dass Ivaness den Gemeinschaftsraum
betrat und sich lässig mit nach oben gestrecktem
Unterarm an eine Wand lehnte.

As trug einen flauschigen, roten Bademantel und tropfte
noch. As hatte anscheinend kurz nach Bjork das Bad
benutzt. "Wurde dir schon Ess- und Trinkgedöns angeboten?", fragte
as.

Bjork schüttelte den Kopf, aber nicht sonderlich
überzeugt. "Ich brauche nichts, danke.", sagte er
freundlich. "Ich habe auf der Fähre gegessen."

Ivaness' Blick schweifte wachsam durch den Raum und in
den Flur zur Garderobe. "Und du hast das Bad zurückgelassen, als
wärest du nie darin gewesen.", sagte as. "Wahrscheinlich hast
du ein eigenes Handtuch dafür benutzt, nach dir auch das Bad
zu trocknen und es in deinen Rucksack gepackt, um
später irgendwo draußen jenes, und was von jenem angenässt
wurde, zu trocknen. Außerdem steht dein Rucksack
maximal platzsparend in einer möglichst Nischen-ähnlichen
Ecke." Ivaness
Gesichtszüge änderten sich zu einem, den Nurek als eine
von wenigen Personen lesen konnte: Der Unsicherheit, ob
as sich gerade furchtbar unhöflich oder verletzend
verhielt. "Tut mir leid. Ich beobachte zu viel.", fügte
as entsprechend hinzu und konnte sich dann doch
nicht zurückhalten, zu fragen: "Hast du Probleme damit,
Raum einzunehmen oder anzunehmen?"

Shit, dachte Nurek. Das war eine sehr direkte Frage. Und
eine krasse Analyse. Vielleicht keine zu krasse für eine
Person, die in ein Badezimmer trat, kurz nachdem eine andere
geduscht hatte.

Aber Bjork wirkte immer noch gelassen. Und nickte. "Ja.", sagte
er und seufzte. "Mir fällt es leichter, wenn ich ausgeruht
bin. Aber nach einer Woche Spiel und der Reise fällt es
mir schwer, mich daran zu erinnern, dass ich durch Annehmen
von Dingen nicht automatisch eine Last bin."

"Ich mache mir Monua. Soll ich dir einen mitbringen?", fragte
Ivaness, den Anschein erweckend, als ginge as nicht darauf
ein, was Bjork gesagt hatte, aber irgendwie tat as das
schon. "Und: Du bist willkommen hier."

Bjork musste leise lachen. Es klang wieder so warm und
herzlich. "Monua.", sagte er und nickte. "Gern."

Aber als Ivaness mit einem Krug und mehreren Bechern
zurückkehrte, glaubte Nurek die Anzeichen der Überwindung
nun besser erkennen zu können, als Bjork sich einen
Becher -- nur halb -- befüllte.

---

Ivaness verabschiedete sich kurz, um sich anzuziehen, und
als as wiederkam, tauchte Linoschka mit iem auf. Linoschka
wirkte matt, fand Nurek, und das war gut. Das hieß, dass
sie endlich geschlafen hatte. "Es tut mir leid.", murmelte
sie.

Alle schüttelten den Kopf und murmelten durcheinander, dass
ihr nichts leidtun müsse.

"Linoschka heiße ich.", sagte sie zu Bjork. "Du kennst
mich als Torf. Aber im Outernet ist mir Linoschka lieber. Ist
das okay für dich?"

Bjork lächelte und nickte. "Ist es okay für dich, wenn
ich bei Bjork bleibe?", fragte er.

"Vollkommen.", bestätigte Linoschka.

"Habt ihr schon gepackt?", fragte Bjork.

Nurek, Marim und Linoschka bestätigten. Nurek hatte keinen Plan, wann
Linoschka gepackt hatte. Sie hatte viel vor sich hergeschoben. Sie
selbst hatte für Marim gepackt. Gestern, als er mit Kopfschmerzen
im Bett gelegen hatte und sich nicht hatte rühren können. Im
Dunkeln. Fast, zumindest, sie hatte ein kleines, blasses
Licht benutzt, das er ihr zur Verfügung gestellt hatte, das
er mal darauf optimiert hatte, dass es am wenigsten
doll im Augenhintergrund brannte. Dann hatte er ihr leise zwischen
Wimmern beschrieben, was er gepackt haben wollte. Sie hatte
versucht, ihm zu erklären, dass es unter solchen Umständen absolut
in Ordnung wäre, noch nicht gepackt zu haben, wenn es eigentlich
losgehen sollte, aber er meinte, die Kopfschmerzen würden sich
leichter verziehen, wenn kein Tatendruck mehr anstünde. Das
Phänomen an sich kannte Nurek. Sie hatte sich gekümmert, so
gut sie gekonnt hatte.

"Ich noch nicht.", sagte Ivaness, wie Nurek heraushörte, nervös, und
Nurek verwunderte alles daran.

"Wolltest du mit?", fragte Marim.

"Ursprünglich war das nicht mein Plan.", sagte Ivaness und bestätigte
damit Nureks letzten Stand. "Aber ich hatte mich gefragt, ob das
nicht doch nett werden könnte. Würdet ihr mich mitnehmen mögen?"

Marim und Bjork stimmten sofort zu. Nurek hätte es auch getan, aber
ihr Blick fiel auf Linoschka. Sie wirkte skeptisch und runzelte
die Stirn.

Auch Ivaness blickte sie einen Moment an. "Eher nicht, dann.", sagte
as.

Bjork wirkte so verwirrt, wie Nurek sich fühlte. "Ich hätte wirklich
nichts dagegen, wenn es um Rücksicht um mich ginge oder so.", sagte er. "Wenn
es um was Privates zwischen euch geht, geht es mich natürlich nichts
an."

"Die nächste Frage wäre gewesen, ob du Interesse an sowas wie
Flirten mit mir hättest.", sagte Ivaness. "Linoschka hat mir
das, glaube ich, angesehen. Und weiß offenbar, dass irgendwas
dagegen spricht."

Nurek begann zu verstehen, worum es
ging, und hatte keinerlei Ahnung, woran Linoschka das bei
ihrem Geschwister abgelesen haben könnte.

"Ah.", machte Bjork. "Tatsächlich ja, tut mir leid. Ich
bin in einer monogamen Beziehung. Darüber hatte ich mit
Linoschka gesprochen, um das im Vorfeld abgeklärt zu haben, wenn
wir in einem Zelt schlafen."

"Dann bleibe ich besser hier und flirte in diesem Internet
mit irgendwelchen anderen lieben Personen.", beschloss Ivaness
mit einem Grinsen im Gesicht.

---

Eine halbe Stunde später brachen sie nach Fjärsholm auf --
Linoschka und Bjork mit Wanderrucksäcken zu Fuß und
Marim und Nurek mit auf Rädern verstautem Gepäck langsam
nebenherfahrend --, von wo sie aus mit einem Zug ein Stück in den Norden
fuhren. Von einem vorab ausgewählten Bahnhof eines winzigen
Kaffs aus erreichten sie ihr Ziel, ein Tal, zu Fuß beziehungsweise
mit Rädern. Allerdings schoben Marim und
Nurek das letzte Stück, weil es dann doch etwas
zu steil war, um bei Bjorks und Linoschkas Wandertempo
nicht weit vorauszurollen. Nurek blickte sich um und
seufzte bei dem Gedanken, das alles wieder hinaufschieben
oder -fahren zu müssen.

Sie hatten sich einen der Wege ausgesucht, an deren
Rändern immer Mal wieder kleine Sanitärhäuschen
standen, und bauten die Zelte auch kaum 100 Meter von
einem auf einer Wiese nahe eines Waldrands auf. Hier
wurde gelegentlich gecampt. Einerseits
eignete sich der Ort, weil es hier
Schatten gab, dessentwegen weniger großflächig Gestrüpp
wucherte, andererseits wucherte hier wiederum weniger
Gestrüpp, weil gelegentlich gecampt wurde. Also
war der Ort als häufigerer Camping-Platz
protokolliert worden. Um
einen Großteil der Natur in Ruhe zu lassen, standen
solche Orte meist für alle Karten als Kartenlayer zur
Verfügung.

Nurek hatte schon, als sie noch die Fahrräder geschoben
hatten, angefangen zu stimmen, Pflanzen aufzuzählen, immer
wieder ihre Namen zu wiederholen. Bjork hatte sie verbessern
wollen, dass das Gestrüpp um sie herum keineswegs mit den Pflanzennamen
übereinstimmte, die sie aufzählte, aber Linoschka hatte
ihm das Phänomen erklärt. Nurek hatte während des
Zeltaufbaus nicht gezittert, aber sich so gefühlt, als
wäre ihr Körper in einem temperaturlosen Schüttelfrost. Marim
nahm ihr das Aufplustern der Doppelmatratze ab. Sie hatten
sich so eine aus einem für so etwas gedachten Selbstbedienungsladen in
Fjärsholm geliehen. Sie waren sehr leicht und sehr
praktisch. Nurek hatte überlegt, ob sie sich ins pieksige
Gras legen sollte, bis er fertig wäre, aber er war
viel schneller fertig als gedacht. Dann stellte er
eine Reihe von Fragen, und das machte er gut.

"Möchtest du nun absolut deine Ruhe haben, oder darf ich
dir Fragen nach deinen Bedürfnissen stellen?", fragte er
und hielt erst einen, dann zwei Finger hoch.

Nurek wäre noch in der Lage gewesen, zu sprechen, aber es
war so viel angenehmer, einfach zwei Finger hochzuhalten.

"Zelteingang auf, oder zu?" Wieder zeigte Marim ein und
dann zwei Finger.

Nurek wählte, dass er offen bleiben sollte. Sie fühlte sich
sonst allein und abgeschnitten.

"Ja, du darfst mit Gumbol kuscheln.", bestätigte Marim
eine Frage, die Nurek noch gar nicht gestellt hatte.

Ihr Blick war einfach halb unbewusst zweimal zum Krokodil
gewandert, das noch oben auf Marims Gepäck festgeschnallt
neben der Matratze im Zelt lag. Sie löste die Schnallen. Es
machte dabei zwei laute Klack-Geräusche, die ihr eigentlich
zu viel waren. Sie legte einen Arm um den weichen Kuschelkörper, der
so sehr nach Marim roch. Und eine Spur staubig.

"Möchtest du mich bei dir auf der Matratze haben, oder lieber
draußen?", fragte Marim.

Dieses Mal antwortete Nurek nicht. Ihr war nicht klar, warum. Sie
hatte Marim gern bei sich, aber irgendetwas stimmte daran nicht.

"Hast du Hunger oder Durst?", fragte Marim, übersprang die letzte
Frage.

Nurek nickte. Das war es. "Kalt ist schlecht.", sagte sie.

Sie hatten allerlei kaltes Essen dabei. Auf dem Funkenfest wären
Lebensmitteldrucker erreichbar, zumindest, solange sie nicht
overloadet im Zelt liegen würden und alles außerhalb davon unerreichbar
wäre.

"Ich sehe zu, dass wir Dinge kochen.", sagte Marim. "Vorher
noch etwas Wichtiges?"

Nurek schossen Tränen in die Augen. Sie mochte es gar
nicht, sich nicht um sich selbst kümmern zu können, und gleichzeitig
gehörte das einfach zu ihrem Leben. Außerhalb der WG zumindest. Das
Haus war vollständig mit EM-Tapete ausgestattet, sodass ein
leerer EM-Anzug im Zweifel so tun konnte, als wäre er eine Person, die
alles für sie erledigen könnte, was sie bräuchte. Und das
war nur der Ausweichplan für eine Menge Equipment, das eher
zum zielgerichteten Erledigen bestimmter Aufgaben gedacht war, wenn
Nurek es nicht konnte.

Aber ohne Internet mitten in der Botanik stand das alles nicht
so unmittelbar zur Verfügung, und auf dem Funkenfest wäre es auch
begrenzt.

---

Nurek konnte aus dem Zelt heraus beobachten, dass Linoschka und
Bjork anfingen, sich für Training aufzuwärmen. Als Linoschka bemerkte, was
Marim vorhatte, unterbrach sie das Aufwärmen und half
ihm stattdessen. Nurek hatte Angst, Bjork damit irgendwie
zu nerven, aber er wirkte überhaupt nicht genervt. Die Sonne stand
angenehmerweise schon etwas tiefer und der Wald warf Schatten auf die
Campingwiese, als das Essen zubereitet war. Nurek schälte sich dazu
aus dem Zelt, um sich dazuzusetzen. Sie fühlte sich matt.

"Es tut mir leid.", sagte sie.

Es fühlte sich etwas déjà-vu-artig an, als Bjork, Linoschka und
Marim nuschelnd versicherten, dass ihr nichts leidtun müsse.

Das Essen war ungewohnt, weil es nicht aus einem Lebensmitteldrucker
kam, sondern aus Kompaktessen hergestellt worden war: Eine Art Brei
mit Stücken. Sie hatten vorher etwas ähnliches getestet. Es war
das beste, was Nurek gerade bekommen konnte, und es half durchaus
sehr.

"Beim Spiel ist zwischendurch auch eine wohl neuroatyische Person
ausgefallen.", bemerkte Bjork.

Der Rest nickte. Das hatten sie alle mitbekommen, weil sie sich
mit dem Spiel ja auseinandergesetzt und darüber geredet hatten. Es
passierte allerdings regelmäßig, dass einzelne Personen im Spiel über ihre
Belastungsgrenze gingen oder getrieben wurden. Es gab ebenso
regelmäßig Erinnerungen, dass Teilnehmende ihre Grenzen beachten, eine
gewisse Rücksicht auf andere nehmen sollten und ähnliches.

"Das war vielleicht nicht so sensibel von mir.", überlegte
Bjork. "Marim hatte deutlich gemacht, dass er eigentlich gerade nicht
so gern über das Spiel reden möchte. Und mir ging es mehr darum, zu
sagen, dass wir hier ein soziales Gespann sein wollen, in
dem du ebenso willkommen bist, wenn du viel beitragen kannst, wie, wenn
das gerade oder auch dauerhaft eben nicht geht."

"Wir machen diese ganze Sache hier vor allem für mich. Weißt
du das?", fragte Nurek.

Bjork schüttelte den Kopf. "Das war mir nicht bewusst und", er
haderte kurz, "verwirrt mich offengestanden."

Marim erklärte die Sache mit dem Funkenfest und Bjork verstand.

"Und ich habe die Geräuschkulisse, die wir programmiert haben, noch
nicht einmal angemacht und mir ist es zu viel.", murmelte Nurek. Sie
merkte, dass sie sich enttäuscht fühlte. Eine Emotion, die sie
selten hatte und deshalb nicht gleich erkannte.

"Ist dir bewusst, dass du das gesamte Funkenfest mit Noise
Cancelling physisch ausblenden kannst?", fragte Bjork. "Du
hast doch Hörimplantate, oder?"

Nurek hob die Brauen und nickte. Dann schüttelte sie über sich
selber den Kopf. "Ich mache so etwas ständig!", sagte sie. "Warum
war mir das nicht klar?"

"Shit, mir war das letzte Nacht eingefallen, und ich
wollte dich fragen, ob du daran gedacht hattest.", sagte Marim.

Linoschka schmunzelte. Es war schon ein bisschen typisch
für so eine Hack-Kommune.

"Dann lasse ich die Geräuschkulisse aus, solange es mir nicht
wirklich gut genug dafür geht.", beschloss Nurek.

Nach dem Essen schwiegen sie ein wenig und ruhten. Es war eine
entspannte Stille. Und schließlich stand Bjork wieder auf, dem
Anschein nach, um die unterbrochene Aktivität von vorhin
wieder aufzunehmen. Linoschka folgte. Aber statt, dass
sie anfingen, blickte Bjork zu ihrem Tisch hinüber und
machte eine einladende Geste. Es war eine wunderschön
selbsterklärende Geste, fand Nurek.

Sie hatte nie gekämpft und hatte nie vorgehabt, Kampfsport
zu erlernen. Aber sie hatte sich für diesen Ausflug
auch vorgenommen, für Linoschka als Trainingssubjekt
zur Verfügung zu stehen. Sie beschloss, es einfach nicht
selber anzusprechen und zu gucken, was Bjork tat. Sie
stand auf, möglichst selbstbewusst und folgte der Geste.

"Hast du Kenntnisse in Orkando?", fragte Bjork.

Das nahm doch die Überraschung weg! Nurek schüttelte
den Kopf.

Bjork lächelte. "Hast du gerade Schwierigkeiten mit
Körperkontakt?"

"Welch komplizierte Frage.", seufzte Nurek. "Ich hätte
jegliche gerade in Kauf genommen. Ist so etwas wie ein
Safe Word 'Rot' okay?"

Bjork nickte, als wäre die Frage überhaupt nicht
sonderbar. "In Orkando gibt es für so etwas meistens
vereinbarte Körpersprache, weil der Sport oft still
stattfindet, aber es geht vor allem auch darum, dass
sich alle Teilnehmenden gut und sicher dabei fühlen. Ein
Safe Word ist für manche eine gute Übergangslösung, bis
sie andere Kommunikation lernen, und für manche bleibt
es eine permanente Lösung. Das ist in Ordnung."

Nurek nickte. Eine unbestimmte Angst verflüchtigte
sich etwas, die sie eigentlich hatte verdrängen wollen.

Bjork trat einen Schritt auf sie zu, sodass er sie mühelos hätte
an den Schultern greifen können. Aber seine
Bewegung machte irgendwie klar, wo sein Schritt aufhören,
und dass er nichts dergleichen tun würde. Er hob einen
Unterarm mit etwas Abstand vor die Brust
und machte eine Bewegung mit dem Kopf, die sie so
interpretierte, dass sie das nachahmen sollte. Bjork
beschwerte sich nicht, als sie es tat. Er trat noch
einen kleinen Schritt auf sie zu, sodass sich ihre Unterarme
an ihrer Außenseite in der Mitte berührten und ein
Kreuz bildeten. Er gab vorsichtig etwas Druck darauf. Nurek
blieb die Wahl dazwischen, Gegendruck aufzubauen oder Bjork
ihren Arm zu sich schieben zu lassen. Ihr Reflex war ersteres
und sie blieb bei der Entscheidung. Dann gab er nach, sodass
sie sich entscheiden konnte, ob sie seinen Arm zu ihm drücken
würde oder ebenfalls Kraft nachlassen würde. Sie entschied
sich wieder für ersteres, einfach aus Neugierde: Was würde
er tun? Er baute wieder mehr Druck auf und grinste. Es
steckte an, als er mit konstantem Druck einen Schritt auf
sie zumachte, und sie dem Druck folgend einen Schritt
zurück machte. Er schob sie auf diese Art langsam noch
zwei Schritte rückwärts, bis sie entschied, zur Seite
auszuweichen. Sie hielt es für ein kampftechnisch nicht sehr
vorteilhaftes Manöver. Aber sie fühlte sich dadurch
selbstbestimmter. Und Bjork folgte der Richtung.

Er ließ den Arm wieder sinken. Ihrer folgte, bis sie sich
nicht mehr berührten. Dann machte er eine Art Dankes- oder
Verbeugungsgeste.

"Wenn ihr zwei mögt, spielt mit der Übung ein bisschen rum.", sagte
er Marim und ihr zugewandt.

Marim trat auf sie zu und fragte sehr leise: "Möchtest du?"

Mit Marim war es ganz anders. Während Linoschka und Bjork ein
viel beeindruckenderes Training etwas weiter weg auf der
Wiese veranstalteten, berührten Nurek und Marim sich nur am
Unterarm und sahen sich dabei gegenseitig ins Gesicht und auf
die Bewegungen. Erst ahmte Nurek Bjorks Einleitung nach, aber
dann mischten sich wie automatisch Elemente aus Paartanz in Marims
und ihre Bewegungen. Es war viel intensiver als auf dem Ball. Vielleicht
sogar, weil sie sich weniger berührten. Oder weil sie mitten
drin die Führung wechselten -- wobei Nurek sie definitiv auch
jetzt am häufigsten hatte. Ihr Körper fühlte sich zunehmend
fiebrig aufgeregt, und wollte sich in einer langen, festen
Umarmung entladen, in der sie das Gefühl des Liebhabens
dieser Person ihr gegenüber besonders stark spüren würde. Sie
hatte diesen Gedanken kaum gedacht, als sie es beide umzusetzen
beschlossen. "Ich hab' dich schrecklich lieb!", flüsterte
sie in sein Ohr.

Er presste sie noch mehr an sich. "Ich" Was auch immer er
sagen wollte, kam nicht aus ihm heraus. Aber
das bisschen fester, dass er sie noch an sich drücken konnte, ohne
dass sie gefühlt zerbrechen würde, gab er ihr noch.

"Du mich auch.", flüsterte sie.

"Ja.", sagte er rasch und schnell.

---

Sie lagen zusammen in ihrem Zeltnest, eng aneinandergekuschelt. Sie hatten
beide vergessen, dass Nächte fast draußen viel kühler waren als im
Haus. Ihnen war nicht kalt -- aber das lag eben daran, dass sie
kuschelten. Mit Gumbol zwischen ihnen. Dies war nicht nur eine
Vorbereitung auf das Funkenfest. Es waren auch für sich ein Tag
und eine Nacht, die sich in ihr Gehirn als positive Erinnerung
einbrennen würden, und in denen sie seit dem Overload jeden
Augenblick genossen hatte. Warum war eine etwas zu kalte erste
Nacht zu zweit in einem Zelt so viel besser und besonderer, als
die meisten daheim? Also, es waren auch einige daheim sehr
schön gewesen. Die erste in ihrem Zimmer zum Beispiel. Vielleicht
liebte Nurek erste Male.

"Ich glaube, ich sollte dir was sagen.", sagte Marim leise.

Es war nicht still draußen. Der Wind rauschte in den Blättern und ein
paar Nachtvögel sangen. Nurek bezweifelte, dass irgendeine Person
vor dem Zelt auch nur eine Ahnung gehabt hätte, dass Marim
etwas gesagt hätte. Sie fragte sich, warum sie darüber nachdachte. Wahrscheinlich
schätzte sie ab, ob Marim, was auch immer er sagen wollte, wirklich
nur ihr sagte, oder Bjork und Linoschka im Nachbarzelt unwillentlich
mithören könnten. Aber so dicht standen die Zelte auch nicht
beieinander. Nurek nickte.

"Ich habe sexuelle Gewalt erlebt.", sagte Marim sachlich. "Sexuelle
Dinge sind nicht dein Thema, und es hat nichts mit dir zu tun. Aber
jedes Mal, wenn eine Situation so schön wird wie gerade, schiebt
mein Gehirn mir diese Erinnerung in den Vordergrund. Ich kann
dann versuchen, sie mit Gewalt wegzuschieben." Marim hörte zu reden
auf.

Nurek fielen eine Reihe an Gründen ein, warum er nicht weitersprach,
und die meisten hingen nicht damit zusammen, dass er schon alles
gesagt hätte, was er hatte loswerden wollen. Aber an sich
tappte sie im Dunkeln. "Was du bisher getan hast?", fragte sie, um
irgendetwas zu fragen.

Marim nickte.

Nurek konnte es nur gerade so erkennen, weil sie sehr
dicht beieinander lagen und sich direkt anzusehen versuchten. "Aus
Rücksicht auf mich?", fragte sie.

"Auch. Schon.", sagte Marim.

Nurek wartete ein paar Momente, ob er andere Gründe ausführen würde, aber
das tat er so schnell nicht. "Wir haben die Regel, dass ich mich
selber abgrenze.", erinnerte sie, ergänzte aber rasch: "Das
klingt in so einer Situation vielleicht harsch. Es tut mir leid."

"Zerstört es nicht einen wunderschönen, positiven Moment, wenn
ich es nicht verdränge, sondern erzähle?", fragte Marim.

Nurek hielt ihn einen Augenblick fester, legte eine Hand auf seinen
Kopf auf sein weiches Haar. "Teilen von schlimmen Dingen kann
zugleich positiv und schön sein.", sagte sie. "Wie es für dich
ist, musst du wissen. Aber es klingt ein wenig so, als hätte es
jeden einzelnen der bisherigen positiven Momente, die du hattest
etwas eingetrübt."

Marim nickte wieder.

Dieses Mal spürte sie es unter der Hand. Sie nahm sie wieder weg, sodass
sie so lagen wie zuvor.

"Darf ich davon erzählen?", fragte Marim. "Ich werde bezüglich der
sexuellen Dinge nicht ins Detail gehen, und zwar auch unabhängig von
dir nicht. Sie sind im Spezifischen nicht wichtig."

"Ja.", sagte Nurek schlicht. Sie fragte sich, ob sie noch einmal
sagen sollte, dass sie sich in so einem Fall auch Spezifisches anhören
würde. Aber das spielte keine Rolle. Und sie fragte sich, ob sie
sich schlecht fühlen sollte, weil für sie die Nacht gerade keine
Spur schlechter wurde.

"Ich werde die Person im Folgenden einfach als *sie* referenzieren. Ich
möchte keine Namen nennen.", leitete Marim ein. "Ich habe sie auf
einem Neujahrsfest kennengelernt. Ich nutze Neujahrsfeste oft, um
neue Personengruppen kennenzulernen. Sie fand mich grazil und
hübsch."

Nurek verkniff sich in Marims Denkpause zu sagen, dass er durchaus
hübsch war. Grazil hätte sie ihn vielleicht nicht unbedingt bezeichnet, aber
er hatte zarte Haut und wirkte auf gewisse Art harmlos, fand sie.

"Sie hat mir ihre halbe Lebensgeschichte erzählt, über ihre Erfahrungen
mit Liebesbeziehungen, und eigentlich hätte mir dabei vielleicht
schon klar werden können, dass sie versucht hat, ihre Idealvorstellungen einer
Beziehung auf andere zu übertragen, und wenn jene nicht dazu
passten, es so zu verargumentieren, als würden jene ihr unrecht
tun.", fuhr Marim fort. "Aber das habe ich damals nicht gesehen. Sie
war traurig und allein und ich habe zugehört."

Nurek nickte vorsichtshalber. Sie lauschte so leise, dass sie keinesfalls
den Eindruck erwecken wollte, eingeschlafen zu sein. Das Bild, das Marim
von sich wiedergab, wirkte nach einer stimmigen, früheren Version
von Marim, fand sie.

"Sie hat sich zu mir hingezogen gefühlt, sexuell, und ich reagiere auf
Interesse anderer Leute oft automatisch mit Neugierde. Ich spürte
also den Drang, da was Vorübergehendes einzugehen, zu experimentieren.",
fuhr er fort. "Ich hatte die Befürchtung, dass sie eine längere Beziehung
will, und habe deshalb von vornherein klar gemacht, dass das höchstens
ein One-Night-Stand wird. Und dass sie die Finger von mir lassen soll, wenn
sie eine längere Beziehung sucht. Sie hat sich einverstanden erklärt."

Nurek hatte eine blasse Ahnung, was jetzt kommen würde, und sie
fühlte sich innerlich elend. Es war in Ordnung, das zu fühlen. Es
gehörte dazu.

"Ich habe den Fehler gemacht, es ein zweites Mal zu wiederholen.", sagte
Marim und pausierte direkt wieder.

Ob er es selber merkte? Ob sie es sagen sollte? Vielleicht vorsichtig, aber
so, dass es klar eine Randbemerkung war und nicht seinen Flow
unterbrechen würde. "Klammer auf, du machst Self-Victim-Blaming, Klammer
zu.", sagte sie. "Aber wenn es gerade nicht ohne geht, achte nicht drauf
und wir reden später drüber."

Mit der heftigen Reaktion hatte sie nicht gerechnet. Sein ganzer Körper
krümmte sich zusammen. Er kramte ihre vergrabene Hand unter sich hervor,
umklammerte sie und presste sie gegen seine Wange. Sie legte die andere
leicht um ihn.

"Ich hätte nichts sagen sollen.", murmelte sie.

"Doch.", wisperte er. "Shit." Er flüsterte abgehackt. "Du hast ja
recht. Und es ist wichtig!"

Sie strich ihm über den Rücken. "Dann sage ich es noch einmal.", sagte
sie leise. "Du bist nicht dafür verantwortlich, was andere Leute auf
dich projizieren. Vor allem nicht, wenn du es vorher klar anders
erklärt hast. Du bist auch nicht verantwortlich für Gewalt, die dir
passiert, nur weil du vorher erahnen kannst, dass sie passiert."

Seine Hand krampfte sich so sehr um ihre, dass sie sich fragte, ob
gleich etwas knacksen würde. Nichts passierte. Er ließ wieder lockerer
und atmete mehrfach langsam ein und aus. Dann nickte er wieder. "Sie
wollte mit mir eine bestimmte sexuelle Sache machen, welche spielt
keine Rolle, und hat mir so lange und auf immer neue Art und
Weise erklärt, warum ich asozial und verletzend wäre, warum ich
mich toxisch verhalten würde, wenn ich nicht einwilligen würde, bis
ich 'ja' gesagt habe.", fuhr er schließlich fort. "Dabei ist es nicht
einmal so, dass ich gegen die Sache an sich etwas einzuwenden gehabt hätte. Ich
habe mich bedrängt gefühlt und deshalb unterbewusst eine Grenze
ziehen wollen. Das ging nicht."

Das war schrecklich. Nurek wusste nicht, was sie dazu sagen
sollte. Es gab keine Worte für ein solch grauenhaftes Verhalten. Sie
drückte von sich aus seine Hand etwas. Aber irgendwas sollte
sie wohl sagen. Manchmal waren schlechte Worte besser als keine. Sie
entschied sich für: "Anzünden."

Marim kicherte kurz. Oder schluchzte? Nein, das war Kichern. "Es
fällt mir auch deshalb jedes Mal wieder ein, wenn ich mich besonders
wohl in meiner Beziehung mit dir fühle, weil ein Grund für das
Wohlfühlen ist, dass du sex repulset bist.", erklärte er. "Ich
führe ausschließlich oberflächliche sexuelle Beziehungen. Ich könnte
das an sich, eine romantische innige Beziehung führen mit einer
Person, mit der ich dann gelegentlich neugierig was Sexuelles
ausprobieren würde, was dann aber nicht Teil des gleichen
Beziehungslayers wäre, sondern ein oberflächlicher anderer. Aber
das Konzept fühlt sich für die wenigsten gut an." Er machte eine
kurze Redepause, in der er nach Luft schnappte, als hätte er zu
atmen vergessen. "Deshalb kommt aber in romantischen Beziehungen
meistens irgendwann die Frage auf, ob wir nicht auch Sex haben
könnten, weil es für die anderen Beteiligten dazugehörig
wirkt, und dann macht mir das Angst, wenn ich kommuniziere, dass
für mich da eine Trennung existiert, dass die anderen Beteiligten
das nicht verstehen würden, und was das für Gefühlschaos macht. Bei
dir kann ich mich dahingehend fallen lassen. Du hast kein
Bedürfnis. Aber, ist das nicht irgendwie mies dir gegenüber?"

"Warum sollte es mies sein?", fragte Nurek.

"Ich weiß nicht, es fühlt sich seltsam an, zu sagen, ich
fühle mich bei dir sicher, weil du sex repulset bist.", formulierte
Marim noch einmal.

"Finde ich gar nicht.", sagte Nurek. "Eigentlich finde ich, fügt
sich das recht natürlich. Dein Interesse an Sex ist mehr
von Beziehungen unabhängig, wenn ich das richtig verstanden habe?"

Marim bestätigte mit einem Nicken und einem zustimmenden Geräusch.

"Also bevorzugst du für Beziehungen Personen, die kein Bedürfnis
haben, weil das Bedürfnis den Drang nach Erfüllung bei dir
auslöst, aber deine Art zu Erfüllen, würde nicht das tatsächliche
Bedürfnis einer Person decken, die eine Beziehung mit Sex
als Bestandteil davon bevorzugt.", sagte Nurek und hoffte, dass
Marim ihrem Gedankengehedder folgen konnte. "Das kann klappen, aber
es geht definitiv nicht ohne ungedeckte Bedürfnisse einher. Bei
uns schon. Das ist entspannt."

"Das ergibt erstaunlich viel Sinn.", sagte Marim. Er strich ihr
über den Kopf. "Ich lächele.", teilte er ihr mit, weil sie es
ja nicht so gut sehen konnte.

Das mitgeteilte Lächeln steckte an. Sie erwiderte Geste und
Mitteilung. Dann lagen sie wieder still da, berührten sich
ein wenig. "Du bist vom Trauma in die Befürchtung
übergegangen, dass mir ein Grund, warum du dich bei mir
sicher fühlst, unangenehm sein könnte.", fasste sie zusammen. "Möchtest
du über etwas davon ausführlicher reden."

"Darüber, wie lieb ich dich habe?", fragte er.

Sie glucksten beide ein bisschen.

"Ich glaube, mir hilft vor allem, dass du das jetzt alles weißt.", sagte
er wieder ernst. "Wenn ich jetzt in Zukunft etwas dazu sagen wollte,
bräuchte ich nur anzuknüpfen."

"Du darfst immer sagen, wenn du gerade daran denkst.", räumte Nurek
ein.

"Vielleicht denke ich jetzt auch viel weniger dran.", sagte Marim. "Also,
sicher, die Erinnerung wird immer Mal wieder hochkommen. Aber sie wird mir
nicht mehr ins Gehirn schreien, dass ich dir davon erzählen soll. Und
dann kann ich sie vielleicht auch wieder zärtlich wegräumen."
