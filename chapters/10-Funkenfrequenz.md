Funkenfrequenz
==============

\Beitext{Nurek}

Es hätte sich nicht so gut anfühlen sollen. Marims Bericht darüber, dass er ohne sie
ständig hier vorbei gelaufen wäre. Es war ein unangenehmer
Struggle. Sie fühlte sich dadurch für etwas hilfreich. Als wäre
es auch ein Glück für Marim, sie dabei zu haben. Es war ein beschissener
Zwiespalt, weil sie sich nicht dadurch wertvoll fühlen wollte. Natürlich
wäre Marim auch so glücklich gewesen, mit ihr zusammen hier zu sein. Nicht
zuletzt, weil sie zusammen ein Konzert besuchen wollten, was für sie
beide ein großes Ereignis war. Sie schaffte es die meiste Zeit über, sich
einfach so wertvoll zu fühlen, einfach nur dafür, dass sie war. Oder
für irgendwelche Eigenarten. Die Phasen, in denen sie so innerlich
einbrach, waren seltener geworden. Sie hatte sie vor allem aus der Kindheit
mitgenommen, aber nun mit guter Therapie wurden sie allmählich weniger. Ganz
weg ging es nie.

Marims Lächeln als er ihr die Hand gegeben hatte, tat gut. Vielleicht
ging es ihr nun auch besser, weil es ein Themenwechsel war und der Fokus
nicht mehr auf ihr lag.

Sie betraten das Museum und blieben erst einmal neben dem Eingang
stehen, um sich umzusehen. Nurek stellte schnell fest, dass sie das
Museum zwar mochte, aber sie im Wesentlichen schon kannte, was hier
ausgestellt war. An einer Wand waren aus vielen Generationen von
Bildschirmen jeweils einer aufgestellt, von alten, kastenförmigen
Monitoren über flache und gekrümmte aus der Hartplastikzeit mit immerhin
Full-HD zu den roll- und faltbaren, die heute üblich waren.

Vermutlich produzierte ein schon recht alter olfaktorischer Emitter Geruch nach
verbranntem Staub. Das war an sich ein Geruch, der stereotyp mit dieser Einrichtung
assoziiert wurde, aber Nurek war sich einigermaßen sicher, dass die Geräte
selber eigentlich nicht so sehr rochen und der Geruch auch nicht
genau traf, wie die Geräte früher gerochen haben mochten. Daher tippte
sie darauf, dass der Emitter, der die Immitation versuchte, schon älter
war und es nicht ganz so gut hinbekam, wie es einem modernen möglich
gewesen wäre. Sie störte der Geruch nicht.

In diesem Augenblick begann ein Laserdrucker sich rhythmisch zu bewegen, ohne
Papier zu bedrucken. Papier war wertvoll, wurde nur noch sehr selten benutzt. Das
Geräusch des Druckers mochte Nurek. Dazu setzte ein summendes anderes Geräusch
ein. Einige Takte lang spielte es eine Art sehr abstrakte Melodie. Noch nicht
sehr ausgearbeitet, vermutete Nurek. Sie brauchte bis zum Ende der Melodie, um
herauszufinden, dass die Geräuschquelle ein Festplattendeck war. Neben
dem Deck stand eine Person an einem alten
Klapplaptop, der nicht rollbar war und dessen Bildschirm ein
konsolenlastiges, altmodisches Bild zeigte. Nurek hatte manchmal alte, simulierte
Rechner in Virtualitäten verwendet. Ein kleiner Ork mit ein paar grauen Locs, die fast bis
zum Boden reichten, stand daneben und betrachtete die gezeigten, bunten
Code-Zeilen.

Nurek blieb mit etwas Abstand neben ihm stehen. "Ist über die Schulter
gucken schlimm für dich?", fragte sie. Eine häufig gestellte Frage, wenn
eine Person einen Rechner bediente, und das nicht ohne Grund.

"Ja, aber nein, aber ja.", sagte die Person, ohne die Augen vom
Bildschirm zu lösen. "Ich bin dadurch langsamer, aber ich arbeite
auch nicht ohne Grund im Museum und nicht in einer ruhigen Ecke. Frag, wenn
du Fragen hast. Das inspiriert!" Der Ork blickte doch zu ihr, als sie
nicht gleich reagierte und winkte sie näher. "Komm her.", forderte
er sie auf. "Ich bin Jonde. Pronomen sie. Und ich beiße nicht."

Nurek kam näher. "Nurek. Sie, ihr, ihr, sie.", sagte sie. "Diese
Redewendung mit dem beißen irritiert mich immer. Weil die Aussage
meistens nicht stimmt."

Jonde hob eine Braue und runzelte die Stirn. "Ich hatte wirklich
nicht vor, dich zu beißen.", sagte sie.

War da Verärgerung in der Stimme? Uffz. "Ich bin von Anfang an
davon ausgegangen, dass du nicht vorhast, mich ohne Einverständnis
meinerseits zu beißen.", sagte Nurek. "Aber beißen ist schon etwas, was
die meisten Personen machen, wenn sie essen. Nicht alle natürlich."

"Oke.", sagte Jonde, etwas längergezogen und seufzte. Die Stimme
klang ein wenig runzlich und eigentlich sehr sympathisch. "Bei dir muss
ich also auf Präzision Acht geben. Bist du gut im Programmieren? Habe
gehört, da ist das eine supi Eigenschaft."

"Mäßig." Nurek vermutete, dass ihre Skills im Vergleich zu denen
der *Fenster* ziemlich schlecht wären.

Sie merkte außerdem, wie die
Konversation ihr Kraftreserven zog, mit denen sie haushalten musste. Aber
sie fühlte sich auch nicht wohl, jetzt sofort zu gehen. Also
blieb sie unschlüssig stehen und sah sich nach Marim um, der sich gerade in
einer anderen Ecke des Museums aufhielt. Sie fing
seinen Blick ein und hielt ihn fest, bis er verstand, dass er zu ihr
kommen sollte. Aber bis er da war, war sie irgendwie doch mit Jonde
in ein Gespräch geraten, das nicht so unangenehm war wie der
Anfang. Jonde hatte einfach angefangen, den Code zu erklären. Sie
erklärte, dass der Programmcode ein Lesen der Festplatte veranlasste, und
jenes wiederum den physischen Ton erzeugte.

"Richtig cool ist es, wenn der Programmcode, mit dem die Platten und Laufwerke so angesteuert
werden, dass sich die Musik ergibt, die wir spielen, sich auch noch als Lyrics für das
entstehende Stück eignen.", erklärte Jonde in einem Tempo, bei dem
Nurek, erschöpft wie sie war, sich anstrengen
musste, gedanklich hinterherzukommen. Die Satzstruktur machte es nicht
leichter. "Daran arbeite ich. Und
auch Leute mit wenig Know-How haben manchmal richtig gute Ideen. Also
nur zu! Fragt, denkt laut, was ihr mögt. Es gibt kein falsch."

Nurek stellte also Fragen, bekam aber völlig unverständliche
Antworten, als es ins Detail hätte gehen sollen. Die Fragen
schienen allerdings tatsächlich zu inspirieren. Jonde schob Code hin und her, und
das Stück, das die Festplatten mit dem Laserdrucker spielten, wurde
etwas komplexer.

"Du bist also Mitglied bei den *Fenstern*?", fragte Marim.

"Ja. Ich stehe dieses Jahr das erste Mal bei einem Stück hier auf
der großen Bühne.", verkündete Jonde. "Kommt ihr?"

"Das ist bisher der Plan.", antwortete Marim.

Jonde lächelte. "Das freut mich sehr. Die Angst, dass niemand käme, ist
auf dem Funkenfest natürlich albern. Aber trotzdem freue ich mich, Gesichter
zu sehen, von denen ich weiß, dass sie da sein werden."

Nurek stellte mit leichtem Unbehagen fest, dass Jondes Interpretation unpräzise
war: Sie planten es bisher. Das war keine Garantie. Nurek stellte am Stress,
den die Unpräzision vorhin und jetzt verursachte, fest, dass sie sachte
über die Grenze ihrer Belastbarkeit gerutscht war. "Ich muss ins Bett.", sagte sie.

Marim übernahm das halbwegs höfliche Verabschieden und sie wanderten zu ihrem Zelt
zurück.

"Mist.", murmelte Nurek, als sie es erreichten. "Das war dann doch eine Spur
zu viel."

Marim sagte nichts dazu. Er blickte sie nur aufmerksam an. Er überredete sie
noch, mit ihm Zähneputzen zu gehen. Das hatten sie vorher ausgemacht, dass
er sie mindestens einmal am Tag überreden solle, egal wie es ihr ginge, weil
sie wusste, dass sie es sonst so lange vernachlässigte, bis es ihr sehr
schlecht ginge.

Dann kam er nach ihr ins Zelt und legte sich ruhig
zu ihr. "Ich habe dich ganz schön lieb.", sagte er leise.

Nureks Gefühlswelt war insgesamt gedämpft, aber das wunderschöne
Gefühl, gern gehabt zu werden, schaffte es doch an die Oberfläche. Sie
antwortete nicht. Ihr war zu warm. Aber sie konnte nicht ohne Decke
schlafen. Irgendwann in der Nacht würde es schon kühler werden. Aber
das Schabernakel-Konzert war schon morgen Nacht, eines der ersten
Konzerte auf dem Festival überhaupt. Wenn sie zu
kurz schlief, nämlich nur bis es wieder hell würde, könnte sie es dann
richtig genießen?

"Denkst du daran, Noise Cancelling zu aktivieren, damit dich
morgen früh nicht Geräusche aufwecken?", fragte Marim.

Nurek erschreckte sich fast. "Natürlich habe ich das vergessen.", sagte
sie, und aktivierte es. Nun hörte sie nur noch Marims Stimme. Und
natürlich ihren Wecker, der auf Spätnachmittag gestellt war, einfach
nur für den Fall, dass sie bis fast an das Konzert heran schlafen
würde.

---

Sie wachte um Mittag herum auf und war hungrig. Marim war
längst wach und las irgendwelche Nachrichten, Blogartikel,
Shortspread-Timelines, was es eben so zu lesen gab, wenn eins
still in einem Zelt herumlag und wartete, bis das Herzwesen
endlich die Augen aufschlug.

"Ich hab's mir überlegt.", sagte Nurek.

"Du fändest es doch okay, wenn ich Feuer spuckte.", schlug
Marim vor.

Nurek lachte. Und schüttelte den Kopf. "Nope. Ist zu warm. Wasser
wäre okay."

"Also..." Marim schaute sie ungläubig an, -- wahrscheinlich gespielt
ungläubig. "Ich kann durchaus Spucke spucken und das ist nicht
so furchtbar weit entfernt von Wasser. Aber ich würde dich eigentlich
so einschätzen, dass du noch weniger gern bespuckt wirst, als
geküsst."

"Da ist was dran.", sagte Nurek. "Ich hatte mir das eher so
Wasserspeier-mäßig vorgestellt."

Marim und sie kicherten gemeinsam, versuchten sich aber zu
beherrschen. Immerhin war dies hier das Calmp. Hier galt, dass
sie nicht zu laut sein sollten.

"Was hast du dir überlegt?", fragte Marim.

"Dass ich dich wegschicke, während ich alleine hier bleibe.", sagte
Nurek. "Ich habe mir überlegt, du bringst mir Frühstück ans Bett. Und
du darfst dabei gern etwas länger brauchen, wenn du möchtest, so eine
Stunde vielleicht. Musst du auch nicht, aber du hattest das gestern
vorgeschlagen."

"Das kann ich einrichten.", meinte Marim. "Ich würde am
liebsten wieder deinen Wünschen gerecht werden. Sprich, wenn du
gern diese Stunde mit dir selber hättest, dann finde ich schon
eine Beschäftigung. Aber ich brauche das nicht."

"Nicht allein.", korrigierte Nurek. "Ich würde mit Linoschka
reden." Es würde bestimmt kein intensives Gespräch
werden.

"Ich würde dir nach einer Dreiviertel Stunde eine Nachricht
schicken und fragen, ob ich länger wegbleiben soll, und du schickst
mir eine Zahl. Und wenn du keine schickst, bin ich in einer
Stunde wieder da, sonst um die Zahl länger. Klingt das gut?", fragte
er.

Nurek zögerte, dann nickte sie. Eigentlich war ihr das zu viel, dann
auch noch Entscheidungen zu fällen. Aber sie überlegte, dass das
dann auch Linoschka übernehmen könnte.

---

Es wurde ein schönes Gespräch. Linoschka schlief auch gerade in der
Botanik. Ohne Zelt dieses Mal und mit nur drei Streichhölzern, um
sich gegebenenfalls ein Feuer zu machen. Linoschka hatte
tatsächlich eines gebraucht. Sie war nördlicher unterwegs. All das
war eine gängige Übung für die dritte Phase des Spiels, also
Training. Und eigentlich sollten sie dabei nicht mit irgendwelchen
Leuten telefonieren, aber Linoschka hielt sich nicht immer daran. Sie
hatte häufiger Sehnsucht. Und zugleich freute sie sich auf
die dritte Phase.

Außerdem bereitete sie mit Bjork zusammen einen Workshop vor, in
dem sie einer Gruppe von Leuten, die wahrscheinlich mindestens
zur Hälfte aus Kindern und Jugendlichen bestehen würde, einiges
zu Orkando zeigen und erklären würden.

"Ich freue mich, dann vielleicht mal auf der anderen Seite zu sein
und zu trainieren.", sagte sie. "Aber ich mag eigentlich nicht
vor vielen Leuten sprechen."

"Lässt sich das so aufteilen, dass du nicht oder wenig vor
Leuten sprichst, und dann später vielleicht, wenn sich Paare
bilden, Dinge zeigst?", fragte Nurek.

"Ich hoffe.", sagte Linoschka. "Ich sollte mit Bjork darüber
reden. Es ist alles so aufregend! Wie ist es bei euch?"

"Auch zu aufregend.", sagte Nurek. "Ich bin hochangespannt
und will gerade nicht über Einzelheiten reden."

Linoschka verstand und beschrieb stattdessen ihre Umgebung und
wie sie das Feuer angezündet hatte, -- aber langsam, sodass
es nicht zu viel Information wäre.

---

Nach ihrem Frühstück im Bett hatten sie eigentlich vor, noch
einen Spaziergang außerhalb des Geländes zu machen, um
die Zeit bis zum Abend abzukürzen, und sich dann den
perfekten Platz zu suchen. Aber auf dem Weg entdeckten
sie eine kleine, witzige Band, die nicht zum Programm gehörte und
sich irgendwo am Rand des Geländes behelfsmäßig aus Paneelen
eine kleine Bühne gebaut hatte. Sie waren ziemlich gut, fand
Nurek. Die Ecke war ansonsten verhältnismäßig ruhig. Sie
konnten sogar sitzen. Sie kuschelten hier, bis der Abend
dämmerte. Nurek versuchte sich hydriert zu halten. Bis
die Band genug gespielt hatte, nach drei Stunden immerhin, und
Nurek und Marim sich Abendessen drucken wollten.

Auf dem Weg verliefen sie sich das erste Mal. Sie kamen
beim LGBTQASIN+-Camp vorbei, das wunderschön beflaggt
und mit Pride-Farben beleuchtet war und wo viele Personen
in schönen Kostümen herumliefen. Marim hätte mit den
langen Ringelstrümpfen, seinem kurzen Kleid und seiner Schminke
sehr gut da hineingepasst. Nurek merkte, wie er am Camp
langsamer wurde.

"Möchtest du dem Camp gern einen Besuch abstatten?", fragte
Nurek.

"Ich kann das auch morgen noch.", sagte Marim. "Mein
Nagellack blättert ab, und kaum wo ist es schöner, sich
Nägel neu zu lackieren, als unter anderen queeren
Leuten."

Nurek lächelte. Sie hätte ihn vielleicht motiviert, jetzt dort
hinzugehen, wenn sein Wunsch nicht Nägel lackieren gewesen
wäre. Sie hörte zu lächeln auf, weil sie sich schlecht
dafür fühlte. "Ich könnte mit dem Geruch von frischem
Nagellack gerade nicht umgehen.", murmelte sie.

"Findest du deshalb abblätternden Lack besonders hübsch?", fragte
Marim grinsend.

Nurek schüttelte den Kopf. "Das ist unabhängig."

"Ich denke frühestens morgen darüber nach, dort hinzugehen, und
dann mit dir gemeinsam. Wir wollten ja auch noch *die Fenster*
hören.", sagte Marim.

Sie hatten gerade ein weiteres Themen-Camp hinter sich
gelassen, von dem sie nicht so leicht erkennen hatten können, was
das Thema war, und standen wieder etwas ratlos herum, einen Plan
suchend, als ein Elb etwa zwei große Schritte vor ihnen stehen
blieb. Marim schaute ihn erwartungsvoll an. Nurek tippte auf
einen der Knöpfe, die sie auf einem Pinboard an ihr
Oberteil geheftet hatte, auf das sie häufige Befehle an
ihren Taschenrechner programmiert hatte. Es deaktivierte
das Noise Cancelling für die nähere Umgebung. Sie war
neugierig, was der Elb wollte. Die Geräuschkulisse war
unangenehm, aber machbar.

"Hi!", sagte der Elb. Er sprach zu Marim, da war sich
Nurek recht sicher.

"Moin!", grüßte Marim zurück.

"Hast du Lust auf Sex?", fragte der Elb. Er war etwa einen
Kopf größer als Marim und hauptsächlich schwarz gekleidet, aber
machte keinen bedrohlichen Eindruck, fand Nurek.

"Nein.", sagte Marim sachlich.

"Okay.", sagte der Elb und wandte sich zum Gehen um.

Nurek runzelte die Stirn. Für Marim schien die Sache abgehakt
zu sein. Er blickte sich weiter um und zeigte schließlich
auf eine Stellwand etwas weiter weg, die ein Plan sein
könnte, als sich der Elb wieder ihm zuwandte. Auch Marim
schenkte ihm erneut Aufmerksamkeit.

"Auch nicht mit gründlichen Absprachen, Konsens und Verhütung
und allem?", fragte der Elb.

"Auch dann nicht.", sagte Marim mit einem freundlichen Lächeln.

"Ich hoffe, ich war nicht aufdringlich.", sagte der Elb. "Ich
wünsche euch alles Gute." Dann eilte er tatsächlich davon.

Marim führte Nurek zielsicher zwischen den anderen Leuten
hindurch bis zum Plan. Es waren nicht so viele und zum
Glück stand auch keine Personentraube davor, wie das nicht selten bei
Plänen der Fall war. Schließlich fanden sie sich darauf zurecht. Sie
wollten einen Abstecher über eine Wasserabzapfstelle machen,
an einem der Kunstfeuer für kleine Gruppen zu Abend essen, wo nie mehr als zehn
Leute gleichzeitig zugelassen waren, was über ein Online-Zuordnungs-
System funktionierte, und anschließend vor der Bühne
auf dem Boden platznehmen.

"Passiert dir sowas häufiger?", fragte Nurek, als sie es bis
an ein solches Kunstfeuer geschafft hatten.

"Dass ich mit dir an Kunstfeuern esse?", fragte Marim grinsend. "Ich
vermute, noch so zwei- bis dreimal, dann weiß ich noch nicht."

"Dass du nach Sex gefragt wirst, von völlig fremden Leuten.", korrigierte
Nurek.

"Ab und zu.", sagte Marim.

"Sagst du manchmal 'ja'?", fragte Nurek.

"Das ist kontextabhängig.", sagte Marim. "Ich gehe manchmal sehr
bewusst auf sogenannte Private Play Partys. Ich bin dort oft
gefragt, sozusagen. Also, anscheinend finden mich viele attraktiv. Und
ich gehe da hin, wenn mir danach ist, auf solche Fragen 'ja' zu
antworten." Marim zog die Sandalen aus und kreuzte die Beine
auf der Sitzfläche, bevor er weiter aß. Er wartete, bis Nurek
sich, ebenfalls mit verschränkten Beinen, dazugesetzt hatte, bevor
er fortfuhr. "Bei anderen Gelegenheiten habe ich
meistens -- wie jetzt -- schon andere Pläne, aber wenn nicht, kommt es
auch mal vor, dass ich 'ja' sage. Ich mag das Abenteuer."

"Gibt es dafür, dass es okay ist, dich zu fragen, irgendein
Zeichen? Sehen sie dir das an?", fragte Nurek. "Denn ich glaube, mich
würde das in jedem Fall nerven und stressen, wenn mich Leute frügen."

"Keine bewussten. Ich habe mich das auch schon gefragt.", sagte Marim
nachdenklich. "Ich kenne nicht viele, die sowas oft gefragt werden. Ich
habe immerhin Gesprächsbereitschaft suggeriert, glaube ich. Aber ob
ich irgendwelche Signale sende, dass das Thema bei mir okay wäre, weiß
ich nicht. Ich hoffe, es ist nicht sowas wie Kleidungsstil. Es
sollte niemand wegen Kleidungsstilen belästigt werden."

"Vielleicht sollte ich zum Test deinen Kleidungsstil ausprobieren.", überlegte
Nurek. "Aber ich glaube, an dir ist mehr. Du bewegst dich so elegant. Deine
Gestik wirkt ein bisschen, hm, ich kenne keine Worte dafür, es ist
ein bestimmter Stil. Den ich übrigens sehr mag."

Marim lächelte und strich sich langsam und jene Gestik übertreibend
mit passend überspitzter Mimik die Haare hinter die Schulter, wo
sie eigentlich schon waren. "Diese?", fragte er.

"Genau!", sagte Nurek.

"Jedenfalls, auf den PPPs -- das ist kurz für Private Play Party -- gehört
es einfach dazu, dass sowas gefragt wird. Ich glaube, das wäre
dann vielleicht eher nichts für dich." Marim grinste sie an und
Nurek nickte. "Darf ich dir ein Detail von einer erzählen, das vielleicht
sexuell sein könnte, aber in etwas anderer Art, die vielleicht nicht
so schlimm für dich sein könnte?"

Nurek blickte ihn skeptisch an, aber stimmte dann neugierig
zu. "Na gut."

"Eine meiner schönsten Nächte war eine auf einer PPP, in der mir eine
andere Person die ganze Nacht die Füße massiert hat. Während ich
schlief auch. Das war sehr wohltuend.", berichtete Marim.

"Das klingt eigentlich nicht so sexuell. Das stimmt.", gab
Nurek zu. Fußmassagen mochte sie auch. Sie sollte Marim mal
fragen, ob er dazu Lust hätte.

"Es ist personenabhängig.", erklärte Marim. "Für manche hat es
nichts mit Sex zu tun, für andere ist es welcher. Für die
Person gehörte Erregung dazu, ich gehe nicht weiter ins Detail. Und
es war ein D/s-Spiel, domination-submission. Für die Person war
wichtig, in der Hierarchie unter mir zu sein. Außerdem war
es vielleicht das einzige Mal, dass ich Sex mit einem Mann gehabt
habe."

"Oh, bist du hetero?", fragte Nurek. Sie fragte sich, warum sie
der Gedanke so überraschte. Vielleicht, weil sie sich darüber einfach
noch keine Gedanken gemacht hatte.

"Ich denke, schon, also in dem Spektrum oder so. Eher zumindest. Ich
leite das davon ab, dass ich mich bisher vor allem mit Personen
auf Beziehungen und Interaktion eingelassen habe, die sich
geschlechtlich wenig bis weiblich einordnen.", erklärte
Marim nachdenklich. "Ich fühle mich etwas unkomfortabel
darüber zu reden, weil, nun ja, sexuelle Orientierung einordnen
ist keine einfache Sache und eher so ein Prozess. Es
liegt nahe, dass ich wohl hetero bin."

"Oh, wenig einordnen, klingt bei mir passend. Ich bin irgendwie
weiblich, denke ich, aber ich fühle eigentlich auch nicht
so viel Geschlecht.", teilte Nurek mit. Es war
spannend, dass sie nie zuvor darüber geredet hatten, obwohl
sie nun schon seit über einem halben Jahr in einer Beziehung
waren. Vielleicht ging es einfach auch ohne gut. "Ich habe aber
gelernt, dass 'sich weiblich einordnen' keine so gute
Wortwahl ist.", wandte Nurek nachträglich
ein. "'Eine Person ist weiblich', wäre
da eine bessere Formulierung."

"Zufällig ist mein bestes Herzwesen trans und steckt ziemlich
tief in den Diskursen drin.", sagte Marim. "Was trotzdem
nicht heißt, dass ich alles richtig wiedergeben könnte. Ich
meine, grundsätzlich hast
du recht. Der Wunsch nach der Formulierung 'Eine Person
ist weiblich.' oder 'ist anderes Geschlechts-beschreibendes
Adjektiv' kommt, weil viele dya cis Personen vor allem früher
ausschließlich bei Personen die Formulierung 'fühlt sich als'
verwendet haben, die trans waren, weil es in ihrer Vorstellung
nicht echt war. Ich würde auch meistens die
ist-Formulierung nehmen. Die Formulierungen sind aber, wenn
ich das richtig mitbekommen habe, in
den letzten Jahren wieder etwas aufgeweicht, weil insgesamt
mehr über Geschlecht gesprochen wird, auch unter cis
Personen. Wenn es nicht othernt verwendet wird, sondern
auch cis Frauen darüber nachdenken, wie sich weiblich
sein anfühlt, ist es etwas anderes.", fasste Marim
zusammen. "Gerade habe ich deshalb 'einordnen' gewählt, weil
ich auch Personen einfassen wollte, die demiweiblich sind, oder
gelegentlich weiblich, aber oft genug, dass sie sich bei einer Frage
'bist du eher weiblich' dort verorten würden. Ich bin nicht
sicher, wie respektvoll das war. Ich glaube, ich rede
da mal mit Anuka drüber."

Nurek nickte. "Das klingt insgesamt sinnvoll, was du sagst. Ich
sollte da vielleicht auch drin stecken. Schließlich ist
auch Mø trans. Aber es ist lange her, dass wir darüber
geredet haben.", sagte sie. Sie fügte hastig hinzu: "Mø
macht da kein Geheimnis draus. Sie sagt, es gehört zu
ihr, wir sollen darüber so reden, wie es uns natürlich
vorkommt."

"Ich weiß.", sagte Marim. Sie schwiegen einen Moment, bis
Marim die nächste Frage stellte: "Du bist dann bi- oder
panromantisch?"

"Ich würde mit einem Baum flirten, wenn wir einen Weg fänden,
miteinander zu kommunizieren. Und einigermaßen auf einer
Wellenlänge wären.", erwiderte Nurek. "Und wenn ich
genauer wüsste, was flirten ist. Jedenfalls
hätte ich mich als omniromantisch eingeordnet, weil
mir die Entität hinter einer Person -- die physikalische
Repräsentation -- völlig gleich ist, aber ich glaube, omniromantisch
war schon anders belegt."

"Ich erinnere mich gerade nicht wie, aber ja, ich glaube schon.", sagte
Marim. Er fügte grinsend hinzu: "Labelsalat."

"Wobei ich den Labelsalat auch mag.", räumte Nurek
ein. "Nur beim Erfinden eines
Labels für mich musste ich dann mal checken, ob das schon
belegt ist. Und mir ist dann kein besseres eingefallen."

"War auch nicht abfällig gemeint.", stellte Marim sanft
klar. "Oh shit, wir wollen los!"

---

Es war ein gutes Gespräch, es hatte wirklich sehr abgelenkt. Sie
eilten zum Geschirrspülen, aber ein freundlicher Bergtroll ohne
Zeitdruck meinte, er würde gern ihr Geschirr spülen, das
würde ihn gerade beruhigen. Marim hatte gefragt, ob es ihm gut ginge
und ob er Hilfe bräuchte und er hatte verneint. Er wirkte auf Nurek etwas neben der
Spur, und als sie es Marim mitteilte, ging es ihm genauso. Vielleicht
hatte sich das 'nein' nur auf die erste Frage bezogen und er
hatte die zweite offen gelassen. Da es
sich außerdem um eines der künstlichen Feuer handelte, wo vor
allem Personen mit Schwierigkeiten mit lauten und wuseligen
Umgebungen waren, und sie kaum Zeit gehabt hätten, noch einmal
zurückzukehren, rief Marim nach einigem Hin- und Herüberlegen, ob
das übergriffig wäre, beim Secruiteam an, und schilderte kurz, was
los war. Er erreichte Gabriane, die aber schon Bescheid wusste. Es
war schon jemand auf Wunsch der Person selbst unterwegs. Nurek
atmete durch. Sie hätte nicht damit gerechnet, dass diese Situation
einen so entlastenden Ausgang bekommen könnte.

Sie suchten sich einen Platz auf der Wiese vor der Bühne, auf der die Band Schabernakel gerade
ihren Soundcheck machte. Sie saßen, wie viele andere, auf dem Boden. Nurek
fühlte sich so aufgeregt, dass es sich nicht aushaltbar anfühlte. Sie
flatterte die ganze Zeit mit den Armen und schwankte mit dem Oberkörper. Marim
ließ sie, gab ihr bald etwas zu trinken, wirkte ruhig und gelassen
und verständnisvoll. Wie diese Emotion des Liebhabens nun auch noch
in sie reinsollte, hatte sie keine Ahnung, aber es war auf einmal
sehr stark. Sie ließ sich in seinen Schoß kippen und weinte leise, weil
alles zu viel war. Er streichelte ihr über den Rücken, wie er es
für sie und solche Situationen gelernt hatte.

Zwischen Soundcheck und Konzertstart gab es einen Zeitraum, in dem
das Gewusel so schlimm wurde, dass Nurek sich fragte, ob sie
weglaufen sollte. Es waren lauter Leute um sie herum. Natürlich
waren sie das. Es war ein Konzert. Sie hatte sich das so
ausgesucht. Sie hatte sich selbst entschieden, zwischen den Leuten
zu stehen und nicht am Rand. Aber nun zweifelte sie an der
Entscheidung.

Irgendwann applaudierte das Publikum. Nurek zwang sich
aufzustehen. Marim hielt sie von hinten fest. Nurek versuchte zu
atmen. Sie würde nicht davon sterben, dass so viele Leute um sie
herum wären. Es gab hier das Securiteam und sie vertraute
darauf. Und auf das Sozialverhalten der Leute. Sie bemerkte, dass
sie um sie herum etwas leiser waren, etwas mehr Abstand hielten, vielleicht
einen besorgten Blick auf sie warfen, aber wenn sie zurücksah, lächelten. Das
musste daran liegen, dass ihr Gesicht auslief und sie zitterte. Und
dann auch lächelte.

Der Applaus verblasste und es wurde leiser, als ein Mikrofon
mehrfach klackte, wie um Bescheid zu geben, dass es gleich
benutzt werden würde. Kurz darauf klang aus den riesigen Stapeln
von Boxen deutlich und nicht zu laut über den ganzen
Platz die Stimme eines der Bandmitglieder:

"Wir werden ja oft gefragt:", leitete
diese ein und machte direkt eine Kunstpause in die Stille. "Was sind 
eigentlich Nakel." Ein leises Raunen ging durch die Menge. "Nun ja, die Nakel, das
sind wohl wir. Und wir sind nicht irgendwelche Nakel, sondern die Nakel
des Schabers. Die Schabernakel sozusagen. Als Schabernakel sind wir fürs
Schaben verantwortlich. Unter anderem schaben wir auf unseren Instrumenten
herum. Besonders gut eignet sich so eine Bratsche zum darauf Herumschaben. Magst
du mal?"

Die Person neben jener am Mikrofon rubbelte mit dem Bogen über ihr Saiten-Instrument, dass
es nicht richtig aussah, und erstaunlich gekonnt dissonante Töne
produzierte. Nurek bekam eine Gänsehaut, und zu ihrer Überraschung fühlte
sie sich plötzlich gut. Eine Euphorie in einer Stärke verdrängte die
Nervosität, die sie so noch nie erlebt hatte, und machte ihren Kopf und
ihre Wahrnehmung seltsam klar. Auf positive Art, erstmal. Aber Nurek
vermutete, dass es später einen Preis haben würde.

Die Person mit der Bratsche reckte sich zum Mikrofon und fragte: "So?"

"Ah, welch schöner Schaber.", sagte die Sprechperson und deutete
auf den Bogen. "Wir könnten glatt sagen, ein Schaber, um den Bogen
zurückzuschlagen zur Musik."

Ohne irgendeine weitere Einleitung fingen sie an, ein Stück zu spielen. Es
klang ohne Frage gut. Nurek kannte es noch nicht. Aber sie brachen es schon
nach wenigen Takten mitten drin ab, ohne erkennbares Zeichen, wie aus dem nichts.

"Bogen versteht ihr?", fragte die Sprechperson.

Die Menge lachte. Sie hatte vorher schon gelacht, aber dieses Mal lachte
Nurek mit und merkte es deshalb bewusster.

Unkoordiniert wirkend, vielleicht gekonnt unkoordiniert, stiegen sie an der
Stelle wieder ein, aber brachen kaum fünf Takte später wieder ab.

Die Person mit der Bratsche reckte sich abermals zum Mikrofon. "Ich
glaube, du überspannst gerade den Bogen."

"Da ist wohl der Schabernack mit mir durchgegangen.", sagte
die Sprechperson von vorher wieder. "Streich das."

Nurek lachte auf. Was für eine großartige Band. Sie wünschte sich gerade, genau
hier zu sein.

Die Person mit der Bratsche erzeugte weitere gekonnt disharmonische Töne. Wieder
fragte sie "So?", sich zum Mikrofon beugend.

"Ich bin völlig durcheinander gekommen.", monierte eine Person im Hintergrund. Sie
hatte eine sehr sachliche, unbeeindruckte Stimme, als ob sie der ganze Schabernack
bisher nichts angegangen wäre.

"Haben wir's vergeigt? Wollen wir zurück an den Anfang?", fragte die Sprechperson.

Die Band stimmte zu. Und dann spielten sie, spielten die Takte, die sie bereits gespielt
hatten, rückwärts. Nurek war sich erst nicht sicher, aber sie bewegten sich dazu auch
wieder rückwärts, wie sie sich zuvor vorwärts bewegt hatten, was Nurek eigentlich
nur unterbewusst mitgeschnitten hatte.

Als sie wieder am Anfang angekommen waren, applaudierte das Publikum laut, und Nurek
hielt sich begeistert die Ohren zu.

"Nach diesem etwas dadaistischen Intro fangen wir dann nun an, wie üblich in der
Mitte des Geschehens.", fasste die Sprechperson mit einer Verbeugung zusammen.

Dann spielen sie das selbe Stück von eben wieder von vorn, aber in völlig anderem Tempo, viel
langsamer, sodass die Musik auf einmal traurig und getragen wirkte.

---

Schabernakel war eine Band, die -- besonders live -- es drauf hatte, Stimmungswechsel
zu verursachen, die Zuhörende ohne Vorwarnung auseinanderzerrten. Sie rissen
Witze in die traurigste Stimmung, oder gingen mitten von einem lustig wirkendem
Teil in tiefe Trauer über. Sie schaukelten zwischen unbeschwert
wirkendem Lebensmut und ernst-philosophischem Lebensmut.

Teils waren die Refrains in alter nöldischer Sprache. Meist sangen sie Kadulan
oder Niederelbisch und letzteres manchmal mit einem alten Dialekt aus der
Zeit verschiedener Völkerwanderungen, weil die Noldafin bis vor gut einem
Jahrhundert noch verfolgt worden waren. Die Inhalte der Stücke waren
überwiegend modern, bezogen sich auf aktuelle Politik, Philosophie
oder das Leben. Spannenderweise wirkten die Texte auf Nurek oft zuerst
klar, und dann hatte sie doch ganz viele Fragen.

Es war eine wundervolle Band. Aber nach vielleicht nicht einmal zehn Stücken
begannen Nureks Beine so schlimm vom Stehen zu schmerzen, dass ihre Aufnahmefähigkeit
gewaltig sank. Sie merkte, wie sie mit aller Kraft versuchte, aufmerksam zu sein, aber
es gelang ihr nicht mehr. Schließlich setzte sie sich auf den Boden zu Marims
Füßen, der aufpasste, dass niemand sie da unten übersah. Aber nun sah sie die
Bühne nicht mehr und fühlte sich nicht so gut.

Nach fünf bis acht weiteren Stücken -- Nurek wusste es nicht -- stand
sie endlich wieder auf und fühlte sich etwas besser, aber auch sehr
matt, als habe sie viel geweint. Sie hatte viel geweint, fiel ihr ein. Nun
spielte die Band noch ein paar ihr sehr vertraute Stücke, die sie hätte
mitsingen können. Eines wurde dann auch vom Publikum mitgesungen, ein Stück, für
das die Band sehr berühmt war. Es war langsam und traurig und glücklich
zugleich. Der Chor war wunderschön. Nurek schloss die Augen und
ließ sich in die getragenen Töne der Menge fallen -- und gegen Marims
Rücken, der die Arme um sie geschlossen hielt.

Sie hätte erwartet, dass
dies das letzte Stück sein würde, behielt aber nur ungefähr recht: Die
Band spielte noch drei Zugaben. Das dritte war ein Stück, für das weitere
Musizierende einer anderen nöldischen, unbekannteren Band mit Chor
Schabernakel ergänzten. Auch dieses Stück war wunderschön. Und
als es vorbei war, löste sich das Konzert auf. Nach einem gewaltigen
Applaus, bei dem sich Nurek wieder die Ohren zuhielt, strömten die
Fans vom Platz.

Aber Nurek rührte sich nicht. Marim drehte sie
herum, sodass sie ihm zugewandt war, und hielt sie fest, stellte
sich so in den Strom, dass eher er angerempelt würde als sie. Aber
auch er wurde nur ein paar Mal angerempelt. Nurek blickte ihm kurz über
die Schulter, warum sie nun mehr Platz hätten. Es hatten sich
drei große Orks und ein ebenso großer Elb gefunden, die einen
Halbkreis um sie gebildet hatten, sodass der abwandernde Strom
Abstand von ihnen nahm. Von so etwas hatte Nurek schon häufiger
mitbekommen, dass es auf dem Funkenfest getan wurde. Das war das Sozialverhalten, auf
das sich Nurek vorhin zu verlassen versucht hatte, und das sie nun brauchte.

Nurek erinnerte sich nur schemenhaft, wie sie vom Platz gelangte,
als endlich ausreichend wenig Leute da waren, dass ihr Körper wieder
mechanische Bewegungen machen konnte. Marim brachte sie wieder
ins Zelt zurück. Es brauchte sehr lange, bis sie sich beruhigte,
aber irgendwann verschwand sie in seltsamen Träumen, die
vielleicht so etwas wie Halbschlaf waren.
