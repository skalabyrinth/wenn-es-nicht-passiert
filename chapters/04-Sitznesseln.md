Sitznesseln
===========

\Beitext{Nurek}

Sie flatterte, während sie von ihrem Zimmer auf die Terrasse und
zurück ging, und dabei sehr sauber artikuliert Pflanzennamen
aussprach. "Brennnessel" zum Beispiel, oder "Buchweizen". Das
Flattern bestand aus dem Wedeln mit den Armen und Schütteln der
Hände, sodass Wind über die Haut strich und sich die Gelenke
ganz locker anfühlten. Stimmen war dran. Es war lange nicht
mehr so intensiv gewesen. Sie war hochangespannt und unter
einer Oberfläche enormer Nervosität eigentlich recht gut
gelaunt.

Irgendwann kam Linoschka aus ihrem Zimmer und breitete fragend
die Arme aus. Nurek ließ sich einfach gegen diesen
großartigen Menschen plumpsen und Linoschka schloss die
kräftigen Arme um sie, schaukelte sie sachte hin
und her. Nurek nahm ihr Körperfett, ihre Muskeln, ihre
ganze Struktur und Haptik gleichzeitig am eigenen Körper
wahr. Es war vertraut. Personen, die sie nicht haptisch schon
gekannt hätte, hätten sie nicht anfassen dürfen.

"Willst du reden?", fragte Linoschka.

Nurek nickte. Das wollte sie.

Draußen am Tisch saß Tjaren. Linoschka fragte, ob sie störten, wenn
sie sich dazusetzten, aber Tjaren hieß sie sogar willkommen. Es war angenehm
kühl, aber vielleicht etwas windig. Leichtere Dinge, die auf dem Tisch
platziert worden wären, wären einfach weggeflogen, und die Prideflaggen, die
an verschiedenen Stellen am Haus montiert waren, flatterten kräftig im Wind,
falteten immer wieder für eine Weile eine Ecke um, die sich erst nach
einer Weile wieder sortierte. Nurek musste darauf starren, und als sie
es bemerkte, setzte sie sich auf die andere Seite des Tisches, sodass
sie in den Garten hinabsah, das Haus im Rücken.

Sie kannte sehr viele Pflanzennamen, wusste aber von vielen nicht, was
für Pflanzen sich dahinter verbargen. Es war einfach nur ein Stimm, sie
hatte irgendwann als Kind alle Pflanzennamen in einem kleinen
Pflanzennachschlagewerk auswendig gelernt, einfach nur, weil sich
die Wörter gut im Mund anfühlten. Tjaren dagegen kannte viele
Pflanzen und kümmerte sich am meisten um den Garten. Tjaren pflanzte
Kräuter, Gemüse und Blumen, die Insekten total toll fanden.

"Was, wenn es ein Erotikfilm wird? Oder eben ein Film
mit Erotik- oder Sexszenen oder sowas?", fragte Nurek. "Ist sicher
unwahrscheinlich, aber nicht ausgeschlossen! Aaaaah!"

"Ich bräuchte mehr Kontext.", stellte Linoschka freundlich fest.

"Marim hat mich zu einem Kinoabend in eine Virtualität
eingeladen.", erklärte Nurek.

"Und er hat mit solchen Details gespart, zu sagen, welcher Film?", fragte
Tjaren. "Oder wird es ein Überraschungsfilm."

"Ich gehe von ersterem aus, aber es könnte zusätzlich auch das
zweite sein.", antwortete Nurek nachdenklich. "Ich habe nicht gefragt,
und ich finde es irgendwie auch spannend, mich überraschen zu lassen."

"2D?", fragte Linoschka.

"Ja, darum habe ich gebeten.", antwortete Nurek.

Irgendwo in ihrem Hinterkopf stellte sich Nurek vorsichtshalber
darauf ein, dass eins der beiden Mitbewohnenden fragen könnte, inwiefern
das ein Date sein könnte oder etwas in der Richtung. Aber
entweder dachten sie gar nicht daran, oder überließen es Nurek, das
zur Sprache zu bringen, falls sie wollte.

Eine starke Windböe wirbelte durch Nureks Haare. Das
Meeresrauschen in einiger Entfernung nahm langsam zu. Ein Spaziergang
würde gut tun, aber erst einmal zu Ende erzählen.

"Eigentlich hat ein Herzwesen von ihm ihn eingeladen
und gefragt, ob er mich mitbringen möchte.", erzählte sie. "Ich
habe dann gefragt, ob ich auch ein Herzwesen mitnehmen darf."

"Klingt nur gerecht.", sagte Tjaren lächelnd. Tjaren war damit
beschäftigt, eine filigrane Figur aus einem Töpfermaterial
zu formen. Tjarens Hände waren schmierig davon und Tjarens
Blick konzentriert.

Nurek sah Linoschka an. Sie hatte sich gefragt, ob sie Linoschka
oder ihr Geschwister fragen sollte. Sie hatte Ivaness sehr, sehr
gern. As gab ihr viel Sicherheit. As hatte diese Fähigkeit, wenn
etwas Ungerechtes passierte, es sofort zu merken und anzusprechen. Für
Marim brauchte Nurek das nicht. Sie hatte keine Ahnung, wie
es sich mit Marims Herzwesen verhielt.

Trotzdem fühlte sie sich mehr danach, Linoschka zu fragen, und
fragte sich, warum, aber kam nicht zu einer sinnvollen Antwort. "Hättest
du Lust, mitzukommen?", fragte Nurek.

Linoschka sah in den Himmel und dachte nach. Am Himmel fegten große
Wolkenschwaden sowie dünnere Wolken in einer Schicht darunter, die
sich langsamer in eine andere Richtung bewegten. "Wow, die Wolken sind schön!", rief
Linoschka, bevor sie antwortete.

Nurek nickte. Das waren sie in der Tat.

"Ich hätte schon Lust.", sagte Linoschka schließlich. "Ich
bin nervös, aus ähnlichen Gründen wie du neulich. Aus Erzählungen
weiß ich ja jetzt ein bisschen Bescheid über Marim, aber er weiß nichts von
mir. Und wenn wir vorher nicht wissen, was für ein Film das
wird, dann wäre mir lieb, wenn du vorher herausfinden könntest, ob
Feindlichkeit gegenüber Noldafin ein Thema ist. Die Inhaltswarnung
bräuchte ich."

"Das kann ich fragen.", sagte Nurek zu. "Das ist vielleicht auch eine Gelegenheit, zu
fragen, ob Erotik vorkommt. Das ist nun nichts, was mich triggert, aber
das Thema und Darstellungen davon nerven mich einfach und ich finde
Austausch von Körpersäften auch echt eher eklig."

"Ich weiß.", sagte Linoschka mit einem vielleicht nachsichtigen
Lächeln im Gesicht. Nurek ließ sich darüber häufiger aus.

Nurek holte ihren Taschenrechner aus der Tasche und tippte
eine Nachricht mit den Fragen an Marim.

"Da ist noch was, was ich dich immer mal fragen wollte.", sagte
Nurek und fühlte sich sehr unsicher dabei. "Hast du eine Meinung
zu Schabernakel?"

"Marim mag die Band auch?", fragte Linoschka, statt zu
antworten.

Nurek nickte.

"Oh!", rief Tjaren. "Heißt das, du findest vielleicht endlich
eine Person, die mit dir zu einem Live-Konzert geht?"

"Vielleicht. Das steht noch nicht ganz fest.", antwortete Nurek.

"Natürlich nicht. Aber als Option?", fragte Tjaren.

Nurek gluckste ein bisschen, weil sie das eben ja schon
beantwortet hatte und nickte einfach noch einmal.

"Ihr braucht von mir keine Genehmigung, um eine
nöldische Band zu mögen.", sagte Linoschka. "Jedenfalls.", schob
sie nach. Das machte sie manchmal.

"Ich weiß.", sagte Nurek. "Ich weiß nur keine Meinung einer
nöldischen Person aus meinem Umfeld zur Band, und frage mich manchmal, wie
ich sie einordnen soll. Ob meine Art, sie zu
feiern, irgendwie problematisch ist und ich das nicht merke. Aber ich sollte
nicht dich, nur weil ich dich zufällig kenne, zu allen
möglichen Dingen fragen, ob sie vielleicht Noldafin-feindlich
sind."

"Du hast mich noch nie zu etwas gefragt, und du darfst
mich das immer fragen, ich habe potenziell nur vielleicht
keine Ahnung, weil ich nicht alle Kunst und so kenne.", sagte
Linoschka sachlich. Dann wirkte sie nachdenklich. "Doch, du
hast mich einmal gefragt. Noch recht am Anfang, als wir uns
kennengelernt haben. Zu einem Film, ob ich die Darstellung
gut fände."

Nurek nickte. Sie erinnerte sich noch gut, obwohl es schon über
acht Jahre her war. Sie hatte seit dem tatsächlich zu nichts
weiterem etwas gefragt, aber Linoschka hatte von sich aus
manchmal über ein Medium, das sich mit Noldafin befasste,
erzählt, wie es ihr gefallen hatte oder Nurek Sicherheit
gebende Überlegungen zur Einordnung dargelegt. Über die Band Schabernakel
hatte Linoschka nie etwas gesagt. Nureks Unterbewusstsein hatte geschwankt
zwischen 'nun ist es zu spät zu fragen, wo doch die Band seit
Jahren bekannterweise meine Lieblingsband ist' und 'Wäre etwas
an meinem Umgang problematisch und fiele es Linoschka auf, hätte sie
das wahrscheinlich gesagt'. Aber nun, als die Planung, die
Band live zu hören, konkreter wurde, stellte sie diese bohrende
Frage eben doch.

"Schabernakel macht so metal-artige Musik, die eben nicht so richtig
meins ist.", sagte Linoschka. "Ich finde an der Band sehr cool, dass
sie ihr eigenes Ding macht, und sich nicht daran orientiert, was
Leute so erwarten, wenn sie 'nöldische Band' hören. Außerdem mag
ich die Texte so an sich. Sie sind gleichzeitig fröhlich, haben
aber eine gewisse Düsternis in der Fröhlichkeit und verbalisieren
Widerstand und Auflehnung. Ich finde mich
als nöldische Person in dem Gefühl wieder, dass
es schon einen Widerstand darstellt, einfach nur glücklich zu
sein. Das bringen die Texte teils gut rüber. Einige verstehe
ich auch nicht. Und ich mag den Musikstil eben nicht, einfach
Geschmackssache, keine Kritik."

"Danke.", sagte Nurek. Sie betonte es auf eine Weise, dass
das Herzchen, das sie in einer schriftlichen Message dahintergesetzt
hätte, hoffentlich hörbar war.

Linoschka lächelte und blickte sie einmal kurz an dafür. "Gern."

Sie saßen ein paar Momente einfach still da. Der Wind fegte ein Blatt
über den Tisch, dass sich ausgerechnet an Tjarens Kunstwerk heftete. Tjaren
entfernte es vorsichtig und strich über die Stelle. Es wirkte so
liebevoll, dass Nurek sich kurz wünschte, diese Skulptur zu sein,
nur mit weniger Schleim am besten.

Nurek hörte das leise Geräusch, das ihr mitteilte, dass ihr eine
Person eine Nachricht geschrieben hatte. Diese Hörimplantate
die zunehmend moderner wurden und mit dem
Internet verbunden waren, hatten außerdem die beste
Soundqualität, die hergestellt werden konnte,
verdeckten die Ohren nicht, waren also kein seltsamer Reiz irgendwo auf
der Haut und sie konnten nicht vergessen werden. Sie sah auf
ihrem Taschenrechner nach. Die Nachricht war von Marim. Die Wahrscheinlichkeit
war etwa zwei Drittel gewesen, schätzte sie. Marim erzählte, dass
es kein Erotik-Film war, und keine Noldafin-Feindlichkeit
vorkam. Es kamen auch keine ausführlichen Sex-Szenen darin vor. Er
hatte nicht nachgeforscht, ob kurze, angedeutete vorkämen, aber
schickte Nurek einen Link auf einen Content-Note-Generator. Dieser
gab nicht preis, um welchen Film es sich handelte, aber sie konnte
jede beliebige Frage zum Film stellen, und eine KI generierte aus ihnen
korrekte Antworten. Nurek hatte irgendwann einmal eine kleine Datenbank
verfasst, welche Themen für sie NoGos waren und auf welche sie wie viel
Lust hatte, die genau dafür gedacht war, an so einen Generator
geschickt zu werden. Sie konnte mit Schiebereglern sogar
tagesformabhängig angepasst werden. Aber sie
verzichtete darauf. Wenn solche Szenen höchstens
am Rande vorkämen, wollte sie es dieses Mal gern austesten. Sie würde
Körpersaftaustausch immer noch nicht mögen, aber sie interessierte
auch, wie Marim darauf reagieren würde oder was für Gespräche
sich dabei ergeben würden.

Sie gab die Informationen an Linoschka weiter, worauf Linoschka
noch einmal bestätigte, dass sie mitkommen würde.

---

Später am Tag ging sie mit Linoschka spazieren. Sie hatte nach ihrem
Gespräch erst einmal gegessen und dann Marim gefragt, ob sie sich vorher
noch einmal treffen oder schriftlich unterhalten wollten. Letzteres
taten sie eigentlich ohnehin viel, nur eben keine konzentrierten,
längeren Gespräche, sondern eher nebenbei. Aber es hatte sich eine weitere
Versuchsperson bei Marim gemeldet und er wäre erst am Abend wieder
ansprechbar, und dann wahrscheinlich nicht mehr sehr konzentriert.

Konzentrationsprobleme kannte sie von ihm schon. Es fühlte sich plötzlich
so an, als würden sie sich gleichzeitig schon lange kennen, und
gerade erst kennengelernt haben.

Einen Spaziergang hatte sie ohnehin noch vorgehabt, und
als Linoschka sie fragte -- per Chat im selben Haus, und
da gab es auch überhaupt nichts zu kritisieren, dass es
ihnen so leichter fiel --, stimmte sie mit einem vorfreudigen
Gefühl ohne Zögern zu.

"Ich möchte auch was erzählen.", sagte Linoschka, als sie Röbersjard
hinter sich gelassen hatten und auf einem Heideweg Richtung
Wald spazierten. "Erst, wenn du fertig bist, falls es noch etwas
zu Marim und Kino zu sagen gibt."

"Ich bin fertig!", versicherte Nurek.

"Ich habe mich endlich in ein Ringtraining getraut.", sagte
Linoschka unsicher. Sie sprach nicht direkt weiter.

"Was ist das?", fragte Nurek also.

"Ich trainiere seit, ich glaube, acht Jahren jetzt Orkando. Das
ist ein Kampfsport, der in Ork-Kulturen entstanden ist. Er ist
sehr vielfältig, und eigentlich ist Kampf auch nicht immer
eine treffende Bezeichnung dafür. Es geht dabei auch viel
um Kommunikation, um das Wahrnehmen einer anderen Person
und ihrer Gefühle mehr durch den Körper als durch Worte.", erklärte
Linoschka. Sie runzelte die Stirn und wirkte noch unsicherer. "Eigentlich
finde ich das zu einfach heruntergebrochen. Ich mag jedenfalls
die Philosophie dahinter. Aber ich habe es bis jetzt nur mit
KIs trainiert, also, mit von Virtualitäten generierten Personen, die
auf mich und meine Fähigkeiten zugeschnitten waren."

Nurek nickte. "Du bist sehr unsicher.", sagte sie
zurückhaltend. "Das ist in Ordnung. Aber kannst du den Grund
dafür verbalisieren?"

"Ja.", sagte Linoschka sachlich. "Ich habe ein Universum
im Kopf und nicht einen Faden. Von diesem Universum weiß ich
nicht, welche Vorkenntnisse du hast."

Das verstand Nurek gut. "Danke.", sagte sie und
lächelte.

Eine Windböe wirbelte feines Laub durch
die Luft, und fühlte sich nass an, obwohl es noch gar
nicht regnete. Es war schön. Nurek liebte diesen Ort für das
Wetter, das von anderen meist ungemütlich genannt wurde, wenn
sie draußen waren, und gemütlich, wenn sie es durch ein
Fenster sahen. Nurek fand es in beiden Fällen gemütlich.

"Ich möchte dir eine bestimmte Sache erzählen, und würde
am liebsten einfach einen Datenbankabgleich mit den Grundlagen
mit deinem Gehirn machen, aber das geht eben nicht.", sagte Linoschka.

Nurek grinste einmal sehr. "Das wäre cool.", sagte sie. Sie
dachte automatisch darüber nach, ob es Lösungen geben
könnte, die es zumindest ein bisschen vereinfachen würden. "Du
könntest mir irgendwelche Stichworte geben, über die ich
mich informieren soll.", schlug sie vor.

Linoschka schüttelte den Kopf. "Dann müsste ich länger
warten. Und eigentlich bin ich fast am Punkt.", sagte
sie.

Linoschkas graues Kleid aus weichem, festem Stoff hob
sich im Wind. Das tat es selten, weil der Stoff so
schwer war, nicht der Flattertyp von Stoff. Darunter
trug sie eine Strumpfhose, die Mø gestrickt hatte,
aus dicker Wolle. Sie trug außerdem Sandalen. Linoschka
mochte es, wenn der Wind durch die Maschen in die
Kleidung eindrang, aber die Kleidung selbst trotzdem
wärmte.

Nurek mochte Linoschkas Stil, weil sie sich dabei nur um
sich selber kümmerte. Linoschka wählte, was sich für sie
gut anfühlte, und ihr war völlig gleich, was andere
dazu sagten. Nurek ließ ihr Zeit, bis sie einen Ansatz
hatte, wie sie fortfahren wollte, und sprach solange nicht.

"Ein Ringtraining bezeichnet Training mit zufällig zusammenortierten
Paaren. Der Begriff leitet sich davon ab, dass früher Personen
dazu zwei Kreise gebildet haben, einen inneren und einen äußeren. Dann
wurde eine Zahl gewürfelt und der innere Ring hat sich entsprechend
viele Personen weiterbewegt.", erklärte Linoschka. "Heute passiert
das per Losverfahren. Eine Gruppe wirft sich als Ganzes
in einen Lostopf, und Paare werden ausgelost. Dabei passiert es, dass
die gleichen Paare auch noch einmal zusammengewürfelt werden können, und
das ist auch bewusst so, damit ein Kennenlernen möglich ist."

Nurek nickte. Sie konnte nicht vermeiden, dass sich ihr
Kopf nun mit Losverfahren und deren Optimierung auseinandersetzte. So
ein Fall, dass ein Paar sich immer wieder bildete, war bestimmt
auch nicht so erwünscht. "Wie war es?", fragte sie, statt
diese Gedanken zu teilen.

Linoschka reagierte nicht sofort, und sagte dann: "Darüber habe
ich mir noch gar keine Gedanken gemacht."

"Musst du auch nicht. Dann geht es dir um was anderes?", fragte
Nurek.

Linoschka grinste. "Ja. Aber es ist trotzdem eine spannende
Frage.", sagte sie. "Ich glaube, ich war eben zu sehr mit dem
Gefühl Nervosität ausgefüllt, als dass da noch viel Raum für
ein gut oder mittel gewesen wäre. Es war nicht schlecht, sonst
wäre ich gegangen. Und es ging dabei darum, an einem Tag mit
etwas mehr Energie dort hinzugehen, weil es eben überhaupt ging
und nicht von vornherein zu viel war. Und weil es nicht
schlimm war, kann ich das nun wieder machen, bis ich mich
gewöhne, und es dadurch überhaupt erst gut werden kann, weil
dann Raum für solche Gefühle da ist."

"Das klingt gut.", sagte Nurek. "Also, es klingt dann so, wie
der bestmögliche Ausgang für mich. Weil einfach mehr nicht
drin war."

Linoschka nickte. "So lässt sich das ausdrücken. Aber das
würde andere Leute als dich zum Beispiel in die irre führen, weil
so ein Superlativ wie 'bestmöglich' irgendwie nicht als
etwas ankommt, was sehr schlecht sein könnte."

"Uffz, ja, das nervt. Leute.", murrte Nurek. Als Linoschka
nichts weiter dazu sagte, außer vorsichtig zu nicken, fragte
sie: "Worum ging es dir eigentlich?"

"Ich war fünf Stunden darin. Ich habe vergessen, dass
Zeit vergeht.", erzählte Linoschka. "Ich konnte aussuchen, ob
ich mit Personen gepaart werden möchte, die ungefähr meine
Fähigkeiten haben, mehr, weniger oder andere. Manchmal macht
es ja einfach Spaß, sehr herausgefordert zu werden und
Potenzial zu sehen, manchmal ist genau das Gegenteil viel
entspannender, etwa beim Training einer anderen Person
viel neuen Input zu geben, aber für eins selbst sind es
nur Routinen. Ich wurde vier Mal dabei einer Person
zusortiert, bei der letzteres der Fall war. Ich hatte
einfach alles zugelassen. Für Bjork, so heißt die Person, gehört
Orkando sehr zum Leben. Er hat ganz viel dazu erzählt und das
war so interessant. Ich habe mich dabei gefühlt, als würde ich über die
Philosophie nicht nur lesen und sie schon durchaus auch verstehen, sondern
in Bewegung und in einer individuellen Ausführung als
Teil der Person sehen. Es ist für mich eine Unsicherheit
verloren gegangen, ob ich das richtig machen würde, -- was
ohnehin eigentlich keine so gute Frage bei Orkando ist."

Auch wenn sie von einer verlorenen Unsicherheit erzählte, empfand
Nurek sie gerade eigentlich immer noch als besonders unsicher.
Und das nicht aus demselben Grund wie vorhin. Sie blickte
Linoschka von der Seite an und nickte vorsichtshalber um zu
signalisieren, dass sie zuhörte.

Sie waren inzwischen im Wald angelangt. Der Wald hatte mehrere
Wege. Wenn Nurek allein unterwegs war, bevorzugte sie die schmalen, bei
denen der Wald immer noch wie ein Wald und nicht wie zwei getrennte
Wälder wirkte, aber mit Linoschka wanderte sie einen breiteren Weg
hinauf zur Burgruine. "Bist du nun wieder unsicher? Oder geht
es um verschiedene Formen von Unsicherheit?", fragte sie schließlich.

"Bjork hat mich gefragt, ob ich Training bei ihm haben möchte.", antwortete
Linoschka schlicht.

Für Nurek war das keine ausreichende Antwort. Es konnte immer noch sehr
viele Gründe geben, warum Linoschka unsicher wäre. Sie könnte ablehnen
wollen, aber nicht wissen wie. Sie könnte noch keine Meinung dazu
haben, sondern noch viele Fragen im Vorfeld klären müssen. Die
vielleicht ihr selbst noch nicht einmal ganz klar waren. "Minus
minus verbose?", fragte Nurek, eine Erweiterung für
Kommandozeilenbefehle verwendend, bei dem die Ausgabe ausführlicher
wäre, als ohne den Zusatz.

"Warum will er das?", fragte Linoschka. "Müsste ich nicht langweilig
sein? Und falls nicht, müsste ich nicht einfach genau so interessant
sein, wie jede andere Person auf meinem Trainingsstand auch, und von
uns gibt es halt tausende?"

Nurek lächelte. Sie hatte vielleicht bei Marim ein bisschen das
gleiche Gefühl: Er hatte so um die fünfhundert Versuchspersonen
schon gehabt, aber nicht die Kapazität, mit jeder einzelnen einen
Privatkontakt anzufangen. "Zufall.", sagte Nurek also. "Du hast
zufällig etwas an dir, was er interessant findet."

"Ja. Und mich macht sehr nervös, nicht zu wissen, was. Denn klarer als du jetzt konnte
er sich dazu auch nicht ausdrücken.", antwortete Linoschka. Dieses
Mal frustriert. Und das war eine Emotion, die Linoschka selten
hatte.

Nurek lächelte nicht mehr und nickte ernst. Sie überlegte, ob
sie noch irgendwelche Ideen hatte, an das Wissen zu kommen. Aber
außer zu fragen gab es eigentlich keine. "Mist.", sagte sie
also sachlich.

"Ich habe zugesagt.", sagte Linoschka. "Abgesehen von diesem
Unwissen hat es nämlich sehr gut getan. Psychisch vor allem."

Nurek lächelte wieder. "Das klingt schön.", sagte sie.

Sie kam sich gerade nicht so wie ein gutes Herzwesen vor. Sie
gab kaum hilfreichen Input, bestätigte nur, indem sie
Selbstverständlichkeiten sagte. Aber vielleicht reichte das
auch. Sie musste nicht immer dazu da sein, Gedanken anderer aktiv
zu sortieren.

Dann fiel ihr etwas ein. "Bjork heißt ein Ork, der am Spiel
teilnimmt. Den Namen gibt es im Outernet nicht so viel und
ich habe keine Ahnung, wie oft er so als Nickname gewählt wird."

*Das Spiel* war ein sehr kreativer, intersubkultureller, sehr
bekannter Wettkampf in vier Phasen. Zumindest sollte er intersubkulturell
sein, aber tatsächlich war vor allem ein sportlicherer Teil der
Hack-Community vertreten. In der ersten Phase fanden sich acht
Spielgruppen, die daran teilnahmen, in der zweiten reduzierten sie
sich auf vier, in der dritten auf zwei, sodass in der vierten dann
eine gewann. Die erste Phase hatte was von einer Schnipseljagd, oder
komplexerem Geo-Cashing, die zweite spielte in selbstgebastelten
Virtualitäten, die dritte irgendwo in der Landschaft im Outernet
fast komplett ohne Internet, und die vierte war eine Art Hybrid. Es
war durchaus interessant, aber Nurek hatte irgendwo in sich eine
Grundabneigung gegen alles mit zu großer Bekanntheit und zu großer
Fanbase. Trotzdem bekam sie auf Social Media am Rande immer mal
wieder etwas mit.

"Es ist der Bjork.", sagte Linoschka. "Aber er nimmt nicht mehr
teil. Er erwähnte, dass seine Spielgruppe letzten Monat in der dritten
Phase rausgeflogen ist, und er nun wieder mehr Personen trainiert, weil
er mehr Zeit hat."

"So up-to-date bin ich wohl nicht.", sagte Nurek. "Ich habe keine
Ahnung, warum ich mich ausgerechnet an den Namen erinnere."

"Er ist eine sehr angenehme Person.", sagte Linoschka schlicht. "Ich
habe tatsächlich von ihm vorher nichts mitbekommen, aber vielleicht
mochtest du seinen Stil."

---

Nurek sah Marim einen vollen weiteren Tag nicht, und vielleicht
war das auch ganz gut. Sie spielte noch einmal Terrorium mit
Linoschka und Ivaness. Sie aßen gemeinsam, dieses Mal drinnen, weil
das Wetter wie erwartet Regen brachte, der gegen die große Fensterscheibe
schüttete. Das erdete. Sie saßen noch lange zusammen, jeweils an
ihren Faltrechnern. Hin und wieder kam ein kleines Gespräch zustande, aber
ansonsten war es ruhig.

---

Sie trafen sich am folgenden Nachmittag. Als Linoschka und Nurek die
Virtualität betraten, bestand sie aus einer großen, schwarzen Leinwand
und vier Sesseln in einem leichten Bogen angeordnet in einigem Abstand
dahinter. Vorn an der Leinwand stand ein Ork, der die Größe derselben
einstellte. Es handelte sich wahrscheinlich um Marims Herzwesen.

"Moin!", grüßte die Person. "Ich bin Anuka, Pronomen 'er, sein, ihm,
ihn'. Mögt ihr mir einmal sagen, ob die Leinwand richtige Größe, Distanz
und Höhe hat?"

Nurek setzte sich auf einen der äußeren Plätze. Sie entschied sich
dazu im Bruchteil eines Moments. Anuka hatte Marim sie einladen
lassen und dabei an ein Date gedacht, da fand er es sicher leicht
frustrierend, wenn Marim und sie nicht gemeinsam in der Mitte sitzen
würden, und Nurek wollte ihm diesen Frust gönnen. Sie beobachtete
seine Mimik, die in der Tat eine Art spielerisches Grinsen widerspiegelte, das
hätte heißen können 'So war das nicht gedacht.', aber Nurek konnte
eigentlich Gesichtsausdrücke fremder Personen nicht lesen. Es
konnte auch heißen 'Ich habe eine Frage gestellt, gedenkst du irgendwann
zu antworten?'. Und das tat sie dann auch. Jedenfalls.

"Bisschen kleiner und etwas tiefer wäre gut für mich.", sagte
sie. "Ich bin Nurek, sie, ihr, ihr sie."

"Torf, sie, ihr, ihr, sie, aber eigentlich auch nicht so
wichtig.", stellte Linoschka sich vor.

Nurek blickte sie überrascht an, aber sah dann rasch weg. Sie hatte
einfach noch nie erlebt, dass sich Linoschka in einer Virtualität
vorstellte.

Linoschka hatte ihren Blick aber wohl bemerkt. "Ein Nickname, den
ich mir für Kontexte zugelegt habe, die ich nicht zwangsläufig mit
meinem Outernet-Leben vermischen möchte.", erklärte sie. "Nicht
sehr einfallsreich. Ich war 14, als ich ihn mir ausgedacht habe. Aber
er hat für mich auch persönliche Bedeutung."

Nurek nickte. Sie nickte noch einmal, als Anuka die Leinwand so
skaliert hatte, dass sie gut gucken können würde.

"Wenn du schon beim Skalieren bist, magst du die Sitzflächen
größer machen? Ich schätze, mehrere von uns lieben es, die
Beine zu verknoten.", bat Linoschka.

Anuka lachte auf. "Damit hätte ich vielleicht rechnen müssen!", rief
er. "Klar!"

Es war eine gute Idee gewesen. Nurek stand kurz auf dazu, und als sie
sich wieder setzte, konnte sie ihre Knie auf eine Seite sortieren und
die Füße auf die andere, und alles insgesamt passte zwischen die
Lehnen. Ausgestreckte Beine stressten einfach.

Linoschka sah sie mit einem kurzen Grinsen an und setzte sich
dann ruhig neben sie. Anuka stand unschlüssig vor den
übrigen zwei Sesseln und kratzte sich am Hinterkopf. Er
hatte, wie viele Orks, nur in der Mitte des Schädels eine breite
Linie Haare von der Stirn bis zum Hinterkopf. Er trug sie nur
ein bis drei Zentimeter lang, und so schwarz, dass Nurek sich
kurz fragte, ob sie gefärbt wären, bis ihr einfiel, dass die
Frage in einer Virtualität ohnehin nicht so viel Sinn ergab. Er
trug ein ebenso schwarzes kurzärmliges Hemd und einen schwarzen
Spitzenrock mit einem auffällig verzierten Gürtel: Es waren
Spikes darauf und edelsteinartige Gnubbel in den Trans-Pride-Farben
rosa, hellblau und weiß.

Anuka stand so lange nachdenklich da, bis Marim endlich auftauchte.

"Es tut mir leid, es hatte sich plötzlich eine weitere Person
für die Studie gemeldet und ich dachte, das passt vorher noch, wenigstens
einen Termin auszumachen.", sagte Marim. "Das hat gar nicht mal so
gut geklappt." Er blickte sich um und entdeckte Linoschka. "Marim
Präsenz. Moment. Marim reicht. Er, sein, ihm, ihn."

Linoschka wiederholte Nickname und Pronomina.

Marim blickte kurz Anuka an, dann, wie sie saßen, grinste Anuka
breit an und setzte sich schließlich neben Linoschka, sodass
Anuka außen saß.

Eine kurze, leicht seltsam-unbehagliche Stille trat ein, bis sie sich
einigten, den Film zu starten. Und was für ein Film. Nurek fragte
sich zunächst, ob die Szenen unchronologisch angeordnet waren -- mit
so etwas hatte sie oft Schwierigkeiten --, oder ob sie einfach quasi
nicht zusammenhingen. Außerdem arbeitete er mit vielen
Körperverformungs-Effekten, die vielen Personen ein seltsames Gefühl gaben, und
für manche sogar Teil einer Phobie waren. Szenen, in denen plötzlich, ohne
Vorwarnung die Stelle, an der der Arm angewachsen war, auf Beckenhöhe
runterrutschte. Zehen, die in die Länge wuchsen, mit denen der Charakter
sich schließlich einen Stift hinters Ohr steckte, ohne überhaupt den
Fuß anzuheben. Die Szenen waren nur kurz, dann wirkten die Körper
wieder realistisch und der Film spielte mit anderen Surrealitäten. Was
zum Slik war das für ein Film?

Nurek mochte ihn trotzdem. Vielleicht auch nur, weil sie mit einer
positiven Grundhaltung herangehen wollte. Aber sie mochte eben auch
generell Surreales. Das Spiel gegen alle Stereotypen im Kopf. Sie
verstörte der Film nicht wegen der Darstellungen, sondern weil er
keine Handlung hatte.

Ihr gefiel der Film bis zur Kussszene. Sie war wieder eine mit
Körperveränderung: Der Mund des Charakters wurde zu einem langen
Rüssel, und er küsste sein eigenes Spiegelbild, das den Rüssel einfach
in sich einsog. Er wirkte dabei labberig flüssig. Es sollte wohl
auch irgendwie zärtlich sein. Dann war der Film vorbei.

"Och nö.", meinte Nurek. "Kussszene zum Schluss. Wie Stereotyp. Dass
selbst so ein Film nicht ohne auskommt."

"Ach so.", sagte Anuka, wirkte vielleicht gespielt überrascht. "Ich
dachte schon, du wärest traurig, dass der Film bereits vorbei ist."

"Ich hätte auch noch eine halbe Stunde mehr davon angesehen, wenn sie
damit diese Szene ausgetauscht hätten.", sagte Nurek.

"Oh, an sich mochtest du ihn?", fragte Anuka, dieses Mal sehr sicher
wirklich überrascht.

"Können wir uns in einen Kreis setzen?", fragte Nurek, bevor sie
antwortete. Sie unterhielten sich schließlich von den äußeren
Plätzen aus miteinander.

"Ja. Vorsicht!", warnte Anuka, als er mit einer Geste steuerte, dass
sich die Sessel in einen Kreis schoben, ohne dass sie dafür aufstehen mussten. Da
sie sich gegenseitig nicht berührten, war es möglich, dass
diese Veränderung für jede Person von ihnen so wirkte, als würden
sich nur die anderen bewegen und in Wirklichkeit bewegte sich niemand.

"Also, ich fand vieles gut. Ich mochte das Spiel mit meiner
Erwartungshaltung sehr.", sagte Nurek. Und betonte dann noch
einmal: "Sehr!", bevor sie fortfuhr. "Aber mit der Handlungslosigkeit
bin ich nicht so gut zurechtgekommen."

"Das ist eine Meta-Ebene!", sagte Anuka. "Ich fand das richtig
krass cool! Immer wieder Handlung andeuten, und dann kommt doch
keine, sodass es frustriert. Bis du verstehst, dass das auch
Teil des Spiels mit deiner Erwartungshaltung ist."

"Oh!", rief Nurek. Sie grübelte noch einmal darüber nach, und
wiederholte auch dies: "Ooh! Ich mag das!"

"Nur den Kuss nicht, ja?", fragte Anuka.

Nurek grinste und nickte. "Küssen kann weg.", sagte sie. "Küssen
und all das Körperflüssigkeits-Austauschen-Gedöns. Aber Leute
sind da so scharf drauf, dass sie wenigstens Küssen überall einbringen
müssen. Setze ich mich mit der Haltung hier irgendwo in die Nesseln?"

"Warum solltest du?", fragte Marim.

"Mir wurde manchmal gesagt, ich verderbe Leuten die Laune an
Filmen mit Erotik, romantischen Küssen und Sex, und sollte
sie dann einfach nicht gucken.", erklärte sie.

"Ich finde, deine Haltung sollte mindestens genau so viel
Raum haben wie die der anderen.", hielt Marim fest. "Und
mich stört sie jedenfalls nicht."

"Nervt dich das auch?", fragte Nurek.

"Erotik und all dies?", fragte Marim. Vielleicht sicherheitshalber.

Nurek nickte.

"Nein. Ich gucke tatsächlich gern Erotik-Filme.", widersprach
er betont. "Aber mir geht dabei echt nichts verloren, wenn du so etwas mies
findest und darüber reden willst. Ganz im Gegenteil. Ich finde es -- solange
es dir dabei gut geht -- bereichernd. Und selbst, wenn ich sie mag, kann
ich auch gern beim Ranten mitmachen.  Ich kann gleichzeitig
Kussszenen mögen, aber ihre immer noch zu oft unhinterfragte
Beliebtheit mies finden."

"Oh ja, das kann er schon!", bestätigte Anuka. "Ich auch. Es gibt
so viele Filme, deren Handlung eine Sex-Szene beinhaltet, ohne
die die Handlung genau so viel Sinn ergibt."

"Wie in diesem Film mit der Kuss-Szene.", murmelte Nurek.

"Und dann schneidet hinterher eine KI den Film so zurecht, dass sie
nicht vorkommt, für das Publikum, das solche Szenen aus verschiedenen
Gründen nicht sehen mag.", fuhr
Anuka fort, ohne auf den Einwand einzugehen. "Das Publikum, das
eher die gekürzte Fassungen anguckt, ist echt nicht klein. Ich
frage mich oft, ob es nicht mehr validieren würde, wenn es
von vornherein zwei gleichwertige Versionen gäbe, mit und
ohne entsprechende Inhalte. Sodass nicht das eine der Standard und das andere die
angepasste Fassung für die anderen ist. Und auf solche Gedanken
hat mich vor allem Marim gebracht."

Marim lächelte.

"Darf ich was sagen?", fragte Linoschka.

"Klar.", sagte Anuka, und auch Marim und Nurek nickten.

Linoschka brauchte ein wenig, um sich zu sammeln und sah
dabei auf den Boden. Nurek kannte das. Wenn Linoschka zuhörte, fiel
es ihr schwer, gleichzeitig eigene Worte in ihrem Kopf zu bilden, um
ihre Gedankenuniversen in Sprache zu übersetzen. Also musste
es dazu ein paar Momente leise sein. Alle ließen ihr die Zeit. "Ich
verstehe, dass du den Kuss nicht gut fandest.", sagte sie schließlich.
"Aber ich finde, er verändert doch die Handlung sehr. Die nicht so
richtig vorhandene. Die Szene am Ende ist Symbol dafür, dass der
Charakter sich selbst akzeptiert, mit dem Unwissen über das
eigene Ich, was als nächstes mit jenem passiert."

"Das ist auch eine coole Interpretation.", stimmte Nurek
zu. "Aber Küsse als das Symbol für Liebe ist halt
auch problematisch."

Linoschka nickte und blickte dabei auf den Boden. "Stimmt.", sagte
sie schließlich. Nachdenklich fügte sie hinzu: "Irgendwie
ist es in deinen Nesseln auch ganz gemütlich." Und zu solch
mysteriösen Statements kam es bei Linoschka, wenn ihre Gedanken zu viele Sprünge in
ihrem Kopf machen durften, bevor sie sie aussprach.

"Magst du etwas genauer beschreiben, wie du in den Sitznesseln
gelandet bist?", fragte Nurek.

"Oh, das ist vielleicht kompliziert.", sagte Linoschka
unsicher. "Du hattest vorhin gefragt, ob du dich in die Nesseln
setzen würdest, wie sonst. Nun hatte ich das Gefühl, ich hätte
mich damit in die Nesseln gesetzt, weil ich diese Kussnormativität
für meinen Einwand noch nicht ganz dekonstruiert hatte. Und
da dachte ich, deine Nesseln sind gemütlicher. Ergibt das nun Sinn?"

Nurek lachte und nickte. "In meinen Nesseln ist genug Platz für
alle, die möchten.", sagte sie.

"Oh, das ist ein schönes Bild.", murmelte Marim und fügte
dann wieder in durchschnittlicher Lautstärke
hinzu: "Mindestens zwei, ich schätze drei von
uns haben ja wahrscheinlich häufiger Probleme, Sprache so
zu nutzen, dass sie generell verstanden wird. Aber uns
gefällt es mit dieser Sprache und wir kommen gut damit
zurecht. Das könnte im Bild bedeuten, dass wir Personen
sind, die gar nicht merken, dass Nesseln normalerweise
pieken würden, und es uns deshalb darin gemütlich machen
können."

"Ich finde den Gang der Diskussion hier ja sehr
interessant.", merkte Anuka an. "Von Küssen, die
eklig sind und weg können, dahin, sich in Nesseln
zu setzen, die eigentlich auch recht gemütlich sind."

---

Sie diskutierten noch eine ganze Weile und gingen später
mehr ins Detail. Irgendwann verabschiedete sich Linoschka, weil
sie am späten Abend noch eine Verabredung mit sich selber
hatte. Anuka ging bald darauf. Er nannte keinen Grund und
für Nurek fühlte es sich so an, als wäre da ein nicht
ausgesprochener, der etwas mit Marim und ihr zu tun hätte. Aber
sie konnte sich nicht ausmalen, was für einer.

"Nicht sicher, ob er uns gern zu zweit haben möchte, um
uns zu verkuppeln, oder ob er sich unwohl fühlt, weil
wir zu viel ins Detail gehen.", murmelte Marim. Ihm
ging es wohl ähnlich.

Das Wort 'verkuppeln' hallte in Nureks Kopf mit einem
unwohligen Gefühl nach. Nicht, dass sie Marim nicht
genug gemocht hätte bis jetzt, um auszuprobieren, ob
sie vielleicht eine romantische Beziehungsebene dazu
entwickeln könnten. Der Gedanke verunsicherte sie
noch mehr. Sie hatte ihn bisher noch nicht gehabt, obwohl
es dafür Anlässe gegeben hätte. Er überraschte sie. Sie
hätte irgendwie nicht damit gerechnet. Vielleicht eben, weil
Marim so viele Versuchspersonen hatte, oder weil
ihr Interesse an wissenschaftlichem und nerdigem Austausch
einfach viel größer gewesen war -- und immer noch war.

Sondern weil Nurek es verabscheute, wenn andere Leute
versuchten, sich in ihre Entscheidungen einzumischen. Sie
fragte sich, ob es dabei egal war, was für welche, oder
ob sie Entscheidungen, die in irgendeiner Art mit
Freundschaften oder Beziehungen zusammenhingen, besonders
schlimm empfand.

"Darf ich dich fragen, was du denkst?", fragte Marim.

"Ja.", erlaubte Nurek, grinste und sagte nichts weiter.

Marim wartete einige Momente, lächelte dann auch und
fragte: "Was denkst du?"

"Gerade sehr frisch, dass ich mag, dass ich diese Alberei
mit dir machen kann.", sagte sie. "Davor habe ich über
Verkupplungsversuche nachgedacht, und dass ich sie nicht leiden
kann. Und davor," Nurek zögerte, bevor sie den Mut dazu und eine
Formulierung fasste, es zu Ende auszusprechen, "dass ich
nicht abgeneigt wäre, herauszufinden, ob eine romantische
Beziehung zwischen uns taugt."

Marim nickte. Er hatte ein sehr kleines Lächeln im Gesicht, noch
von vorher, das nicht verschwand, und dann langsam breiter
wurde. Er nahm außerdem die eigenen Knie in den Arm. Diese
Strümpfe waren einfach wunderschön, fand Nurek. "Ich
glaube, es wäre fair gewesen, dir gesagt zu haben, dass ich
vermögt in dich bin. Das wollte ich heute.", sagte er.

"Vermögt?", fragte Nurek.

"Das leitet sich von mögen ab, wie verliebtsein
von lieben.", erklärte Marim. "Das Wort habe ich erfunden."

"Oh!" Nurek merkte, dass ihre Hände vor Begeisterung
flatterten, als hätte sie das gar nicht verursacht, sondern
ihr Körper machte es von alleine. "Ein schönes Wort, aber
mir ist das immer noch nicht ganz klar.", sagte sie. "Ist
vermögen dann eine Vorstufe von mögen?"

"Uffz, nein. Ich mag dich.", widersprach Marim. "Dann
eher eine Nachstufe. Es geht bei der Verformung des
Wortes eher darum, dass so eine Art Hibbeligkeit hinzukommt, eine
innere Freude und Begeisterung jedes Mal, wenn ich an
dich denke, die mich einfach lächeln lässt."

"Dann bin ich auch vermögt in dich.", überlegte Nurek. Sie
runzelte die Stirn und grübelte noch etwas. "Allerdings bin
ich dann in sehr viele Leute vermögt."

"Da spricht ja nichts gegen! Das kann total schön
sein.", sagte Marim.

Sie saßen wieder ein paar Momente still da. Nurek hatte
das Bedürfnis, näher an Marim zu sitzen, aber nicht neben
ihm. Sie saßen immer noch in diesem Sesselkreis und die beiden
unbesetzten Sessel jeweils links und rechts von ihnen waren
so groß, dass sich der Abstand fast nach Rufenmüssen
anfühlte. Aber irgendwie wirkte Nureks Lieblingsvirtualität
auch gerade nicht nach der richtigen Gemütlichkeit. Sie
war nicht weich genug. Sie musste grinsen, als sie sich das
moosig steinige Bauwerk als Hüpfburg oder Kissen vorstellte.

"Du denkst schon wieder heimlich.", sagte Marim. "Was
völlig in Ordnung ist, natürlich. Aber es macht in dem
Kontext schon etwas neugierig."

"Du hast doch so unsagbar viele schöne Virtualitäten." Nurek
versuchte ein verschmitztes Grinsen, hatte aber bei solchen
Anforderungen keine Ahnung, wie gut das klappte. "Ist
auch sowas wie ein Deckennest dabei?"

Marim holte eine Art Klappaquarium aus seiner Rocktasche. Als
er es ausklappte und im Raum zwischen ihnen etwa auf Augenhöhe
befestigte -- es brauchte dazu keine Verbindung zum Boden oder
so etwas --, schwammen tatsächlich Fische und Quallen darin. Aber mit einer Geste
verschwand das Wasser und wurde durch eine riesige Welt ersetzt, die nur
aus verschiedenen bunten Kissen bestand. Die Fische
schwammen irritiert davon und die Quallen bewegten sich nach
oben aus dem Ausschnitt, den das Aquarium darstellte. Niemand
wusste, wohin. "Meinst du so etwas?"

Nurek runzelte die Stirn. Ja, so etwas meinte sie, aber sie
hatte den spontanen Drang, etwas zu perfektionieren. "Ein
bisschen dunkler, und etwas weniger bunt könnte sie sein.", sagte
sie. "Ich mag zwar bunt, aber es ist mir oft zu viel
durcheinander. Und ich bin generell ein wenig lichtempfindlich. Ich
mag eher Dunkelheit und verbinde sie mehr mit Ruhe."

"Sind dir meine Strümpfe zu viel?", fragte Marim plötzlich
besorgt.

"Nein, die sind klasse. Die bleiben lokal an deinen
Beinen und laufen nicht irgendwo unkontrolliert herum oder
gestalten den ganzen Hintergrund.", beruhigte Nurek. Nach
kurzem Reflektieren, dass aus dem ersten Satz in diesem
Zusammenhang vielleicht nur hervorgehen würde, dass die
Strümpfe nicht störten, aber nicht, dass sie sie wirklich
gut fand, versuchte sie ernsthafter hinzuzufügen: "Ich
mag die Strümpfe wirklich sehr!" Aber sie schaffte es nicht, ihren
albernen Unterton abzulegen. Der war da jetzt einfach. Sie
kicherte darüber, dass ihre alberne Stimmung sich ihrer
Kontrolle entzogen hatte. "Mir geht es gut!", stellte sie
fest.

"Das ist so schön!", sagte Marim, die letzten zwei
Wörter durch lange Vokale und ein Lächeln
hervorhebend. Er öffnete das Menü für
das Aquarium wieder für sie beide und überließ es Nurek, die
Dunkelheit passend einzustellen und die Farben zu sortieren.

Anschließend wechselten sie die Virtualität. Das fühlte
sich sehr erleichternd an. Sie konnten sich unabhängig von
Sitzmöbeln den richtigen Abstand zueinander aussuchen. Es
ergab sich irgendwie, dass sie sich einander zugewandt mit
etwa einer drei Viertel Armlänge Distanz in die Kissen legten, statt sich wie so
oft im Schneidersitz hinzusetzen.

Aber vor allem erleichterte es, weil
dies kein geschlossener Raum mehr war. Er erstreckte sich einfach ins
Unendliche, ein bewölkter Abendhimmel darüber und ein
frischer, leichter Wind wehte durch die Virtualität. "Besser.", sagte
Nurek.

"Ich würde gern mit ein paar Fragen ein bisschen was abstecken.", sagte
Marim, als sie zur Ruhe gekommen waren. "Das wird weniger albern. Ist
das gerade okay für dich?"

Nurek grinste automatisch albern. "Ja.", sagte sie und kämpfte
dagegen an. "Ich habe die Stimmung gerade nicht so ganz unter
Kontrolle, aber ich kann trotzdem sachlich reden."

"Das reicht mir.", versicherte Marim. "Ich bin etwas
unkoordiniert. Ich würde anfangen mit der Frage: Würdest du
dich als sex-repulset bezeichnen? Oder magst du lieber keine
Label? Oder was anderes?"

Nurek musste schnauben, weil ihr die Antwort so offensichtlich
vorkam, aber trotzdem okay fand, dass Marim nachfragte. "Das
Label trage ich. Ich bin asexuell.", fügte sie hinzu.

"Muss ja nicht einhergehen.", sagte Marim und lächelte. "Ich
bin eine asexuelle Schlampe. Ich bin asexuell, aber nicht
sex-repulset."

"Das ist eine Kombination, in die ich noch nicht viel
Einblick habe, muss ich zugeben.", sagte Nurek. "Ich
informiere mich. Gibt es akut etwas, was ich wissen sollte?"

"Eigentlich nicht, aber ich kann dir kurz beschreiben, was es
bedeutet, wenn du magst.", schlug Marim vor.

Nurek nickte. Sie fühlte dabei die Weiche des Kissens an ihrem unteren
Ohr. "Gern."

"Ich fühle keine sexuelle Anziehung zu Personen. Ich habe nie, wenn
ich eine Person angucke, das Gefühl oder den Gedanken, mit dieser
Person wäre Sexuelles etwas, wonach es mich irgendwie drängt, wonach
ich mich sehne oder sowas.", fasste Marim zusammen. "Ich mag
aber sexuelle Interaktion an sich. Es hat für mich aber nichts
mit Personen oder persönlicher Beziehung zu tun. Das macht es
manchmal schwer, Personen dafür zu finden."

"Mich nicht.", sagte Nurek, fast als Reflex. "Also, ich bin
keine Person dafür."

"Ich weiß.", sagte Marim. "Und das ist umgekehrt irgendwie
entspannend. Mit dir möchte ich ja herausfinden, ob wir eine
persönliche Beziehung entwickeln oder ausbauen möchten. Weil
ich viel sexuell interagiere, waren manche Personen in der
Vergangenheit verwirrt, wenn ich mit ihnen eine romantische
oder eine Kuschelbeziehung anfangen mochte, dass
für mich Sex kein Teil davon wäre. Ich wurde einmal sehr
unter Druck gesetzt. Es ist entspannend, dass das zwischen
uns nie aufkommen wird." Marim wirkte plötzlich sehr nachdenklich
und etwas verwirrt. "Es sei denn, dir macht es etwas aus, dass
ich mit anderen Personen wahrscheinlich hin und wieder Sex
haben werde. Eben nur für Sex."

Nurek schüttelte den Kopf. "Solange du nicht das Bedürfnis
hast, mir ausführlich davon zu erzählen, mach, was du willst.", sagte
sie. "Ich meine, ich werde auch über deine Studie viele private
Details nicht erfahren. Das klingt spontan persönlicher."

Marim nickte. "Das ist definitiv persönlicher. Alles im
Zusammenhang der Studie.", sagte er. "Zur
Frage mit dem darüber reden zurückkommend: Ich kann gut Themen
aussparen, aber du sagst immer so Sachen wie 'ausführlich' dazu, oder
fängst selbst mit dem Thema an. Wo sind deine Grenzen?"

Nurek lächelte, weil sie keine spontane Antwort hatte, und
das letzte Mal er es gewesen war, der auf eine Grenzen-Frage
einige Zeit gebraucht hatte. "Darf ich vorher eine sehr persönliche
Frage stellen?"

Marim nickte einfach. Dabei bewegte sich sein weiches Haar auf seiner
Schulter. Es sah aus, als müsste es sich schön anfühlen.

"Ich sollte keine Detailfragen über das Bedrängen stellen, das
dir widerfahren ist. Aber du hattest schon einmal angedeutet, als
es darum ging, dass ich dir zu begeistert war, dass so etwas in
der Richtung vorgefallen ist.", leitete Nurek ein. "Ging es
dabei um die gleiche Situation?"

"Ja.", sagte Marim. "Und du darfst fragen. Es ist eine beschissene
Situation gewesen, und sie hat Nachwirkungen bis heute, aber ich
bin trotzdem jederzeit dazu bereit, darüber zu reden. Zumindest
mit Leuten, die mich dafür nicht abwerten."

Nurek nickte. "Ich frage vielleicht irgendwann. Und du darfst
jederzeit davon erzählen. In dem Zusammenhang sogar, wenn es
um sexuelle Dinge geht.", versicherte sie. "Gerade wollte
ich nur einordnen können, ob es zusammenhängt, und habe
kein Bedürfnis, von mir aus nach mehr zu fragen."

Marim nickte. Er wirkte ernst, fand sie. Und Nurek war froh, dass
sie nicht gerade jetzt eine neue Welle Albernheit überrollte.

"Ich habe übrigens ein Geheimnis, das ich nicht so oft teile.", sagte
sie. Vielleicht um abzulenken, damit die Albernheit auch nicht kam, weil
sie gerade an sie dachte. "Ich mag Kussszenen in Filmen in Wirklichkeit
eigentlich teilweise gern angucken. Sie sind oft emotional intensiv. Ich
finde sie als Symbol für Beziehungen unangenehm. Und so in Echt mit
Personen finde ich das eklig. Aber je nach Mindset, mit dem ich
einen Film angucke, mag ich die Szenen an sich schon. Ist das
seltsam?"

Marim grinste und schüttelte den Kopf. "Romantik, die ich empathisch aus
Beschreibungen in Büchern und Filmen und so nachempfinde, funktioniert
ganz anders, als reale. Das ist so seltsam. Also", Marim unterbrach sich
und runzelte die Stirn. "Ich hätte nicken sollen. Es ist seltsam. Aber
ich glaube, das haben viele. Also, es ist als Phänomen seltsam
und nicht du bist mit dem Empfinden seltsam. Ich zumindest habe
diese Diskrepanz stark. Schon wieder so ein Fremdwort. Diese
Distanz, das nicht zusammenpassen von Romantik in Geschichten als
Mitempfinden bei fiktiven Charakteren und Romantik zwischen
mir und einer realen Person."

"Ich war irgendwie ziemlich überrascht.", sagte Nurek. "Ich
habe ja vor meinen ersten Kussversuchen Küsse in Büchern oder
Filmen gelesen beziehungsweise gesehen. Und
geschlossen, dass mir das gefallen müsste, auch
wenn ich mir wirklich nicht vorstellen konnte, dabei Flüssigkeit
auszutauschen. Also habe ich ziemlich trocken geküsst. Mein
Kommentar beim ersten Kuss war so ein skeptisches 'interessant'." Sie
musste bei der Erinnerung wieder grinsen, obwohl sie gar nicht
so angenehm war.

"Wie ist es angekommen?", fragte Marim.

"Oh, keine Sorge.", sagte Nurek. "Wir hatten ja vorher schon
wegen der Nässe gesprochen und die Person hatte sich darauf
eingestellt, dass das vielleicht nichts wird. Eher als ich. Wir
waren noch eine Weile befreundet, haben uns aber dann aus den
Augen verloren."

"Das klingt nicht sehr toll, aber auch nicht so schlimm.", sagte
Marim.

Nurek nickte und lächelte. Es fühlte sich schön an, dass Marim
andere Erfahrungen hatte als sie, aber das nicht dazwischen kam, als
er sich in sie und ihre Situation hineinversetzte.

"Die Frage mit der Grenze steht noch aus.", erinnerte Marim.

"Ich möchte aber mit noch einer Frage ablenken.", sagte
Nurek. Die Albernheit kehrte zurück.

Marim grinste und lud sie ein: "Bring es an!" Er sah schön
aus dabei, fand sie.

"Genauso, wie ich nicht genau weiß, was ein Date ist, weiß ich
nicht genau, wo eine romantische Beziehung anfängt.", sagte
Nurek. "Beziehungsweise, ich finde das gerade hier romantisch."

Marim nickte. "Einverstanden.", sagte er.

Dieses Mal grinste Nurek wieder. "Zurück zu den ernsten Betriebsabsprachen
und so:", sagte sie. "Wir sollen also verkuppelt werden. Eventuell.
Und ich frage mich, was rebellischer wäre: Wenn wir nun schon sagen, wir
sind in einer romantischen Beziehung, oder erst später. Gar nicht wäre
wahrscheinlich am rebellischsten. Aber das ist ja nicht unbedingt, was
wir wollen."

Marim nickte. "Wichtiges Kriterium dafür, eine Beziehung einzugehen: Wie
rebellisch ist es?", hielt er fest.

"Ist das schlimm?", fragte Nurek.

Marim schüttelte den Kopf. "Ich bin dafür zu haben. Ich mag
das auch nicht so von Anuka. Wenn es das ist. Ich werde da
noch Klarheit mit ihm schaffen."

"Und?", fragte Nurek. "Was wäre rebellischer?"

Marim blickte sie eine Weile nachdenklich an, ohne zu
antworten. Das Bild wirkte gleichzeitig so vertraut und
noch so neu. Und Nurek fühlte sich wärmer, nicht auf
einer Temparaturskala sondern emotional, als sie seinen
Körper und sein Gesicht betrachtete. Er war weich und
ruhig, entspannt vielleicht.

"Beides ist nicht so richtig befriedigend.", sagte
er schließlich. "Sowas wie 'Wir hatten doch schon längst
eine romantische Beziehung' wäre eine Option. Dabei
wissen wir das erst jetzt, weil wir unser Verhältnis
vorher noch nicht interpretiert haben. Aber vielleicht
ist Tee und Minze im Übermaß bereits romantisch."

"Auf eine andere Weise ist es das.", sagte Nurek.

"Auf eine andere Weise, ja.", bestätigte Marim.

"Ich würde mich damit nicht ehrlich fühlen.", überlegte
Nurek.

"In Ordnung.", versicherte Marim. "Aber wenn wir sagen, wir
haben jetzt eine, dann fühlst du dich damit wohl?"

Nurek merkte, wie irgendetwas sehr viel Aufmerksamkeit forderndes
durch ihren Körper floss, Hormone oder so etwas. Sie nickte. Sie
schloss die Augen und atmete, um dieses Gedöns in ihrem
Körper ein wenig zu beruhigen. Zumindest soweit, dass sie
wieder sprechen konnte. "Du hattest auch Kuschelbeziehung
mit angesprochen. Ist das für dich die gleiche Ebene oder
was anderes?"

"Etwas anderes, aber die Ebenen haben für mich eine ähnliche
Relevanz.", sagte Marim. "Ich kann romantische Beziehungen
ohne Kuscheln führen, und Kuschelbeziehungen ohne Romantik, und
mir fehlt dabei jeweils nichts. Aber ich mag auch beides
zusammenführen, und ich habe die Ebenen ähnlich gern."

Seine Augen bewegten sich anders als sonst, stellte Nurek fest. Sie
bewegten sich rasch zu verschiedenen Punkten. Nurek wusste
nicht genau welche. Sie fühlte sich etwas nervös, aber auch
gut dabei. Als wäre sie wichtig.

"Was möchtest du?", fragte Marim.

"Ich frage, weil ich gerade Lust hätte, kuscheln
auszuprobieren.", sagte sie.

Marim lächelte und nickte. "Ich mag auch."

Sie lagen eine Weile still da, und steckten sich dann beide
gegenseitig mit grinsen an. Dann traute sich Nurek ein kleines
Stückchen näher zu rücken. Marim tat das selbe.

Nurek hatte lange nicht mehr mit einer noch nicht vertrauten
Person gekuschelt. Marim war mindestens ebenso unsicher. Es
gab dieses klassische Problem, dass die Gesichter sich
nicht zu dicht sein durften. In der Virtualität wäre wenigstens
Atem kein Problem gewesen, aber sie deaktivierten die Atemhaptik
nicht. Also musste zwangsläufig ein Kopf unter dem anderen
liegen, oder eine Person mit dem Rücken zur anderen oder
sowas.

Als sie abwechselnd, tonlos kichernd, dicht genug aneinandergerückt
waren, traute sich Nurek, Marims Wange anzufassen. So zarte Haut. Sie
strich ihm von dort über den Hals und Marim schloss dabei die
Augen.

"Kuscheln mit aufgeregten Romantik-Gefühlen, oder eher entspannt,
gelassen?", fragte er.

Nurek fand, dass er schon recht aufgeregt klang. "Kannst du
es unterdrücken? Theoretisch?" Und, weil sie das Missverständnispotenzial
erkannte, fügte sie kurz darauf hinzu: "Das ist noch keine
Entscheidung meinerseits."

"Ich kann es ganz ausstellen.", sagte Marim. "Ich funktioniere
da als Spiegel und auf Basis von Konsens. Wenn dich das Verliebtheits-Gefühl
beim Kuscheln stört, dann fühlt es sich nicht mehr existenzberechtigt
und löst sich innerhalb von Millistunden komplett auf."

"Also doch schon Verliebtheit?", filterte Nurek daraus und grinste
sehr breit.

"Irgendwie wohl schon.", gab Marim zu.

Nurek umarmte seinen Kopf, sodass er unter ihrem Kinn auf ihrer
Brust landete -- vorsichtig, sodass sie merkte, wie er
der Bewegung freiwillig folgte. "Du darfst das gern fühlen. Ich
hatte das vorhin auch schon." Sie spürte wie sich sein Körper
entspannte. Oder anders anspannte. Er legte den oberen Arm
um ihren Bauch und begann sanft, ihren Rücken zu streicheln. Nurek
strich durch sein langes weiches Haar und genoss die
feine Haptik viel mehr, als sie das Minzblatt je hätte wertschätzen
können.
